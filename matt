#!/usr/bin/env perl
use strict;
use warnings;
use Cwd qw(abs_path);
use File::Basename;

my $abspath = abs_path($0);
my ($name,$path,$suffix) = fileparse($abspath);
my $path_matt=$path;

if(@ARGV>0 && ($ARGV[0] eq "--version" || $ARGV[0] eq "-version")){
	my $version=`head -n 1 $path_matt/VERSION_AND_CHANGELOG 2>&1`;
	if($?!=0){$version="unknown"
	}else{    $version=~s/\s//g;}
	print "$version\n";
	exit(0);
}

# read subcommands
my @script_calls=(<$path/abilities/*>,<$path/experimental_abilities/*>);

# remove files which are for sure no Matt commands
my @tmp=@script_calls;
@script_calls=();
foreach my $scr_f (@tmp){
	if($scr_f =~ /~$/){next}
	if($scr_f =~ /bak$/){next}
	if($scr_f =~ /tmp$/){next}
	if($scr_f =~ /temp$/){next}
	if($scr_f =~ /swp$/){next}
	if($scr_f =~ /your_requests_to_matt\.txt$/){next}

	push(@script_calls,$scr_f);
}

# mapping of subcommand to perl script
my %subcmd2scriptcall=();
my $script_call;
foreach $script_call (@script_calls){
	($name,$path,$suffix)=fileparse($script_call);
	$name =~ s/\..*$//;  # remove all file extensions
	$subcmd2scriptcall{$name}=$script_call;
}

my $cmd;
if(!defined($ARGV[0]) || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/){
	my $version=`head -n 1 $path_matt/VERSION_AND_CHANGELOG 2>&1`;
	if($?!=0){$version="unknown"
	}else{    $version=~s/\s//g;}
	print "\nMatt v. $version\n\n";

	$cmd=`Rscript --version 2>&1`;
	if($?!=0){die "Rscript is not available. Make sure it is in your PATH or install it.\n\n"}
	$cmd=`perl --version 2>&1`;
	if($?!=0){die "Perl is not available. Make sure it is in your PATH or install it.\n\n"}

	print"Usage: matt <command> ...\n\n";
	print "Commands:\n";
	
	my %overview=("retrieval"=>[], "manipulation"=>[], "seq_extraction"=>[], "seq_analysis"=>[], "import_table"=>[], "math"=>[], "extras"=>[], "super"=>[]);
	my %category_descr=("retrieval"=>"Retrieve data from table", "manipulation"=>"Manipulate table", "seq_extraction"=>"Retrieve sequences", "seq_analysis"=>"Analyze sequences","import_table"=>"Import data / check table", "math"=>"Maths and statistics", "extras"=>"Other", "super"=>"High-level analyses");
	# get short descriptions from all scripts
	foreach $script_call (@script_calls){
		$cmd="$script_call code:001";
		my $line = `$cmd`;
		my @fs=split("\t",$line);
		push(@{$overview{$fs[0]}},join("",@fs[1..3]));
	}
	
	my $prefix_offset="  ";
	my $col_width=55;
	my @output_lines=();
	# first column
	foreach my $category (("import_table","retrieval","manipulation","seq_extraction","seq_analysis")){
	    push(@output_lines,"");
		push(@output_lines,"*".$category_descr{$category});
		foreach my $script_short_descr (@{$overview{$category}}){push(@output_lines,$prefix_offset.$script_short_descr);} 
	}
	# second column
	my $line_counter=-1;
	foreach my $category (("math","extras","super")){
		$line_counter+=2;
		# fill up first column such that is has desired width
		$output_lines[$line_counter] .= (" " x ($col_width - length($output_lines[$line_counter])));
		$output_lines[$line_counter] .="*".$category_descr{$category};
		foreach my $script_short_descr (@{$overview{$category}}){
			$line_counter++;
			$output_lines[$line_counter] .= (" " x ($col_width - length($output_lines[$line_counter])));
			$output_lines[$line_counter] .= $prefix_offset.$script_short_descr;
			}
	}
	
	print join("\n",@output_lines);
	print "\n\n";
	print "Attention: Tables processed by Matt must contain a header with column names and must not contain \" characters\n";
	print "           with exception of regular expressions. All other \" characters will be ignored and removed. When using\n";
	print "           MS Excel for table generation, please save tables in format Windows Text. Matt recognizes Windows newlines,\n";
	print "           but not DOS nor old-style MacOS newlines (CR or \\r only). Use command chk_nls to see and check newlines in tables.\n";
	print "\n\n";
	exit(1);
}

if(!defined($subcmd2scriptcall{$ARGV[0]})){die "\nmatt error: command $ARGV[0] is not available.\n\n";}

open(my $log,">>"."your_requests_to_matt.txt");
# construct command for log
my $call="matt ".join(" ",@ARGV);
print $log "$call\n";
close($log);


# get requested subcommand
my $subcmd=shift(@ARGV);
$script_call=$subcmd2scriptcall{$subcmd};
# add arguments 
foreach my $arg (@ARGV){
	$script_call.=" \"".$arg."\"";
}

# execute requested command
#print $script_call."\n";
system("$script_call");
my $exit_status=$?;

# Perl only says: "The only universally recognized values for EXPR are 0 for success 
# and 1 for error; other values are subject to interpretation depending on the 
# environment in which the Perl program is running."
if($exit_status == 0){exit(0);}else{exit(1);} 
