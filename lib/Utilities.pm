package Utilities;
use strict;
use warnings;
use Exporter;
use Scalar::Util qw( looks_like_number openhandle );
use POSIX qw(floor ceil);

our @ISA= qw( Exporter );

# these CAN be exported.
our @EXPORT_OK = qw( open_fct_file  is_element_in_aref get_min_from_aref get_max_from_aref 
					 get_min_max_from_aref
					 uniq_from_aref min max prnt_line  get_pwm_scores_for_seq    
					 read_pwm get_starts_of_exact_matches 
					 is_numeric rvcmplt  write_pwm get_pwm_scores_for_seqs 
					 get_mean_from_aref get_median_from_aref get_gcc_from_str ceil floor
					 shuffle_elements_in_aref get_type_from_ensembl_tr_biotype 
					 get_idxs_of_desc_ordering_aref get_idxs_of_asc_ordering_aref 
					 get_idx_of_max_element_aref get_idx_of_min_element_aref 
					 get_longest_str_from_aref substrs are_all_valid_R_colors 
					 get_sum_from_aref get_pwm_hits_for_seqs subtract_pwms
					 get_regexp_hits_for_seqs logarithmize_pwm 
					 exponentiate_pwm learn_hmm0_as_pwm_from_seqs 
					 get_sd_without_minus_1_from_aref clean_line split_line 
					 clean_and_split_line clean_line_leave_quotes rand_string
					 first_index first_index_exact);

# these are exported by default.
our @EXPORT = @EXPORT_OK;

# works like MoreUtils first_index
# returns index of first occurrence of pattern $regexp in array $aref or -1
sub first_index{
        my ($regexp,$aref)=@_;

        for(my $i=0;$i<@$aref;$i++){
                if($aref->[$i]=~/$regexp/){return($i)}
        }

return(-1);
}

sub first_index_exact{
        my ($exp,$aref)=@_;

        for(my $i=0;$i<@$aref;$i++){
                if($aref->[$i] eq $exp){return($i)}
        }

return(-1);
}


# reads in a PWM model from a fct file
#
# Comments are allowed in a PWM
# In file $fn, the PWM can be defined in different ways to be more flexible.
# 1.) alphabet x positions with column and row names like so => MATT annotation
#SYMBOL	POS_1	POS_2	POS_3	...	
#A  ......
#C  ......
#G  ......
#T  ......
#
# 2.) positions x alphabet where positions are not annotated but the alphabet lie so
#A	C	G	T
#0.2	0.2	0.4	0.2
#... 	...	...	...
# or alphabet x positions
#A	0.2	0.3	...	...	...	...
#C	0.2	0.1	...	...	...	...
#G	0.1	0.1	...	...	...	...
#T	0.5	0.5	...	...	...	... 
#
# 3.) positions x alphabet without annotated alphabet and without annotated positions => in this case you need to specify the alphabet letters
#0.2	0.3	...	...	...	...
#0.2	0.1	...	...	...	...
#0.1	0.1	...	...	...	...
#0.5	0.5	...	...	...	...
#
# In this case, you need to call read_pwm with specifying the letters of the alphabet in the same order as their values occur in the file, 
# e.g., read_pwm <FILE> <SCRIPTNAME> A,C,G,T 
#
# The PWM can be defined by probabilities, log-probabilities or absolut counts. This gets detected automatically.
# 
# If you give an additional parameter which is numeric, this is used as a form of "pseudo count" which is interpreted as the minimal probaility
# you want for each letter at each position. If, e.g., 0.3 0.2 0.5 0.0 is given and the minimal probability is 0.01 then 0.01 probability mass is
# taken from the values 0.3 0.2 0.5 proportionally and put to the last probability. In this example, 0.01 * 0.3 is taken from 0.3,
# 0.01 * 0.2 is taken from 0.2, and 0.01 * 0.5 is taken from 0.5 and 0.01 is added to 0.0.
# Probabilities are not changed if all are larger then the specified minimal probability.
# If the PWM is given by absolute counts or log-probabilities, these are first translated into probabilities before the pseudo count is applied.
#
# You can call this function as read_pwm <FILE> <SCRIPTNAME> or read_pwm <FILE> <SCRIPTNAME> A,C,G,U or read_pwm <FILE> <SCRIPTNAME> A,C,G,U 0.01 or
# read_pwm <FILE> <SCRIPTNAME> 0.01 A,C,G,U  or read_pwm <FILE> <SCRIPTNAME> 0.01, whatever suites best.
#
# A pwm of length L is represented by an array of length L.
# At each position it contains references to a hash which maps the symbols at this position to its LOG-probability.
sub read_pwm {
	my ($fn,$calling_script_name)=@_;
	my $minprob="";
	my $letters="";

	if(@_>2){if(looks_like_number($_[2])){$minprob=$_[2];}else{$letters=$_[2];}}
	if(@_>3){if(looks_like_number($_[3])){
			if($minprob ne ""){die "ERROR in read_pwm called from script $calling_script_name: two numbers are given as minimal probability but only one should be given in file $fn\n";}
			$minprob=$_[3];
		}else{
			if($letters ne ""){die "ERROR in read_pwm called from script $calling_script_name: two alphabets are specified but only one should be given in file $fn\n";}
			$letters=$_[3];
		}
	}

	if($minprob eq ""){$minprob=0;}
	my @alph_given=();
	if($letters ne ""){@alph_given=split(",",$letters);}	

	my @header;
	open(my $fh,$fn) or die "while trying to open PWM file $fn: $!";
	while(<$fh>){
		$_=clean_line($_);
		if(length($_)==0 || substr($_,0,1) eq "#"){next;}
		@header=split_line($_);
		last;
	}
	
	my @lines=();
	while (<$fh>){$_=clean_line($_);if(length($_)==0 || substr($_,0,1) eq "#"){next;}push(@lines,$_);}close($fh);
	
	# detect of pwm is given by alphabet x positions or positions x alphabet
	my @alph=();
	my $positions_in_columns=0;
	my @fs;
	my $all_are_letters;
	my @lines_tmp;
	my @alph_tmp;
	if($header[0]=~ /SYM/i || $header[0]=~ /ALPH/i || $header[0]=~ /LET/i || $header[0]=~ /BASE/i || $header[0]=~ /NUC/i){
		$positions_in_columns=1;
		@alph=();
		@lines_tmp=();
		foreach my $line (@lines){
			@fs=split("\t",$line);
			push(@alph,$fs[0]);
			
			# reorder columns such that we have Pos_1 Pos_2 Pos_3
			my @fs_tmp=();
			for(my $j=1;$j<@fs;$j++){
				if($header[$j]=~/POS_(\d+)/){
					$fs_tmp[$1-1]=$fs[$j];
				}elsif(looks_like_number($fs[$j])){
					$fs_tmp[$1-1]=$fs[$j];
				}else{				
					die "Don't understand structure of file $fn\n";
				}
			}
			for(my $j=0;$j<@fs_tmp;$j++){if(!defined($fs_tmp[$j])){die "ERROR in read_pwm called from script $calling_script_name: header in PWM file does not look like SYMBOL{TAB}POS_1{TAB}POS_2... in file $fn\n";}}
			push(@lines_tmp,join("\t",@fs_tmp));
		}
		@lines=@lines_tmp;
	}else{
		$all_are_letters=1; # in first row
		foreach my $letter (@header){if(looks_like_number($letter)){$all_are_letters=0;}}
		if($all_are_letters){
			if($header[0]=~ /POS/i){
				@alph=@header[1..(scalar(@header)-1)];
				# remove first column
				for(my $i=0;$i<@lines;$i++){
					@fs=split("\t",$lines[$i]);
					$lines[$i]=join("\t",@fs[1..(@fs-1)]);
				}
				
			}else{
				@alph=@header;
			}
		}else{
			@lines_tmp=();
			$all_are_letters=1; # in first column
			@alph_tmp=();
			push(@alph_tmp,$header[0]);
			if(looks_like_number($header[0])){$all_are_letters=0;}
			push(@lines_tmp,join("\t",@header[1..$#header]));
			foreach my $line (@lines){
				@fs=split("\t",$line);
				push(@alph_tmp,$fs[0]);
				if(looks_like_number($fs[0])){$all_are_letters=0;}
				push(@lines_tmp,join("\t",@fs[1..$#fs]));
			}
			if($all_are_letters){
				$positions_in_columns=1;
				@alph=@alph_tmp;
				@lines=@lines_tmp;
			}else{
				$positions_in_columns=1;
				if(@alph<1){die "Error in read_pwm called from script $calling_script_name: no alphabet given and could not extract alphabet from file $fn\n";}
				@lines_tmp=(join("\t",@header));
				foreach my $line (@lines){push(@lines_tmp,$line);}
				@lines=@lines_tmp;
				
				# length of given alph does not fit number of rows
				if(@alph!=@lines){die "Error in read_pwm called from script $calling_script_name: there are ".scalar(@lines)." rows but the alphabet only contains ".(@alph)." letters in file $fn\n";}
				
			}
		}
	}
	
	if(@alph_given>0){
		if(@alph>0 && scalar(@alph)!=scalar(@alph_given)){die "Given alphabet @alph_given and inferred alphabet @alph don't match in length\n";}
		@alph=@alph_given;
	}

	my @rowsums_1=();for(my $i=0;$i<@lines;$i++){$rowsums_1[$i]=0;}
	my @colsums_1=();for(my $i=0;$i<@alph;$i++){$colsums_1[$i]=0;}
	my @rowsums_2=();for(my $i=0;$i<@lines;$i++){$rowsums_2[$i]=0;}
	my @colsums_2=();for(my $i=0;$i<@alph;$i++){$colsums_2[$i]=0;}
	my $row=-1;
	foreach my $line (@lines){
		@fs=split("\t",$line);
		$row++;
		for(my $col=0; $col<@fs; $col++){
			$rowsums_1[$row]+=$fs[$col];
			$colsums_1[$col]+=$fs[$col];
			$rowsums_2[$row]+=exp($fs[$col]);
			$colsums_2[$col]+=exp($fs[$col]);
		}
	}

	my $n_pos=0;
	my $mode="prob"; # how pwm is given: by probs, log-probs or absolut values
	if($positions_in_columns){
		if(abs($colsums_1[0]-1)<0.01){$mode="prob";}
		if($colsums_1[0]<=0){$mode="logprob";}
		if($colsums_1[0]>1.01){$mode="absval";}
		$n_pos=@colsums_1;
	}else{
		if(abs($rowsums_1[0]-1)<0.001){$mode="prob";}
		if($rowsums_1[0]<=0){$mode="logprob";}
		if($rowsums_1[0]>1.001){$mode="absval";}
		$n_pos=@rowsums_1;
	}

	my @pwm=();for(my $i=0;$i<$n_pos;$i++){push(@pwm,{});}

	my @norm_consts;
	for(my $pos=0;$pos<$n_pos;$pos++){
		@fs=();
		unless($positions_in_columns){
			@fs=split("\t",$lines[$pos]);
			@norm_consts=@rowsums_1;
			if($mode eq "logprob"){
				@norm_consts=@rowsums_2;
			}
		}else{
			foreach my $line (@lines){push(@fs, (split("\t",$line))[$pos] );}
			@norm_consts=@colsums_1;
			if($mode eq "logprob"){
				@norm_consts=@colsums_2;
			}
		}

		my $prob_redist=0;
		my $prob_sum=0;
		for(my $letter=0; $letter<@fs;$letter++){
			# turn into probs
			if($mode eq "logprob"){$fs[$letter]=exp($fs[$letter]);}  # log naturalis
			# re-normalize in any case (also for mode=prob because sometimes people round heavily the probs of pwms)
			$fs[$letter]=$fs[$letter]/$norm_consts[$pos];
			# check for min prob
			if($fs[$letter]<$minprob){$prob_redist+=$minprob-$fs[$letter];
			}else{$prob_sum+=$fs[$letter];}
		}
		
		if($prob_redist>0){
			for(my $letter=0; $letter<@fs;$letter++){
				if($fs[$letter]<$minprob){$fs[$letter]=$minprob;
				}else{
					$fs[$letter]-=$prob_redist * ($fs[$letter]/$prob_sum);
					if($fs[$letter]<$minprob){die "Error in read_pwm called from script $calling_script_name: minprob is set to $minprob and too large because re-distributing probability mass is not possible such that all probabilities are larger minprob in file $fn\n";}
				}
			}
		}
		$prob_sum=0;
		for(my $letter=0; $letter<@fs;$letter++){$prob_sum+=$fs[$letter];$pwm[$pos]->{$alph[$letter]}=log($fs[$letter]);}
		if(abs($prob_sum-1)>0.000001){die "Error in read_pwm called from script $calling_script_name: after re-normalization of probabilities with minprob=$minprob the sum of probabilites for positions ".($pos+1)." deviate from 1 more then 0.000001 in file $fn\n";}
	}

return(\@pwm,\@alph);
}


# get a random string composed by letters (uc and lc) and numbers
sub rand_string{
        my $L=$_[0];

        my @tmp=qw/ A B C D E F G H I J K L M N O P Q R S T U V W X Y Z 0 1 2 3 4 5 6 7 8 9 /;
        my @as=(@tmp);
        foreach my $t (@tmp){push(@as,lc($t))}

        my $ret="";
        for(my $i=0;$i<$L;$i++){
                $ret.=$as[rand(@as)];
        }

return($ret);
}

# all input lines from tables should be treated with this sub to
# 1.) delete any newline characters (LF, CR)
# 2.) delete quotes
sub clean_line{
        $_[0]=~s/[\n\r]//g;
        $_[0]=~s/"//g;
return($_[0]);
}

sub clean_line_leave_quotes{
        $_[0]=~s/[\n\r]//g;
return($_[0]);
}

sub split_line{
return(split("\t",$_[0],-1));
}

# 1.) removes newline characters by deleteing them
# 2.) removed quotes
# 3.) split at tabulators
sub clean_and_split_line{
return(split_line(clean_line($_[0])));
}


# 1. pwm as arrayref
# 2. file name (can be omitted, if given, output goes into file not onto screen)
# !!: internally PWMs always should have log-probs; but they get written-out with probs for easy interpretation
sub write_pwm {
	my $pwm_aryref=$_[0];
	my $fn=$_[1];   # if omitted print to STDOUT, if given, can be file name (file will be created newly), or filehandle to write into 
	my @alph=keys(%{$pwm_aryref->[0]});
	@alph=sort(@alph);
	my $n_pos=@{$pwm_aryref};
	my $fn_was_filename=0;

	# exponentiate PWM
	$pwm_aryref=exponentiate_pwm($pwm_aryref);

	my $fh;
	if(defined($fn)){
		$fh = openhandle($fn);
		if(!defined($fh)){  # $fn was not a file handle
			open($fh,">$fn") or die "Cannot open file $fn for writing PWM into.\n";
			$fn_was_filename=1;
		}
	}else{	$fh=*STDOUT;}
	
	print $fh "SYMBOL";
	for(my $pos=0;$pos<$n_pos;$pos++){print $fh "\tPOS_".($pos+1);}print $fh "\n";
	foreach my $letter (@alph){
		print $fh "$letter";
		for(my $pos=0;$pos<$n_pos;$pos++){print $fh "\t".$pwm_aryref->[$pos]->{$letter};}print $fh "\n";
	}
	
	if($fn_was_filename){close($fh);}
}



sub get_sum_from_aref{
	my $aref=$_[0];
	my $mode=$_[1];   # if ignoreSTRINGS -> all strings which are not numeric will be ignored
	my $abs=$_[2];    # if abs-> sum will be calculated for absolute values in aref
	
	if(!defined($mode)){$mode="";}
	if(!defined($abs)){$abs="";}
	
	my $sum=0;
	my $n=0;
	foreach my $v (@{$aref}){if($mode eq "ignoreSTRINGS" && !is_numeric($v)){next;}
		if($abs eq "abs"){$sum+=abs($v)}else{$sum+=$v}
		$n++;
	}
	if($n==0){return("NA");}

return($sum);
}

sub get_regexp_hits_for_seqs{
	my $seqs_aref = $_[0];
	my $regexps_aref= $_[1];  # each regexp is tested indiviually
	                          # and the merged hits are returned
	                          # if two regexps match same pos, this pos is
	                          # returned only once
	my $mode      = $_[2]; # single -> search on sequences as given
	                       # double1 -> search on sequences as given and on rev-cmplmt
	                       #            positions of hits are wrt. to each strand
	                       #                             *2
	                       #                ================
	                       #                  *1
	                       #                hit *1 has pos 3, hit *2 has pos 3 as well
	                       #                and only pos 3 is returned once
	                       #
	                       # double2 -> search on both strands, but positions of hits
	                       #            are wrt. the forward strand only
	                       #            in the example above: hit *1 keeps pos 3,
	                       #            but hit *2 has pos has pos 14 and (3,14) is
	                       #            returned	                          

	# construct one perl regexp which subsums all individial regexps
	my $regexps="(?=(".join("|",@$regexps_aref)."))";

	my @ret=();
	foreach my $seq (@$seqs_aref){
		push(@ret, get_regexp_hits_for_seq($seq,$regexps,$mode) );
	}

return(\@ret,$regexps);
}

sub get_regexp_hits_for_seq{
	my $seq=$_[0],
	my $regexp=$_[1];
	my $mode      = $_[2]; # single -> search on sequences as given
	                       # double1 -> search on sequences as given and on rev-cmplmt
	                       #            positions of hits are wrt. to each strand
	                       #                             *2
	                       #                ================
	                       #                  *1
	                       #                hit *1 has pos 3, hit *2 has pos 3 as well
	                       #                and only pos 3 is returned once
	                       #
	                       # double2 -> search on both strands, but positions of hits
	                       #            are wrt. the forward strand only
	                       #            in the example above: hit *1 keeps pos 3,
	                       #            but hit *2 has pos has pos 14 and (3,14) is
	                       #            returned

	my %startposs;
	# positions of hits should start with 1 not with 0
	while ($seq =~ /$regexp/gi){$startposs{pos($seq)+1}=1;}

	if($mode =~ /double/){
		$seq=rvcmplt($seq);
		while ($seq =~ /$regexp/gi){
			my $startpos=pos($seq)+1;  #positions of hits should start with 1 not with 0
			my $hit_len=length($1);

			if($mode eq "double2"){
				$startpos=length($seq)-$startpos-$hit_len+2;
			}

			$startposs{$startpos}=1;
		}
	}

	my @ret=sort {$a <=> $b} keys(%startposs);

return(\@ret);
}


# input
# 1.) array of sequences
# 2.) aref to array which contains arefs to pwms (fg) (all pwms will be logarithmized before use)
# 3.) threshold on log-pwm-score 
#     or ratio pwm_fg / pwm_bg to predict hits, e.g., 2 and prob_fg/prob_bg >= 2 
# 4.) aref to pwm (bg)  , can be omitted, if given, you need to give a ratio 
sub get_pwm_hits_for_seqs{
	my $seqs_aref = $_[0];
	my $fgpwms_aref= $_[1]; # if several pwms given, each is tested individually but
	                       # positions are returned only once; meaning if pwm1 and pwm2
	                       # have a hit at pos 5, pos 5 is returned only once
	my $thresh_aref= $_[2];# as many thresholds as we have fgpwms
	my $mode      = $_[3]; # single -> search on sequences as given
	                       # double1 -> search on sequences as given and on rev-cmplmt
	                       #            positions of hits are wrt. to each strand
	                       #                             *2
	                       #                ================
	                       #                  *1
	                       #                hit *1 has pos 3, hit *2 has pos 3 as well
	                       #                and only pos 3 is returned once
	                       #
	                       # double2 -> search on both strands, but positions of hits
	                       #            are wrt. the forward strand only
	                       #            in the example above: hit *1 keeps pos 3,
	                       #            but hit *2 has pos has pos 14 and (3,14) is
	                       #            returned
	my $bgpwms_aref= $_[4]; 

	if($bgpwms_aref && @$bgpwms_aref>1 && @$fgpwms_aref != @$bgpwms_aref){die "get_pwm_hits_for_seqs: the number of bg PWMs must be 1 or equal to number of fg PWMs\n";}


	# go over all fg PWMs
	my ($fgpwm_aref,$bgpwm_aref,$pwm_aref,$thresh_tmp,@ret);
	for(my $i=0;$i<@$fgpwms_aref;$i++){
		$fgpwm_aref=$fgpwms_aref->[$i];

		if($bgpwms_aref){
			if(@$bgpwms_aref>1){$bgpwm_aref=$bgpwms_aref->[$i];
			}else{$bgpwm_aref=$bgpwms_aref->[0];}

			$pwm_aref=subtract_pwms($fgpwm_aref,$bgpwm_aref);		
			$thresh_tmp=log($thresh_aref->[$i]);
			
		}else{
			$pwm_aref=$fgpwm_aref;
			$thresh_tmp=$thresh_aref->[$i];
		}
	
		for(my $j=0;$j<(@$seqs_aref);$j++){
			my $seq=$seqs_aref->[$j];
			$ret[$j]=get_pwm_hits_for_seq($seq,$pwm_aref,$thresh_tmp,$mode);
		}
	}

return(\@ret);
}

# positions start with 1: PWM hit at first position of string -> return value 1
sub get_pwm_hits_for_seq{
	my $seq = $_[0];
	my $pwm_aref= $_[1];
	my $thresh = $_[2]; # only positions with score >= thresh will be returned
	my $mode      = $_[3]; # single -> search on sequences as given
	                       # double1 -> search on sequences as given and on rev-cmplmt
	                       #            positions of hits are wrt. to each strand
	                       #                             *2
	                       #                ================
	                       #                  *1
	                       #                hit *1 has pos 3, hit *2 has pos 3 as well
	                       #                and only pos 3 is returned once
	                       #
	                       # double2 -> search on both strands, but positions of hits
	                       #            are wrt. the forward strand only
	                       #            in the example above: hit *1 keeps pos 3,
	                       #            but hit *2 has pos has pos 14 and (3,14) is
	                       #            returned
	
	my $scores_aref;
	my $scores_rc_aref;
	
	$scores_aref=get_pwm_scores_for_seq($seq,$pwm_aref);
	if($mode =~ /double/){
		$scores_rc_aref=get_pwm_scores_for_seq(rvcmplt($seq),$pwm_aref);
	}
	
	# determine at each pos max score if mode is double
	if($mode =~ /double/){
		if($mode eq "double1"){
			for(my $i=0;$i<@$scores_aref;$i++){if($scores_rc_aref->[$i] > $scores_aref->[$i]){$scores_aref->[$i]=$scores_rc_aref->[$i];}}
		}else{ #if($mode eq "double2"){
			for(my $i=0;$i<@$scores_aref;$i++){if($scores_rc_aref->[-$i-1] > $scores_aref->[$i]){$scores_aref->[$i]=$scores_rc_aref->[-$i-1];}}
		}
	}


	my @ret=();
	for(my $i=0;$i<@$scores_aref;$i++){
#print $scores_aref->[$i]."\n";
		if($scores_aref->[$i] ne "NA" && $scores_aref->[$i]>=$thresh){
#print "push ".($i+1)."\n";
			push(@ret,$i+1);
		}
	}

return(\@ret);
}


# learns homogene MM of order 0 and puts it into
# the structure of a PWM of length L (which has the same parameters at all positions)
# parameters are log probabilities
sub learn_hmm0_as_pwm_from_seqs{
	my $seqs_aref = $_[0];
	my $L=$_[1];
	my $min_prob=$_[2];    # minimal probability, could be 1/1000
	my $alph_aref=$_[3];   # can be omitted; 
						   # if given, this alphabet will be used / enforced (letters not in alph will not be considered)
						   # if not given, the alphabet will be deduced from the data
	
	my %counts=();
	for(my $i=0;$i<@$seqs_aref;$i++){
		#print $seqs_aref->[$i]."\n";
		foreach my $letr (split("",$seqs_aref->[$i])){$counts{$letr}++;}
	}
	
	# remove unwanted letters from the alphabet
	if($alph_aref){
		my %tmp;foreach my $letr (@$alph_aref){$tmp{$letr}=1;}
		# remove letters not in given alphabet
		foreach my $letr (keys %counts){
			unless($tmp{$letr}){delete($counts{$letr});}
		}
		# add letters in given alphabet but not in counts
		foreach my $letr (@$alph_aref){unless($counts{$letr}){$counts{$letr}=0;}}
	}else{
		$alph_aref=keys(%counts);
	}
	
	# make sure we reach min prob
	my $normalizing_const=get_sum_from_aref([values(%counts)]);
	my $subst=0;
	my @letrs=();
	foreach my $letr (keys %counts){
		$counts{$letr}/=$normalizing_const;
		if($counts{$letr}<$min_prob){
			$subst+=$min_prob-$counts{$letr};
			$counts{$letr}=$min_prob;
		}else{
			push(@letrs,$letr);
		}
	}
	# we need to adujst
	my $sum;
	if($subst>0){
		$sum=0;foreach my $letr (@letrs){$sum+=$counts{$letr};}
		foreach my $letr (@letrs){
			$counts{$letr} -= $counts{$letr}/$sum * $subst;
		}
	}
	
	# checl if probs sum up to 1 and logaritmize
	$sum=0;
	foreach my $letr (@letrs){$sum+=$counts{$letr};$counts{$letr}=log($counts{$letr});}
	if($sum-1 > 1e-9){die "learn_pwm_from_seqs: sum of determined probabilities deviates from 1; something might have gone wrong; try to use a smaller min probability parameter\n";}

	my @pwm=();
	for(my $l=0;$l<$L;$l++){
		$pwm[$l]=\%counts;
	}

return(\@pwm,$alph_aref);
}


# input:  pwm array reference of pwm with probs OR log-probs (both is acceptable)
# return: new pwm array reference of new pwm with log-probs (in any case, pwm gets copied) 
sub logarithmize_pwm{
	my $pwm_aref=$_[0];

	my $is_already_logarithmized=0;
	foreach my $ltr (keys %{$pwm_aref->[0]}){		
		if($pwm_aref->[0]->{$ltr}<0){$is_already_logarithmized=1;}
	}

	my $pwm_aref_new=[];
	for(my $pos=0;$pos<@{$pwm_aref};$pos++){
		$pwm_aref_new->[$pos]={};
		foreach my $ltr (keys %{$pwm_aref->[$pos]}){
			if($is_already_logarithmized){
				$pwm_aref_new->[$pos]->{$ltr}=$pwm_aref->[$pos]->{$ltr};
			}else{
				$pwm_aref_new->[$pos]->{$ltr}=log($pwm_aref->[$pos]->{$ltr});
			}
		}
	}

return($pwm_aref_new);
}

# input:   two refs to PWMs with log-probs PWM_1 and PWM_2
# returns: new PWM=PWM_1-PWM_2  (log-probs subtracted)
sub subtract_pwms{
	my ($pwm1_aref,$pwm2_aref)=@_;
	
	if(@$pwm1_aref != @$pwm2_aref){die die "subtract_pwms: both PWMs are defined for different length (".scalar(@$pwm1_aref)." and ".scalar(@$pwm2_aref).")\n";}

	my %alph_joint;
	foreach my $letr (keys(%{$pwm1_aref->[0]})){$alph_joint{$letr}=1;}
	foreach my $letr (keys(%{$pwm2_aref->[0]})){$alph_joint{$letr}++;}
	foreach my $letr (keys %alph_joint){unless($alph_joint{$letr}==2){die "subtract_pwms: bg PWMs and fg PWMs must be defined for same alphabet but they aren't\n";}}

	my $pwm_aref=[];
	for(my $l=0;$l<@$pwm1_aref;$l++){
		$pwm_aref->[$l]={};
		foreach my $letr (keys %alph_joint){$pwm_aref->[$l]->{$letr}=$pwm1_aref->[$l]->{$letr}-$pwm2_aref->[$l]->{$letr};}
	}

return($pwm_aref);	
}

# input:  pwm array reference of pwm with probs OR log-probs (both is acceptable)
# return: new pwm array reference of new pwm with probs (in any case, pwm gets copied) 
sub exponentiate_pwm{
	my $pwm_aref=$_[0];

	my $pwm_is_logarithmized=0;
	foreach my $ltr (keys %{$pwm_aref->[0]}){		
		if($pwm_aref->[0]->{$ltr}<0){$pwm_is_logarithmized=1;}
	}

	my $pwm_aref_new=[];
	for(my $pos=0;$pos<@{$pwm_aref};$pos++){
		$pwm_aref_new->[$pos]={};
		foreach my $ltr (keys %{$pwm_aref->[$pos]}){
			if($pwm_is_logarithmized){
				$pwm_aref_new->[$pos]->{$ltr}=exp($pwm_aref->[$pos]->{$ltr});
			}else{
				$pwm_aref_new->[$pos]->{$ltr}=$pwm_aref->[$pos]->{$ltr};
			}
		}
	}

return($pwm_aref_new);
}

# input:
# 1.) array of seqs
# 2.) pwm aryref (with log-probs)
# output:
# reference to array with as many entries as we have sequences
# at each position the array contains a reference to an array which contais all PWM scores of the corresponding PWM 
sub get_pwm_scores_for_seqs {
	my $seqs_aryref=$_[0];
	my $pwm_aryref=$_[1];
	
	my @ret=();
	foreach my $seq (@$seqs_aryref){
		push(@ret, get_pwm_scores_for_seq($seq,$pwm_aryref) );
	}

return(\@ret);
}

# input:  sequence and reference to pwm with log-probs
# output: the scores of the given PWM for all possible PWM positions in the given seq 
sub get_pwm_scores_for_seq {
	my ($seq,$pwm_aref)=@_;
	my $motif_len=scalar(@$pwm_aref);
	
	# one score for each posible positions from left to right of sequences
	my @scores=();

	# trivial case -> return empty scores array
	if(length($seq)<$motif_len){return(\@scores);}
	
	my @seq=split("",$seq);
	my $score;
	my $summand;

	for(my $i=0;$i<length($seq)-$motif_len+1;$i++){
		$score=0;
		for(my $j=0;$j<$motif_len;$j++){
			$summand=$pwm_aref->[$j]->{ $seq[$i+$j] };
			
			# letter is not part of alphabet
			if(!defined($summand)){$score="NA";last;}
		
			if($summand=='-inf'){$score='-inf';last;}
		
			$score+=$summand;
		}
		
		push(@scores,$score);
	}

return(\@scores);
}


sub are_all_valid_R_colors{
	my $colors_comma_separated=$_[0];
	my $path_of_calling_script=$_[1];
	my $cmd="$path_of_calling_script/../utilities/are_all_valid_R_colors.r $colors_comma_separated";
	my $ret=`$cmd`;
	if($ret eq "TRUE"){return(1);}else{return(0);}
}


sub get_starts_of_exact_matches {  # old solution
	# string and search_string
    my ($str, $re) = @_;
    grep {pos($str) = $_; $str =~ /\G(?:$re)/} 0 .. length($str) - 1;
}


sub is_numeric{
	looks_like_number($_[0]);	
}

# to search for exact hits
sub get_pwm_for_kmer {
	my ($kmer,$symbs_aref)=@_;
	
	my @kmer=split("",$kmer);
	my @pwm=();
	my $href;
	for(my $i=0;$i<@kmer;$i++){
		$href={};
		# log probs
		foreach my $symb (@$symbs_aref){$href->{$symb}='-inf';} 
		$href->{$kmer[$i]}=0;
	}
return(\@pwm,$symbs_aref);
}

# as input one string
# letters other than A, C, G, T, U do not count and will be neglected totally
sub get_gcc_from_str{
	my $str=uc($_[0]);
	if(length($str)==0){return("NA");}

	my $N_G = ($str =~ s/G/G/ig);
	my $N_C = ($str =~ s/C/C/ig);
	my $N_A = ($str =~ s/A/A/ig);
	my $N_T = ($str =~ s/T/T/ig);
	my $N_U = ($str =~ s/U/U/ig);

	if($N_G+$N_C+$N_A+$N_T+$N_U==0){return("NA")}

return(($N_G+$N_C)/($N_G+$N_C+$N_A+$N_T+$N_U));
}



# opens a fct file; returns: header, ids for requested columns, file handle at positon after header, comment lines before header
# my ($fh,$col_ns_aref,$col_ids_aref,$comments) 
sub open_fct_file {
	my ($fn,$col_names_aref,$calling_script_name)=@_;
	
	open(my $fh,"<".$fn) or die "Sorry, you requested $calling_script_name but: I cannot open file $fn";
	
	my $comment_lines="";
	my @col_ns;
		
	while(<$fh>){
		$_=clean_line($_);
		if(length($_)==0){next};
		if(substr($_,0,1) eq "#"){$comment_lines.="$_\n";next;}
		@col_ns=split_line($_);
		last;
	}
	
	# get ids for column names
	my %col_ns;
	my $i=0;
	my %col_ns_freq=();
	foreach my $col_n (@col_ns){$col_ns{$col_n}=$i++;$col_ns_freq{$col_n}++;}
	
	# check for double column names
	foreach my $col_n (keys %col_ns_freq){
		if($col_ns_freq{$col_n}>1){
			die "$calling_script_name: column names in file $fn are not unique; found column $col_n ".$col_ns_freq{$col_n}." times\n";
		}
	}
	
	# go over requested column names and determine their index
	my @col_ids;
	foreach my $col_n (@{$col_names_aref}){
		$i=$col_ns{$col_n};
		if(!defined($i)){die "Sorry, you requested $calling_script_name but: column $col_n does not exist in file $fn";}
		push(@col_ids,$i);
	}	
return($fh,\@col_ns,\@col_ids,$comment_lines);
}

sub rvcmplt{
	$_=$_[0];
	#tr/ACGTacgt\[\]/TGCAtgca\]\[/;
	tr/ABCDGHMNRSTUVWXYabcdghmnrstuvwxy\[\]/TVGHCDKNYSAABWXRtvghcdknysaabwxr\]\[/;
	$_=reverse;	
return($_);
}

sub is_element_in_aref{
	my $aref=$_[0];
	my $v_given=$_[1];
	my $ret=0;
	foreach my $v (@{$aref}){if($v eq $v_given){$ret=1;last;}}
return($ret);
}


sub get_min_from_aref{
	my $aref=$_[0];
	my $mode=$_[1];   # if ignoreSTRINGS -> all strings which are not numeric will be ignored; this includes empty strings
	if(!defined($mode)){$mode="";}
	
	my $min_v=9**9**9;
	my $n=0;
	foreach my $v (@{$aref}){if($mode eq "ignoreSTRINGS" && ($v eq "" || !is_numeric($v))){next;} if($v<$min_v){$min_v=$v;} $n++;}
	
	if($n==0){return("NA");}
	
return($min_v);
}

sub get_max_from_aref{
	my $aref=$_[0];
	my $mode=$_[1];   # if ignoreSTRINGS -> all strings which are not numeric will be ignored
	if(!defined($mode)){$mode="";}
	
	my $max_v=-9**9**9;
	my $n=0;
	foreach my $v (@{$aref}){if($mode eq "ignoreSTRINGS" && ($v eq "" || !is_numeric($v))){next;} if($v>$max_v){$max_v=$v;} $n++;}
	
	if($n==0){return("NA");}
	
return($max_v);
}

sub get_min_max_from_aref{
	my $aref=$_[0];
	my $mode=$_[1];   # if ignoreSTRINGS -> all strings which are not numeric will be ignored
	my $abs=$_[2];    # abs -> returns that value from aref which has the max / min absolut value

	if(!defined($mode)){$mode="";}
	if(!defined($abs)){$abs="";}

	my $max_v=-9**9**9; if($abs eq "abs"){$max_v=0}
	my $min_v= 9**9**9;
	my $n=0;
	foreach my $v (@{$aref}){if($mode eq "ignoreSTRINGS" && ($v eq "" || !is_numeric($v))){next;}
		if($abs eq "abs"){
			if(abs($v)>abs($max_v)){$max_v=$v;}
			if(abs($v)<abs($min_v)){$min_v=$v;}
		}else{
			if($v>$max_v){$max_v=$v;}
			if($v<$min_v){$min_v=$v;}
		}
		$n++;
	}

	if($n==0){return("NA","NA");}

return($min_v,$max_v);
}

sub get_mean_from_aref{
	my $aref=$_[0];
	my $mode=$_[1];   # if ignoreSTRINGS -> all strings which are not numeric will be ignored
	if(!defined($mode)){$mode="";}
	
	if(scalar(@{$aref})==0){return("NA");}
	
	my $sum=0;
	my $n=0;
	foreach my $v (@{$aref}){if($mode eq "ignoreSTRINGS" && ($v eq "" || !is_numeric($v))){next;} $sum+=$v;$n++;}
	
	if($n==0){return("NA");}
	
return($sum/$n);
}


sub get_sd_without_minus_1_from_aref{
	my $aref=$_[0];
	my $mode=$_[1];   # if ignoreSTRINGS -> all strings which are not numeric will be ignored
	if(!defined($mode)){$mode="";}

	my $m=get_mean_from_aref($aref,$mode);
	if($m eq "NA"){return("NA");}

	my $sum=0;
	my $n=0;
	foreach my $v (@{$aref}){if($mode eq "ignoreSTRINGS" && ($v eq "" || !is_numeric($v))){next;}
		$sum+=($v-$m)**2;$n++;
	}
	
	if($n==0){return("NA");}
	if($sum==0){return(0);}
	
return(sqrt($sum/$n));
}

sub get_median_from_aref{
	my $aref=$_[0];
	my $mode=$_[1];   # if ignoreSTRINGS -> all strings which are not numeric will be ignored
	if(!defined($mode)){$mode="";}

	if(scalar(@{$aref})==0){return("NA");}
	
	my $aref2=[];
	foreach my $v (@{$aref}){if($mode eq "ignoreSTRINGS" && ($v eq "" || !is_numeric($v))){next;} push(@{$aref2},$v);}
	
	if(scalar(@{$aref2})==0){return("NA");}

	my $aref3=sort_asc_num_aref( $aref2 );
	
	my $pos=scalar(@{$aref3})/2;
	my $pos2=int(scalar(@{$aref3})/2-0.5);
	# we have exactly one positions
	if($pos == $pos2){return($aref3->[$pos]);}
	
	# we have two positions and return the mean
return(($aref3->[$pos]+$aref3->[$pos2])/2);
}

sub sort_asc_num_aref{
	my $aref=$_[0];
	my @sorted = sort { $a <=> $b } @{$aref};	
return(\@sorted);
}

# returns array indices for a ascending ordering of the numeric values in aref
sub get_idxs_of_asc_ordering_aref{
	my $aref=$_[0];
	my @aa=@{$aref};
	my @idxs = sort { $aa[$a] <=> $aa[$b] } 0 .. $#aa;
return(\@idxs);
}

# returns array indices for a ascending ordering of the numeric values in aref
sub get_idxs_of_desc_ordering_aref{
	my $aref=$_[0];
	my @aa=@{$aref};
	my @idxs = sort { $aa[$b] <=> $aa[$a] } 0 .. $#aa;
return(\@idxs);
}


sub get_idx_of_max_element_aref{
	my $aref=$_[0];
	my $id_max;
	my $v_max=-9**9**9;
	my $v_tmp;
	for(my $i=0; $i<@{$aref};$i++){$v_tmp=$aref->[$i];if($v_tmp>$v_max){$v_max=$v_tmp;$id_max=$i;}}
return($id_max);
}

sub get_idx_of_min_element_aref{
	my $aref=$_[0];
	my $id_min;
	my $v_min=9**9**9;
	my $v_tmp;
	for(my $i=0; $i<@{$aref};$i++){$v_tmp=$aref->[$i];if($v_tmp<$v_min){$v_min=$v_tmp;$id_min=$i;}}
return($id_min);
}

sub sort_desc_num_aref{
	my $aref=$_[0];
	my @sorted = sort { $b <=> $a } @{$aref};	
return(\@sorted);
}


# apply substr with same offset and length on array of strings
sub substrs{
	my $aref=$_[0];
	my $ofs=$_[1];
	my $l=$_[2];
	my @ret=();
	foreach my $s (@$aref){push(@ret,substr($s,$ofs,$l));} 
return(\@ret);
}

sub get_longest_str_from_aref{
	my $aref=$_[0];
	my $l=-1;
	my $ret="";
	foreach my $s (@$aref){unless(defined($s)){next;};if(length($s)>$l){$l=length($s);$ret=$s;}}
return($ret);
}


sub uniq_from_aref {
	my %hash;
	my @ret=();
	my $pos_undef=-1;
	foreach my $v (@{$_[0]}){
		if(!defined($v)){
			if($pos_undef==-1){$pos_undef=scalar(@ret);}
			next;
		}
		if(!defined($hash{$v})){push(@ret,$v);}
		$hash{$v}=1;
	}
	if($pos_undef>-1){splice(@ret,$pos_undef,0,undef);}
return(\@ret); 
}

# min of two numbers
sub min {
	my ($a,$b)=($_[0],$_[1]);
	if($a<=$b){
		return($a);
	}else{
		return($b);
	}
}

# max of two numbers
sub max {
	my ($a,$b)=($_[0],$_[1]);
	if($a<=$b){
		return($b);
	}else{
		return($a);
	}
}

sub prnt_line {
	my $fh=$_[0];
	my $fields_aref=$_[1];
	my $separator=$_[2];
	my $ids_aref=$_[3];
	
	my @fs=();
	foreach my $id (@{$ids_aref}){push(@fs,$fields_aref->[$id]);}
	
	print $fh join($separator,@fs)."\n";
}

# suffle around entries in @array in place; algo= fisher_yates_shuffle
sub shuffle_elements_in_aref {
    my $array = $_[0];
    my $seed  = $_[1];
    
    if(defined($seed)){srand($seed);my $trash=rand();}
    
    my $i;
    for ($i = @$array; --$i; ) {
        my $j = int rand ($i+1);
        next if $i == $j;
        @$array[$i,$j] = @$array[$j,$i];
    }
}

# mapping of all ensembl transcript biotypes to
# 1. noncoding
# 2. coding
# 3. pseudo
# 4. rest
# 5. undefined
sub get_type_from_ensembl_tr_biotype{
	my $tr_bt=lc($_[0]);
	my $ret="undefined";

	# http://www.ensembl.org/Help/Faq?id=468:
	# Protein coding: IGC gene, IGD gene, IG gene, IGJ gene, IGLV gene, IGM gene, IGV gene, IGZ gene, nonsense mediated decay, nontranslating CDS, non stop decay, polymorphic pseudogene, TRC gene, TRD gene, TRJ gene.
	# Pseudogene: disrupted domain, IGC pseudogene, IGJ pseudogene, IG pseudogene, IGV pseudogene, processed pseudogene, transcribed processed pseudogene, transcribed unitary pseudogene, transcribed unprocessed pseudogene, translated processed pseudogene, TRJ pseudogene, unprocessed pseudogene
	# Long noncoding: 3prime overlapping ncrna, ambiguous orf, antisense, antisense RNA, lincRNA, ncrna host, processed transcript, sense intronic, sense overlapping
	# Short noncoding: miRNA, miRNA_pseudogene, miscRNA, miscRNA pseudogene, Mt rRNA, Mt tRNA, rRNA, scRNA, snlRNA, snoRNA, snRNA, tRNA, tRNA_pseudogene

	if($tr_bt eq "protein_coding"){$ret="coding";}
	elsif($tr_bt eq "processed_transcript"){$ret="noncoding";}
	elsif($tr_bt eq "retained_intron"){$ret="noncoding";}
	elsif($tr_bt eq "non_coding"){$ret="noncoding";}

	elsif($tr_bt eq "ig_gene"){$ret="coding";}
	elsif($tr_bt eq "ig_c_gene"){$ret="coding";}
	elsif($tr_bt eq "ig_d_gene"){$ret="coding";}
	elsif($tr_bt eq "ig_j_gene"){$ret="coding";}
	elsif($tr_bt eq "ig_lv_gene"){$ret="coding";}
	elsif($tr_bt eq "ig_v_gene"){$ret="coding";}
	elsif($tr_bt eq "ig_m_gene"){$ret="coding";}
	elsif($tr_bt eq "ig_z_gene"){$ret="coding";}
	elsif($tr_bt eq "tr_c_gene"){$ret="coding";}
	elsif($tr_bt eq "tr_d_gene"){$ret="coding";}
	elsif($tr_bt eq "tr_j_gene"){$ret="coding";}
	elsif($tr_bt eq "tr_lv_gene"){$ret="coding";}
	elsif($tr_bt eq "tr_v_gene"){$ret="coding";}
	elsif($tr_bt eq "tr_m_gene"){$ret="coding";}
	elsif($tr_bt eq "tr_z_gene"){$ret="coding";}
	elsif($tr_bt eq "nonsense_mediated_decay"){$ret="coding";}
	elsif($tr_bt eq "non_stop_decay"){$ret="coding";}
	elsif($tr_bt eq "nontranslating_cds"){$ret="coding";}
	elsif($tr_bt eq "coding"){$ret="coding";}

	elsif($tr_bt eq "sense_intronic"){$ret="noncoding";}
	elsif($tr_bt eq "ribozyme"){$ret="noncoding";}
	elsif($tr_bt eq "mt_rrna"){$ret="noncoding";}
	elsif($tr_bt eq "snorna"){$ret="noncoding";}
	elsif($tr_bt eq "scarna"){$ret="noncoding";}
	elsif($tr_bt eq "lincrna"){$ret="noncoding";}
	elsif($tr_bt eq "linc_rna"){$ret="noncoding";}
	elsif($tr_bt eq "snrna"){$ret="noncoding";}
	elsif($tr_bt eq "trna"){$ret="noncoding";}
	elsif($tr_bt eq "trna_pseudogene"){$ret="noncoding";}
	elsif($tr_bt eq "snlrna"){$ret="noncoding";}
	elsif($tr_bt eq "rrna"){$ret="noncoding";}
	elsif($tr_bt eq "srna"){$ret="noncoding";}
	elsif($tr_bt eq "sense_overlapping"){$ret="noncoding";}
	elsif($tr_bt eq "antisense"){$ret="noncoding";}
	elsif($tr_bt eq "mt_trna"){$ret="noncoding";}
	elsif($tr_bt eq "mirna"){$ret="noncoding";}
	elsif($tr_bt eq "mirna_pseudogene"){$ret="noncoding";}
	elsif($tr_bt eq "pre_mirna"){$ret="noncoding";}
	elsif($tr_bt eq "misc_rna"){$ret="noncoding";}
	elsif($tr_bt eq "misc_rna_pseudogene"){$ret="noncoding";}
	elsif($tr_bt eq "macro_lncrna"){$ret="noncoding";}
	elsif($tr_bt eq "bidirectional_promoter_lncrna"){$ret="noncoding";}
	elsif($tr_bt eq "scrna"){$ret="noncoding";}
	elsif($tr_bt eq "3prime_overlapping_ncrna"){$ret="noncoding";}
	elsif($tr_bt eq "vaultrna"){$ret="noncoding";}
	elsif($tr_bt eq "ambiguous_orf"){$ret="noncoding";}
	elsif($tr_bt eq "antisense_rna"){$ret="noncoding";}
	elsif($tr_bt eq "ncrna host"){$ret="noncoding";}
	elsif($tr_bt eq "ncrna_host"){$ret="noncoding";}
	elsif($tr_bt eq "noncoding"){$ret="noncoding";}
	elsif($tr_bt eq "ncrna"){$ret="noncoding";}

	elsif($tr_bt eq "polymorphic_pseudogene"){$ret="pseudo";}
	elsif($tr_bt eq "processed_pseudogene"){$ret="pseudo";}
	elsif($tr_bt eq "unprocessed_pseudogene"){$ret="pseudo";}
	elsif($tr_bt eq "pseudogene"){$ret="pseudo";}
	elsif($tr_bt eq "transcribed_unprocessed_pseudogene"){$ret="pseudo";}
	elsif($tr_bt eq "transcribed_processed_pseudogene"){$ret="pseudo";}
	elsif($tr_bt eq "transcribed_unitary_pseudogene"){$ret="pseudo";}
	elsif($tr_bt eq "unitary_pseudogene"){$ret="pseudo";}
	elsif($tr_bt eq "translated_processed_pseudogene"){$ret="pseudo";}
	elsif($tr_bt eq "disrupted domain"){$ret="pseudo";}
	elsif($tr_bt eq "ig_pseudogene"){$ret="pseudo";}
	elsif($tr_bt eq "ig_c_pseudogene"){$ret="pseudo";}
	elsif($tr_bt eq "ig_d_pseudogene"){$ret="pseudo";}
	elsif($tr_bt eq "ig_j_pseudogene"){$ret="pseudo";}
	elsif($tr_bt eq "ig_lv_pseudogene"){$ret="pseudo";}
	elsif($tr_bt eq "ig_v_pseudogene"){$ret="pseudo";}
	elsif($tr_bt eq "ig_m_pseudogene"){$ret="pseudo";}
	elsif($tr_bt eq "ig_z_pseudogene"){$ret="pseudo";}
	elsif($tr_bt eq "tr_c_pseudogene"){$ret="pseudo";}
	elsif($tr_bt eq "tr_d_pseudogene"){$ret="pseudo";}
	elsif($tr_bt eq "tr_j_pseudogene"){$ret="pseudo";}
	elsif($tr_bt eq "tr_v_pseudogene"){$ret="pseudo";}
	elsif($tr_bt eq "tr_lv_pseudogene"){$ret="pseudo";}
	elsif($tr_bt eq "tr_m_pseudogene"){$ret="pseudo";}
	elsif($tr_bt eq "tr_z_pseudogene"){$ret="pseudo";}
	elsif($tr_bt eq "pseudo"){$ret="pseudo";}

	elsif($tr_bt eq "tec"){$ret="rest";}
	elsif($tr_bt eq "rest"){$ret="rest";}

return($ret);
}

1;
