## Matt
**Unix toolkit** for feature analysis of alternative splicing events. Includes basic functions for manipulating tables, extraction of exon and intron-related features, discriminate feature analysis, motif maps for RNA-binding proteins, and more.

## [Wiki](https://gitlab.com/aghr/matt/-/wikis/home): Detailed documentation and the syntax of all Matt commands with explanations in the [Wiki](https://gitlab.com/aghr/matt/-/wikis/home)

## In this README: Installation, License, How to cite

**Author**:  Andre Gohr<br><br>
Matt was developed while I was working for the Centre for Genomic Regulation ([CRG](https://www.crg.eu/en)) with [Manuel Irimia](https://www.crg.eu/en/programmes-groups/irimia-lab) and [Juan Valcarcel](https://www.crg.eu/en/programmes-groups/valcarcel-lab). Credits go to their support and the CRG.


## Contents
1. [Dependencies](#dependencies)
2. [License](#license)
3. [Installation](#installation)
4. [How to cite](#how-to-cite)
5. [Disclaimer](#disclaimer)

## Dependencies
1. Runs on any Unix-like system
2. Perl interpreter and basic Perl packages
3. fastq-dump
4. R, Rscript
5. pdflatex (with packages: KOMA-Script, babel, fontenc, lmodern, graphicx, parskip, fancyvrb)
6. any PDF viewer
7. any Web browser
6. SVM_BPfinder 
    * you install it through the Matt installation process
    * implemented in Python 2
7. SVMlight
    * you install it through the Matt installation process
    * implemented in C
8. Yeo & Burge's maximum-entropy models for human canonical splice sites [Xmaxentscan_scoreseq.html](http://hollywood.mit.edu/burgelab/maxent/Xmaxentscan_scoreseq.html), [Xmaxentscan_scoreseq_acc.html](http://hollywood.mit.edu/burgelab/maxent/Xmaxentscan_scoreseq_acc.html)
    * included in the download of Matt
    * downloaded December 12, 2016, from [http://hollywood.mit.edu/burgelab/maxent/download/](http://hollywood.mit.edu/burgelab/maxent/download/)
9. [CISBP-RNA](http://cisbp-rna.ccbr.utoronto.ca) binding motifs of RNA binding proteins in form of position weight matrices
    * included in the download of Matt
    * build 0.6, downloaded February 15, 2017 from [http://cisbp-rna.ccbr.utoronto.ca/](http://cisbp-rna.ccbr.utoronto.ca/)


## License
Matt is published under GNU Lesser General Public License v3.0. This means, 
* Matt is open-source
* you can freely use Matt, but without any warranty
* you can freely re-distribute Matt as is, or as a kind of library of other software
* you can freely extend Matt and use your extensions for your purposes
* if you want to make your extensions public, you will need to apply the GNU Lesser General Public License v3.0 to your extensions, meaning they must stay open-source and be available freely to everyone

If you have any questions, please get in touch with the author.

The Matt root directory contains as well data and software from third parties which come with their own licensing. These data and software are in sub-folders:
1. data_external
2. external_progs
3. lib_external
and their contents are excluded from the license given to Matt.

         
License text can be found in LICENSE and COPYING.LESSER in the root directory of Matt. For more details, you might visit: [https://choosealicense.com/licenses/lgpl-3.0/](https://choosealicense.com/licenses/lgpl-3.0/)


## Installation
Latest version:
1. clone the git repository: e.g. git clone https://gitlab.com/aghr/matt.git
2. make INSTALL script executable: chmod u+rwx ./INSTALL 
3. run installation script: ./INSTALL

Check out [releases](https://gitlab.com/aghr/matt/-/releases):
1. download and extract source code of a specific release
2. make INSTALL script executable: chmod u+rwx ./INSTALL 
3. run installation script: ./INSTALL


## How to cite
### Matt in Bioinformatics

A. Gohr, M. Irimia Matt: Unix tools for alternative splicing analysis, _Bioinformatics_, 2019, [DOI: 10.1093/bioinformatics/bty606](https://doi.org/10.1093/bioinformatics/bty606) 

### Splice site strengths from Yeo & Burge's maximum-entropy models

As Matt internally uses Yeo & Burge's maximum-entropy models for computing splice site strengths, please cite 

[Yeo et al., Maximum entropy modeling of short sequence motifs with applications to RNA splicing signals, 2003, DOI: 10.1089/1066527041410418](http://online.liebertpub.com/doi/abs/10.1089/1066527041410418) 

whenever you publish results related to splice site strength. This includes the following splice site features which you might have obtained with Matt's commands [get_efeatures](https://gitlab.com/aghr/matt/-/wikis/o5_analyze_seqs/get_efeatures), [get_ifeatures](https://gitlab.com/aghr/matt/-/wikis/o5_analyze_seqs/get_ifeatures), [cmpr_exons](https://gitlab.com/aghr/matt/-/wikis/o8_high_level/cmpr_exons), [cmpr_introns](https://gitlab.com/aghr/matt/-/wikis/o8_high_level/cmpr_introns):

* MAXENTSCR_HSAMODEL_UPSTRM_5SS - maximum entropy score of 5ss of up-stream exon using a model trained with human splice sites
* MAXENTSCR_HSAMODEL_3SS - maximum entropy score of 3ss using a model trained with human splice sites
* MAXENTSCR_HSAMODEL_5SS - maximum entropy score of 5ss using a model trained with human splice sites
* MAXENTSCR_HSAMODEL_DOWNSTRM_3SS - maximum entropy score of 3ss of down-stream exon using a model trained with human splice sites
* DIFF_MAXENTSCR_HSAMODEL_3SS_VS_DOWNSTRM_3SS
* DIFF_MAXENTSCR_HSAMODEL_5SS_VS_UPSTRM_5SS

### Motif analyses use binding motifs of RNA binding proteins from CISBP-mRNA

As Matt uses binding motifs of RNA binding proteins as provided by the database [CISBP-RNA](http://cisbp-rna.ccbr.utoronto.ca/), please cite 

[Ray et al., A compendium of RNA-binding motifs for decoding gene regulation, 2013, DOI: 10.1371/journal.pcbi.1001016](http://www.nature.com/nature/journal/v499/n7457/full/nature12311.html) 

whenever you publish results obtained with Matt's command [test_cisbp_enrich](https://gitlab.com/aghr/matt/-/wikis/o8_high_level/test_cisbp_enrich), [rna_maps_cisbp](https://gitlab.com/aghr/matt/-/wikis/o8_high_level/rna_maps_cisbp) or when you have used any of the CISBP-RNA binding motifs in sub-folder data_external/cisbp_rna of Matt installation.


### Exon- and intron-related features contain predicted branch points from SVM_BPfinder

As Matt internally uses [SVM_BPfinder](http://regulatorygenomics.upf.edu/Software/SVM_BP/) for the prediction of branch points, please cite 

[Corvelo et al., Genome-Wide Association between Branch Point Properties and Alternative Splicing, 2010, DOI: 10.1038/nature12311](http://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1001016)

and [SVMlight](http://matt.crg.eu/#http://svmlight.joachims.org/) used by SVM_BPfinder

Joachims, Making large-Scale SVM Learning Practical. Advances in Kernel Methods - Support Vector Learning, B. Schölkopf and C. Burges and A. Smola (ed.), MIT-Press, 1999 

whenever you publish results related to predicted branch points. This includes the following features which you might have obtained with Matt's commands [get_efeatures](https://gitlab.com/aghr/matt/-/wikis/o5_analyze_seqs/get_efeatures), [get_ifeatures](https://gitlab.com/aghr/matt/-/wikis/o5_analyze_seqs/get_ifeatures), [cmpr_exons](https://gitlab.com/aghr/matt/-/wikis/o8_high_level/cmpr_exons), [cmpr_introns](https://gitlab.com/aghr/matt/-/wikis/o8_high_level/cmpr_introns): 

* DIST_FROM_MAXBP_TO_3SS_UPINTRON - distance to 3ss of best precited BP
* SEQ_MAXBP_UPINTRON - sequence of best predicted BP
* SCORE_FOR_MAXBP_SEQ_UPINTRON - BP sequence score of best predicted BP
* PYRIMIDINECONT_MAXBP_UPINTRON - Pyrimidine content between the BP adenine and the 3 prime splice site for best BP
* POLYPYRITRAC_OFFSET_MAXBP_UPINTRON - Polypyrimidine track offset relative to the BP adenine for best BP
* POLYPYRITRAC_LEN_MAXBP_UPINTRON - Polypyrimidine track length for best BP
* POLYPYRITRAC_SCORE_MAXBP_UPINTRON - Polypyrimidine track score for best BP
* BPSCORE_MAXBP_UPINTRON - SVM classification score of best BP
* NUM_PREDICTED_BPS_UPINTRON - number of all predicted BPs which have a positive BP score
* MEDIAN_DIST_FROM_BP_TO_3SS_UPINTRON - like DIST_FROM_MAXBP_TO_3SS but median over top-3 predicted BPs
* SEQ_BPS_UPINTRON - comma-separated list of sequences of top-3 predicted BPs
* MEDIAN_SCORE_FOR_BPSEQ_UPINTRON - like SCORE_FOR_MAXBP_SEQ but median over top-3 predicted BPs
* MEDIAN_PYRIMIDINECONT_UPINTRON - like PYRIMIDINECONT_MAXBP but median over top-3 predicted BPs
* MEDIAN_POLYPYRITRAC_OFFSET_UPINTRON - like POLYPYRITRAC_OFFSET_MAXBP but median over top-3 predicted BPs
* MEDIAN_POLYPYRITRAC_LEN_UPINTRON - like POLYPYRITRAC_LEN_MAXBP but median over top-3 predicted BPs
* MEDIAN_POLYPYRITRAC_SCORE_UPINTRON - like POLYPYRITRAC_SCORE_MAXBP but median over top-3 predicted BPs
* MEDIAN_BPSCORE_UPINTRON - like BPSCORE_MAXBP but median over top-3 predicted BPs
* DIST_FROM_MAXBP_TO_3SS_DOINTRON - distance to 3ss of best precited BP
* SEQ_MAXBP_DOINTRON - sequence of best predicted BP
* SCORE_FOR_MAXBP_SEQ_DOINTRON - BP sequence score of best predicted BP
* PYRIMIDINECONT_MAXBP_DOINTRON - Pyrimidine content between the BP adenine and the 3 prime splice site for best BP
* POLYPYRITRAC_OFFSET_MAXBP_DOINTRON - Polypyrimidine track offset relative to the BP adenine for best BP
* POLYPYRITRAC_LEN_MAXBP_DOINTRON - Polypyrimidine track length for best BP
* POLYPYRITRAC_SCORE_MAXBP_DOINTRON - Polypyrimidine track score for best BP
* BPSCORE_MAXBP_DOINTRON - SVM classification score of best BP
* NUM_PREDICTED_BPS_DOINTRON - number of all predicted BPs which have a positive BP score
* MEDIAN_DIST_FROM_BP_TO_3SS_DOINTRON - like DIST_FROM_MAXBP_TO_3SS but median over top-3 predicted BPs
* SEQ_BPS_DOINTRON - comma-separated list of sequences of top-3 predicted BPs
* MEDIAN_SCORE_FOR_BPSEQ_DOINTRON - like SCORE_FOR_MAXBP_SEQ but median over top-3 predicted BPs
* MEDIAN_PYRIMIDINECONT_DOINTRON - like PYRIMIDINECONT_MAXBP but median over top-3 predicted BPs
* MEDIAN_POLYPYRITRAC_OFFSET_DOINTRON - like POLYPYRITRAC_OFFSET_MAXBP but median over top-3 predicted BPs
* MEDIAN_POLYPYRITRAC_LEN_DOINTRON - like POLYPYRITRAC_LEN_MAXBP but median over top-3 predicted BPs
* MEDIAN_POLYPYRITRAC_SCORE_DOINTRON - like POLYPYRITRAC_SCORE_MAXBP but median over top-3 predicted BPs
* MEDIAN_BPSCORE_DOINTRON - like BPSCORE_MAXBP but median over top-3 predicted BPs


### Exon- and intron-related features contain binding score of human SF1

As Matt uses the human SF1 binding motif (position weight matrix) published by Corioni et al., please cite 

[Corioni et al., Analysis of in situ pre-mRNA targets of human splicing factor SF1 reveals a function in alternative splicing, 2011, DOI: 10.1093/nar/gkq1042](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3061054/) 

whenever you publish results related to this human SF1 binding motif. This includes the following features which you might have obtained with Matt's commands [get_efeatures](https://gitlab.com/aghr/matt/-/wikis/o5_analyze_seqs/get_efeatures), [get_ifeatures](https://gitlab.com/aghr/matt/-/wikis/o5_analyze_seqs/get_ifeatures), [cmpr_exons](https://gitlab.com/aghr/matt/-/wikis/o8_high_level/cmpr_exons), [cmpr_introns](https://gitlab.com/aghr/matt/-/wikis/o8_high_level/cmpr_introns):

* SF1_HIGHESTSCORE_3SS_UPINTRON - highest score of a SF1 positon weight matrix trained with human data in the last 150 nt 3 prime intron positons of up-stream intron
* SF1_HIGHESTSCORE_3SS_DOINTRON - highest score of a SF1 positon weight matrix trained with human data in the last 150 nt 3 prime intron positons of down-stream intron



## Disclaimer

This software Matt (SOFTWARE PRODUCT) is provided by Andre Gohr (THE PROVIDER) "as is" and "with all faults."
THE PROVIDER makes no representations or warranties of any kind concerning the safety, suitability, 
lack of viruses, inaccuracies, typographical errors, or other harmful components of this 
SOFTWARE PRODUCT. There are inherent dangers in the use of any software, and you are solely 
responsible for determining whether this SOFTWARE PRODUCT is compatible with your equipment 
and other software installed on your equipment. You are also solely responsible for the protection 
of your equipment and backup of your data, and THE PROVIDER will not be liable for any damages you 
may suffer in connection with using, modifying, or distributing this SOFTWARE PRODUCT.
