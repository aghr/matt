#!/usr/bin/env perl
use strict;
use warnings;
use File::Temp qw(tempfile);
use File::Copy qw(move);
use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.

my $MYCOMMAND="add_range";

# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "manipulation\t$MYCOMMAND\t:  \tadd column with range of values";
        exit(1);
}

if(!defined($ARGV[0]) || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/ || @ARGV<2 || @ARGV>4){
	print "\nmatt $MYCOMMAND <TABLE> <C_NAME> [<SRT> <DLT>]\n\n";
	print "   ... add a column with a ranging value to right end of TABLE.\n";
	print "       By default, the ranging value starts at 0 and increases by 1 for each row\n";
	print "   <TABLE>     : tab-separated table\n";
	print "   <C_NAME>    : column name of new columns\n";
	print "   <SRT> <DLT> : if given: ranging value starts at SRT (in first row) and changes by DLT each row.\n\n";
	print "   Output : table TABLE with addtional column C_NAME will be printed to screen\n\n";
	exit(1);
}


my ($file_n,$col_n,$start,$d)=@ARGV;
if(!defined($start)){$start=0}
if(!defined($d)){$d=1}

if(!is_numeric($start)){die "Argument SRT which is $start must be numeric.\n"}
if(!is_numeric($d)){die "Argument DLT which is $d must be numeric.\n"}

open(my $fh,"<".$file_n) or die "Sorry, you requested add_val but: I cannot open file $file_n.";

my $found_header=0;
my $val=$start;
while(<$fh>){
	$_=clean_line($_);
	# remove empty line
	if(length($_)==0){next;}
	if(substr($_,0,1) eq "#"){print "$_\n"; next;}
	
	if($found_header==0){
		if(/$col_n/){die "Sorry, you requested add_val but: there is already a column $col_n in file $file_n.";}
		print "$_\t$col_n\n";
		$found_header=1;
		next;
	}
	
	print "$_\t$val\n";
	$val+=$d;
}

close($fh);

exit(0); # success
__END__
