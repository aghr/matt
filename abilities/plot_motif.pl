#!/usr/bin/env perl
use strict;
use warnings;
use File::Temp qw(tempfile);

use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.


my $MYCOMMAND="plot_motif";

# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "seq_analysis\t$MYCOMMAND\t:      \tplot motif / PWM";
        exit(1);
}

if(@ARGV<1 || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/ || @ARGV>5){
        print "\nmatt $MYCOMMAND <TABLE> [<C_SEQ>] [<C_GROUP>] [<-f PDF_NAME>]\n\n";
        print "   ... plot a motif as given by aligned sequences or a PWM into PDF file.\n\n";
	print "   <TABLE>     : tab-separated table defining a PWM (in matt format) or containing align sequences\n";
	print "                   If you specify only <TABLE>, it must contain a PWM, if you further specify\n";
	print "                   C_GROUP it must contain align sequences\n";
	print "   <C_SEQ>     : name of column with aligned sequences, all of identical length\n";
	print "                   Might contain several sequences in one cell separated by :\n";
	print "   <C_GROUP>   : name of column containing some kind of group information defining sub-sets of sequences\n";
	print "                   If you omit C_GROUP, all sequences will be used for the motif plot.\n";
	print "                   If you specify C_GROUP, a motif plot will be create for each sub-set of sequences.\n";
	print "                   If you specify C_GROUP[grp1,grp2,..], a motif plot will be created the defined\n";
	print "                   sub-sets of sequences (grp1, grp2,...).\n";
	print " <-f PDF_NAME> : if you specify -f PDF_NAME, e.g., -f sf1_pwm.pdf, the resulting PDF file will have\n";
	print "                   this name. If you have defined C_GROUP, one plot (PDF) will be created for each group\n";
	print "                   and group IDs will be added to PDF_NAME.\n";
	print "                   If you don't specify PDF_NAME, the PDF file name will be <TABLE>_motifplot.pdf\n\n";
	print "   Output: one or several PDF files\n\n";
	print "   IMPORTANT: if you define C_SEQ and C_GROUP, you need to define C_SEQ first then C_GROUP.\n";
	print "\n";
        exit(1);
}

my $fn_tab=$ARGV[0];
my $cn_seq="";   # is $cn_seq ne "" -> this also indicates that we are given a file with a PWM
my $cn_grp="";
my $fn_pdf="";
my %selected_grps=();
my $check_grps=0;

for(my $i=1;$i<@ARGV;$i++){
	if($ARGV[$i] eq "-f"){$fn_pdf=$ARGV[++$i];next;}
	if($cn_seq eq ""){$cn_seq=$ARGV[$i];next;}  # first column must be C_SEQ
	if(!$cn_grp eq ""){
		die "Something seems to be wrong with parameters: first define C_SEQ, then C_GROUP.";
	}else{
		$cn_grp=$ARGV[$i];
		if($ARGV[$i] =~ /\[(.*)\]/){  # we have definition of sub-sets 
        		my @fs=split(",",$1);
        		foreach my $tmp (@fs){$selected_grps{$tmp}=1;}
        		$check_grps=1;
        		($cn_grp = $ARGV[$i] ) =~  s/\[.*$//i;  # delete sub-set definitions from column name
		}
	}
}
my @col_ns=();
if($cn_seq ne ""){$col_ns[0]=$cn_seq;}
if($cn_grp ne ""){$col_ns[1]=$cn_grp;}

# prepare output file name
if($fn_pdf eq ""){
	# we shorten down to the last positions after the last /
	($fn_pdf = $fn_tab) =~ s/^.*\///i;
	# cut off all what comes after the first .
	($fn_pdf = $fn_pdf) =~ s/\..*$//i; 
	$fn_pdf.="_motifplot.pdf";
}
# cut off all what comes after the first .
my $fn_pdf_tmp;
($fn_pdf_tmp = $fn_pdf) =~ s/\..*$//i;


# generate PWMs from seqs
my %pwms=();  # one for each sub-set
my @fn_pwm=();
my @fn_out=();
my @alph_ACGT=("A","C","G","T");
my @alph_ACGU=("A","C","G","U");
if($cn_seq ne ""){
	my ($fh,$col_ns_aref,$col_ids_aref,$comments) = open_fct_file($fn_tab,[@col_ns],$MYCOMMAND);
	my $seq_cid=$col_ids_aref->[0];
	my $grp_cid=-1;if(@col_ns>1){$grp_cid=$col_ids_aref->[1];}
	my @fs;
	my $grp="hj1321wuiwy<1242fjwuiw6897b";
	my $seq;
	my $i;
	my $aref;
	while(<$fh>){
		$_=clean_line($_);
		if(length($_)==0 || substr($_,0,1) eq "#"){next;}
		my @fs=split_line($_);
		
		$seq=$fs[$seq_cid];if(length($seq)==0){next;}  # we ignore empty sequences
		
		if($grp_cid!=-1){
			$grp=$fs[$grp_cid];
			if($check_grps && !defined($selected_grps{$grp})){next;}
		}
		unless(defined($pwms{$grp})){
			$pwms{$grp}=[];
			my ($seq_tmp)=split(":",$seq);
			for($i=0; $i<length($seq_tmp);$i++){$pwms{$grp}->[$i]={};}
		}
		
		$aref=$pwms{$grp};
		foreach my $seq_tmp (split(":",$seq)){
			if(length($seq_tmp)==0){next;}
			if(length($seq_tmp) != scalar(@{$aref})){die "Sequences in column $cn_seq are not all of the same length.";}
			$i=-1;
			foreach my $l (split("",$seq_tmp)){$i++;$aref->[$i]->{$l}++; $aref->[$i]->{"sum_over_position_klfjeuihqb"}++;}
		}
		$pwms{$grp}=$aref;
	}
	close($fh);
	
	foreach $grp (keys %pwms){
		$aref=$pwms{$grp};
		if($grp eq "hj1321wuiwy<1242fjwuiw6897b"){ # we only have one group not specified by user
			push(@fn_out,$fn_pdf);
		}else{  # use has selected some sub-sets of seqs
			push(@fn_out,$fn_pdf_tmp."_$grp.pdf");
		}
		my ($fh_tmp,$tmpfile_n) = tempfile( DIR => ".", UNLINK => 1);
		# write PWM
		my @alph=@alph_ACGT;
		@fs=("SYMBOL");for(my $i=0;$i<@{$aref};$i++){
					push(@fs,"POS_".($i+1));
					if(defined($aref->[$i]->{"U"})){@alph=@alph_ACGU;}
				}
		print $fh_tmp join("\t",@fs)."\n";
		foreach my $l (@alph){
			@fs=($l);
			my $v;
			for(my $i=0;$i<@{$aref};$i++){$v=$aref->[$i]->{$l};if(!defined($v)){push(@fs,0);}else{push(@fs,$v/$aref->[$i]->{"sum_over_position_klfjeuihqb"});}}
			print $fh_tmp join("\t",@fs)."\n";
		}
		push(@fn_pwm,$tmpfile_n);
		close($fh_tmp);
	}
}

# we are given a PWM for plotting
if($cn_seq eq ""){

	# read in PWM with utility function that understands many different forms of PWMs automatically
	my ($pwm_aref,$alph_aref)=read_pwm($fn_tab);
	
	# and write it in matt format temporarily
	my ($fh_tmp,$tmpfile_n) = tempfile( DIR => ".", UNLINK => 1);
	write_pwm($pwm_aref,$fh_tmp);
	close($fh_tmp);
	
	@fn_pwm=($tmpfile_n);
	@fn_out=($fn_pdf);
}

for(my $i=0;$i<@fn_pwm;$i++){
	if(-e $fn_out[$i]){warn "Output PDF file to write into ($fn_out[$i]) already exists. Will not generate PWM and proceed.";next;}
	my $cmd = "$scr_abspath/../utilities/freq_and_infcontent_plot.r $fn_pwm[$i] $fn_out[$i]";
	my @ret=`$cmd`;die "$!" if $?!=0;
	print join("\n",@ret);
	unlink($fn_pwm[$i]);
}

exit(0); # success
__END__
