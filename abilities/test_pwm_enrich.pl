#!/usr/bin/env perl
use strict;
use warnings;
use File::Temp qw(tempfile);

use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.


my $MYCOMMAND="test_pwm_enrich";

# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "seq_analysis\t$MYCOMMAND\t: \ttest PWM motif enrichment";
        exit(1);
}

if(@ARGV<11 || @ARGV>11 || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/){
    print "\nmatt $MYCOMMAND <TABLE_SEQS> <C_SEQS> <C_DATASET> <SEARCH_STRAND> <TABLE_PWMS> <C_PWM_NAMES> <C_PWM_FILES>...\n";
    print "                ... <C_PWM_THRESHOLDS> <BG_MODEL> <MODE> <N_ITERATIONS>\n\n";
    print "   ... compare pairs of sequence data sets wrt. the number of predicted PWM hits in these sets (significant enrichment or depletion).\n\n";
    print "   <TABLE_SEQS>      : tab-separated table containing sequences to be used\n";
    print "   <C_SEQS>          : name of column with sequences (can be of variable length)\n";
    print "   <C_DATASET>       : name of column containing information (IDs) about the groups the sequences belong to\n";
    print "                         In addition, you need to specify which groups of sequences you want to compare!\n";
    print "                         E.g., DATASET[fg1,bg1,fg2,bg2] will compare groups fg1 vs bg1, and fg2 vs bg2. The order is always foreground background,\n";
    print "                         forground background... . You can specify as many pairs of groups as you want.\n";
    print "   <SEARCH_STRAND>   : single -> search on sequences as given\n";
    print "                         double1 -> search also on rev-complement and positions of hits are wrt. each strand\n";
    print "                         double2 -> like double1 but hit positions are wrt. forward strand (sequences as given)\n";
    print "   <TABLE_PWMS>      : tab-separated table containing information on PWMs to be used for testing. Can be one or many.\n";
    print "   <C_PWM_NAMES>     : name of column in TABLE_PWMs with short names of PWMs; All PWMs with the same name will be tested together\n";
    print "                         and hit positions will be reported once, i.e., if two PWMs have a hit the same position, this position is reported only once.\n";
    print "                         You can also sub-select PWMs from TABLE_PWMS with: C_PWM_NAMES[name1,name2,name3] and only PWMs with these names will be considered.\n";
    print "   <C_PWM_FILES>     : name of column of TABLE_PWMS which contains file names of tables describing the PWMs (see also command get_pwm).\n";
    print "   <C_PWM_THRESHOLDS>: name of column of TABLE_PWMS which contains thresholds on PWM scores for predicting hits. Its meaning chganges as argument BG_MODEL changes.\n";
    print "                         * if <BG_MODEL>=NO_BG_MODEL: thresholds are on the log-score of the PWMs (they are <= 0 and only positions with a\n";
    print "                           higher (or equal) score are predicted to be a hit).\n";
    print "                         * if <BG_MODEL>=INFER_BGPWM: thresholds are on ratios of P_fgpwm / P_bgpwm and should be 2, 5, 10 or higher\n";
    print "                           and only positions where this ratio is higher (or equal) are predicted to be a hit.\n";
    print "                         * if <BG_MODEL>=a file to a bg PWM: same as for INFER_BGPWM\n";
    print "   <BG_MODEL>        : defines type of background model to be used\n"; 
    print "                         * if <BG_MODEL>=NO_BG_MODEL: no backgroud model is used\n";
    print "                         * if <BG_MODEL>=INFER_BGPWM: a background model (homogeneous Markov model of oder 0) is inferred for each data set (fg and bg) individually.\n";
    print "                         * if <BG_MODEL> is a file to a table describing a PWM: this PWM will be used as background model for all data sets; The background PWM will be cut\n";
    print "                               to the lengths of the fg pwms if necessary.\n";
    print "   <MODE>            : can be either \'quant\' or a integer\n";
    print "                         * if <MODE>=quant: enrichment is defined wrt. ratio of normalized hit counts (quantitatively)\n";
    print "                         * if <MODE>=<INTEGER>: enrichment is defined as ratio of sequences with status YES/NO (having hit/having no hit)\n";
    print "                           where a sequence is YES if it has at least <INTEGER> hits.\n";
    print "   <N_ITERATIONS>    : number of iterations of permutation test used for estimating p-values, e.g., 10000 or more\n\n";
    print "   Output: a table with all results (number of hits, test statistics, p values) is written to STDOUT.\n";
    print "           P value encoding: p value <= 0.05/0.01/0.001 : */**/***\n";
    print "\n";
    exit(1);
}

my ($fn_seq,$cn_seq,$cn_ds,$strand_mode,$fn_pwms,$cn_pwmns,$cn_pwmfs,$cn_pwmts,$bg_model,$mode,$N)=@ARGV;
unless($strand_mode eq "single" || $strand_mode eq "double1" || $strand_mode eq "double2"){die "Please supply information on search locus: single or double1, double2?.";}

my @pwms=();
my @pwm_fs=();
my @threshs=();
my @pwm_ns=();
my $cmd;
my @fs;

# get pwm group names
if($cn_pwmns =~ /(.+)\[(.+?)\]/){
	$cn_pwmns=$1;
	@pwm_ns=split(",",$2);
}else{
	$cmd="matt col_uniq $fn_pwms $cn_pwmns | matt get_cols - ${cn_pwmns}";
	#print "$cmd\n";
	@fs=split("\n",`$cmd`);
	@pwm_ns=@fs[1..(@fs-1)];
}

# for each group, get PWMs
foreach my $n (@pwm_ns){
	$cmd="matt get_rows $fn_pwms $cn_pwmns]${n}[ | matt get_cols - $cn_pwmfs";
	#print "$cmd\n";
	@fs=split("\n",`$cmd`);
	@fs=@fs[1..(@fs-1)];
	# save file names for output
	push(@pwm_fs,[@fs]);

	my $aref_tmp=[];
	foreach my $pwm_f (@fs){
		my ($pwm_aref,$alph_aref)=read_pwm($pwm_f,$MYCOMMAND);
		push(@$aref_tmp,$pwm_aref);
	}

	push(@pwms,$aref_tmp);

	$cmd="matt get_rows $fn_pwms $cn_pwmns]${n}[ | matt get_cols - $cn_pwmts";
	#print "$cmd\n";
	@fs=split("\n",`$cmd`);
	push(@threshs,[@fs[1..(@fs-1)]]);
}

# for each group get joined cell contents of all columns from PWM description table
$cmd="matt get_colnms $fn_pwms | matt get_cols - COL_NAMES";
@fs=split("\n",`$cmd`);
if($cn_pwmns eq "NAME"){
	for(my $i=0;$i<@fs;$i++){if($fs[$i] eq "NAME"){$fs[$i]="NAME_2";}}
}
my $pwm_descr_header=join("\t",@fs[1..(@fs-1)]);

my @pwm_descr=();
foreach my $n (@pwm_ns){
	$cmd="matt get_rows $fn_pwms $cn_pwmns]${n}[";
	#print "$cmd\n";
	@fs=split("\n",`$cmd`);
	@fs=@fs[1..(@fs-1)];

	my @joined_cells=();
	foreach my $line (@fs){
		my @fs2=split("\t",$line,-1);
		for(my $i=0; $i<@fs2;$i++){
			unless($joined_cells[$i]){$joined_cells[$i]=[];}
			push(@{$joined_cells[$i]},$fs2[$i]);
		}
	}
	
	for(my $i=0;$i<@joined_cells;$i++){ $joined_cells[$i]=join(",",@{$joined_cells[$i]});}
	push(@pwm_descr,join("\t",@joined_cells));
}

# bg pwm
my $bgpwm_aref;
if($bg_model ne "INFER_BGPWM" && $bg_model ne "NO_BG_MODEL"){
	my ($bgpwm_aref_tmp,$alph_aref)=read_pwm($bg_model,$MYCOMMAND);
	$bgpwm_aref=$bgpwm_aref_tmp;
}


$cn_ds=~/(.+)\[(.+?)\]/;
$cn_ds=$1;
unless($2){die "Could not extract data-set ids from $cn_ds. Please check syntax.";}
my @datasets=split(",",$2);
if( @datasets % 2){die "$MYCOMMAND: you need to specify pairs of data sets (fg1,bg1,fg2,bg2,...) but you gave $cn_ds.";}

# check if data sets are present in table
$cmd="matt col_uniq $fn_seq $cn_ds";  
@fs=split("\n",`$cmd`);  die "$cmd\n$!" if $?!=0;
foreach my $dataset (@datasets){my $found=0; for(my $i=0;$i<@fs;$i++){if($fs[$i]=~/^$dataset\t/){$found=1;last;}} if($found==0){die "Group $dataset is not present in file $fn_seq in column $cn_ds\n"}}


#output header
print "NAME"
	."\tSEARCH_ON_STRAND"
	."\tMODE"
	."\tBG_MODEL"
	."\tDATASETNAME_FG"
	."\tNSEQS_FG"
	."\tNHITS_ALL_FG"
	."\tNHITS_START_FG"
	."\tNHITS_INTERNAL_FG"
	."\tNHITS_END_FG"
	."\tNSEQS_WITHHITS_ALL_FG"
	."\tNSEQS_WITHHITS_START_FG"
	."\tNSEQS_WITHHITS_INTERNAL_FG"
	."\tNSEQS_WITHHITS_END_FG"
	."\tTESTSTAT_ALL_FG"
	."\tTESTSTAT_START_FG"
	."\tTESTSTAT_INTERNAL_FG"
	."\tTESTSTAT_END_FG"
	."\tDATASETNAME_BG"
	."\tNSEQS_BG"
	."\tNHITS_ALL_BG"
	."\tNHITS_START_BG"
	."\tNHITS_INTERNAL_BG"
	."\tNHITS_END_BG"
	."\tNSEQS_WITHHITS_ALL_BG"
	."\tNSEQS_WITHHITS_START_BG"
	."\tNSEQS_WITHHITS_INTERNAL_BG"
	."\tNSEQS_WITHHITS_END_BG"
	."\tTESTSTAT_ALL_BG"
	."\tTESTSTAT_START_BG"
	."\tTESTSTAT_INTERNAL_BG"
	."\tTESTSTAT_END_BG"
	."\tENRICHMENT_ALL_FG_VS_BG"
	."\tENRICHMENT_FIRST_FG_VS_BG"
	."\tENRICHMENT_INTERNAL_FG_VS_BG"
	."\tENRICHMENT_END_FG_VS_BG"
	."\tN_ITS_PERMUTATIONTEST"
	."\tPVALUE_ALL"
	."\tPVALUE_FIRST"
	."\tPVALUE_INTERNAL"
	."\tPVALUE_END"
	."\tSIGNIFICANT_RESULT_ALL"
	."\tSIGNIFICANT_RESULT_FIRST"
	."\tSIGNIFICANT_RESULT_INTERNAL"
	."\tSIGNIFICANT_RESULT_END"
	."\t$pwm_descr_header"
	."\n";


my (@fg,@bg,@nhits_fg,@nhits_bg,@l_fg,@l_bg);
my (@teststat_fg,@teststat_bg);
my $seq_tmp;
my $nhits;
my $kmer_search_exp;
# for testing if the hit positions differ between fg and bg
# therefor each seq gets divided into the first 1/3, the middle 1/3 and the last 1/3
# and we count the hits in these regions and compare them, i.e.,
# first 1/3 fg vs bg, middle 1/3 fg vs bg, last 1/3 fg vs bg
my @hits;
my @nseqs_with_hits; 
my @nhits;
my @teststats;
my @teststats_diffs;
my @teststats_sums;
my @enrichments;
my @Ns_more_extreme;
my @pvals;
my @sig_levels;
my @relations;
my $hits_aref_tmp;
my $bgpwms_aref;
my $max_pwm_L;
my $N_its;
my %alph_href=();

for(my $i=0; $i<@datasets;$i+=2){
	$cmd="matt get_rows $fn_seq $cn_ds]${datasets[$i]}[ | matt get_cols - $cn_seq";
	#print "$cmd\n";
	my @fs=split("\n",`$cmd`);
	@fg=@fs[1..@fs-1];
	
	$cmd="matt get_rows $fn_seq $cn_ds]${datasets[$i+1]}[ | matt get_cols - $cn_seq";
	#print "$cmd\n";
	@fs=split("\n",`$cmd`);
	@bg=@fs[1..@fs-1];
	#print scalar(@fg)." ".scalar(@bg)."\n";

	for(my $j=0;$j<@pwm_ns;$j++){
		

		(@nhits_fg,@nhits_bg,@l_fg,@l_bg)=((),(),(),());
		@hits=([],[],[],[],[],[],[],[]);  # order all hits fg, hits first 1/3 fg, hits middle 1/3 fg, hits last 1/3 fg, same for bg
		
		if($bg_model eq "INFER_BGPWM" || $bgpwm_aref){
			if($bg_model eq "INFER_BGPWM"){
				$max_pwm_L=-1;
				# bg pwms need to be of same length as fg pwms
				for(my $k=0; $k<@{$pwms[$j]};$k++){
					foreach my $ltr (keys %{$pwms[$j]->[$k]->[0]}){$alph_href{$ltr}=1;}  # get joint alphabet
					if(scalar(@{$pwms[$j]->[$k]})>$max_pwm_L){$max_pwm_L=scalar(@{$pwms[$j]->[$k]});}
				}
				($bgpwm_aref)=learn_hmm0_as_pwm_from_seqs(\@fg,$max_pwm_L,0.001,[keys %alph_href]);
			}
			# all bgpwms need to be of the same length as the fg pwms
			$bgpwms_aref=[];
			for(my $k=0; $k<@{$pwms[$j]};$k++){
				my $bgpwm_aref_tmp=[];
				for(my $l=0;$l<@{$pwms[$j]->[$k]};$l++){push(@$bgpwm_aref_tmp,$bgpwm_aref->[$l]);}
				push(@$bgpwms_aref,$bgpwm_aref_tmp);
			}
		}
		
		# get hits and seq lengths from fg data			
		$hits_aref_tmp=get_pwm_hits_for_seqs(\@fg,$pwms[$j],$threshs[$j],$strand_mode,$bgpwms_aref);	
		for(my $k=0;$k<@fg;$k++){
			 $hits[0]->[$k]=0; $hits[1]->[$k]=0; $hits[2]->[$k]=0; $hits[3]->[$k]=0;
			 my $seq_len=length($fg[$k]);
			 my $seq_len_1=$seq_len/3;
			 my $seq_len_2=2*$seq_len/3;
			 $l_fg[$k]=$seq_len;
			 $nhits=0;
			 foreach my $hit_pos (@{$hits_aref_tmp->[$k]}){
				$nhits++;
    				if($hit_pos < $seq_len_1){$hits[1]->[$k]+=1;}elsif($hit_pos < $seq_len_2){$hits[2]->[$k]+=1;}else{$hits[3]->[$k]+=1;}
    			  }
			 
			 $hits[0]->[$k]=$nhits;
		}

		if($bg_model eq "INFER_BGPWM" || $bgpwm_aref){
			if($bg_model eq "INFER_BGPWM"){
				($bgpwm_aref)=learn_hmm0_as_pwm_from_seqs(\@bg,$max_pwm_L,0.001,[keys %alph_href]);				
			}
			# all bgpwms need to be of the same length as the fg pwms
			$bgpwms_aref=[];
			for(my $k=0; $k<@{$pwms[$j]};$k++){
				my $bgpwm_aref_tmp=[];
				for(my $l=0;$l<@{$pwms[$j]->[$k]};$l++){push(@$bgpwm_aref_tmp,$bgpwm_aref->[$l]);}
				push(@$bgpwms_aref,$bgpwm_aref_tmp);
			}
		}

		# get hits and seq lengths from bg data				
		$hits_aref_tmp=get_pwm_hits_for_seqs(\@bg,$pwms[$j],$threshs[$j],$strand_mode,$bgpwms_aref);
		for(my $k=0;$k<@bg;$k++){
			 $hits[4]->[$k]=0; $hits[5]->[$k]=0; $hits[6]->[$k]=0; $hits[7]->[$k]=0;
			 my $seq_len=length($bg[$k]);
			 my $seq_len_1=$seq_len/3;
			 my $seq_len_2=2*$seq_len/3;
			 $l_bg[$k]=$seq_len;
			 $nhits=0;
			 foreach my $hit_pos (@{$hits_aref_tmp->[$k]}){
			 	$nhits++;
    				if($hit_pos < $seq_len_1){$hits[5]->[$k]+=1;}elsif($hit_pos < $seq_len_2){$hits[6]->[$k]+=1;}else{$hits[7]->[$k]+=1;}
    			  }
			 $hits[4]->[$k]=$nhits;
		}
	
		@nseqs_with_hits=(0,0,0,0,0,0,0,0); # number of sequences with hits; 0: across entire seq in fg, 1: across first 1/3 in fg, 2: across middle 1/3 fg, across last 1/3 in fg, .. same with bg
		@nhits=(0,0,0,0,0,0,0,0);
		for(my $l=0;$l<@hits;$l++){
			for(my $k=0;$k<@{$hits[$l]};$k++){
				$nhits[$l]+=$hits[$l]->[$k];
				$nseqs_with_hits[$l]+= $hits[$l]->[$k] > 0 ? 1 : 0;
		}}

		# normalize counts 
		if($mode eq "quant"){
			for(my $k=0;$k<@fg;$k++){$hits[0]->[$k]=$hits[0]->[$k]/$l_fg[$k];}
			for(my $l=1;$l<4;$l++){for(my $k=0;$k<@fg;$k++){$hits[$l]->[$k]=$hits[$l]->[$k]/($l_fg[$k]/3);}}
			for(my $k=0;$k<@bg;$k++){$hits[4]->[$k]=$hits[4]->[$k]/$l_bg[$k];}
			for(my $l=5;$l<8;$l++){for(my $k=0;$k<@bg;$k++){$hits[$l]->[$k]=$hits[$l]->[$k]/($l_bg[$k]/3);}}
		}else{
			for(my $l=0;$l<8;$l++){for(my $k=0;$k<@{$hits[$l]};$k++){$hits[$l]->[$k] = $hits[$l]->[$k] >= $mode ? 1 : 0;}}
		}

		# get test statistics
		for(my $l=0;$l<8;$l++){$teststats[$l]=get_mean_from_aref($hits[$l]);}
		
		# get test stat diffs
		for(my $l=0;$l<4;$l++){$teststats_diffs[$l]=abs( $teststats[$l]-$teststats[$l+4]);}
				
		# get enrichments
		for(my $l=0;$l<4;$l++){
			if($teststats[$l+4]>0){
				$enrichments[$l]=$teststats[$l]/$teststats[$l+4];
			}else{
				$enrichments[$l]="NA";
			}
		}
		
		# do permutation test
		@Ns_more_extreme=(0,0,0,0);
		my @ids=0..(@fg+@bg-1);
		$N_its=0;
		for(my $n=0;$n<$N;$n++){
			my $id;
			@teststats_sums=(0,0,0,0,0,0,0,0);
			shuffle_elements_in_aref(\@ids);
			for(my $k=0;$k<@ids;$k++){
				$id=$ids[$k];
				if($k<@fg){
					if($id<@fg){
						for(my $l=0;$l<4;$l++){$teststats_sums[$l]+=$hits[$l]->[$id];}
					}else{
						for(my $l=0;$l<4;$l++){$teststats_sums[$l]+=$hits[$l+4]->[$id-@fg];}
					}
				}else{
					if($id<@fg){
						for(my $l=0;$l<4;$l++){$teststats_sums[$l+4]+=$hits[$l]->[$id];}
					}else{
						for(my $l=0;$l<4;$l++){$teststats_sums[$l+4]+=$hits[$l+4]->[$id-@fg];}
					}
				}
			}
			
			for(my $l=0;$l<4;$l++){
				$Ns_more_extreme[$l]+=abs( ($teststats_sums[$l]/@fg) - ($teststats_sums[$l+4]/@bg) ) >= $teststats_diffs[$l] ? 1 : 0;  
			}
			$N_its++;
			
			if($n>0 && ($n % 10000==0 || $n==1000)){
				# break out here already if p-value too larger
				my $break=0;
				if($n>1000){
					for(my $l=0;$l<4;$l++){
						if( ($Ns_more_extreme[$l]+1)/($N_its+1)>0.1 ){$break++;}
					}
				}else{
					for(my $l=0;$l<4;$l++){
						if( ($Ns_more_extreme[$l]+1)/($N_its+1)>0.4 ){$break++;}
					}
				}
				
				if($break==4){
					last;
				}
			}
		}

		# compute p values, significance levels, relations
		for(my $l=0;$l<4;$l++){
			$pvals[$l]= ($Ns_more_extreme[$l]+1)/($N_its+1);
			if($pvals[$l]<=0.001){$sig_levels[$l]="***";
			}elsif($pvals[$l]<=0.01){$sig_levels[$l]="**";
			}elsif($pvals[$l]<=0.05){$sig_levels[$l]="*";
			}else{$sig_levels[$l]="";}

			if($sig_levels[$l] ne ""){
				if($teststats[$l]<$teststats[$l+4]){$sig_levels[$l]=$datasets[$i]."<".$datasets[$i+1]." ".$sig_levels[$l];
				}else{ $sig_levels[$l]=$datasets[$i].">".$datasets[$i+1]." ".$sig_levels[$l] } 
			}
		}

		# output
		print "$pwm_ns[$j]"
		      ."\t$strand_mode"
		      ."\t$mode"
		      ."\t$bg_model"
		      ."\t$datasets[$i]"
		      ."\t".scalar(@fg)
		      ."\t".$nhits[0]
		      ."\t".$nhits[1]
		      ."\t".$nhits[2]
		      ."\t".$nhits[3]
		      ."\t".$nseqs_with_hits[0]
		      ."\t".$nseqs_with_hits[1]
		      ."\t".$nseqs_with_hits[2]
		      ."\t".$nseqs_with_hits[3]
		      ."\t".join("\t",@teststats[0..3])
		      ."\t$datasets[$i+1]"
		      ."\t".scalar(@bg)
		      ."\t".$nhits[4]
		      ."\t".$nhits[5]
		      ."\t".$nhits[6]
		      ."\t".$nhits[7]
		      ."\t".$nseqs_with_hits[4]
		      ."\t".$nseqs_with_hits[5]
		      ."\t".$nseqs_with_hits[6]
		      ."\t".$nseqs_with_hits[7]
		      ."\t".join("\t",@teststats[4..7])
		      ."\t".join("\t",@enrichments)
		      ."\t$N_its"
		      ."\t".join("\t",@pvals)
		      ."\t".join("\t",@sig_levels)
		      ."\t".$pwm_descr[$j]
		      ."\n";
	}
}	


exit(0); # success
__END__