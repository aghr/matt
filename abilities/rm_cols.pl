#!/usr/bin/env perl
use strict;
use warnings;
use File::Temp qw(tempfile);
use File::Copy qw(move);

use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.


my $MYCOMMAND="rm_cols";


# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "manipulation\t$MYCOMMAND\t:    \tremoves columns from table";
        exit(1);
}


if(!defined($ARGV[0]) || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/ || @ARGV<2){
        print "\nmatt $MYCOMMAND <TABLE> <C_NAME1> [<C_NAME2> <C_NAME3> ...]\n\n";
	print "   ... remove columns from table\n\n";
	print "   <TABLE>     : tab-separated table\n";
	print "   <C_NAME*>   : names of columns to be removed\n\n";
	print "   Output : the specified columns get removed from the file TABLE and written to STDOUT.\n";
	print "\n";
        exit(1);
}

my $fn=$ARGV[0];
my @col_ns_rm=();
for(my $i=1;$i<@ARGV;$i++){push(@col_ns_rm,$ARGV[$i])}


# create temporary file; will be deleted automatically at the end of script or if error occurs
my ($fh_tmp,$tmpfile_n) = tempfile( DIR => ".", UNLINK => 1);

my ($fh,$col_ns_aref,$col_ids_rm_aref,$comments)=open_fct_file($fn,[@col_ns_rm],$MYCOMMAND);

my @col_ids_stay=();
my @col_ns_stay=();
OUTER :
  for(my $i=0; $i<@$col_ns_aref;$i++){
	foreach my $col_n (@col_ns_rm){
		if($col_n eq $col_ns_aref->[$i]){next OUTER;}
	}
	push(@col_ids_stay,$i);
	push(@col_ns_stay,$col_ns_aref->[$i]);
}

print $fh_tmp $comments;
print $fh_tmp join("\t",@col_ns_stay)."\n";
print join("\t",@col_ns_rm)."\n";


while(<$fh>){
	$_=clean_line($_);
	if(length($_)==0 || substr($_,0,1) eq "#"){next;}
	my @fs=split_line($_);
	
	print $fh_tmp join("\t",@fs[ @col_ids_stay ])."\n";
	print join("\t",@fs[ @$col_ids_rm_aref ])."\n";
}
close($fh);
close($fh_tmp);

move($tmpfile_n,$fn);


exit(0); # success
__END__