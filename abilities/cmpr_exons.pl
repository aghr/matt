#!/usr/bin/env perl
use strict;
use warnings;
use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.
use File::Temp qw(tempfile);
use File::Copy;

my $MYCOMMAND="cmpr_exons";

# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "super\t$MYCOMMAND\t:   \tdiscriminate feature analysis";
        exit(1);
}

if(@ARGV<12 || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/ || @ARGV>20){
    	print "\nmatt $MYCOMMAND <TABLE> <C_START> <C_END> <C_CHR> <C_STRAND> <C_GENEID> <GTF> <FASTA> <SPECIES> ...\n";
    	print "   ... <MAXLEN_BP_ANALYSIS> <C_GROUP> <OUTPUT_DIR> [-colors] [-keepdups] [-notrbts] [-f <FIELD>] [-p <PVAL>]\n\n";
    	print "   ... compare exons of different groups wrt. their exon features\n";
    	print "       Will extract exon features, use Mann-Whitney-U test to test for stat. sig. differences\n";
    	print "       between groups and generate a summary in form of a PDF document. The table with all extracted\n";
    	print "       exon features and results of the stat. tests will be available for later use as well.\n";
    	print "\n";
    	print "   <TABLE>      : tab-separated table describing exons\n";
    	print "   <C_START>, <C_END>, <C_CHR>, <C_STRAND>, <C_GENEID> : names of columns of TABLE with the specified information on exons\n";
    	print "   <GTF>        : a GTF file describing the gene structure; gene IDs in field gene_id (to change this default use argument -f <FIELD>) of GTF must match\n";
    	print "                    those in column C_GENEID of TABLE. The GTF should also contain information on gene type (conding, non-coding).\n";
    	print "                    It's recommended to use ENSEMBL GTFs\n";
    	print "   <FASTA>      : FASTA file with all genome sequences; chromosome IDs must match those in column C_CHR in TABLE\n";
    	print "   <SPECIES>    : choose one from Hsap, Ptro, Mmul, Mmus, Rnor, Cfam, Btau.\n";
	print "                    Branch point features are only available for above listed species.\n";
	print "                    If your species is different but close to one listed species, you could use the closest one to get BP features.\n";
    	print "   <MAXLEN_BP_ANALYSIS> : length of the 3'-ends of introns scanned for branch point analysis, e.g., 150 in general should work OK.\n";
    	print "   <C_GROUP>    : name of column with group IDs defining groups of exons\n";
    	print "                  All group combinations will be tested, e.g., if C_GROUP contains three\n";
    	print "                  group ids: grp1 vs. grp2, grp1 vs. grp3, grp2 vs. grp3 will be compared.\n";
    	print "                  Use C_GROUR[grp1,grp2,..] to sub-select groups for comparison. The last group listed should be the reference group.\n";
    	print "   <OUTPUT_DIR> : folder for outout files; This folder must not exist already.\n";
    	print "                    The summary files will be put in this folder.\n";
    	print "   -colors      : Colors of the box plots; when omitted all boxes appear gray.\n";
    	print "                    You must specify one color for each group which you have specified via C_GROUP.\n";
    	print "                    For available colors see: http://www.stat.columbia.edu/~tzheng/files/Rcolor.pdf\n";
    	print "                    Example: if C_GROUP[g1,g2,g3], then -colors:blue,blue,yellow .\n";
    	print "   -keepdups    : By default, copies of the same exons are neglected. With this argument you can\n";
    	print "                    change this behaviour and keep duplicates. An exon is defined by values in columns <C_START> <C_END> <C_CHR>\n";
    	print "                    <C_STRAND> <C_GNAME>. Exons having identical values in these columns are assumed to be copies of each other.\n";
    	print "   -notrbts     : If your GTF does not contain Havana transcript biotypes or if it contains unknown transcript biotypes, you might\n";
    	print "                    switch off the exploitation of these biotypes by adding this argument. For more details, see matt get_ifeatures .\n";
    	print "   -f FIELD     : Gene ids in TABLE and GTF must coincide. Often GTF contain in the last column different fields like gene_id or gene_name.\n";
	print "                      When you specify -f FIELD, gene ids will be taken from field FIELD from GTF. Default is gene_id.\n";
	print "   -p PVAL      : Significant results highlighted in the report are those with a p-value at most PVAL. Default is -p 0.05\n";    	
    	print "\n";
    	print "   Output: all output files will be put into folder OUTPUT_DIR. You'll find summary.pdf containing the report of the comparison,\n";
    	print "           but also a table with all extracted exon features, one table with all statistics per feature across groups and all box plots\n";
    	print "           as distinct graphics files for later use.\n";
    	print "\n";
    	print "   Hint: For more information on exon features (and related arguments) see help message for matt get_efeatures.\n";
    	print "   Hint: To extract gene ids from GTF, you could use Matt's command retr_geneids. Add them to TABLE with Matt's command add_cols.\n";
    	print "\n";
exit(1);
}


my ($fin,$c_start,$c_end,$c_scf,$c_strand,$c_gname,$gtf,$fasta,$species,$maxlen_bpa,$c_grp,$outdir)=@ARGV[0..11];


unless(-e $gtf){die "Cannot find GTF $gtf."}
unless(-e $fasta){die "Cannot find FASTA $fasta."}
unless(-e $fin){die "Cannot find input file $fin."}


my $colors="";
my $keepdups=0;
my $notrbiotypes="";
my $gid_search_pattern="gene_id";
my $pvalthresh=0.05;
for(my $i=12;$i<@ARGV;$i++){
	if($ARGV[$i] =~ /-colors:(.+)/){$colors=$1;next;}
	if($ARGV[$i] eq "-keepdups"){$keepdups=1;next;}
	if($ARGV[$i] eq "-notrbts"){$notrbiotypes="-notrbts";next;}
	if($ARGV[$i] eq "-f"){$gid_search_pattern=$ARGV[$i+1];$i++;next;}
	if($ARGV[$i] eq "-p"){$pvalthresh=$ARGV[$i+1];$i++;next;}
	die "Do not understand argument $ARGV[$i].";
}

if(!is_numeric($pvalthresh) || $pvalthresh<0 || $pvalthresh>1){die "PVAL must be numeric and 0<=PVAL<=1.\n"}
if($colors ne "" && !are_all_valid_R_colors($colors,$scr_abspath)){die "Check the colors; at least one is not a valid R color.";}

if(-e $outdir){#die "Output directory $outdir already exists. Delete it or choose another directory name.";
}else{
	mkdir($outdir);
}

my @grps1=();
if($c_grp =~ /\[(.*)\]/){  # get groups from C_GROUP argument
	@grps1=split(",",$1);
	($c_grp = $c_grp ) =~  s/\[.*$//i;  # delete sub-set definitions from column name
	if(@grps1<2){die "You have specified less then 2 groups\n";}
}
my $cmd;
my @ret;
my @grps2=();
# extract groups from c_grp
$cmd="matt col_uniq $fin $c_grp";
@ret=`$cmd`;die "$!" if $?!=0;
for(my $i=1;$i<@ret;$i++){
	($grps2[$i-1])=split("\t",$ret[$i]);
}

my @grps=@grps2;
unless(@grps1==0){
	my %h=();foreach my $v (@grps2){$h{$v}=1;}
	foreach my $v (@grps1){if(!defined($h{$v})){die "Group $v does not exists in column $c_grp in file $fin\n";}  }
	@grps=@grps1;	
}
if(@grps<2){die "Number of defined groups is ".scalar(@grps).", but it should be at least 2. Please check syntax of call and table.";}

# define colors for groups
if($colors eq ""){for(my $i=0;$i<@grps;$i++){$colors.="lightgrey,";} $colors=substr($colors,0,length($colors)-1);}
my @fs=split(",",$colors);
my $N_cols=scalar(@fs);
if($N_cols != @grps){die "You have specified $N_cols colors and ".scalar(@grps)." groups but both numbers are not equal.";}


# get basename of input file and create from it name of new file which contains all exons and extracted exon features
my $fin_ext;
($fin_ext = $fin) =~ s/^.*\///i;     # delete path definitions
($fin_ext = $fin_ext) =~ s/\..*$//i; # cut off all what comes after the first .
$fin_ext=$outdir."/".$fin_ext."_with_efeatures.tab";

# extract exon features for each group
my $fout;
my @nums_1=();
my @nums_2=();
for(my $i=0;$i<@grps;$i++){
	my $grp=$grps[$i];
	
	# isolate sub-group of exons
	$fout=$outdir."/".$grp.".tab";
		unless($keepdups){
		$cmd="matt get_rows $fin $c_grp]${grp}[ | matt mk_uniq - $c_start $c_end $c_scf $c_strand $c_gname > $fout";   # omit potential exon duplicates
	}else{
		$cmd="matt get_rows $fin $c_grp]${grp}[ > $fout";   # keep potential exon duplicates
	}
	print `$cmd`;die "$!" if $?!=0;
	#print "exit status: $?\n";

	# extract exon features for sub-group of exons
	$cmd="matt get_efeatures $fout $c_start $c_end $c_scf $c_strand $c_gname $gtf $fasta $species $maxlen_bpa $notrbiotypes -f $gid_search_pattern | matt add_cols $fout -";
	print `$cmd`;die "$!" if $?!=0;

	# count number of events
	my ($fh,$col_ns_aref,$col_ids_aref,$comments)=open_fct_file($fout,[],$MYCOMMAND);
	my $c_tmp=0;
	while(<$fh>){$_=clean_line($_);if(length($_)==0 || substr($_,0,1) eq "#"){next;}$c_tmp++;}
	close($fh);
	push(@nums_1,$c_tmp);

	# reduce to those exons which have been found in the GTF
	$cmd="matt get_rows $fout EXON_FOUND_IN_GTF]yes[ > ${fout}.tmp";
	print `$cmd`;die "$!" if $?!=0;

	# count number of events
	($fh,$col_ns_aref,$col_ids_aref,$comments)=open_fct_file("${fout}.tmp",[],$MYCOMMAND);
	$c_tmp=0;
	while(<$fh>){$_=clean_line($_);if(length($_)==0 || substr($_,0,1) eq "#"){next;}$c_tmp++;}
	close($fh);
	push(@nums_2,$c_tmp);
		
	# join all tables into one big final table
	if($i==0){
		move("${fout}.tmp",$fin_ext);
	}else{
		$cmd="matt add_rows $fin_ext ${fout}.tmp";
		#print $cmd."\n";
		print `$cmd`;die "$!" if $?!=0;
		#print "exit status: $?\n";
	}
	
	# remove temporary files
	unlink("${fout}.tmp");
	unlink($fout);
}


# generate summary
chomp(my $mattversion=`matt --version`);
$fin_ext=~s/$outdir\///; # delete outdir as in generate_summary_efeatures.r we change into outdir and work from there
$cmd = "$scr_abspath/../utilities/generate_summary_efeatures.r $scr_abspath/.. $outdir $fin_ext $c_grp $fin \"".join(";",@grps)."\" \"".join(";",@nums_1)."\" \"".join(";",@nums_2)."\" $keepdups $colors $mattversion $pvalthresh";
@ret=`$cmd`;die "$!" if $?!=0;

print "Generating summary has finished.\n";
print "If you want to re-generate the summary with different colors,\n";
print "change the colors in the end of the following command and re-run it:\n$cmd\n\n";

exit(0); # success
__END__
