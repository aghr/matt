#!/usr/bin/env perl
use strict;
use warnings;

use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.


my $MYCOMMAND="extr_seqs";

# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "seq_extraction\t$MYCOMMAND\t: \textract sequences from table";
        exit(1);
}


if(!defined($ARGV[0]) || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/ || @ARGV<2 || @ARGV>3){
        print "\nmatt $MYCOMMAND <TABLE> <C_SEQ> [<ID_DEFINITION>]\n\n";
        print "   ... extract sequences from a table and get them in FASTA format\n\n";
	print "   <TABLE>         : tab-separated table\n";
	print "   <C-SEQ>         : name of column with sequences\n";
	print "   <ID_DEFINITION> : comma-separated list of column names\n";
	print "                     Their values get concatenated separated by \'_\' and the resulting\n";
	print "                     string is used as sequence ID in the FASTA output\n";
	print "                     If you omit ID_DEFINITION, sequence IDs will be consecutive numbers\n\n";
	print "   Output: sequences in FASTA format to STDOUT\n";
	print "\n";
        exit(1);
}

my ($fn_tab,$cn_seqs,$cnms)=@ARGV;
my @col_ns=();
if($cnms){@col_ns=split(",",$cnms);}
push(@col_ns,$cn_seqs);

##
# main
##
my ($fh,$col_ns_aref,$col_ids_aref,$comments) = open_fct_file($fn_tab,[@col_ns],$MYCOMMAND);
my @col_ids_seqid;for(my $i=0;$i<@col_ns-1;$i++){push(@col_ids_seqid,$col_ids_aref->[$i]);}
my $col_id_seq=$col_ids_aref->[@col_ns-1];

my @fs;
my $seq_id=0;

while(<$fh>){
	$_=clean_line($_);
	if(length($_)==0 || substr($_,0,1) eq "#"){next;}
	
	@fs=split_line($_);
	$seq_id++;
	if($cnms){print ">".join("_",@fs[@col_ids_seqid])."\n$fs[ $col_id_seq ]\n";
	}else{print ">".$seq_id."\n$fs[ $col_id_seq ]\n";}
}
close($fh);


exit(0); # success
__END__