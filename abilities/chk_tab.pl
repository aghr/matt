#!/usr/bin/env perl
use strict;
use warnings;
use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.


my $MYCOMMAND="chk_tab";

# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "import_table\t$MYCOMMAND\t:  \tcheck format of table";
        exit(1);
}

if(!defined($ARGV[0]) || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/ || @ARGV<1){
        print "\nmatt $MYCOMMAND <TABLE>\n\n";
        print "   ... checks TABLE for compatibility with matt.\n\n";
        print "   <TABLE>  : tab-separated table\n\n";
        print "   Output to STDOUT: table which lists row numbers of all rows which\n";
        print "   do not have the same number of elemets as the header.\n\n";
        exit(1);
}

my ($fh,$col_ns_aref,$col_ids_aref,$comments)=open_fct_file($ARGV[0],[],$MYCOMMAND);
my $num_cols=scalar(@{$col_ns_aref});

my $header="ROW_NUMBER\tHAS_NUM_FIELDS\tSHOULD_HAVE_NUM_FIELDS\tCOMMENT\n";
my $row=1+scalar(split("\n",$comments));
while(<$fh>){
	$row++;
	$_=clean_line($_);
	if(length($_)==0){
		unless($header eq "0"){print $header;$header=0;}
		print "$row\tNA\tNA\tEMPTY_LINE\n";
		next;
	}
	if(substr($_,0,1) eq "#"){next;}
	my @fs=split_line($_);
	if(@fs!=$num_cols){
		unless($header eq "0"){print $header;$header=0;}
		print "$row\t".scalar(@fs)."\t$num_cols\tNA\n";
	}
}
close($fh);

unless($header eq "0"){
	print "Table looks ok. All lines have the same number (".$num_cols.") of fields as the header or are comment lines.\n";
	exit(0);  # success
}else{
	exit(1);  # error
}