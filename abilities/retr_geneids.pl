#!/usr/bin/env perl
use strict;
use warnings;
use File::Copy;
use File::Temp qw(tempfile);
use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.

my $MYCOMMAND="retr_geneids";


# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "extras\t$MYCOMMAND\t: \tretrieve gene IDs from GTF";
        exit(1);
}

#######
## SUBS
#######


# returns GENEID when we find extactly one gene in starts and ends which overlaps with given interval s--e
# returns NA in any other case (no overlapping gene or several different overlapping genes)
sub get_gid{
        my $s=$_[0];
        my $e=$_[1];
        my $starts=$_[2];
        my $ends=$_[3];
        my $gids=$_[4];

	# happens when a scaffold is in VAST-TOOLs table but not in GTF
	if(!defined($starts)){return("NA_")}  # NA_ is a code for knowing later that this event should not checked for a gene id a second nor third time 

        # binary search: find index of first intron start in $starts which is > $e (end)
        my $i=0; my $k=@{$starts}-1; my $j;
        
        if($k==-1){return("NA")}  # empty starts
        
        if($k>$i){ # more than one entries in starts
        	while(1){
        		if($i==$k-1){
                        	if($starts->[$i]>$e){$i=$i}else{$i=$k;}
                        	last;
                	}
                	$j=floor($i+($k-$i)/2);
                	if($starts->[$j]<=$e){$i=$j;}else{$k=$j;}
        	}
        }

	my $out=0;
	#if($s==2564511 && $e==2569150){$out=1;}

        my $gid="";
        my $NmaxOverlappingGenes=50;
        my $c=0;
        for(;$i>=0;$i--){$c++;
                if($out){print "$i\n";}
                if($e>=$starts->[$i] && $s<=$ends->[$i]){  # overlap  
                        if($out){print $gids->[$i]."\n";}
                        if($gid eq ""){
                                $gid=$gids->[$i]
                        }else{
                                if($gids->[$i] ne $gid){
                                        return("NA"); # overlaps with two different genes
                                }
                        }
                }

                if($c>$NmaxOverlappingGenes){last;}
                #if($i>0 && $s>$ends->[$i] && $starts->[$i] > $ends->[$i-1]){last}  
                # keep in mind: intervals (genes) are sorted wrt to start coordinate
                # 1--3  4--10  7--8  12--14   because 4--10 and 7--8 overlap we need to go as far backwards until we find a gene with does not overlap with its predecessor and which ends before start $s 
        }

        if($gid eq ""){$gid="NA";}

return($gid);
}



#######
## Main
#######
if(!defined($ARGV[0]) || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/ || @ARGV<6){
	print "\nmatt $MYCOMMAND <TABLE> <START> <END> <SCAFFOLD> <STRAND> <GTF> [-f <FIELD>] [-n <COLNAME1>] [-h <COLNAME2>] [-m]\n\n"; # [-h <COLNAME2>]\n\n";
	print "   ... extracts gene IDs from GTF for genomic events in TABLE.\n\n";
	print "   <TABLE>     : tab-separated table decribing genomic events (exons, introns, etc).\n";
	print "   <START>     : column of TABLE with contains start coordinates of events\n";
	print "   <END>       : column with end coordinates\n";
	print "   <SCAFFOLD>  : column with scaffold / chromosome\n";
	print "   <STRAND>    : column with strand information\n";
	print "   <GTF>       : a GTF or GFF file with gene annotation\n";
	print "-f <FIELD>     : field in GTF containing gene IDs. Default is gene_id .\n";
	print "-n <COLNAME1>  : column name of output column. Default is GENEID\n";
	print "-h <COLNAME2>  : If TABLE has a column with column name COLNAME2 which contains the same value for all events from the same gene (group of events)\n";
	print "                 (e.g. trivial gene names, some other gene IDs), then values in this column can be used\n";
	print "                 to speed-up and complete the extraction of gene IDs from the GTF. In this case, please specify: -h COLNAME2\n";
	print "                 Details: If some events with the same entry in COLNAME2 all got exactly the same gene ID extracted but some are left without,\n";
	print "                 then these remaining events will get assigned the extraced gene ID of their peers of the same gene.\n";
	print "   -m          : Can be used in combination with -h <COLNAME2>. Affects results only if events of one group got assigned different gene ids.\n";
	print "                 In this case, the most frequent gene id per group is assigned to all events of this group (majority vote).\n";
	print "\n   Output : one column with header GENEID with extracted gene IDs. Will report a gene ID\n";
	print "            for events only if they overlap with exactly one gene on the same strand in GTF. If the event is in a region overlap\n";
	print "            several genes, this functions checks for exact match of the event coordinates (+/-1) with any exon starts or ends of\n";
	print "            all genes and, if it matches to exons of exactly one gene, it outputs it's gene ID. If given argument -h, it tries to refine\n";
	print "            extracted gene ids as described. All events for which no gene id could be extracted get a NA.\n\n"; 
	exit(1);
}

my ($f,$stc,$ec,$sfc,$src,$gtf)=@ARGV;
my ($field,$cn,$cn2,$force)=("gene_id","GENEID","",0);
for(my $i=6;$i<@ARGV;){
	if($ARGV[$i] eq "-f"){$field=$ARGV[$i+1];$i+=2;next}
	if($ARGV[$i] eq "-n"){$cn=$ARGV[$i+1];$i+=2;next}
	if($ARGV[$i] eq "-h"){$cn2=$ARGV[$i+1];$i+=2;next}
	if($ARGV[$i] eq "-m"){$force=1;$i+=1;next}
	die "Argument $ARGV[$i] is unknown.",
}

# read out all genes from GTF
my %starts;
my %ends;
my %chr;
my %str;
open(my $fh,$gtf) or die "$!";
my $gid;
while(<$fh>){chomp; my $line=$_;  my @fs=split("\t"); if(@fs<9){next}; if(substr($line,0,1) eq "#"){next;}
        if($line =~ /$field\s*"?(.+?)"?\s*[;,\n]/){ 
        	$gid=$1;
        	if(!defined($starts{$gid})){
        		$starts{$gid}=$fs[3];
        		$ends{$gid}=$fs[4];
        		$chr{$gid}=$fs[0];
        		$str{$gid}=$fs[6];
        	}else{ # find maximal gene extend
        		if($fs[3]<$starts{$gid}){$starts{$gid}=$fs[3]}
        		if($fs[4]>$ends{$gid}){$ends{$gid}=$fs[4]}
        	}
        }
}
close($fh);

# create data structure for efficient search
my %starts2;
my %ends2;
my %gids2;
foreach my $gid (keys %starts){
	my ($chr,$str,$s,$e)=($chr{$gid},$str{$gid},$starts{$gid},$ends{$gid});
	if(!defined($starts2{$chr})){
		$starts2{$chr}={};
		$ends2{$chr}={};
		$gids2{$chr}={};
	}
	if(!defined($starts2{$chr}->{$str})){
		$starts2{$chr}->{$str}=[];
		$ends2{$chr}->{$str}=[];
		$gids2{$chr}->{$str}=[];
	}
	push(@{$starts2{$chr}->{$str}},$s);
	push(@{$ends2{$chr}->{$str}},$e);
	push(@{$gids2{$chr}->{$str}},$gid);
}
%starts=();
%ends=();
%chr=();
%str=();

# sort arrays according to start coordinate
foreach my $k1 (keys %starts2){foreach my $k2 (keys %{$starts2{$k1}}){
	#print "$k1 $k2\n";
	my @aa=@{$starts2{$k1}->{$k2}};
	my @bb=@{$ends2{$k1}->{$k2}};
	my @cc=@{$gids2{$k1}->{$k2}};
	
	my @idx = sort { $aa[$a] <=> $aa[$b] } 0 .. $#aa;

	@aa=@aa[@idx];
	@bb=@bb[@idx];
	@cc=@cc[@idx];
	
	$starts2{$k1}->{$k2}=\@aa;
	$ends2{$k1}->{$k2}=\@bb;
	$gids2{$k1}->{$k2}=\@cc;	
}}


my ($col_ns_aref,$col_ids_aref,$comments);
if($cn2 eq ""){
	($fh,$col_ns_aref,$col_ids_aref,$comments)=open_fct_file($f,[$stc,$ec,$sfc,$src],$MYCOMMAND);
}else{
	($fh,$col_ns_aref,$col_ids_aref,$comments)=open_fct_file($f,[$stc,$ec,$sfc,$src,$cn2],$MYCOMMAND);
}


#my $k1="KN150702.1";
#$k1="chr7";
#foreach my $k2 (keys %{$starts2{$k1}}){
#	unless($k2 eq "+"){next;}
#	print "$k1 $k2\n";
#	my @aa=@{$starts2{$k1}->{$k2}};
#	my @bb=@{$ends2{$k1}->{$k2}};
#	my @cc=@{$gids2{$k1}->{$k2}};
#	print "starts: "; print join(" ",@aa[(336,337,338,339,340,341,342)])."\n";
#	print "ends  : "; print join(" ",@bb[(336,337,338,339,340,341,342)])."\n";
#	print "gids  : "; print join(" ",@cc[(336,337,338,339,340,341,342)])."\n";
#}


my %done;
my @evts=();
my @gids=();
my @grps=();
my %check_second_time;
my $do_second_check=0;
while(<$fh>){
	$_=clean_line($_);
	if(length($_)==0 || substr($_,0,1) eq "#"){push(@gids,"#");push(@evts,"#");push(@grps,"#");next;}
	my @fs=split_line($_);
	
	my ($s,$e,$chr,$str,$grp)=@fs[@{$col_ids_aref}];
	my $evt="$s\t$e\t$chr\t$str";
	
	my $gid=$done{$evt};
	unless(defined($gid)){
		$gid=get_gid($s,$e,$starts2{$chr}->{$str},$ends2{$chr}->{$str},$gids2{$chr}->{$str});
		# returned gid can be a gene id, NA (not found), or NA_ which is a code for the case when a event has a scaffold which is not present in GTF
	}
	$done{$evt}=$gid;
	
	push(@evts,$evt);
	push(@gids,$gid);
	if($cn2 ne ""){push(@grps,$grp)}
	
	if($gid eq "NA"){
		$check_second_time{"$chr\t$str\t$s"}="";
		$check_second_time{"$chr\t$str\t$e"}="";
		$do_second_check=1;
	}
}
close($fh);

# do we need to do second check with exact coordinate matches?
my $do_third_check=0;
if($do_second_check){
	my ($s,$e,$chr,$str,$gid);
	open(my $fh,$gtf) or die "$!";
	while(<$fh>){chomp; my $line=$_;  my @fs=split("\t"); if(@fs<9){next}; if(substr($line,0,1) eq "#"){next;}
		if($line =~ /$field\s*"?(.+?)"?\s*[;,\n]/){ 
        		$gid=$1;
        		$s=$fs[3];
        		$e=$fs[4];
        		$str=$fs[6];
        		$chr=$fs[0];
        	
        		my $k;
        		foreach my $coord ($s,$e){foreach my $delta (-1,0,1){
        			$k="$chr\t$str\t".($coord+$delta);
        			if(defined($check_second_time{$k})){$check_second_time{$k}=$check_second_time{$k}.",$gid"}
        		}} 
        	}
	}
        close($fh);
        
        for(my $i=0;$i<@gids;$i++){
        	if($gids[$i] eq "NA"){   # gids which are NA_ will here not be checked a second time because their scaffold is not present in GTF
        		($s,$e,$chr,$str)=split("\t",$evts[$i]);
        		$gid="NA";
        		foreach my $gid_tmp (split(",",$check_second_time{"$chr\t$str\t$s"}),split(",",$check_second_time{"$chr\t$str\t$e"})){
        			if($gid_tmp ne ""){
        				if($gid eq "NA"){$gid=$gid_tmp;next;}
        				if($gid ne "NA" && $gid eq $gid_tmp){next;}else{$gid="NA";last;}
        			}
        		}
        		$gids[$i]=$gid;  
        		if($gid eq "NA"){$do_third_check=1;}      		 		 
        	}
        }
}


if($do_third_check && $cn2 ne ""){
	my %grps;
	for(my $i=0;$i<@gids;$i++){
		if($gids[$i] eq "#"){next;}
		if(!defined($grps{$grps[$i]})){$grps{$grps[$i]}=[];}
		push(@{$grps{$grps[$i]}},$i);
	}
	
	foreach my $grp (keys %grps){
		if($grp eq "" || $grp eq "NA"){next;}
		my %gids=();
		foreach my $i (@{$grps{$grp}}){
			if($gids[$i] ne "" && $gids[$i] ne "NA" && $gids[$i] ne "#" && $gids[$i] ne "NA_"){
				$gids{$gids[$i]}++;
			}
		}

		$gid="";
		my $maxC=-1;
		my @gids_tmp=keys %gids;
		if(@gids_tmp==1){$gid=$gids_tmp[0];}
		if(@gids_tmp>1 && $force){  # find most frequent gid in group of events
			foreach my $gid_tmp (@gids_tmp){
				if($gids{$gid_tmp}>$maxC){$gid=$gid_tmp;$maxC=$gids{$gid_tmp}}
			}
		}
		
		if($gid ne ""){ # we found only one gene id in this group -> set all missing gene ids in this group to gid
			foreach my $i (@{$grps{$grp}}){
				if($force==0 && $gids[$i] eq "" || $gids[$i] eq "NA"){$gids[$i]=$gid;}
				if($force){$gids[$i]=$gid;}
			}
		}
	}
}


print "$cn\n";
foreach $gid (@gids){if($gid eq "NA_"){print "NA\n"}else{print "$gid\n";}}

exit(0); # success
__END__