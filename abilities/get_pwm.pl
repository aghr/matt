#!/usr/bin/env perl
use strict;
use warnings;

use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.


my $MYCOMMAND="get_pwm";

# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "import_table\t$MYCOMMAND\t:  \tget PWM in Matt format";
        exit(1);
}


if(!defined($ARGV[0]) || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/ || @ARGV<2){
	print "\nmatt $MYCOMMAND <TABLE> [<C_SEQ> [<C_GROUP>] <MIN-PROB> [<ALPHABET>]\n\n";
	print "   ... reads in PWM from text file and transforms it into matt's PWM-format or learns PWM from given sequences\n\n";
	print "   <TABLE>    : tab-separated table describing PWM or containing sequences\n"; 
	print "                PWM can be given by probs, log-probs, raw counts, and rows or columns may correspond to letters\n";
	print "   <C_SEQ>    : if TABLE contains sequences, column with header C_SEQ must containing aligned sequences.\n";
	print "                All sequences must be of same length. One cell may contain several sequences separated by , (comma)\n";
	print "                The length of the PWM will be the length of the sequences.\n";
	print "   <C_GROUP>  : if TABLE contains sequences, column with header C_GROUP contains group ids of sequences and\n";
	print "                a PWM is generated from the sequences of each group.\n";
	print "                For sub-selecting groups: C_GROUP[gid1,gid2,..]\n";
	print "   <MIN-PROB> : desired minimal probability to prevent zero-probs whose log is not defined (e.g. 0.001)\n";
	print "   <ALPHABET> : defines alphabet\n";
	print "                 e.g., A,C,G,T means first letter is A, next C, next G, last T\n";
	print "                 Use this argument when you want to re-define the alphabet of a given PWM or when sequences\n";
	print "                 do not contain all alphabet letters.\n\n";
	print "   Output to STDOUT: PWM in matt PWM-format printed to screen.\n";
	print "   Exception: when C_GROUP given, individual PWMs will be written to files GRPID_mattpwm.tab\n";
	print "\n";
	exit(1);
}


my ($fn,$min_prob,$letters,$c_seq,$c_grp);
$fn=$ARGV[0];
if(@ARGV==2){
	$min_prob=$ARGV[1];
}else{
	if(is_numeric($ARGV[@ARGV-1])){
		$min_prob=$ARGV[@ARGV-1];
	}else{
		$min_prob=$ARGV[@ARGV-2];
		$letters=$ARGV[@ARGV-1];
	}
}
unless(is_numeric($ARGV[1])){
	$c_seq=$ARGV[1];
}

my %selected_grps;
if(@ARGV>2 && $ARGV[2] ne $min_prob){
	$c_grp=$ARGV[2];
	if($c_grp =~ /(.+)\[(.+)\]/){
		$c_grp=$1;
		foreach my $gid (split(",",$2)){if($gid){$selected_grps{$gid}=1;}}
	}
}

my %predef_alph=();if($letters){foreach my $ltr (split(",",$letters)){$predef_alph{$ltr}=1}}
my %pwm_arefs;
my %alph_hrefs;
my $grp="hj1321wuiwy<1242fjwuiw6897b";  # standard random group-id if user has not defined C_GROUP

unless($c_seq){	# given file contains already PWM
	if($letters){
		($pwm_arefs{$grp},$alph_hrefs{$grp})=read_pwm($fn,$MYCOMMAND,$letters,$min_prob);
	}else{
		($pwm_arefs{$grp},$alph_hrefs{$grp})=read_pwm($fn,$MYCOMMAND,$min_prob);
	}
}else{          # given file contains sequences
	my @col_ns=($c_seq);if($c_grp){$col_ns[1]=$c_grp;}
	my ($fh,$col_ns_aref,$col_ids_aref,$comments) = open_fct_file($fn,\@col_ns,$MYCOMMAND);
	my $seq_cid=$col_ids_aref->[0];
	my $grp_cid=-1;if(@col_ns>1){$grp_cid=$col_ids_aref->[1];}

	while(<$fh>){
		$_=clean_line($_);
		if(length($_)==0 || substr($_,0,1) eq "#"){next;}
		my @fs=split_line($_);

		# get all sequences from cell; if no sequences are given -> go to next row in table
		my $seq_l=-1;
		my @seqs=();foreach my $seq (split(",",$fs[$seq_cid])){if(length($seq)>0){$seq_l=length($seq);push(@seqs,$seq)}};if(@seqs==0){next;}

		# get group if user has specified C_GROUP; and check group if user has sub-selected group ids; go to next row in table if group is not among sub-selection of group ids
		if($grp_cid!=-1){$grp=$fs[$grp_cid];if(%selected_grps && !defined($selected_grps{$grp})){next;}}
		
		# initiate new (empty) PWM if we come across $grp the first time; initiate zero counts for letters of alphabet if alphabet given
		unless(defined($pwm_arefs{$grp})){$pwm_arefs{$grp}=[];for(my $l=0; $l<$seq_l;$l++){$pwm_arefs{$grp}->[$l]={};if($letters){foreach my $ltr (keys %predef_alph){$pwm_arefs{$grp}->[$l]->{$ltr}=0;}}}}
		# initiate new (empty) alphabet hash for this group
		unless(defined($alph_hrefs{$grp})){$alph_hrefs{$grp}=();}
		
		# count occurrences of letters across the sequence
		my $aref=$pwm_arefs{$grp};
		foreach my $seq (@seqs){
			# length check; all sequences need to have same length
			if(length($seq) != scalar(@{$aref})){die "Sequences in column $c_seq are not all of the same length.";}
			my $i=-1;
			foreach my $l (split("",$seq)){
				if($letters){unless($predef_alph{$l}){next;} }
				$i++;
				$aref->[$i]->{$l}++;
				$aref->[$i]->{"sum_over_position_klfjeuihqb"}++;
				$alph_hrefs{$grp}->{$l}=1;
			}
		}
	}
	close($fh);
	
	# normalize all probability parameters
	foreach $grp (keys %pwm_arefs){
		my $aref=$pwm_arefs{$grp};
		foreach my $pos (0..( scalar(@$aref)-1 )){
			my $shifted_prob_mass=0;
			my @unshifted_ltrs=();
			my $sum_prob_unshifted_ltrs=0;
			my $sum_counts=$aref->[$pos]->{"sum_over_position_klfjeuihqb"};
			delete($aref->[$pos]->{"sum_over_position_klfjeuihqb"});
			# guaranetee that we have entire alphabet at all positions
			foreach my $ltr (keys %{$alph_hrefs{$grp}}){unless(defined($aref->[$pos]->{$ltr})){$aref->[$pos]->{$ltr}=0;}}	
			foreach my $ltr (keys %{$aref->[$pos]}){
				$aref->[$pos]->{$ltr}/=$sum_counts;
				if($min_prob && $aref->[$pos]->{$ltr}<$min_prob){$shifted_prob_mass+=$min_prob-$aref->[$pos]->{$ltr};$aref->[$pos]->{$ltr}=$min_prob	
				}else{push(@unshifted_ltrs,$ltr);$sum_prob_unshifted_ltrs+=$aref->[$pos]->{$ltr}}
			}
			# if we have shifted some prob mass due to min_prob parameter
			if($shifted_prob_mass>0){
				foreach my $ltr (@unshifted_ltrs){
					$aref->[$pos]->{$ltr}-=$aref->[$pos]->{$ltr}/$sum_prob_unshifted_ltrs * $shifted_prob_mass; 
				}
			}
			# at last, check if probs are properly normalized (due to re-shifting of prob mass and if min_prob was set too high, probs might get improperly normalized)
			$sum_counts=0;
			foreach my $ltr (keys %{$aref->[$pos]}){$sum_counts+=$aref->[$pos]->{$ltr};}
			if(abs(1-$sum_counts)>0.000001){die "Probabilities cannot be normalized properly. Try to set parameter min_prob (currently set to $min_prob) to lower value to make proper normalization possible.";}
		}
	}
}


# output of PWMs
foreach $grp (keys %pwm_arefs){
	if($grp eq "hj1321wuiwy<1242fjwuiw6897b"){ # we only have one PWM and user has not specified C_GROUP -> output to screen
		write_pwm($pwm_arefs{$grp});
	}else{
		write_pwm($pwm_arefs{$grp},"".$grp."_mattpwm.tab");
	}
}

exit(0); # success
__END__