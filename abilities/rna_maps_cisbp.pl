#!/usr/bin/env perl
use strict;
use warnings;
use File::Copy;
use Cwd qw(abs_path getcwd);
use File::Temp qw(tempfile);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.

########
## Subs
########
sub normalize{
	my $v=$_[0];   my $c=$_[1];   if($c!=0){ return($v/$c) }else{ return "NA" }	
}

##################


my $MYCOMMAND="rna_maps_cisbp";

# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "super\t$MYCOMMAND\t: \twith all CISBP-RNA/k-mer motifs";
        exit(1);
}

if(defined($ARGV[0]) && $ARGV[0] eq "-shownames"){
        print `matt col_uniq $scr_abspath/../data_external/cisbp_rna/regexps/cisbp_iupac.tab NAME_RBP`;
        exit(0);
}


if(@ARGV<6 || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/){
	print "\nmatt $MYCOMMAND <TABLE> <C_C1_COORD> <C_EX_START> <C_EX_END> <C_C2_COORD> <C_SCAFFOLD> <C_STRAND> <C_GROUP> <WIN_L> <EXPART_L> <INTPART_L> ... \n";
	print "       ...  <FASTA> <MOTIF_CODE> [-d <OUT_DIR>] [-p <P_VALUE>] [-fdr <FDR>] [-t <EX_OR_INT_VIEW>] [-s <RSEED>] [-ymax <YMAX>] [-names <RBPS>] [-show]\n\n";
	print "   ... produces motif RNA maps for all k-mers (2-5) or all CISBP-RNA IUPAC motifs\n\n";
	print "   Note: this command calls rna_maps with predefined motifs. For details on the arguments,\n";
	print "         please read the help message of rna_maps\n\n";
	print "   <MOTIF_CODE>   : 2 for all ACTG 2-mers, 3 for all 3-mers, 4 for all 4-mers, 5 for all 5-mers,\n";
	print "                    and cisbprna_regexps for all binding motifs with available IUPAC motif in CISBP-RNA\n";
	print "   -names <RBPS>  : To sub-select specific RBPs, e.g., -names MSI,MBL,HOW. matt $MYCOMMAND -shownames lists all RBPs.\n";
	print "                    Can also be used to sub-select ACGT kmers with the corresponding MOTIF_CODE, e.g., -names AACG,ATTG\n";
	print "   -show          : If given, the motif description file with be copied into the current working directory and the\n";
	print "                    Matt rna_maps call with be printed to screen; But no analysis will be done.\n";
	print "\n";
	print "\n   Output: motif RNA maps as PDF graphics for all requested motifs in specified output directory.\n";
	print "             Furtheron, a PDF called 0_all_motif_rna_maps.pdf with an overview of all motif RNA maps.\n";
	print "             And, if -p or -fdr are specified, a single PDF called 0_significant_motif_rna_maps.pdf with\n";
	print "             motif RNA maps with significant enrichments.\n";
	print "\n   Hint1: Invoking arguments -p and -fdr may increase significantly the runtime. If you are checking many motifs,\n";
	print "            you may not apply the permutation test by not using -p nor -fdr and scroll through the PDF report. Motif RNA maps\n";
	print "            are sorted in the report according to the signal distance between any group and the reference group. You could then\n";
	print "            re-run this analysis with a few interesting motifs only applying the permutation test\n";
	print "\n   Hint2: To define the reference group, run this command with C_GROUP[up,down,ndiff] assuming the group ids to be up, down, ndiff.\n";
	print "            The last listed group is considered to be the reference group.\n";
	print "\n   Hint3: Matt $MYCOMMAND -shownames lists all names of RBPs available.\n";
	print "\n";
	exit(1);
}

my $pos=12;
if($ARGV[$pos] ne "2" && $ARGV[$pos] ne "3" && $ARGV[$pos] ne "4" && $ARGV[$pos] ne "5" && $ARGV[$pos] ne "cisbprna_regexps"){
	die "Given MOTIF_CODE (=$ARGV[$pos]) is not among allowed values (2, 3, 4, 5, cisbprna_regexps).";
}

my $rbps="";
my $onlyshow=0;
my $onlyshow_id=0;
my $i=0;
for(;$i<@ARGV;$i++){if($ARGV[$i] eq "-names"){$rbps=$ARGV[$i+1];} if($ARGV[$i] eq "-show"){$onlyshow=1;$onlyshow_id=$i}}

if($ARGV[$pos] eq "cisbprna_regexps"){
	copy("$scr_abspath/../data_external/cisbp_rna/regexps/cisbp_iupac.tab","./cisbp_iupac.tab") or die "$!";
	# get real names for selected RBPs
	my $names="";
	if($rbps ne ""){
		my $cmd="matt get_rows cisbp_iupac.tab NAME_RBP]".$rbps."[ | matt get_cols - NAME";
		my @names=`$cmd`;
		chomp(@names);
		shift(@names); # remove header
		if(@names==0){die "Did not find RBPs $rbps in CISBP-RNA. Check with matt $MYCOMMAND -shownames available RBPs.\n"}
		$names=join(",",@names);
		$ARGV[$#ARGV+1]="-names $names";  # here we over-write -names coming from column NAME_RBP with new -names that are more specific from column NAME
                                                  # the point here is that some RBPs occur several times in cisbp_rna.tab with the same entry in NAME_RBP but unique names in NAME that should be used in rna_maps
	}
		
	$ARGV[$pos]="cisbp_iupac.tab TYPE NAME REGEXP THRESH BGMODEL";
}else{
	my $fn="ACGT_".$ARGV[$pos]."mers.tab";
	copy("$scr_abspath/../data/kmer_regexps/$fn","./$fn") or die "$!";
	$ARGV[$pos]="$fn TYPE NAME REGEXP THRESH BGMODEL";
}


if($onlyshow){ # remove -show
	splice(@ARGV,$onlyshow_id,1);
}

my $cmd="matt rna_maps ".join(" ",@ARGV);

if($onlyshow){
	print "$cmd\n"
}else{
	print `$cmd`;
}

exit(0); # success
__END__
