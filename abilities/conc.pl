#!/usr/bin/env perl
use strict;
use warnings;

use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.


my $MYCOMMAND="conc";

# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "retrieval\t$MYCOMMAND\t:       \tconcatenate columns";
        exit(1);
}

if(!defined($ARGV[0]) || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/ || @ARGV<4){
	print "\nmatt $MYCOMMAND <TABLE1> <OUTPUT_CNAME> <SEP_SYMBOL> <C_NAME1> <C_NAME2> [<C_NAME3> ...]\n\n";
	print "   ... concatenates entries from specified columns, separated by a specied separation symbol.\n\n";
	print "   <TABLE>        : tab-separated table\n";
	print "   <OUTPUT_CNAME> : name of output column which will contain concatenated entries\n";
	print "   <SEP_SYMBOL>   : separation symbol, e.g., '_', empty string '', or white space ' ' (last two must be\n";
	print "                       enclosed in ' '\n";
	print "   <C_NAME*>      : names of columns which should be used for constructing row IDs\n";
	print "                    Their values get concatenated with \'_\' as separator character.\n";
	print "                    Values concatenated across these columns should uniquely identify rows.\n\n";
	print "   Output to STDOUT: a table with one column OUTPUT_CNAME containing the concatenated entries\n\n";
        exit(1);
}

my $fn=shift(@ARGV);
my $out_cn=shift(@ARGV);
my $sep_sym=shift(@ARGV);

my ($fh,$col_ns_aref,$col_ids_aref,$comments)=open_fct_file($fn,[@ARGV],$MYCOMMAND);


print "$out_cn\n";

while(<$fh>){	
	$_=clean_line($_);
	if(length($_)==0 || substr($_,0,1) eq "#"){print "#\n";next;}
	my @fs=split_line($_);
	print join($sep_sym,@fs[@{$col_ids_aref}])."\n";
}

close($fh);


exit(0); # success
__END__