#!/usr/bin/env perl
use strict;
use warnings;
use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.


my $MYCOMMAND="get_cols";

# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "retrieval\t$MYCOMMAND\t:   \tretrieve columns";
        exit(1);
}


if(!defined($ARGV[0]) || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/ || @ARGV<2){
        print "\nmatt $MYCOMMAND <TABLE> <C_NAME1> <C_NAME2> ...\n\n";
        print "   ... retrieve columns from table.\n\n";
        print "   <TABLE>   : tab-separated text table\n"; 
	print "   <C_NAME*> : names of columns to be retrieved\n\n";
        print "   Output to STDOUT: a table like TABLE but reduced to the specified columns\n\n";
        exit(1);
}


my ($fh,$col_ns_aref,$col_ids_aref,$comments) = open_fct_file(shift(@ARGV),[@ARGV],$MYCOMMAND);

my @col_ids=@{$col_ids_aref};

# header
print join("\t",@ARGV)."\n";

while(<$fh>){
	$_=clean_line($_);
	if(length($_)==0 || substr($_,0,1) eq "#"){next;}

	my @fs=split_line($_);
	print join("\t",@fs[@col_ids])."\n";
}
close($fh);

exit(0); # success
__END__