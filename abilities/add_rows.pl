#!/usr/bin/env perl
use strict;
use warnings;
use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.

my $MYCOMMAND="add_rows";

# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "manipulation\t$MYCOMMAND\t:   \tadd rows at bottom";
        exit(1);
}


if(!defined($ARGV[0]) || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/ || @ARGV!=2){
        print "\nmatt $MYCOMMAND <TABLE_TARGER> <TABLE_SOURCE>\n\n";
        print "   ... add rows from TABLE_SOURCE to bottom of TABLE_TARGET\n\n";
	print "   <TABLE_TARGET> : tab-separated table\n";
	print "   <TABLE_SOURCE> : tab-separated table\n";
	print "   IMPORTANT      : All column names in TABLE_TARGET need to be as well in TABLE_SOURCE.\n";
	print "                    Columns of TABLE_SOURCE not in TABLE_TARGET will be neglected.\n";
	print "   NOTE           : TABLE_SOURCE and TABLE_TARGET can be the same file.\n\n";
	print "   Output : no output will be writte to screen but file of TABLE_TARGET gets extended by rows.\n\n";
        exit(1);
}

my ($file_tar,$file_sour)=@ARGV[0..1];

my @col_names_tar;
open(my $fh_tar,"<".$file_tar) or die "Sorry, you requested add_rows but: I cannot open file $file_tar.";
while(<$fh_tar>){
	$_=clean_line($_);
	# comment line
	if(length($_)==0 || substr($_,0,1) eq "#"){next;}
	@col_names_tar=split_line($_);
	last;
}
close($fh_tar);

# for appending
open($fh_tar,">>".$file_tar) or die "Sorry, you requested add_rows but: I cannot open file $file_tar.";
open(my $fh_sour,"<".$file_sour) or die "Sorry, you requested add_rows but: I cannot open file $file_sour.";

my @fs;
my $header_checked=0;
my $collected_comments="";
my @permuted_col_ids=();

my $c=0;
while(<$fh_sour>){
	$_=clean_line($_);
	if(length($_)==0){next;}
	# comment line at beginning
	if($header_checked==0 && substr($_,0,1) eq "#"){$collected_comments.="$_\n";next;}
	# comment line in the middle of the source file
	if($header_checked==1 && substr($_,0,1) eq "#"){print $fh_tar "$_\n";next;}
	
	@fs=split_line($_);
	
	if($header_checked==0){
		my $permuted_col_ids_length_old=scalar(@permuted_col_ids);
		foreach my $col_n_tar (@col_names_tar){
			for(my $i=0;$i<@fs;$i++){if($col_n_tar eq $fs[$i]){push(@permuted_col_ids,$i);}}
			# we need to find exactly one source column for each target column
			if(@permuted_col_ids==$permuted_col_ids_length_old){
				die "Sorry, you requested add_rows but: I don't find a matching column with name $col_n_tar in file $file_sour.";
			}
			if(@permuted_col_ids>$permuted_col_ids_length_old+1){
				die "Sorry, you requested add_rows but: for column with name $col_n_tar I find several matching columns in file $file_sour.";
			}
			$permuted_col_ids_length_old++;
		}
		
		print $fh_tar $collected_comments;
		$header_checked=1;
		# we don't want to output the header a second time
		next;
	}

$c++;
for(my $i=0;$i<@permuted_col_ids;$i++){
	if(!defined($fs[$permuted_col_ids[$i]])){print "not defined i=$i, id=$permuted_col_ids[$i], length(fs)=".scalar(@fs)." c=$c.";}
}
	
	print $fh_tar join("\t",@fs[@permuted_col_ids])."\n";

}
close($fh_sour);
close($fh_tar);


exit(0); # success
__END__
