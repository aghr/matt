#!/usr/bin/env perl
use strict;
use warnings;

use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.


my $MYCOMMAND="get_sj";

# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "import_table\t$MYCOMMAND\t:   \tget SANJUAN CEs output";
        exit(1);
}


if(!defined($ARGV[0]) || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/ || @ARGV!=2){
	print "\nmatt $MYCOMMAND <SANJUAN_CEs_TABLE> <SPECIES>\n\n";
	print "   ... adds Ensembl gene ids to SANJUAN CEs output table\n\n"; 
	print "   <SANJUAN_CEs_TABLE> : SANJUAN CEs output table\n";
	print "                           This table should have column names: SKIP_JUNC, CHROMOSOME,\n";
	print "                           E_START,E_END, STRAND,GENE_SYMBOL\n";
	print "   <SPECIES>           : one of ath10, ce11, danRer10, dm6, hg19,\n";
	print "                           hg38, mm09, mm10, vicPac1\n";
	print "\n";
	print "   Output: same table as <SANJUAN_CEs_TABLE> with additional column GENEID_ENSEMBL\n";
	print "           and lines with more than one GENE_NAME get duplicated for each gene name\n";
	print "\n";
	exit(1);
}


my ($fn,$sp)=@ARGV;
my $fn_gn2gi=$scr_abspath."/../data/gname_2_gid_ensembl/${sp}_gname2gid.txt.gz";
my %gn2gi=();
open(my $fh_tmp,"gunzip -c $fn_gn2gi |") or die "Cannot open file $fn_gn2gi for reading.";
my $header=<$fh_tmp>;
while(<$fh_tmp>){chomp;my @fs=split("\t",$_,-1);
	$gn2gi{$fs[0]}=$fs[1];
}

unless(-e $fn_gn2gi && -f $fn_gn2gi){die "$fn_gn2gi Cannot find mapping of gene names to Ensembl gene ids for given species $sp. Please check if species was given correctly."}

my ($fh,$col_ns_aref,$col_ids_aref,$comments)=open_fct_file($fn,["GENE_SYMBOL"],$MYCOMMAND);
my $cid_gsym=$col_ids_aref->[0];

print join("\t",@$col_ns_aref)."\tGENEID_ENSEMBL\n";

my $c=0;
while(<$fh>){
	$_=clean_line($_);
	my @fs=split_line($_);
	my @fs2=split(",",$fs[$cid_gsym]);
	
	foreach my $gn (@fs2){
		$c++;
		my $gn_uc=uc($gn);
		my $gi=$gn2gi{$gn_uc};
		
		if(!defined($gi)){die "Cannot find a Ensembl gene id for gene name $gn in line $c of input table $fn. You may delete this line from the input table and re-run.";}
		
		$fs[$cid_gsym]=$gn;
		print join("\t",@fs)."\t$gi\n";
	}
}

close($fh);

exit(0); # success
__END__