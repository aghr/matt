#!/usr/bin/env perl
use strict;
use warnings;
use File::Temp qw(tempfile);

use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.

my $MAXENTMODEL_DIR=$scr_abspath."/../external_progs/maxent_splicesitescore_burge_humanmodels";
my $MYCOMMAND="get_sss";

# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "seq_analysis\t$MYCOMMAND\t:         \tget SS strengths";
        exit(1);
}


if(@ARGV<1 || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/ || @ARGV!=3){
        print "\nmatt $MYCOMMAND <TABLE> <C_3SS_SEQ> <C_5SS_SEQ> \n\n";
        print "   ... determine strength of 3' and 5' splice sites using maximum-entropy models for human\n";
	print "       cannonical splice sites\n\n";
	print "   <TABLE>     : tab-separated table containing sequence of splice sites\n";
	print "   <C_3SS_SEQ> : name of column with sequences of 3' splice sites\n";
	print "                   The 3'ss sequences need to be of length 23 = 20 up-stream [intron] and\n";
	print "                   3 down-stream [exon] positions, e.g., ttccaaacgaacttttgtAGgga\n";
	print "   <C_5SS_SEQ> : name of column with sequences of 5' splice sites\n";
	print "                   The 5'ss sequences need to be of length 9 = 3 up-stream [exon] and\n";
	print "                   6 down-stream [intron] positions, e.g., gagGTaagc\n\n";
	print "   Output: a table with two colums STRENGTH_3SS and STRENGTH_5SS\n\n";
	print "   IMPORTANT1 : The strength of sequences with letters other than ACGTacgt will be NA (not available)\n";
	print "   IMPORTANT2 : Sequences from - strand need to be already reverse complemented.\n";
	print "\n";
	print "Alternative calls:\n";
	print "  * matt $MYCOMMAND <TABLE> -3 <C_3SS_SEQ>  : to get strength only for 3' splice sites\n";
	print "  * matt $MYCOMMAND <TABLE> -5 <C_5SS_SEQ>  : to get strength only for 5' splice sites\n";
	print "\nUsed external tool:\n";
	print "  * Maximum entropy modeling of short sequence motifs with applications to RNA splicing signals\n";
	print "    Yeo et al., 2003, DOI: 10.1089/1066527041410418\n\n";
        exit(1);
}


my ($seqfeat_file,$cn_3ss,$cn_5ss)=@ARGV;
my @col_ns=($cn_3ss,$cn_5ss);
my $do3=1;
my $do5=1;
if($cn_3ss eq "-3"){$cn_3ss=$cn_5ss;$do5=0;@col_ns=($cn_3ss);}
if($cn_3ss eq "-5"){$do3=0;@col_ns=($cn_5ss);}
##
# main
##

my ($fh,$col_ns_aref,$col_ids_aref,$comments) = open_fct_file($seqfeat_file,[@col_ns],$MYCOMMAND);
my $l_cols_ids_aref=scalar(@$col_ids_aref);

my @fs;
my $seq;
my $row_counter=0;

# create temporary file; will be deleted automatically at the end of script or if error occurs
my ($fh_tmp_3,$tmpf_nm_3) = tempfile( DIR => ".", UNLINK => 1);
my ($fh_tmp_5,$tmpf_nm_5) = tempfile( DIR => ".", UNLINK => 1);
while(<$fh>){
	$_=clean_line($_);
	if(length($_)==0 || substr($_,0,1) eq "#"){next;}
	my @fs=split_line($_);

	$row_counter++;
	
	if($do3){
		$seq=$fs[ $col_ids_aref->[0] ];
		if(length($seq)!=23){die "Found a 3'-ss with length != 23.";}
		print $fh_tmp_3 ">$row_counter\n".$seq."\n";
	}
	
	if($do5){
		$seq=$fs[ $col_ids_aref->[$l_cols_ids_aref-1] ];
		if(length($seq)!=9){die "Found a 5'-ss with length != 9.";}
		print $fh_tmp_5 ">$row_counter\n".$seq."\n";
	}
}
close($fh);close($fh_tmp_3);close($fh_tmp_5);

my (@scores3,@scores5,@output);
# get output
if($do3){
	@output=`$MAXENTMODEL_DIR/score3.pl $tmpf_nm_3`;
	# >1 score=-10.18
	# GAAAATGTTATTTTGGGAATAAC
	# >2 score=-15.36
	# ATAACTAAATGTCGTACCTTCAT
	@scores3=();
	foreach my $line (@output){
        	$line=clean_line($line);
        	if(substr($line,0,1) eq ">"){
        		@fs=split(" ",$line);
        		$scores3[ substr($fs[0],1) ]=substr($fs[1],6);
        	}
	}
}

if($do5){
	@output=`$MAXENTMODEL_DIR/score5.pl $tmpf_nm_5`;
	@scores5=();
	foreach my $line (@output){
		$line=clean_line($line);
        	if(substr($line,0,1) eq ">"){
        		chomp($line);
        		@fs=split(" ",$line);
        		$scores5[ substr($fs[0],1) ]=$fs[2];;
        	}
	}
}

if($do3 && $do5){print "STRENGTH_3SS\tSTRENGTH_5SS\n";}
if($do3 && !$do5){print "STRENGTH_3SS\n";}
if($do5 && !$do3){print "STRENGTH_5SS\n";}
my ($sss3,$sss5);
for(my $i=1;$i<=$row_counter;$i++){
	($sss3,$sss5)=("NA","NA");
	if($do3 && defined($scores3[$i])){$sss3=$scores3[$i];}
	if($do5 && defined($scores5[$i])){$sss5=$scores5[$i];}
	
	if($do3 && $do5){print "$sss3\t$sss5\n";}
	if($do3 && !$do5){print "$sss3\n";}
	if($do5 && !$do3){print "$sss5\n";}
}


exit(0); # success
__END__