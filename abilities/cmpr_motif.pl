#!/usr/bin/env perl
use strict;
use warnings;
use File::Temp qw(tempfile);

use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.


my $MYCOMMAND="cmpr_motif";

# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "seq_analysis\t$MYCOMMAND\t:      \tplot motif / PWM comparison";
        exit(1);
}

if(@ARGV<3 || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/ || @ARGV>8){
        print "\nmatt $MYCOMMAND <TABLE1> [<C_SEQ1> [<C_GROUP1>]] [<TABLE2> <C_SEQ2> <C_GROUP2>] <F_NAME>\n\n";
        print "   ... plot a comparative motif plot for two motifs (given by PWM or sequences) into PDF file.\n\n";
	print "   <TABLE1>   : defining a PWM (in matt format; see get_pwm) or containing align sequences\n";
	print "               If you specify <TABLE> without C_SEQ it must contain a PWM, otherwise it must\n";
	print "               contain sequences align all oft the same length.\n";
	print "   <C_SEQ1>   : name of column in TABLE containing aligned sequences (lengths must be identical)\n";
	print "   		Exception: empty cells are allowd and will be neglected.\n";
	print "   		Can contain several sequences in one cell separated by ,\n";
	print "   <C_GROUP1> : name of column in TABLE containing group information (sub-sets of sequences)\n";
	print "               If you omit C_GROUP all sequences belong to one group.\n";
	print "               If you specify C_GROUP[grp], a motif will determined and compared for\n";
	print "               sequences from sub-set grp.\n";
	print "               If you specify C_GROUP[grp1,grp2], motifs for these two groups will be compared.\n";
	print "   <TABLE2>   : as <TABLE1>. Can be omitted if you specify <C_GROUP1>[grp1,grp2] with two groups.\n";
	print "   <C_SEQ2>   : as <C_SEQ>. Can be omitted if you omit <TABLE2>.\n";
	print "   <C_GROUP2> : as <C_GROUP2>. Can be omitted if all sequences from C_SEQ2 should be used or if you omit <TABLE2>.\n";
	print "   <F_NAME>   : the resulting PDF file will have this name. Must be given as last argument.\n\n";
	print "   Output: PDF file with name <F_NAME>\n";
	print "\n";
        exit(1);
}

my $fin_1="";
my $fin_2="";
my $cseq_1="";
my $cseq_2="";
my $cgrp_1="";
my $grp_1="";
my $cgrp_2="";
my $grp_2="";
my $fout="";

my @fs;
for(my $i=0;$i<@ARGV;$i++){
	if($i==0){$fin_1=$ARGV[$i];next;}
	if($i==@ARGV-1){$fout=$ARGV[$i];next;}
	if($ARGV[$i] =~ /\[(.*)\]/){
		if($fin_2){
			@fs=split(",",$1);
			$grp_2=$fs[0];
			if(@fs>1){die "Given arguments are not interpretable. If you give two files you can only define one group for the second file. Check syntax.";}
			($cgrp_2 = $ARGV[$i] ) =~  s/\[.*$//i;  # delete sub-set definitions from column name
			next;
		}
		@fs=split(",",$1);
		$grp_1=$fs[0];
		($cgrp_1 = $ARGV[$i] ) =~  s/\[.*$//i;  # delete sub-set definitions from column name
		if(@fs>1){$grp_2=$fs[1];$cseq_2=$cseq_1;$cgrp_2=$cgrp_1;$fin_2=$fin_1;}
		next;
	}else{
		if(-f $ARGV[$i]){
			$fin_2=$ARGV[$i];
			next;
		}
		
		if($fin_2){
			$cseq_2=$ARGV[$i];
		}else{	$cseq_1 = $ARGV[$i];
		}
	}
}
if($fin_2 eq "" && $grp_2 eq ""){die "Given arguments are not interpretable. Don't find information on group 2. Please check syntax.";}

if(-e $fout){die "Output PDF file to write into ($fout) already exists. Will not generate PWM and proceed.";exit(0);}

my $nm_tmp;
my @fn_pwm=();
my @nms=();
my ($cseq_12,$cgrp_12,$grp_12,$fin_12);

for(my $dummy=0;$dummy<2;$dummy++){
	if($dummy == 0){($cseq_12,$cgrp_12,$grp_12,$fin_12)=($cseq_1,$cgrp_1,$grp_1,$fin_1);}
	if($dummy == 1){($cseq_12,$cgrp_12,$grp_12,$fin_12)=($cseq_2,$cgrp_2,$grp_2,$fin_2);}
	
	#print "$fin_12, $cseq_12, $cgrp_12, $grp_12,\n";

	# derive short name for this sequence set from file name
	# we shorten down to the last positions after the last /
	($nm_tmp = $fin_12) =~ s/^.*\///i;
	# cut off all what comes after the first .
	($nm_tmp = $nm_tmp) =~ s/\..*$//i;
	# cut off all what comes after the first _
	($nm_tmp = $nm_tmp) =~ s/_.*$//i;
	
	if($grp_12 ne ""){$nm_tmp=$grp_12;}

	if($cseq_12 eq ""){  # TABLE is PWM
		# read in PWM with utility function that understands many different forms of PWMs automatically
		my ($pwm_aref,$alph_aref)=read_pwm($fin_12);
		# and write it in matt format temporarily
		my ($fh_tmp,$tmpfile_n) = tempfile( DIR => ".", UNLINK => 1);
		write_pwm($pwm_aref,$fh_tmp);
		close($fh_tmp);
		push(@fn_pwm,$tmpfile_n);
		push(@nms,$nm_tmp);
	}else{
		my ($fh_tmp,$tmpfile_n) = tempfile( DIR => ".", UNLINK => 1);
		close($fh_tmp);
		my $cmd="matt get_pwm $fin_12 $cseq_12";
		if($cgrp_12 eq "" && $grp_12 ne ""){die "Please check syntax of call.";}
		if($cgrp_12 ne ""){$cmd.=" $cgrp_12";}
		if($grp_12 ne ""){$cmd.="[$grp_12]";}
		$cmd.=" 0.001 > $tmpfile_n";
		`$cmd`;die "$!" if $?!=0;
		if($grp_12 ne ""){`cat ${grp_12}_mattpwm.tab > $tmpfile_n`;die "$!" if $?!=0;}
		push(@fn_pwm,$tmpfile_n);
		push(@nms,$nm_tmp);
	}
}

my $cmd = "$scr_abspath/../utilities/motif_compare_plot.r $fn_pwm[0] $nms[0] $fn_pwm[1] $nms[1] $fout";
my @ret=`$cmd`;die "$!" if $?!=0;
print join("\n",@ret);
for(my $i=0;$i<@fn_pwm;$i++){unlink($fn_pwm[$i]);}

exit(0); # success
__END__
