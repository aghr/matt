#!/usr/bin/env perl
use strict;
use warnings;

use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.

my $MYCOMMAND="get_seqs";

# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "seq_extraction\t$MYCOMMAND\t:  \tget sequences from FASTA";
        exit(1);
}


if(!defined($ARGV[0]) || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/ || @ARGV<6 || @ARGV>11){
	print "\n*Mode 1:\n";
        print " matt $MYCOMMAND <TABLE> <C_START> <C_END> <C_SCAFF> <C_STRAND> <FASTA_FILE> [-order]\n\n";
        print "   ... retrieve genomic sequences from START to END; use this mode e.g. for exon sequences\n\n";
	print "   <TABLE>      : tab-separated table containing data on genomic regions\n";
	print "   <C_START>    : name of column with START coordinates\n";
	print "   <C_END>      : name of column with END coordinates\n";
	print "   <C_SCAFF>    : name oif column with scaffold IDs; must be the same as used in FASTA_FILE\n";
	print "   <C_STRAND>   : name of column with stand (+ or -)\n";
	print "   <FASTA_FILE> : name of FASTA file which contains genomic sequences (e.g., chromosome sequences)\n\n";
	print "   -order       : if added as last argument, coordinates from columns C_START and C_END will be swapped\n";
	print "                  if C_START>C_END which guarantees that the START coordinate is always smaller or equal\n";
	print "                  than the END coordinate.\n";
	print "   Output: table with one column SEQ containing the retrieved sequences to STDOUT\n\n";
	print "\n*Mode 2:\n";
	print " matt $MYCOMMAND <TABLE> <C_START> <C_END> <UPSTRM_L> <DOWNSTRM_L> <C_SCAFF> <C_STRAND> <FASTA_FILE> [-order]\n\n";
	print "   ... retrieve sequences up- and down-stream of region START-END (START, END exclusive)\n";
	print "       Use this mode e.g. for intronic sequences up- and down-stream of exons\n\n";
	print "   <UPSTRM_L>   : lengt of up-stream sequences, i.e. on + strand: from START-UPSTR_L to START-1\n";
	print "   <DOWNSTRM_L> : lengt of down-stream sequences, i.e. on + strand: from END+1 to END+DOWNSTR-L\n\n";
	print "   Output: table with two columns SEQ_UP and SEQ_DOWN containing the retrieved sequences to STDOUT\n\n";
	print "\n*Mode 3:\n";
	print " matt $MYCOMMAND <TABLE> <C_START> <C_END> <UPSTRM_L1> <DOWNSTRM_L1> <UPSTRM_L2> <DOWNSTR_L2> <C_SCAF> ...\n";
	print "                 ... <C_STRAND> <FASTA_FILE> [-order]\n\n";
	print "   ... retrieve sequences around START position and arround END position\n";
	print "       Use this mode e.g. for extracting 3' and 5' splice sites of introns\n\n";
	print "   <UPSTRM_L1> and <DOWNSTR_L1> : define length of sequences to be extracted around START (+ strand) or END (- strand)\n";
	print "   <UPSTRM_L2> and <DOWNSTR_L2> : define length of sequences to be extracted around END (+ strand) or START (- strand)\n\n";
	print "   Example: if START/END define an intron, then with UPSTRM_L1=3 DOWNSTRM_L1=6 UPSTRM_L2=20 DOWNSTRM_L2=3\n";
	print "            you will extract 5ss xxxGTxxxx and 3ss xxxxxxxxxxxxxxxxxxAGxxx\n\n";
	print "   Output: table with two columns SEQ_UP and SEQ_DOWN containing the extracted sequences to STDOUT\n\n";
	print "   IMPORTANT1: sequences from - strand get reverse-complemented\n";
	print "   IMPORTANT2: the first sequence position has index 1 not 0\n";
	print "   IMPORTANT3: scaffold/chromosome ids in C_SCAFF must be the same as in FASTA_FILE\n\n";
        exit(1);
}


my ($seqfeat_file,$cn_start,$cn_end,$cn_scfdid,$cn_strand,$fasta_file,$n_upstream,$n_downstream,@col_ns,$seq_type,$n_upstream_2,$n_downstream_2);
my $extract_mode="START_END";
my $order=0;


if(@ARGV<=7){
	($seqfeat_file,$cn_start,$cn_end,$cn_scfdid,$cn_strand,$fasta_file,$order)=@ARGV;
	@col_ns=($cn_start,$cn_end,$cn_scfdid,$cn_strand);
}elsif(@ARGV<=9){
	($seqfeat_file,$cn_start,$cn_end,$n_upstream,$n_downstream,$cn_scfdid,$cn_strand,$fasta_file,$order)=@ARGV;
	@col_ns=($cn_start,$cn_end,$cn_scfdid,$cn_strand);
	$extract_mode="INTRONS";
}else{
	($seqfeat_file,$cn_start,$cn_end,$n_upstream,$n_downstream,$n_upstream_2,$n_downstream_2,$cn_scfdid,$cn_strand,$fasta_file,$order)=@ARGV;
	@col_ns=($cn_start,$cn_end,$cn_scfdid,$cn_strand);
	$extract_mode="SPLICESITES";
}

if(defined($order) && $order ne "-order"){die "Cannot interpret last given argument $order which is supposed to be -order but it looks different.";}

###########
## SUBs
###########
sub get_substr{# seq, start, length
	my $href=$_[0];
	my $scid=$_[1];
	my $start=$_[2];
	my $len=$_[3];
	
	if($len<=0){die "Problem with extracting a sequence from the given FASTA sequences: zero or negative length: start=".($start+1).", end=".($start+$len).", scaffold=$scid. Is start>=end?";}
	if($start<0){die "Probem with extracting a sequence from given FASTA: start coordinate is smaller then zero but must be >=0: : start=".($start+1).", end=".($start+$len).", scaffold=$scid."}
	
	my $ret="";
	{
	local $SIG{__WARN__}=sub{}; #Temporarily suppress warnings
	    $ret=substr($href->{$scid},$start,$len);
	}
	
	if(!defined($ret)){die "Problem with extracting a sequence from the given FASTA sequences: got no sequence: start=".($start+1).", end=".($start+$len).", scaffold=$scid. Do the FASTA sequences and the coordinates come from the same genome?";}
	if(length($ret)!=$len){die "Problem with extracting a sequence from the given FASTA sequences: length of obtained sequence does not fit to predicted length: start=".($start+1).", end=".($start+$len).", scaffold=$scid, length=$len. Maybe start or end coordinates are outside of sequence $scid?"}

return($ret);
}

sub get_seqs_from_fasta{
	open (my $fh, "<".$_[0]) || die "Sorry, you requested get_seq but: I cannot open file $_[0]\n";
	
	my ($seq,$chr,%chrseq);
	
	$/="\>";<$fh>;
	while (<$fh>){ # Get a hash with the sequence of each chromosome.
		/(.+?)\n(.+)/s;
    	$chr=$1;
    	$seq=$2;
    	# remove everything from header after first whitespace
    	$chr=~s/ .+//;
    	$seq=~s/[^A-Za-z]//g;
    	$seq=~tr/a-z/A-Z/; 
    	$chrseq{$chr}=$seq;
	}
	close($fh);
	$/="\n";

return(\%chrseq);
}
#####################

#######
# MAIN
#######

my $seqs_href=get_seqs_from_fasta($fasta_file);

my ($fh,$col_ns_aref,$col_ids_aref,$comments) = open_fct_file($seqfeat_file,[@col_ns],$MYCOMMAND);

my ($seq_out_1,$seq_out_2,$start,$end,$ref_pos_start,$ref_pos_end,$upstr_pos,$downstr_pos,$strand,$scaf_id);

# print header
if($extract_mode eq "START_END"){print "SEQ\n";}else{print "SEQ_UP\tSEQ_DOWN\n";}
	
while(<$fh>){
	$_=clean_line($_);
	if(length($_)==0 || substr($_,0,1) eq "#"){next;}
	my @fs=split_line($_);

	$strand=$fs[ $col_ids_aref->[@col_ns-1] ];
	$scaf_id=$fs[ $col_ids_aref->[@col_ns-2] ];

	#if(!defined($seqs_href->{$scaf_id})){die "$MYCOMMAND: I do not find the scaffold with id ".$fs[ $col_ids_aref->[ @col_ns-2 ] ]." in $fasta_file";}

	if($extract_mode eq "START_END"){
		$start=$fs[ $col_ids_aref->[0] ]-1;
		$end=$fs[ $col_ids_aref->[1] ]-1;
		if(defined($order) && $start>$end){($start,$end)=($end,$start);}
		$seq_out_1=get_substr($seqs_href,$scaf_id,$start,$end-$start+1);
		if($strand eq "-"){$seq_out_1=rvcmplt($seq_out_1);}
		
		print $seq_out_1."\n";
		
		next;
	}

	if($extract_mode eq "INTRONS"){
		$ref_pos_start=$fs[ $col_ids_aref->[0] ]-1;
		$ref_pos_end=$fs[ $col_ids_aref->[1] ]-1;
		if(defined($order) && $ref_pos_start>$ref_pos_end){($ref_pos_start,$ref_pos_end)=($ref_pos_end,$ref_pos_start);}
	
		if($strand eq "+"){
			$seq_out_1=get_substr($seqs_href,$scaf_id,$ref_pos_start-$n_upstream,$n_upstream);
			$seq_out_2=get_substr($seqs_href,$scaf_id,$ref_pos_end+1,$n_downstream);
		}else{
			$seq_out_1=rvcmplt( get_substr($seqs_href,$scaf_id,$ref_pos_end+1,$n_upstream) );
			$seq_out_2=rvcmplt( get_substr($seqs_href,$scaf_id,$ref_pos_start-$n_downstream,$n_downstream) );
		}
		
		# SEQ_UP \t SEQ_DOWN
		print "$seq_out_1\t$seq_out_2\n";
		
		next;
	}
	
	if($extract_mode eq "SPLICESITES"){
		$ref_pos_start=$fs[ $col_ids_aref->[0] ]-1;
		$ref_pos_end=$fs[ $col_ids_aref->[1] ]-1;
		if(defined($order) && $ref_pos_start>$ref_pos_end){($ref_pos_start,$ref_pos_end)=($ref_pos_end,$ref_pos_start);}

		if($strand eq "+"){
			#print "".($ref_pos_start-$n_upstream)." ".($n_upstream+$n_downstream)." ".($ref_pos_end-$n_upstream_2+1)." ".($n_upstream_2+$n_downstream_2)."\n"; 
			$seq_out_1=get_substr($seqs_href,$scaf_id,$ref_pos_start-$n_upstream,$n_upstream+$n_downstream);
			$seq_out_2=get_substr($seqs_href,$scaf_id,$ref_pos_end-$n_upstream_2+1,$n_upstream_2+$n_downstream_2);
		}else{
			$seq_out_1=rvcmplt( get_substr($seqs_href,$scaf_id,$ref_pos_end-$n_downstream+1,$n_upstream+$n_downstream) );
			$seq_out_2=rvcmplt( get_substr($seqs_href,$scaf_id,$ref_pos_start-$n_downstream_2,$n_upstream_2+$n_downstream_2) );
		}
		
		# SEQ_UP \t SEQ_DOWN
		print "$seq_out_1\t$seq_out_2\n";
		
		next;
	}
}
close($fh);


exit(0); # success
__END__