#!/usr/bin/env perl
use strict;
use warnings;

use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.


my $MYCOMMAND="get_seql";

# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "seq_analysis\t$MYCOMMAND\t:        \tdetermine sequence length";
        exit(1);
}


if(!defined($ARGV[0]) || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/ || @ARGV<2){
        print "\nmatt$MYCOMMAND <TABLE> <C_SEQ1> [<C_SEQ2> ...]\n\n";
        print "   ... determine sequence length\n\n";
	print "   <TABLE> : tab-separated table with sequences\n";
	print "   <C_SEQ> : names of columns with sequences\n\n";
	print "   Output: a table with columns CSEQ_LENGTH containing the sequence lengths for all\n";
	print "           columns given by C_SEQ to STDOUT.\n";
	print "\n";
        exit(1);
}


##
# main
##
my ($fh,$col_ns_aref,$col_ids_aref,$comments) = open_fct_file(shift(@ARGV),[@ARGV],$MYCOMMAND);

# output header
my @header=();
foreach my $tmp_col (@$col_ids_aref){push(@header,$col_ns_aref->[$tmp_col]."_LENGTH");}
print join("\t",@header)."\n";


my @fs;
while(<$fh>){
	$_=clean_line($_);
	if(length($_)==0 || substr($_,0,1) eq "#"){next;}
	my @fs=split_line($_);
	
	my @ret=();
	foreach my $col_id (@$col_ids_aref){
		push(@ret,length($fs[$col_id]));
	}
	print join("\t",@ret)."\n";
}
close($fh);


exit(0); # success
__END__