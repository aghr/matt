#!/usr/bin/env perl
use strict;
use warnings;
use File::Temp qw(tempfile);

use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.


my $MYCOMMAND="get_ifeatures";

# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "seq_analysis\t$MYCOMMAND\t:   \tget intron features";
        exit(1);
}

# I use two codes for
# 1: encoding if a features should be included in the summary report with graphics
#    if there is a blank at the last positions: plot this featured in summary report  else: should not be plotted
# 2: encoding if feature is BP feature and needs to be excluded from results table if user has defined species = NA
#    if last or second last symbol is . -> it is a BP feature  otherwise: not
my @feature_expl=(
	"INTRON_ID - unique intron id like <gene_id>:<start>,<end>,<scaffold>,<strand>" # 0
	,"GENE_BIOTYPE - if available (coding, noncoding, pseudo, rest)"
	,"INTRON_FOUND_IN_GTF - (yes,no)",
	,"INTRON_LENGTH "
	,"UPEXON_MEDIANLENGTH - if intron is in several transcripts, it might have different up-stream exons, and this is the median length of them "
	,"DOEXON_MEDIANLENGTH - same as UPEXON_MEDIANLENGTH but for down-stream exons "
	,"RATIO_UPEXON_INTRON_LENGTH - median up-stream exon length / intron length "
	,"RATIO_DOEXON_INTRON_LENGTH - median down-stream exon length / intron length " # 7
	,"INTRON_GCC - GC content of entire intron sequence "	 # 8
	,"UPEXON_GCC - median GC content of up-stream exons for all occurrences of intron "
	,"DOEXON_GCC - same as UPEXON_MEDIANGCC but for down-stream exons "	
	,"RATIO_UPEXON_INTRON_GCC - median GC content of up-stream exons / GC content of intron "
	,"RATIO_DOEXON_INTRON_GCC - same as RATIO_UPEXON_INTRON_GCC but for down-stream exons "
	,"SF1_HIGHESTSCORE_3SS - highest score of a SF1 positon weight matrix trained with human data in the last 150 nt 3 prime intron positons "	
	,"LEN_SEQ_INTRON_3SS_USED_FOR_BP_ANALYSIS - length of intron sequence used for extraction of branch point features"  # highest SF1 score and BP analysis	
	,"INTRON_5SS_20INT10EX_GCC - GC content of last 10 exon and first 20 intron positions at 5 prime end of intron "
	,"INTRON_3SS_20INT10EX_GCC - GC content of last 20 intron and first 10 exon positions at 3 prime end of intron "
	,"MAXENTSCR_HSAMODEL_5SS - maximum entropy score of 5ss using a model trained with human splice sites "
	,"MAXENTSCR_HSAMODEL_3SS - maximum entropy score of 3ss using a model trained with human splice sites "
	,"DIST_FROM_MAXBP_TO_3SS - Distance to 3ss of best precited BP. "
	,"SEQ_MAXBP - sequence of best predicted BP."
	,"SCORE_FOR_MAXBP_SEQ - BP sequence score of best predicted BP. "
	,"PYRIMIDINECONT_MAXBP - Pyrimidine content between the BP adenine and the 3 prime splice site for best BP. "
	,"POLYPYRITRAC_OFFSET_MAXBP - Polypyrimidine track offset relative to the BP adenine for best BP. "
	,"POLYPYRITRAC_LEN_MAXBP - Polypyrimidine track length for best BP. "
	,"POLYPYRITRAC_SCORE_MAXBP - Polypyrimidine track score for best BP. "
	,"BPSCORE_MAXBP - SVM classification score of best BP. "
	,"NUM_PREDICTED_BPS - number of all predicted BPs which have a positive BP score. "
	,"MEDIAN_DIST_FROM_BP_TO_3SS - like DIST_FROM_MAXBP_TO_3SS but median over top-3 predicted BPs. "
	,"SEQ_BPS - comma-separated list of sequences of top-3 predicted BPs."
	,"MEDIAN_SCORE_FOR_BPSEQ - like SCORE_FOR_MAXBP_SEQ but median over top-3 predicted BPs. "
	,"MEDIAN_PYRIMIDINECONT - like PYRIMIDINECONT_MAXBP but median over top-3 predicted BPs. "
	,"MEDIAN_POLYPYRITRAC_OFFSET - like POLYPYRITRAC_OFFSET_MAXBP but median over top-3 predicted BPs. "
	,"MEDIAN_POLYPYRITRAC_LEN - like POLYPYRITRAC_LEN_MAXBP but median over top-3 predicted BPs. "
	,"MEDIAN_POLYPYRITRAC_SCORE - like POLYPYRITRAC_SCORE_MAXBP but median over top-3 predicted BPs. "
	,"MEDIAN_BPSCORE - like BPSCORE_MAXBP but median over top-3 predicted BPs. " # 35 
	,"INTRON_FOUND_IN_THESE_TRS - transcript ids where intron was found in" # 36
	,"MEDIAN_TR_LENGTH - median length of transcripts the intron occurs in "
	,"MEDIAN_INTRON_NUMBER - number of introns of transcripts where intron occurs in "
	,"INTRON_MEDIANRANK - median rank of all occurrences of this intron (rank = ordinal number of intron in transcript)"
	,"INTRON_MEDIANRELATIVERANK - similar to INTRON_MEDIANRANK, relative rank = rank / number of all introns in transcript, is between 0 and 1 "
	,"INTRON_MEDIANRELATIVERANK_3BINS - median bin into which INTRON_MEDIANRELATIVERANK falls when binning 0-1 into 3 bins "
	,"INTRON_MEDIANRELATIVERANK_5BINS - similar to INTRON_MEDIANRELATIVERANK_3BINS with 5 bins "
	,"INTRON_MEDIANRELATIVERANK_10BINS - similar to INTRON_MEDIANRELATIVERANK_3BINS with 10 bins "
	,"NTRS_ALL_FOR_GENE - number of transcripts of gene where the intron occurs in "
	,"NTRS_WITH_INTRON - number of transcripts where intron occurs in"
	,"NTRS_WITH_INTRON_AS_FIRST_INTRON - number of transcripts where intron occurs in and where it is the first intron"
	,"PROP_FIRST_INTRON - NTRS_WITH_INTRON_AS_FIRST_INTRON / NTRS_WITH_INTRON "
	,"NTRS_WITH_INTRON_AS_LAST_INTRON - number of transcripts where intron occurs in and where it is the last intron"
	,"PROP_LAST_INTRON - NTRS_WITH_INTRON_AS_LAST_INTRON / NTRS_WITH_INTRON "
	,"NTRS_WITH_INTRON_AS_INTERNAL_INTRON - number of transcripts where intron occurs in and where it is a internal intron"
	,"PROP_INTERNAL_INTRON - NTRS_WITH_INTRON_AS_INTERNAL_INTRON / NTRS_WITH_INTRON "
	,"NTRS_WITH_INTRON_IN_UTR - number of transcripts where intron occurs in and where it overlaps with a UTR"
	,"PROP_INTRON_IN_UTR - NTRS_WITH_INTRON_IN_UTR / NTRS_WITH_INTRON "
	,"INTRON_COOCCURS_WITH_OTHER_INTRONS - Does this intron co-occurs with other introns of the same intron set? (yes, no)"
	,"INTRON_COOCCURS_WITH_THESE_INTRONS - INTRON_ID of co-occurring introns"
	,"MEDIAN_INTRON_INTRON_COOCCURRENCE_NUMBER_OVER_ALL_TRS - median number of co-occurring introns over all transcripts this intron occurs in" # 56
	,"SEQ_5SS - sequence of 5ss" # 57
	,"SEQ_3SS - sequence of 3ss"
	,"SEQ_INTRON - sequence of intron"
	,"SEQ_INTRON_3SS_USED_FOR_BP_ANALYSIS - sequence of introns (3 prime ends) as used for extraction of branch point features"
	,"SEQ_LONGESTUPEXON - sequence of longest up-stream exon"
	,"SEQ_LONGESTDOEXON - sequence of longest down-stream exon"
	,"SEQ_INTRON_5SS_6INT3EX - sequence of 3 exon and 6 intron positions from 5 prime intron end (used for MAXENTSCORE_BURGEHUMANMODEL_5SS)"
	,"SEQ_INTRON_3SS_20INT3EX - sequence of 20 intron and 3 exon positions from 3 prime intron end (used for MAXENTSCORE_BURGEHUMANMODEL_3SS)"
	,"SEQ_INTRON_5SS_20INT10EX - sequence of 10 exon and 20 intron positions from 5 prime intron end"
	,"SEQ_INTRON_3SS_20INT10EX - sequence of 20 intron and 10 exon positions from 3 prime intron end" # 66
);


if(@ARGV==1 && $ARGV[0] eq "explain"){
	print "\nOverview of intron features:\n\n";
	my $c=0;
	foreach my $f (@feature_expl){$c++;if($c<10){$c.=".  ";}elsif($c<100){$c.=". "}else{$c.="."}print "   $c $f\n";}
	print "\n";
exit(0);
}


if(@ARGV<9 || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/){
    print "\nmatt $MYCOMMAND <TABLE> <C_START> <C_END> <C_CHR> <C_STRAND> <C_GENEID> <GTF> <FASTA> <SPECIES> [<MAXLEN_BP_ANALYSIS>] [-notrbts] [-f FIELD] [<FILE:SCORENAME>]\n\n";
    print "   ... determine features for introns as described by TABLE\n\n";
	print "   <C_START>        : name of column with start coordinates of introns\n";
	print "   <C_END>          : name of column with end coordinates of introns\n";
	print "   <C_CHR>          : name of column with chromosome information for introns\n";
	print "   <C_STRAND>       : name of column with strand information\n";
	print "   <C_GENEID>       : name of column which contains IDs of genes the introns belong to. If TABLE does not contain a column with gene ids,\n";
	print "                        the corresponding genes the exons belong to can be determined automatically.\n";
	print "                        In this case, use Matt's command retr_geneids on TABLE and add extracted gene ids to TABLE first.\n";
	print "   <GTF>            : GTF file describing transcripts for all genes\n";
	print "                        The GTF should contain information on UTRs and transcript biotypes (like GTFs from Ensembl).\n";
	print "   <FASTA>          : FASTA file containing sequences for all chromosomes / scaffolds\n";
	print "                      If you set FASTA to NA (not available), then all but sequence-related features will be extracted.\n";
	print "   <SPECIES>        : choose one from Hsap, Ptro, Mmul, Mmus, Rnor, Cfam, Btau, NA (for all other species)\n";
	print "                        Branch point features are only available for above listed species.\n";
	print "                        If your species is different but close to one listed species, you could use this one to get BP features.\n";
	print "<MAXLEN_BP_ANALYSIS>: maximal length of the 3' intron ends considered for feature HUMANSF1_HIGHESTSCORE_3SS\n";
	print "                        and BP analysis features (can be omitted; default length = 150)\n";
	print "                        Important: the 20 first 5' intron positions will be never used for the BP analysis.\n";
	print "   -notrbts         : no transcript_biotypes; if given, all transcripts in <GTF> will be considered as they are.\n";
	print "                      If NOT given, <GTF> must contain a transcript_biotype field for all transcripts as for example\n";
	print "                      the GTF files from Ensembl. Transcripts then get classified according to their biotype into\n";
	print "                      coding, noncoding, pseudo, rest. A gene is coding if it contains at least one coding tr and all\n";
	print "                      other transcripts of a coding gene will be neglected. A gene is noncoding/pseudo if all its trs are\n";
	print "                      noncoding/pseudo. Transcripts classified as rest will be neglected.\n";
	print "                      column containing the information of the biotype of the gene the intron was found in.\n";
	print "                      Infos on biotypes: http://www.ensembl.org/Help/Faq?id=468 . You can add the field transcript_biotype to the <GTF>\n";
	print "                      by yourself and it might contain as well these values: noncoding, coding, pseudo, rest.\n";
	print "   -f FIELD         : gene ids in TABLE and GTF must coincide. Often GTF contain in the last column different fields like gene_id or gene_name.\n";
	print "                      When you specify -f FIELD, gene ids will be taken from field FIELD from GTF. Default is gene_id.\n";
	print "   <FILE:SCORENAME> : if given, file FILE should contain transcript ids. The output then\n";
	print "                      contains a column SCORENAME which contains for each intron i of gene g\n";
	print "                      the proportion of transcripts from FILE from gene g containing i\n";
	print "                      of all transcripts from gene g overlapping i\n\n";
   	print "   Output: a table with columns containing the extracted features to STDOUT\n\n";
	print "   To get an explanation of extracted features run: matt get_ifeatures explain\n\n";
	print "Used external tools:\n";
	print "   1. for maximum entropy scores\n";
      	print "      Maximum entropy modeling of short sequence motifs with applications to RNA splicing signals\n";
	print "      Yeo et al., 2003, DOI: 10.1089/1066527041410418\n";
	print "   2. for extraction of branch point features and classification of branch points\n";
	print "      Genome-wide association between branch point properties and alternative splicing\n";
	print "      Corvelo et al., 2010, DOI: 10.1371/journal.pcbi.1001016\n";
	print "      http://regulatorygenomics.upf.edu/Software/SVM_BP\n\n";
exit(1);
}

my ($seqfeat_file,$cn_start,$cn_end,$cn_chr,$cn_strand,$cn_gname,$gtf_file,$fasta_file,$species)=@ARGV;

unless($species=~/Hsap|Ptro|Mmul|Mmus|Rnor|Cfam|Btau|NA/){die "Wrong SPECIES argument; you have specified: $species.";}

my @col_ns=($cn_start,$cn_end,$cn_chr,$cn_strand,$cn_gname);

my $int_3ss_len=150; # default value
my $notrbts=0;  # no transcript biotypes
my @feature_names=();
my @feature_tid_hrefs=();
my @feature_counts_arefs=();
my $gid_search_term="gene_id";
if(@ARGV>9){
	for(my $i=9;$i<@ARGV;$i++){
		my @fs=split(":",$ARGV[$i]);   # e.g. Hsa19_trids_coding_trs.txt:CODING_SCORE
		
		if(scalar(@fs)==1){
			if(is_numeric($fs[0])){$int_3ss_len=$fs[0];next;}
			if($fs[0] eq "-notrbts"){$notrbts=1;next;}
			if($fs[0] eq "-f"){$gid_search_term=$ARGV[$i+1];$i++;next;}
			die "Cannot interpret argument $ARGV[$i].";
		}
		if(scalar(@fs)>2){die "Cannot interpret argument $ARGV[$i].";}
		
		push(@feature_names,$fs[1]);
		my $href={};
		open(my $fh,"<".$fs[0]) or die "Cannot open file $fs[0].";
		while(<$fh>){
			$_=clean_line($_);
			if(length($_)==0 || substr($_,0,1) eq "#"){next;}
			$href->{ $_ }=1;  # should only contain tr ids; one per line
		}
		close($fh);
		push(@feature_tid_hrefs,$href);
		push(@feature_counts_arefs,[]);
	}
}

##
# main
##

my ($fh,$col_ns_aref,$col_ids_aref,$comments) = open_fct_file($seqfeat_file,[@col_ns],$MYCOMMAND);
my $col_start=$col_ids_aref->[0];
my $col_end=$col_ids_aref->[1];
my $col_chr=$col_ids_aref->[2];
my $col_strand=$col_ids_aref->[3];
my $col_gname=$col_ids_aref->[4];

my @fs;
my $seq;

# input
my (@istarts,@iends,@ichrs,@istrands, @ignames,@iids);
my $iid;
my $N_introns=0;
# read introns
while(<$fh>){
	$_=clean_line($_);
	if(length($_)==0 || substr($_,0,1) eq "#"){next;}
	@fs=split_line($_);
	push(@istarts,$fs[$col_start]);
	push(@iends,$fs[$col_end]);
	push(@ichrs,$fs[$col_chr]);
	push(@istrands,$fs[$col_strand]);
	push(@ignames,$fs[$col_gname]);
	$iid=join(",",$fs[$col_start],$fs[$col_end],$fs[$col_chr],$fs[$col_strand],$fs[$col_gname]);
	push(@iids,$iid);
	$N_introns++;	
}
close($fh);

# check GTF is gene ids are really in $gid_search_term 
my %gids_tmp=();
open(my $gtf,$gtf_file) or die "$gtf_file: $!";
while(<$gtf>){
	$_=clean_line_leave_quotes($_);
	@fs=split_line($_);
	if(length($_)==0 || substr($_,0,1) eq "#" || @fs!=9){next;}
	my $gid="";
	if($fs[8]=~/$gid_search_term \"(.*?)\"/){$gid=$1;}if($gid eq ""){next;}
	$gids_tmp{$gid}=1;
}
close($gtf);

my $N_gid_hits=0;
my $emax=1000; if($emax>@ignames){$emax=@ignames;}
for(my $i=0; $i<$emax; $i++){
	if($gids_tmp{$ignames[$i]}){$N_gid_hits++;}
}

if($N_gid_hits<$emax/2){die "Please check argument -f FIELD. It looks like only less than 500 of 1000 checked gene ids from TABLE could be found in field $gid_search_term in GTF.";}

# get gtf content in hash: content for each combination of chr and strand
my (%exons,%utrs,%gid2trs);
open($gtf,$gtf_file) or die "$!";
my %trbt=();  # transcript biotype -> if user has not specified -notrbt each transcript must have a field transcript_biotype
              # we delete all noncoding, pseudo, rest transcripts from coding genes -> and store for the remaining trs the information that they occur in a coding gene  
	      # a noncoding gene must contain only noncoding trs -> and we store for these that they occur in a noncoding gene
	      # same for pseudo genes
	      # rest genes get neglected
my %gbt=();   # gene biotype
my $trbt;
my $gbt;
while(<$gtf>){
	$_=clean_line_leave_quotes($_);
	@fs=split_line($_);
	if(length($_)==0 || substr($_,0,1) eq "#" || @fs!=9){next;}
	my $gid="";
	if($fs[8]=~/$gid_search_term \"(.*?)\"/){$gid=$1;}if($gid eq ""){next;}
	my $tid="";
	if($fs[8]=~/transcript_id \"(.*?)\"/){$tid=$1;}if($tid eq ""){next;}
	unless($notrbts){
		if($fs[8]=~/transcript_biotype \"(.*?)\"/){ $trbt=$1;
		}elsif($fs[8]=~/gene_biotype \"(.*?)\"/){    $trbt=$1; 
		}else{ if(!defined($trbt{$tid})){die "No transcript_biotype nor gene_biotype for transcript $tid.";}}
		
		$trbt=get_type_from_ensembl_tr_biotype($trbt);
		if($trbt eq "undefined"){die "Unknown transcript_biotype ($trbt) for transcript $tid. Try running get_efatures with argument -notrbts .";}
		if($trbt eq "rest"){next;}
		$trbt{$tid}=$trbt;
		$gbt=$gbt{$gid};
		if(!defined($gbt)){$gbt{$gid}=$trbt;
		}else{
			if($trbt eq "coding"){$gbt{$gid}=$trbt; # we always set coding
			}elsif($trbt eq "noncoding"){
				if($gbt ne "coding"){$gbt{$gid}=$trbt;}
			}else{
				if($gbt ne "coding" && $gbt ne "noncoding"){$gbt{$gid}=$trbt;} # pseudo
			}
		}
	}

	if(!defined($gid2trs{$gid})){$gid2trs{$gid}={};}
	$gid2trs{$gid}->{$tid}=1;  							# remember that trs with tid comes from gene gid

	if($fs[2]=~ /exon/i){
		unless($exons{$tid}){$exons{$tid}="";} # -> aryref which contains refrences to arys containg start/ends of exons for each tr
		# start / end
		$exons{$tid}.=join("\t",$fs[3],$fs[4])."\n";
	}
	if($fs[2]=~ /utr/i){
		unless($utrs{$tid}){$utrs{$tid}="";}
		# start / end
		$utrs{$tid}.=join("\t",$fs[3],$fs[4])."\n";
	}
}
close($gtf);


# clean gtf: delete transcripts which are not coding from coding genes
#my $ndel=0;
#my $ntrs=0;
unless($notrbts){
	foreach my $gid (keys %gid2trs){
		$gbt=$gbt{$gid};
		if($gbt ne "coding"){next;}
		foreach my $tid (keys %{$gid2trs{$gid}}){
			$trbt=$trbt{$tid};
			if($gbt eq "coding" && $trbt ne "coding"){delete($gid2trs{$gid}->{$tid});}
		}
	}
}
my %already_worked_on_tr=();
my (@exon_starts,@exon_ends,@utr_starts,@utr_ends,@sorted_pos);
my ($gid,$istart,$iend,$istrand,$ichr);

my @gbt=(); # gene biotype for gene in which intron occurs
my @irank=();
my @irelrank=();
my $ex_counter=-1;
my @ex_starts=();  # -> for all up/do-stream exs
my @ex_ends=();
my @ex_chr=();
my @ex_strand=();
my @upex_ids=();   # at pos i which corresponds to intron i -> array ref with ids of up-stream exs in @ex_starts and @ex_ends
my @doex_ids=();
my @ntrs_in_utr=();
my @ntrs_is_firstintron=();
my @ntrs_is_lastintron=();
my @ntrs_is_internalintron=();
my @ntrs_withintron=(); # number of trs where intron was found in
my @ntrs_all=();
my @n_introns=();  # contains for each intron an array with number of introns of all trs where the retained intron appears in
my %exon_pos=();
my $ex_key;
my @feature_counts;
my @this_intron_occurs_in_trs=();
my %tid2iids=();
my @trlengths=();  # contains lengths of transcripts the intron occurs in
my @tr_ids=(); # contains transcript ids where intro was found in


for(my $j=0;$j<@feature_counts_arefs;$j++){$feature_counts[$j]=0;}

for(my $i=0; $i<@istarts;$i++){
	$gid="";$gid=$ignames[$i];  # $gid might be "" in rare cases where we don't have a gene id for a ASE
	unless($notrbts){$gbt[$i]=$gbt{$gid};}
	$istart=$istarts[$i];
	$iend=$iends[$i];
	$istrand=$istrands[$i];
	#print "gid=$gid start=$istart end=$iend strand=$istrand\n"; # XXX
	$ichr=$ichrs[$i];
	$iid=join(",",$istart,$iend,$ichr,$istrand,$gid);
	$ntrs_in_utr[$i]=0;
	$ntrs_is_firstintron[$i]=0;
	$ntrs_is_lastintron[$i]=0;
	$ntrs_withintron[$i]=0;
	$ntrs_is_internalintron[$i]=0;
	$ntrs_all[$i]=0;
	$n_introns[$i]=[];
	$this_intron_occurs_in_trs[$i]=[];
	for(my $j=0;$j<@feature_counts;$j++){$feature_counts[$j]=0;}
	$tr_ids[$i]="";
	
	# collect this information across all trs
	my @intron_ranks=();
	my @intron_relranks=();
	my @upex_starts=();
	my @upex_ends=();
	my @doex_starts=();
	my @doex_ends=();
	my @tr_lengths_tmp=();
	
	# go over each tr of that gene
	foreach my $tid (keys %{$gid2trs{$gid}}){
	#print "tid=$tid\n"; # XXX
		$ntrs_all[$i]++;
		if(!defined($already_worked_on_tr{$tid})){  # sort exons and utrs wrt. start
			@exon_starts=();@exon_ends=();
			foreach my $line (split("\n",$exons{$tid})){@fs=split("\t",$line);push(@exon_starts,$fs[0]);push(@exon_ends,$fs[1]);}
			@sorted_pos = sort { $exon_starts[$a] <=> $exon_starts[$b] } 0 .. $#exon_starts;
			@exon_starts=@exon_starts[@sorted_pos];@exon_ends=@exon_ends[@sorted_pos];
			my $tmp_str="";
			for(my $j=0;$j<@exon_starts;$j++){$tmp_str.="$exon_starts[$j]\t$exon_ends[$j]\n";}
			$exons{$tid}=$tmp_str;
			
			if($utrs{$tid}){
				@utr_starts=();@utr_ends=();
				foreach my $line (split("\n",$utrs{$tid})){@fs=split("\t",$line);push(@utr_starts,$fs[0]);push(@utr_ends,$fs[1]);}
				@sorted_pos = sort { $utr_starts[$a] <=> $utr_starts[$b] } 0 .. $#utr_starts;
				@utr_starts=@utr_starts[@sorted_pos];@utr_ends=@utr_ends[@sorted_pos];
				$tmp_str="";
				for(my $j=0;$j<@utr_starts;$j++){$tmp_str.="$utr_starts[$j]\t$utr_ends[$j]\n";}
				$utrs{$tid}=$tmp_str;
			}
		}

		@exon_starts=();@exon_ends=();@utr_starts=();@utr_ends=();
		foreach my $line (split("\n",$exons{$tid})){
			@fs=split("\t",$line);
			push(@exon_starts,$fs[0]);
			push(@exon_ends,$fs[1]);
		}
		
		# collect all up/do-stream exon pairs of overlapping introns
		# if we have more than 1 overlapping intron -> we select at the end the most up/do-stream exons and negelct all others
		my @upex_starts_tmp=();
		my @upex_ends_tmp=();
		my @doex_starts_tmp=();
		my @doex_ends_tmp=();
		my $isfirstintron=0;
		my $islastintron=0;
		my $isinutr=0;
		my $isinternalintron=0;
		
		#print $istrand." ".$exon_starts[0]."<".$istart." && ".$iend."<".$exon_ends[@exon_ends-1]."   ".scalar(@exon_starts)."=".scalar(@exon_ends);
		
		if($exon_starts[0]<$istart && $iend<$exon_ends[@exon_ends-1] && @exon_starts>1){  # this trs overlaps the intron
			my $upex_s_tmp=undef;
			my $upex_e_tmp=undef;
			my $doex_s_tmp=undef;
			my $doex_e_tmp=undef;
			#print "\t yes\n";
			my $found_overlapping_intron=0;
			my $tr_len=$exon_ends[@exon_ends-1]-$exon_starts[0];  # end of last exon - start of first exon (here we neglect UTRs)
			for(my $j=1; $j<@exon_starts;$j++){
				# we have a overlaping intron  -> we could have several overlapping introns
				if($exon_starts[$j]>$istart && $exon_ends[$j-1]<$iend){
					# exclude cases like this where the ri is super large
					# RI           #####--------------------RI--------------------######    -> several overlapping introns
					#     ####----###E1##---------###E2###--------##E3###--------###E4###
					# ranks are the avgs of all overlaping introns
					# but the up/do-stream exons should only be E1 & E4 NOT E2 nor E3 
					
					if($istrand eq "+"){
						push(@intron_ranks,$j);
						push(@intron_relranks,$j/(@exon_starts-1));
						if($j==1){$isfirstintron=1;}
						if($j==(@exon_starts-1)){$islastintron=1;}
						if($j>1 && $j<@exon_starts-1){$isinternalintron=1;}
						push(@upex_starts_tmp,$exon_starts[$j-1]);
						push(@upex_ends_tmp,$exon_ends[$j-1]);
						push(@doex_starts_tmp,$exon_starts[$j]);
						push(@doex_ends_tmp,$exon_ends[$j]);
					}else{
						push(@intron_ranks,@exon_starts-$j);
						push(@intron_relranks,(@exon_starts-$j)/(@exon_starts-1));
						if($j==@exon_starts-1){$isfirstintron=1;}
						if($j==1){$islastintron=1;}
						if($j>1 && $j<@exon_starts-1){$isinternalintron=1;}
						push(@upex_starts_tmp,$exon_starts[$j]);
						push(@upex_ends_tmp,$exon_ends[$j]);
						push(@doex_starts_tmp,$exon_starts[$j-1]);
						push(@doex_ends_tmp,$exon_ends[$j-1]);
					}
					
					$found_overlapping_intron=1;
					#print "\t intron found in GTF\n"; #XXX
				}
			}

			if($found_overlapping_intron){
				$ntrs_withintron[$i]++;
				# collect feature count
				for(my $j=0;$j<@feature_counts;$j++){if(defined($feature_tid_hrefs[$j]->{$tid})){$feature_counts[$j]++;}}
				push(@{$n_introns[$i]},scalar(@exon_ends)-1);  # save number of introns of this trs
				push(@{$this_intron_occurs_in_trs[$i]},$tid);
				if(!defined($tid2iids{$tid})){$tid2iids{$tid}=$iid;}else{$tid2iids{$tid}.=":".$iid;}
				# check if intron overlaps with utr (only check if we actually have a annotated utr and if the intron does not yet overlap with a utr in another tr)
				if(defined($utrs{$tid})){foreach my $line (split("\n",$utrs{$tid})){@fs=split("\t",$line);if($fs[0]<$iend && $fs[1]>$istart){$isinutr=1;}}}
				push(@tr_lengths_tmp,$tr_len);
				if($tr_ids[$i] eq ""){$tr_ids[$i]="$tid"}else{$tr_ids[$i].=":$tid";}
			}

			# we do the summation here in order not to count several times if by chance in a tr we have several overlapping introns
			$ntrs_is_firstintron[$i]+=$isfirstintron;
			$ntrs_is_lastintron[$i]+=$islastintron;
			$ntrs_is_internalintron[$i]+=$isinternalintron;
			$ntrs_in_utr[$i]+=$isinutr;
		}else{
			#print "\t intron not found in GTF\n"; #XXX
		}

		# we found at least one overlapping intron -> take as up/do-stream exon the most up/do-stream exon
		if(@upex_starts_tmp>0){
			if($istrand eq "+"){
				push(@upex_starts,$upex_starts_tmp[0]);
				push(@upex_ends,$upex_ends_tmp[0]);
				push(@doex_starts,$doex_starts_tmp[@doex_starts_tmp-1]);
				push(@doex_ends,$doex_ends_tmp[@doex_ends_tmp-1]);
			}else{
				push(@upex_starts,$upex_starts_tmp[@upex_starts_tmp-1]);
				push(@upex_ends,$upex_ends_tmp[@upex_ends_tmp-1]);
				push(@doex_starts,$doex_starts_tmp[0]);
				push(@doex_ends,$doex_ends_tmp[0]);
			}
		}

		$already_worked_on_tr{$tid}=1;
	}# for over trs

#print "doex_starts - doex_ends:\n";
#for(my $ii=0;$ii<@doex_starts;$ii++){
#	my $vv=$doex_starts[$ii];
#	my $vw=$doex_ends[$ii];
#	if(defined($vv)){print "$vv - ";}else{print "undefined - ";}
#	if(defined($vw)){print "$vw\n";}else{print "undefined\n";}
#}

#print "upex_starts - upex_ends:\n";
#for(my $ii=0;$ii<@doex_starts;$ii++){
#	my $vv=$upex_starts[$ii];
#	my $vw=$upex_ends[$ii];
#	if(defined($vv)){print "$vv - ";}else{print "undefined - ";}
#	if(defined($vw)){print "$vw\n";}else{print "undefined\n";}
#}


	push(@upex_ids,[]);
	push(@doex_ids,[]);

	if(@upex_starts>0){
		for(my $j=0;$j<@upex_starts;$j++){
			$ex_key=$upex_starts[$j].",".$upex_ends[$j].",".$ichr.",".$istrand;
			
			unless(defined($exon_pos{$ex_key})){ # first time we come across this exons
				$exon_pos{$ex_key}=++$ex_counter;
				push(@ex_starts,$upex_starts[$j]);
				push(@ex_ends,$upex_ends[$j]);
				push(@ex_chr,$ichr);
				push(@ex_strand,$istrand);
			}
			push(@{$upex_ids[$i]}, $exon_pos{$ex_key} );
		}
	}
	
	if(@doex_starts>0){
		for(my $j=0;$j<@doex_starts;$j++){
			$ex_key=$doex_starts[$j].",".$doex_ends[$j].",".$ichr.",".$istrand;

			unless(defined($exon_pos{$ex_key})){ # first time we come across this exons
				$exon_pos{$ex_key}=++$ex_counter;
				push(@ex_starts,$doex_starts[$j]);
				push(@ex_ends,$doex_ends[$j]);
				push(@ex_chr,$ichr);
				push(@ex_strand,$istrand);
			}
			push(@{$doex_ids[$i]}, $exon_pos{$ex_key} );
		}
	}

	if(@intron_ranks>0){push(@irank,get_median_from_aref(\@intron_ranks));}else{push(@irank,"NA");}
	if(@intron_ranks>0){push(@irelrank,get_median_from_aref(\@intron_relranks));}else{push(@irelrank,"NA");}
	if(@tr_lengths_tmp>0){push(@trlengths,get_median_from_aref(\@tr_lengths_tmp));}else{push(@trlengths,"NA");}

	# store counts for user specified features if we have any
	for(my $j=0;$j<@feature_counts;$j++){push(@{$feature_counts_arefs[$j]},$feature_counts[$j]);}		
}

# clear memory
%exons=();%utrs=();%gid2trs=();%exon_pos=();%already_worked_on_tr=();%gids_tmp=();

#print scalar(@ex_starts)."\n";
#print scalar(@ex_ends)."\n";

#foreach my $v (@ex_starts){print "$v\n";}

#print scalar(@istarts)."\n";
#print scalar(@upex_ids)."\n";
#foreach my $v (@{$upex_ids[0]}){print "$v\n";}

#print "\nUPexs\n";
#for(my $i=0;$i<@istarts;$i++){
#	print "\n$i\nEx starts:";
#	foreach my $v (@ex_starts[ @{$upex_ids[$i]} ]){print "\t$v";}
#	print "\nEx ends:";
#	foreach my $v (@ex_ends[ @{$upex_ids[$i]} ]){print "\t$v";}
#	print "\n";
#}

#print "\nDOexs\n";
#for(my $i=0;$i<@istarts;$i++){
#	print "\n$i\nEx starts:";
#	foreach my $v (@ex_starts[ @{$doex_ids[$i]} ]){print "\t$v";}
#	print "\nEx ends:";
#	foreach my $v (@ex_ends[ @{$doex_ids[$i]} ]){print "\t$v";}
#	print "\n";
#}


# get intron sequences
my @iseqs=();
my @igcc=();
my $cmd;
if($fasta_file ne "NA"){
	# create temporary file; will be deleted automatically at the end of script or if error occurs
	my ($fh_tmp,$tmpfile_n) = tempfile( DIR => ".", UNLINK => 1);
	print $fh_tmp "INTRON_START\tINTRON_END\tCHR\tSTRAND\n";
	for(my $i=0;$i<@istarts;$i++){
		print $fh_tmp "$istarts[$i]\t$iends[$i]\t$ichrs[$i]\t$istrands[$i]\n"; 
	}
	close($fh_tmp);
	$cmd="matt get_seqs $tmpfile_n INTRON_START INTRON_END CHR STRAND $fasta_file";
	open(my $cmd_fh,"$cmd |");<$cmd_fh>;
	while(<$cmd_fh>){
		chomp($_);
		push(@iseqs,$_);
		push(@igcc,get_gcc_from_str($_));	
	}
	close($cmd_fh);
	unlink($tmpfile_n);
	if(@iseqs != $N_introns){die "The orginal table ".$seqfeat_file." contains $N_introns introns but I found sequnences only for ".scalar(@iseqs).".";}
}

# get exon sequences
my @ex_gcc=();
my @ex_len=();
my @ex_seq=();
# create temporary file; will be deleted automatically at the end of script or if error occurs
my ($fh_tmp,$tmpfile_n) = tempfile( DIR => ".", UNLINK => 1);
print $fh_tmp "EXON_START\tEXON_END\tCHR\tSTRAND\n";
for(my $i=0;$i<@ex_starts;$i++){
	print $fh_tmp "$ex_starts[$i]\t$ex_ends[$i]\t$ex_chr[$i]\t$ex_strand[$i]\n";
	push(@ex_len,$ex_ends[$i]-$ex_starts[$i]+1);
}
close($fh_tmp);
if($fasta_file ne "NA"){
	$cmd="matt get_seqs $tmpfile_n EXON_START EXON_END CHR STRAND $fasta_file";
	open(my $cmd_fh,"$cmd |");<$cmd_fh>;  
	while(<$cmd_fh>){
		chomp($_);
		push(@ex_seq,$_);
		push(@ex_gcc,get_gcc_from_str($_));	
	}
	unlink($tmpfile_n);
	if(@ex_seq != @ex_starts){die "I searched for ".scalar(@ex_starts)." exon sequences but found only ".scalar(@ex_seq).".";}
}

# get 3&5 splice sites
my @ss5_seq=();
my @ss5_gcc=();
my @ss3_seq=();
my @ss3_gcc=();
my @ss5_2010_seq=();
my @ss5_2010_gcc=();
my @ss3_2010_seq=();
my @ss3_2010_gcc=();
if($fasta_file ne "NA"){
	# create temporary file; will be deleted automatically at the end of script or if error occurs
	my ($fh_tmp,$tmpfile_n) = tempfile( DIR => ".", UNLINK => 1);
	print $fh_tmp "INTRON_START\tINTRON_END\tCHR\tSTRAND\n";
	for(my $i=0;$i<@istarts;$i++){
		#  extracts 5ss xxxGTxxxx and 3ss xxxxxxxxxxxxxxxxxxAGxxx
		print $fh_tmp "$istarts[$i]\t$iends[$i]\t$ichrs[$i]\t$istrands[$i]\n";
	}
	close($fh_tmp);
	$cmd="matt get_seqs $tmpfile_n INTRON_START INTRON_END 3 6 20 3 CHR STRAND $fasta_file";
	open(my $cmd_fh,"$cmd |");<$cmd_fh>;  
	while(<$cmd_fh>){
		chomp($_);
		@fs=split("\t",$_,-1);
		push(@ss5_seq,$fs[0]);
		push(@ss5_gcc,get_gcc_from_str($fs[0]));
		push(@ss3_seq,$fs[1]);
		push(@ss3_gcc,get_gcc_from_str($fs[1]));
	}
	if(@ss5_seq != $N_introns){die "The orginal table ".$seqfeat_file." contains $N_introns introns but I found 5ss sequnences only for ".scalar(@ss5_seq).".";}
	if(@ss3_seq != $N_introns){die "The orginal table ".$seqfeat_file." contains $N_introns introns but I found 3ss sequnences only for ".scalar(@ss5_seq).".";}

	# get 5SS_20INT10EX and 3SS_20INT10EX
	$cmd="matt get_seqs $tmpfile_n INTRON_START INTRON_END 10 20 20 10 CHR STRAND $fasta_file";
	open($cmd_fh,"$cmd |");<$cmd_fh>;  
	while(<$cmd_fh>){
		chomp($_);
		@fs=split("\t",$_,-1);
		push(@ss5_2010_seq,$fs[0]);
		push(@ss5_2010_gcc,get_gcc_from_str($fs[0]));
		push(@ss3_2010_seq,$fs[1]);
		push(@ss3_2010_gcc,get_gcc_from_str($fs[1]));
	}
	unlink($tmpfile_n);

	if(@ss5_2010_seq != $N_introns){die "The orginal table ".$seqfeat_file." contains $N_introns introns but I found 5ss 20INT10EX sequnences only for ".scalar(@ss5_2010_seq).".";}
	if(@ss3_2010_seq != $N_introns){die "The orginal table ".$seqfeat_file." contains $N_introns introns but I found 3ss 20INT10EX sequnences only for ".scalar(@ss5_2010_seq).".";}
}


# get max ent score for splice sites
my @sss3=();
my @sss5=();
if($fasta_file ne "NA"){
	my ($fh_tmp,$tmpfile_n) = tempfile( DIR => ".", UNLINK => 1);
	print $fh_tmp "3SS_SEQ\t5SS_SEQ\n";
		for(my $i=0;$i<@ss5_seq;$i++){
		print $fh_tmp "$ss3_seq[$i]\t$ss5_seq[$i]\n";
	}
	close($fh_tmp);
	$cmd="matt get_sss $tmpfile_n 3SS_SEQ 5SS_SEQ";
	open(my $cmd_fh,"$cmd |");<$cmd_fh>;  
	while(<$cmd_fh>){
		chomp($_);
		@fs=split("\t",$_,-1);
		push(@sss3,$fs[0]);
		push(@sss5,$fs[1]);
	}
	unlink($tmpfile_n);

	if(@sss5 != $N_introns){die "The orginal table ".$seqfeat_file." contains $N_introns introns but I could determine the 5ss strength only for ".scalar(@sss5).".";}
	if(@sss3 != $N_introns){die "The orginal table ".$seqfeat_file." contains $N_introns introns but I could determine the 3ss strength only for ".scalar(@sss5).".";}
}


# score of best SF1 hit in the intron sequences
my @max_sf1_scores=();
my @seqs_bp_analysis=();
if($fasta_file ne "NA"){
	my $minprob=0.000001;
	my @tmp=read_pwm("$scr_abspath/../data_external/pwm/sf1_human_l7.tab",$MYCOMMAND,$minprob);
	my $pwm_aryref=$tmp[0];   # PWM has length 7
	my $alph_aryref=$tmp[1];
	@tmp=();

	# we search only in the 150 ($int_3ss_len) most down-stream positions of the introns
	# we always leave out the 20 most up-stream positions
	# * if intron length >=170 -> we search in the 150 most down-stream positions
	# * if intron length < 170 -> we search in the intron_length-25 most down-stream positions

	my $tmp_seq;
	foreach my $seq (@iseqs){
		if(length($seq)>20){
			$tmp_seq=substr($seq, 20);  # we always neglect the first 20 positions at the 5' end
			# we shorten down to the last positions which do not contain any N
			($tmp_seq = $tmp_seq) =~ s/^.*N//i;
			if(length($tmp_seq)>$int_3ss_len){$tmp_seq=substr($tmp_seq,length($tmp_seq)-$int_3ss_len);}
			# get_pwm_scores_for_seq return also NA if there are symbols in the sequences not previously defined like Ns
			# ignoreSTRINGS -> these NA values are not considered for determination of maximum
			push(@max_sf1_scores, get_max_from_aref( get_pwm_scores_for_seq( $tmp_seq, $pwm_aryref ) , "ignoreSTRINGS" ) );
			push(@seqs_bp_analysis,$tmp_seq);
			next;
		}
		push(@max_sf1_scores,"NA");
		push(@seqs_bp_analysis,"");
	}
}

# branch point analysis
# write 3ss ends of introns into fasta file
my @dist23ss=();
my @bp_seq=();
my @bp_seq_vomscore=();
my @pyrimidine_content=();
my @polypy_offset=();
my @polypy_len=();
my @polypy_score=();
my @bp_score=();
my @nbp_pos_score=();
if($species ne "NA" && $fasta_file ne "NA"){
	my ($fh_tmp,$tmpfile_n) = tempfile( DIR => ".", UNLINK => 1);
	for(my $i=0;$i<@iseqs;$i++){
		my $tmp_seq=$seqs_bp_analysis[$i];
		if(length($tmp_seq)==0){next;}
		# seqs need to be lower case and must not contain n (this has been checked already before)
		print $fh_tmp ">$i\n".lc($tmp_seq)."\n";
	}
	close($fh_tmp);
	$cmd="$scr_abspath/../external_progs/SVM-BPfinder-3M/svm_bpfinder.py -i $tmpfile_n -s $species -l $int_3ss_len";
	open(my $cmd_fh,"$cmd |");<$cmd_fh>; # header
	while(<$cmd_fh>){
		# seq_id	agez	ss_dist	bp_seq	bp_scr	y_cont	ppt_off	ppt_len	ppt_scr	svm_scr
		@fs=clean_and_split_line($_);
		my $i=$fs[0];
		if(!defined($dist23ss[$i])){
			$dist23ss[$i]=[];
			$bp_seq[$i]=[];
			$bp_seq_vomscore[$i]=[];
			$pyrimidine_content[$i]=[];
			$polypy_offset[$i]=[];
			$polypy_len[$i]=[];
			$polypy_score[$i]=[];
			$bp_score[$i]=[];
		}
		push(@{$dist23ss[$i]},$fs[2]);
		push(@{$bp_seq[$i]},uc($fs[3]));
		push(@{$bp_seq_vomscore[$i]},$fs[4]);
		push(@{$pyrimidine_content[$i]},$fs[5]);
		push(@{$polypy_offset[$i]},$fs[6]);
		push(@{$polypy_len[$i]},$fs[7]);
		push(@{$polypy_score[$i]},$fs[8]);
		push(@{$bp_score[$i]},$fs[9]);
	}
	unlink($tmpfile_n);
	
	# from all BP we consider only the top-3 BPs
	my $topN=3;
	my $topN_tmp;
	for(my $i=0;$i<@bp_score;$i++){
		if(defined($bp_score[$i])){
			my $idxs=get_idxs_of_desc_ordering_aref($bp_score[$i]);
			
			# from all BPs get the number of those with score > 0
			my $n_bp_with_pos_score=0;
			foreach my $j (@{$idxs}){if($bp_score[$i]->[$j]>0){$n_bp_with_pos_score++;}else{last;}}
			$nbp_pos_score[$i]=$n_bp_with_pos_score;
			
			# if we have less then topN BPs
			if(scalar(@{$bp_score[$i]})<$topN){ $topN_tmp=scalar(@{$bp_score[$i]}); }else{$topN_tmp=$topN;}
			
			my @idxs=@{$idxs}[0..($topN_tmp-1)];
			$dist23ss[$i]=[@{$dist23ss[$i]}[@idxs]];
			$bp_seq[$i]=[@{$bp_seq[$i]}[@idxs]];
			$bp_seq_vomscore[$i]=[@{$bp_seq_vomscore[$i]}[@idxs]];
			$pyrimidine_content[$i]=[@{$pyrimidine_content[$i]}[@idxs]];
			$polypy_offset[$i]=[@{$polypy_offset[$i]}[@idxs]];
			$polypy_len[$i]=[@{$polypy_len[$i]}[@idxs]];
			$polypy_score[$i]=[@{$polypy_score[$i]}[@idxs]];
			$bp_score[$i]=[@{$bp_score[$i]}[@idxs]];
		}
	}
}


# OUTPUT OF ALL FEATURES
# OUTPUT OF FEATURES
my @header_out=();
if($species ne "NA" && $fasta_file ne "NA"){for(my $i=0; $i<@feature_expl;$i++){($header_out[$i])=split(" ",$feature_expl[$i]);}
}
if($species eq "NA" && $fasta_file ne "NA"){
	for(my $i=0; $i<@feature_expl;$i++){
		unless($feature_expl[$i] =~ /\.( |)$/){  # BP features which are here unavailable have end in . or .blank
			($header_out[$i])=split(" ",$feature_expl[$i]);
		}
	}
}
if($fasta_file eq "NA"){  # same output features regardless of species
	my @feature_expl_tmp=@feature_expl[(0..7,36..56)];
	for(my $i=0; $i<@feature_expl_tmp;$i++){($header_out[$i])=split(" ",$feature_expl_tmp[$i]);}
}
for(my $j=0;$j<@feature_names;$j++){push(@header_out,"NTRS_".$feature_names[$j]);push(@header_out,"PROP_".$feature_names[$j]);}
print join("\t",@header_out)."\n";

my ($tmp1,$tmp2,$tmp3,$bin,%tmp,$intron_not_found);
for(my $i=0; $i<$N_introns;$i++){
	$intron_not_found=0;if(@{$this_intron_occurs_in_trs[$i]}==0){$intron_not_found=1;}
	
	# INTRON_ID
	$iid=$iids[$i];
	print $iid;
	
	# GENE_BIOTYPE
	unless($notrbts){
		if(defined($gbt[$i])){print "\t$gbt[$i]";}else{print "\t";}
	}else{print "\t";}
	
	# INTRON_FOUND_IN_GTF
	if($intron_not_found){print "\tno"}else{print "\tyes";}

	# INTRON_LENGTH
	$tmp1=$iends[$i]-$istarts[$i]+1;
	print "\t$tmp1";
	
	# UPEXON_MEDIANLENGTH
	$tmp2=get_median_from_aref( [ @ex_len[@{$upex_ids[$i]}] ] );  # returns NA if @{$upex_ids[$i]} is empty array
	print "\t$tmp2";
	
	# DOWNEXON_MEDIANLENGTH
	$tmp3=get_median_from_aref( [ @ex_len[@{$doex_ids[$i]}] ] );  # returns NA if @{$doex_ids[$i]} is empty array 
	print "\t$tmp3";
	
	# RATIO_UPEXON_INTRON_LENGTH
	if($tmp2 ne "NA" && $tmp1 ne "NA" && $tmp1>0){print "\t".($tmp2/$tmp1);}else{print "\tNA";}
	
	# RATIO_DOWNEXON_INTRON_LENGTH
	if($tmp3 ne "NA" && $tmp1 ne "NA" && $tmp1>0){print "\t".($tmp3/$tmp1);}else{print "\tNA";}
	
	# INTRON_GCC
	if(@igcc>0){
		$tmp1=$igcc[$i];print "\t$tmp1";

		# UPEXON_MEDIANGCC
		$tmp2=get_median_from_aref( [ @ex_gcc[@{$upex_ids[$i]}] ] );  # NA if @{$upex_ids[$i]} is empty
		print "\t$tmp2";
	
		# DOWNEXON_MEDIANGCC
		$tmp3=get_median_from_aref( [ @ex_gcc[@{$doex_ids[$i]}] ] );  # NA if @{$doex_ids[$i]} is empty
		print "\t$tmp3";
	
		# RATIO_UPEXON_INTRON_GCC
		if($tmp2 ne "NA" && $tmp1 ne "NA" && $tmp1>0){print "\t".($tmp2/$tmp1)."";}else{print "\tNA";}
	
		# RATIO_DOWNEXON_INTRON_GCC
		if($tmp3 ne "NA" && $tmp1 ne "NA" && $tmp1>0){print "\t".($tmp3/$tmp1)."";}else{print "\tNA";}
	
		# HUMANSF1_HIGHESTSCORE_3SS
		print "\t$max_sf1_scores[$i]";

		# LEN_SEQ_INTRON_3SS_USED_FOR_BP_ANALYSIS
		$tmp1=$seqs_bp_analysis[$i];
		if(defined($tmp1)){ print "\t".length( $tmp1 ).""; }else{ print "\tNA"; }

		# INTRON_5SS_20INT10EX_GCC
		print "\t$ss5_2010_gcc[$i]";
	
		# INTRON_3SS_20INT10EX_GCC
		print "\t$ss3_2010_gcc[$i]";
	}

	# MAXENTSCORE_BURGEHUMANMODEL_5SS
	if(@sss5>0){print "\t$sss5[$i]";}

	# MAXENTSCORE_BURGEHUMANMODEL_3SS
	if(@sss3>0){print "\t$sss3[$i]";}

	# BP features
	if($species ne "NA" && $fasta_file ne "NA"){

		# we have branch point features for this intron
		if(defined($bp_score[$i])){
			# branch point features for branch point with max score -> is always first entry in arrays because the are sorted according to decreasing BP score
			print "\t".$dist23ss[$i]->[0];
			print "\t".$bp_seq[$i]->[0];
			print "\t".$bp_seq_vomscore[$i]->[0];
			print "\t".$pyrimidine_content[$i]->[0];
			print "\t".$polypy_offset[$i]->[0];
			print "\t".$polypy_len[$i]->[0];
			print "\t".$polypy_score[$i]->[0];
			print "\t".$bp_score[$i]->[0];

			# number of BPs with positive score
			print "\t$nbp_pos_score[$i]";

			# median of branch point features (only for topN BPs = 3)
			print "\t".get_median_from_aref($dist23ss[$i]);
			print "\t".join(",",@{$bp_seq[$i]});
			print "\t".get_median_from_aref($bp_seq_vomscore[$i]);
			print "\t".get_median_from_aref($pyrimidine_content[$i]);
			print "\t".get_median_from_aref($polypy_offset[$i]);
			print "\t".get_median_from_aref($polypy_len[$i]);
			print "\t".get_median_from_aref($polypy_score[$i]);
			print "\t".get_median_from_aref($bp_score[$i]); 
		}else{
			# we do not have branch point features for this intron
			print "\tNA\t\tNA\tNA\tNA\tNA\tNA\tNA\tNA\tNA\t\tNA\tNA\tNA\tNA\tNA\tNA";
		}
	}

	# INTRON_FOUND_IN_THESE_TRS
	print "\t".join(":",sort(split(":",$tr_ids[$i])));
	
	# MEDIAN_TR_LENGTH
	print "\t$trlengths[$i]";
	
	# MEDIAN_INTRON_NUMBER  (of trs where intron i occurs in)
	print "\t".get_median_from_aref($n_introns[$i]);
	
	# INTRON_MEDIANRANK
	print "\t$irank[$i]";   # is NA if intron not found in any tr
	
	# INTRON_MEDIANRELATIVERANK
	print "\t$irelrank[$i]";  # is NA if intron not found in any tr
	
	# INTRON_MEDIANRELATIVERANK_3BINS
	$bin="NA";
	if($irelrank[$i] ne "NA"){ $bin=int( $irelrank[$i] / 0.3333333333333333 ) + 1;if($bin==4){$bin=3;} }
	print "\t$bin";
	
	# INTRON_MEDIANRELATIVERANK_5BINS
	$bin="NA";
	if($irelrank[$i] ne "NA"){ $bin=int( $irelrank[$i] / 0.2 ) + 1;if($bin==6){$bin=5;} }
	print "\t$bin";
	
	# INTRON_MEDIANRELATIVERANK_10BINS
	$bin="NA";
	if($irelrank[$i] ne "NA"){ $bin=int( $irelrank[$i] / 0.1 ) + 1;if($bin==11){$bin=10;} }
	print "\t$bin";

	# NTRS_ALL_FOR_GENE
	print "\t$ntrs_all[$i]";

	# NTRS_WITH_INTRON
	$tmp1=$ntrs_withintron[$i];
	print "\t$tmp1";
	
	# NTRS_WITH_INTRON_AS_FIRST_INTRON
	$tmp2=$ntrs_is_firstintron[$i];
	print "\t$tmp2";
	
	# PROP_FIRST_INTRON
	if($tmp1>0){print "\t".($tmp2/$tmp1);}else{print "\tNA";}
	
	# NTRS_WITH_INTRON_AS_LAST_INTRON
	$tmp2=$ntrs_is_lastintron[$i];
	print "\t$tmp2";
	
	# PROP_LAST_INTRON
	if($tmp1>0){print "\t".($tmp2/$tmp1);}else{print "\tNA";}
	
	# NTRS_WITH_INTRON_AS_INTERNAL_INTRON
	$tmp2=$ntrs_is_internalintron[$i];
	print "\t$tmp2";
	
	# PROP_INTERNAL_INTRON
	if($tmp1>0){print "\t".($tmp2/$tmp1);}else{print "\tNA";}
	
	# NTRS_WITH_INTRON_IN_UTR
	$tmp2=$ntrs_in_utr[$i];
	print "\t$tmp2";
	
	# PROP_INTRON_IN_UTR
	if($tmp1>0){print "\t".($tmp2/$tmp1);}else{print "\tNA";}

	my @cnts_co_introns=();
	my $cooccurs_with_other_intron="no";
	%tmp=();
	foreach my $tid (@{$this_intron_occurs_in_trs[$i]}){
		my %tmptmp=();
		foreach my $iid_tmp (split(":",$tid2iids{$tid})){
			if($iid_tmp ne $iid){$tmp{$iid_tmp}=1; $tmptmp{$iid_tmp}=1;$cooccurs_with_other_intron="yes";}
		}
		push(@cnts_co_introns,scalar(keys %tmptmp));
	}
	if($intron_not_found){$cooccurs_with_other_intron="NA";}  # intron not found in any tr

	# INTRON_COOCCURS_WITH_OTHER_INTRONS
	print "\t$cooccurs_with_other_intron";
	
	# INTRON_COOCCURS_WITH_THESE_INTRONS
	print "\t".join(":",sort(keys %tmp));

	# MEDIAN_INTRON_INTRON_COOCCURRENCE_NUMBER_OVER_ALL_TRS
	print "\t".get_median_from_aref(\@cnts_co_introns);  # is NA if @cnts_co_introns is empty

	# SEQ_5SS
	if(@iseqs>0){
		print "\t".substr($iseqs[$i],0,2);
		
		# SEQ_3SS
		print "\t".substr($iseqs[$i],-2);
	
		# SEQ_INTRON
		my $tmp_seq=$iseqs[$i];
		print "\t$tmp_seq";
	}
		
	# SEQ_INTRON_3SS_USED_FOR_BP_ANALYSIS
	if(@seqs_bp_analysis>0){print "\t".$seqs_bp_analysis[$i];}

	# SEQ_LONGESTUPEXON
	if(@ex_seq>0){
		my $ex_seq="";
		foreach my $seq_tmp (@ex_seq[ @{$upex_ids[$i]} ]){if(length($seq_tmp)>length($ex_seq)){$ex_seq=$seq_tmp;}};
		print "\t$ex_seq";
	
		# SEQ_LONGESTDOWNEXON
		$ex_seq="";
		foreach my $seq_tmp (@ex_seq[ @{$doex_ids[$i]} ]){if(length($seq_tmp)>length($ex_seq)){$ex_seq=$seq_tmp;}}
		print "\t$ex_seq";
	}
	
	# SEQ_INTRON_5SS_6INT3EX
	if(@ss5_seq>0){print "\t$ss5_seq[$i]";}
	
	# SEQ_INTRON_3SS_20INT3EX
	if(@ss3_seq>0){print "\t$ss3_seq[$i]";}
	
	# SEQ_INTRON_5SS_20INT10EX
	if(@ss5_2010_seq>0){print "\t$ss5_2010_seq[$i]";}
	
	# SEQ_INTRON_3SS_20INT10EX
	if(@ss3_2010_seq>0){print "\t$ss3_2010_seq[$i]";}  # no last TAB
		
	# counts and proportions for user specified features
	my $str_tmp="\t";
	for(my $j=0;$j<@feature_counts;$j++){
		$tmp1=$ntrs_withintron[$i];
		$tmp2=$feature_counts_arefs[$j]->[$i];

		# count
		if($intron_not_found){$str_tmp.="NA\t";
		}else{$str_tmp.="$tmp2\t";}

		# proportion
		if($tmp1>0 && !$intron_not_found){$str_tmp.=$tmp2/$tmp1."\t";}else{$str_tmp.="NA\t";}
	}
	# omit last tab
	print substr($str_tmp,0,length($str_tmp)-1);
	
	print "\n";
}

exit(0); # success
__END__
