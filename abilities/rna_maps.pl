#!/usr/bin/env perl
use strict;
use warnings;
use File::Temp qw(tempfile);
use File::Copy;
use File::Path qw/make_path/;
use Cwd qw(abs_path getcwd);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.

########
## Subs
########
sub normalize{
	my $v=$_[0];   my $c=$_[1];   if($c!=0){ return($v/$c) }else{ return "NA" }	
}

##################


my $MYCOMMAND="rna_maps";

# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "super\t$MYCOMMAND\t:     \tgenerate motif RNA maps";
        exit(1);
}

if(@ARGV<18 || @ARGV>30 || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/){
	print "\nmatt $MYCOMMAND <TABLE> <C_C1_COORD> <C_EX_START> <C_EX_END> <C_C2_COORD> <C_SCAFFOLD> <C_STRAND> <C_GROUP> <WIN_L> <EXPART_L> <INTPART_L> ...\n";
	print "                  ...  <FASTA> <TABLE_MOTIFS> <C_TYPE> <C_NAME> <C_EXPR_FILE> <C_THRESH> <C_BGMODEL>\n";
	print "                  ... [-d <OUT_DIR>] [-p <P_VALUE>] [-fdr <FDR>] [-t <EX_OR_INT_VIEW>] [-s <RSEED>] [-ymax <YMAX>] [-names <NAMES>]\n\n";
	print "   ... produces motif RNA maps for binding motifs given as PWM or REGEXPS\n\n";
	print "   <TABLE>        : tab-separated table with coordinates and further information on exons and their flanking exons\n";
	print "   <C_C1_COORD>   : name of column with end of flanking exon C1. The empty string or NA in this column are interpreted as missing C1 coordinate.\n";
	print "   <C_EX_START>   : name of column with start of central exon\n";
	print "   <C_EX_END>     : name of column with end of central exon\n";
	print "   <C_C2_COORD>   : name of column with start exon C2; The empty string or NA in this column are interpreted as missing C2 coordinate.\n";
	print "                    Note: C_C1_COORD and C_C2_COORD will get internally re-arranged automatically such that C_C1_COORD is smaller than C_C2_COORD.\n";
	print "                          C_EX_START and C_EX_END will get internally re-arranged automatically such that C_EX_START is smaller than C_EX_END.\n";
	print "   <C_SCAFFOLD>   : name of column with scaffold id for exons\n";
	print "   <C_STRAND>     : name of column with strand information for exons\n";
	print "   <C_GROUP>      : name of column with group ids of exons. To sub-select groups, do e.g.: GROUP[up,down,ndiff]\n";
	print "   <WIN_L>        : length of sliding window; must be un-even like 26 or 51\n";
	print "   <EXPART_L>     : length of exon part to be considered for RNA map\n";
	print "   <INTPART_L>    : length of intron part to be considered for RNA map\n";
	print "   <FASTA>        : FASTA file with genomic sequences\n";
	print "                    If <TABLE> contains data from different species, <TABLE> must have a column with species ids, e.g., SPECIES with ids hsa, and mmu\n";
	print "                    Then you would need to specify a FASTA file for each species and let Matt know about which FASTA belongs to which species id and which\n";
	print "                    is the name of the column with species ids, e.g.: SPECIES,hsa:/path/to/hsa.fasta,mmu:/path/to/mmu.fasta\n";
	print "   <TABLE_MOTIFS> : tab-separated table describing motifs; Must have five columns to which you refer with C_TYPE, C_NAME, C_EXPR_FILE, C_THRESH, C_BGMODEL\n";
	print "                    Values in C_THRESH and C_BGMODEL can be set to NA for REGEXP motifs as these pieces of information are necessary for PWM motifs only.\n";
	print "   <C_TYPE>       : name of column of TABLE_MOTIFS with type (must contain REGEXP or PWM)\n";
	print "   <C_NAME>       : name of column of TABLE_MOTIFS with name of motif. Motif names must be unique in file TABLE_MOTIFS.\n";
	print "                    Note: names should contain only a-zA-Z0-9+-_.,[]{} . If you don't get a PDF report, maybe names contained special chars.\n";
	print "   <C_EXPR_FILE>  : name of column of TABLE_MOTIFS with Perl regular expression or link to file with PWM\n";
	print "   <C_THRESH>     : name of column of TABLE_MOTIFS with thresholds for PWM motifs (must be NA for regular expression)\n";
	print "                      The threshold applies to the log-score of the motif PWM, or, if a BG model is given, to the likelihood ratio fg/bg.\n";
	print "                      In the latter case, with 10 e.g. hits are reported if they are 10 times more likely under the motif PWM than under the bg model.\n";
	print "   <C_BGMODEL>    : name of column of TABLE_MOTIFS with information on background model (must be NA for regular expression)\n";
	print "                       If it is NA, no bg model is used. If it is INFER_BGPWM, a homogeneous Markov model of order 0 is inferred from all sequences\n";
	print "                       extracted from file TABLE. If it is not NA nor INFER_BGPWM, it is assumed to be a link to a file which must\n"; 
	print "                       describe a background PWM model.\n";
	print "   <OUT_DIR>      : output directory; specify as -d OUT_DIR\n";
	print "   <P_VALUE>      : if a p value is specified, a permutation is run and regions with significant differences (p value <= then specified)\n";
	print "                    are highlighted in the motif RNA map. You need to specify the p value and the number of permutations (>=100) e.g., -p 0.05 1000\n";
	print "                    Important: when using -p, you need to explicitly specify the groups to be compared with argument <C_GROUP>.\n";
	print "                    E.g., GROUP[up,down,ndiff] -> the last group, here ndiff, is then the reference group all other groups should be compared to.\n";
	print "   <FDR>          : if a FDR rate is specified, a permutation test is run for estimating p values and regions are highlighted where the expected FDR\n";
	print "                    is at most the specified value. You need to specify the FDR (e.g. 0.1) and the number of permutations (>=100) e.g., -fdr 0.1 1000\n";
	print "                    Important: when using -fdr, you need to explicitly specify the groups to be compared with argument <C_GROUP>.\n";
	print "                    E.g., GROUP[up,down,ndiff] -> the last group, here ndiff, is then the reference group all other groups should be compared to.\n";	
	print " <EX_OR_INT_VIEW> : choose type of RNA map (exon-centralized: -t exon; or intron-centralized: -t intron). Default is exon-centralized.\n";
	print "                    For introns: <C_EX_START> <C_EX_END> have the meaning of <C_INT_START> <C_INT_END>\n";
	print "   <RSEED>        : any number to be used as seed for the random generator\n";
	print "   <YMAX>         : By default, the range of the y-axis of the motif maps will be determined dynamically. Setting YMAX fixes the upper end of the y-axis.\n";
	print "   -names <NAMES> : Subselection of motifs, e.g., -names MSI,MBL,HOW where these motif names must occur in file TABLE_MOTIFS in column C_NAME.\n";
	print "\n   Output: motif RNA maps as PDF for each motif defined in TABLE_MOTIFS in specified output directory.\n";
	print "           Furthermore, a single PDF called 0_all_motif_rna_maps.pdf with an overview of all motif RNA maps.\n";
	print "           And, if -p or -fdr are specified, a single PDF called 0_significant_motif_rna_maps.pdf with motif RNA maps with significant enrichments.\n";
	print "\n";
	print "\n   Hint1: Invoking arguments -p and -fdr may increase significantly the runtime. If you are checking many motifs,\n";
	print "            you may not apply the permutation test by not using -p nor -fdr and scroll through the PDF report. Motif RNA maps\n";
	print "            are sorted in the report according to the signal distance between any group and the reference group. You could then\n";
	print "            re-run this analysis with a few interesting motifs only applying the permutation test\n";
	print "\n   Hint2: To define the reference group, run this command with C_GROUP[up,down,ndiff] assuming the group ids to be up, down, ndiff.\n";
	print "            The last listed group is considered to be the reference group.\n";
	print "\n";
	exit(1);
}


my ($f,$c1,$s,$e,$c2,$scf,$str,$grp,$w_l,$wex_l,$wint_l,$fasta,$f2,$c_t,$c_n,$c_e_f,$c_thresh,$c_bgmodel)=@ARGV;
my $mode="region";
my $ex_or_int_view="exon";  # default
my $out_dir=".";
my $pval=1;  # setting it to 1 switches off p-value computation and highlighting of regions with sig. differences
my $nperms="";
my $fdr=1;   # setting it to 1 switches off the fdr computation (permutation test and highlighting)    
my $rand_seed=undef;
my $ymax=-1;
my $selected_names="";

for(my $i=@ARGV-10;$i<@ARGV;$i++){
		if(substr($ARGV[$i],0,1) ne "-"){next;}
		if($ARGV[$i] eq "-d"){$out_dir=$ARGV[$i+1];$i++;next;}
		if($ARGV[$i] eq "-t"){$ex_or_int_view=$ARGV[$i+1];$i++;next;}
		if($ARGV[$i] eq "-p"){$pval=$ARGV[$i+1];$nperms=$ARGV[$i+2];$i+=2;next;}
		if($ARGV[$i] eq "-fdr"){$fdr=$ARGV[$i+1];$nperms=$ARGV[$i+2];$i+=2;next;}
		if($ARGV[$i] eq "-s"){$rand_seed=$ARGV[$i+1];$i++;next;}
		if($ARGV[$i] eq "-ymax"){$ymax=$ARGV[$i+1];$i++;next;}
		if($ARGV[$i] eq "-names"){$selected_names=$ARGV[$i+1];$i++;next;}
		die "Do not understand argument $ARGV[$i]. Please check for correct argument name and order.";
}
unless(-e $out_dir){make_path($out_dir) or die "$!";}

# Check if input exon table exists
unless(-e $f){die "File $f does not exist.\n"}
# ... and actually contains exons
my $cmd="wc -l $f";
my @fs=`$cmd`;
my @fs2=split(/\s+/,$fs[0]);
if($fs2[0]<2){exit(0)}
# Check if motif table exists
unless(-e $f2){die "File $f does not exist.\n"}
# ... and actually contains motifs
$cmd="wc -l $f2";
@fs=`$cmd`;
@fs2=split(/\s+/,$fs[0]);
if($fs2[0]<2){exit(0)}



# argument checks
if($ymax!=-1 && (!is_numeric($ymax) || $ymax<=0)){die "Given ymax value ($ymax) must be numeric and greater than 0.\n"}
if(defined($rand_seed) && !is_numeric($rand_seed)){die "Given random seed $rand_seed seems not numeric.\n"}
if(!is_numeric($pval)){die "P value ($pval) given by argument -p seems not numeric.";}
if($nperms ne "" && (!is_numeric($nperms) || $nperms<100)){die "Given number of permutations ($nperms) is not numeric or smaller than the recommended smallest value of 100.";}
if($nperms eq ""){$nperms=0;}
if(!is_numeric($fdr)){die "FDR ($fdr) given by argument -fdr seems not numeric.";}
if($pval!=1 && $fdr!=1){die "You seem to have specified both arguments -p and -fdr. Please specify only one of them."}
unless($ex_or_int_view eq "exon" || $ex_or_int_view eq "intron"){die "Argument EX_OR_INT_VIEW seems to be $ex_or_int_view but must be either exon or intron";}
if($w_l % 2 == 0){die "Window length (WIN_L) must be un-even but you specified $w_l.";}

# set random generator
if(defined($rand_seed)){srand($rand_seed);my $trash=rand();} 

if($ex_or_int_view eq "intron"){($wint_l,$wex_l)=($wex_l,$wint_l);}

# set further variables
my $wex_l2=$wex_l+($w_l-1)/2;   # length of sliding window effectively increases wex_land wint_l; -1 because we start at exon borders and already include one exon position 
my $wint_l2=$wint_l+($w_l-1)/2; # wex_l and wint_l are only used for sequence retrieval
my $ext_seq_l=$wex_l2+$wint_l2;
my $seq_l=$wex_l+$wint_l;
my $c_grp=$grp;
my %gids_select;
my @gids_select;
if($grp=~/(.+)\[(.+)\]/){$c_grp=$1; my $i=0; @gids_select=split(",",$2); foreach my $gid (@gids_select){$gids_select{$gid}=$i++;} }
if(@gids_select==0 && $pval<1){die "You have specified a p-value with argument -p but you have not explicitly specified groups to be compared with argument <C_GROUP>. Please read the help message for <C_GROUP> and <P_VALUE>."}
# we take all group ids if not yet defined 
$cmd="matt get_cols $f $c_grp";
my @gids=`$cmd`; die "Retrieving group ids failed" if $?!=0; chomp(@gids); 
if(@gids_select==0){
	my $j=0; for(my $i=1;$i<@gids;$i++){unless(defined($gids_select{$gids[$i]})){$gids_select{$gids[$i]}=$j++; push(@gids_select,$gids[$i])}} 
}else{
	my %found_gids=();
	for(my $i=1;$i<@gids;$i++){$found_gids{$gids[$i]}=1}
	foreach my $gid_tmp (keys %gids_select){if(!defined($found_gids{$gid_tmp})){die "User selected GROUP ID $gid_tmp was not found in data table in column $c_grp.\n"}}
}
# check group ids
if(@gids_select<1){die "Found less than one group ID. Need at least two."}
if(@gids_select>10){die "Found more than 10 group IDs. The maximum are 10 different group Ids."}
my $ngrps=scalar(@gids_select);


# check if user has defined subset of motifs to be used   -> find pattern at the end of file name [bla,fla,bke,..]
my @chosen_motifs=split(",",$selected_names);
my %chosen_motifs; $chosen_motifs{$_}++ for (@chosen_motifs);

# check table with REGEXPs / PWMs
my ($fh,$col_ns_aref,$col_ids_aref,$comments)=open_fct_file($f2,[$c_t,$c_n,$c_e_f,$c_thresh,$c_bgmodel],$MYCOMMAND);
my ($t,$n,$f_e,$thresh,$bgmodel)=($col_ids_aref->[0],$col_ids_aref->[1],$col_ids_aref->[2],$col_ids_aref->[3],$col_ids_aref->[4]);
my $we_need_to_infer_bgpwms=0;
my $max_motif_l=0;
my @alph=();
my $minprob=0.001;
@chosen_motifs=();
while(<$fh>){
	$_=clean_line_leave_quotes($_);
	if(length($_)==0 || substr($_,0,1) eq "#"){next;}
	my @fs=split_line($_);
	
	if(!defined($fs[$t]) || !($fs[$t] eq "REGEXP" || $fs[$t] eq "PWM")){die "Type of motif must either be PWM or REGEXP, please check your input line $_ .";}
	
	if($fs[$t] eq "REGEXP" && (!defined($fs[$n]) || !defined($fs[$f_e]))){
		die "You need to specify a motif name and a regular expression for REGEXP in input line $_.";
	}
	
	if($fs[$t] eq "PWM" && (!$fs[$f_e] || !(-e $fs[$f_e]))){
		die "For a PWM motif you need to specify a file with the motif PWM, please check your input line $_ .";
	}
	
	if($fs[$t] eq "PWM" && (!$fs[$n] || !$fs[$thresh] || !is_numeric($fs[$thresh]) || !$fs[$bgmodel])){
		die "For a PWM motif you need to specify a motif name and a numeric threshold, please check your input line $_ .";
	}
	
	if($fs[$t] eq "PWM" && (!$fs[$bgmodel] || !($fs[$bgmodel] eq "NA" || $fs[$bgmodel] eq "INFER_BGPWM" || -e $fs[$bgmodel]))){
		die "For a PWM motif you need to specify for the background PWM either NA, INFER_BGPWM or a file with the background PWM, please check your input line $_ .";
	}
	
	if($fs[$t] eq "PWM" && $fs[$bgmodel] eq "INFER_BGPWM"){
		$we_need_to_infer_bgpwms=1;
		my($pwm_aref,$symbs_aref,$comments_pwm)=read_pwm($fs[$f_e],$MYCOMMAND,$minprob);  # this is only to get the alphabet from the fg model and the length of the motif
		if(scalar(@$pwm_aref)>$max_motif_l){$max_motif_l=scalar(@$pwm_aref);}
		foreach my $letter (@$symbs_aref){unless(is_element_in_aref(\@alph,$letter)){push(@alph,$letter)}}
	}
	
	push(@chosen_motifs,$fs[$n]);  # get all motif names
}
close($fh);

# if user has not sub-selected motifs
if(scalar(keys %chosen_motifs)==0){
	$chosen_motifs{$_}++ for (@chosen_motifs);
}else{
	foreach my $tmpv (keys %chosen_motifs){
		if(first_index_exact($tmpv,\@chosen_motifs)==-1){die "Motif with name $tmpv not found in table $f2.\n"}
	}
}

# matrix with statistics
# columns: pos of the sliding window
# rows   : number of cases (skipped exons or retained introns) 
my @stats=();for(my $i=0;$i<4*$seq_l;$i++){push(@stats,[])};
# contains group ids along rows
my @grps;
my @n_per_grp=();

# write temporary file with all exons passing group id filter
# create temporary file; will be deleted automatically at the end of script or if error occurs
# will contain all sequences to be extracted 
my ($fh_tmp,$tmpfile_n) = tempfile( DIR => ".", UNLINK => 1);
my @fasta_split=split(",",$fasta);
print $fh_tmp "START\tEND\tSCAFFOLD\tSTRAND\tGROUP\tCASE_COUNTER\tSEQ_ID\tSPECIES\n";  # SEQ_ID is CASE_COUNTER + GROUP_ID + OFFSET
my @cols;
if(@fasta_split>1){
	($fh,$col_ns_aref,$col_ids_aref,$comments)=open_fct_file($f,[$c1,$s,$e,$c2,$scf,$str,$c_grp,$fasta_split[0]],$MYCOMMAND);
	@cols=@$col_ids_aref;@cols=@cols[0..(@cols-3)];
}else{
	($fh,$col_ns_aref,$col_ids_aref,$comments)=open_fct_file($f,[$c1,$s,$e,$c2,$scf,$str,$c_grp],$MYCOMMAND);
	@cols=@$col_ids_aref;@cols=@cols[0..(@cols-2)];
}

my @vs_tmp;
my @vs2_tmp;
my $seq_cntr=-1;
my @regions_bgpwms=();  # start end scaffold strand grpid start end scaffold strand grpid .... for each exon/intron case
my @species_suffixes=();
my ($skip_c1, $skip_c2);
while(<$fh>){
	$_=clean_line($_);
	if(length($_)==0 || substr($_,0,1) eq "#"){next;}
	my @fs=split_line($_);
	my $grp_id=$fs[$col_ids_aref->[6]];
	my $species_suffix="NA";
	if(@fasta_split>1){$species_suffix=$fs[$col_ids_aref->[7]];}

	if(defined($gids_select{ $grp_id })){
		my($c1c,$sc,$ec,$c2c,$scfv,$strv)=@fs[@cols];
		if( $sc > $ec ){($sc,$ec)=($ec,$sc);}   # get them into right order

		# if C1 and/or C2 is unknown, e.g., for first or last exons, then c1c and c2c can be either NA or the empty string ''
		# in these cases, we set c1c and c2c artificially so that extraction of all coordinates goes through but we memorize that in the end we must not write/use c1 and/or c2
		($skip_c1,$skip_c2)=(0,0);
		if($c1c eq "" || $c1c eq "NA"){
			if($c2c eq "" || $c2c eq "NA"){  # C1 and C2 are unknown
				$c1c=$sc-$wint_l2-1;
				$c2c=$ec+$wint_l2+1;
				($skip_c1,$skip_c2)=(1,1);
			}else{                           # C1 unknown, C2 known
				if($c2c>$ec){
					$c1c=$sc-$wint_l2-1;  # C2 comes after exon end, so C1 must be smaller than exon start  (strand +)
					$skip_c1=1;
				}else{
					$c1c=$ec+$wint_l2+1;  # C2 is supposed to be smaller than exon start, so C1 must be greater than exon end (strand -)
					$skip_c2=1;  # later due to ordering of c1c and c2c, c1c will become c2c
				}
			}
		}elsif($c2c eq "" || $c2c eq "NA"){     # C1 known, but C2 unknown
			if($c1c<$sc){
				$c2c=$ec+$wint_l2+1;  # C1 comes before exon start, so C2 must be greater than exon end  (strand +)
				$skip_c2=1;
			}else{
				$c2c=$sc-$wint_l2-1;  # C1 is supposed to be greater than exon end, so C2 must be smaller than exon start (strand -)
				$skip_c1=1;   # later due to ordering of c1c and c2c, c2c will become c1c
			}
		}

		if( $c1c > $c2c ){($c1c,$c2c)=($c2c,$c1c);}  # get them into right order
		
		# information on entire region to be used for training bg pwms
		if(!$skip_c1 && !$skip_c2){
			push(@regions_bgpwms,$c1c-$wex_l2,$c2c+$wex_l2,$scfv,$strv,$grp_id);
		}elsif($skip_c1 && !$skip_c2){
			push(@regions_bgpwms,$sc-$wint_l2,$c2c+$wex_l2,$scfv,$strv,$grp_id);
		}elsif(!$skip_c1 && $skip_c2){
			push(@regions_bgpwms,$c1c-$wex_l2,$ec+$wint_l2,$scfv,$strv,$grp_id);
		}else{  
			push(@regions_bgpwms,$sc-$wint_l2,$ec+$wint_l2,$scfv,$strv,$grp_id); 
		}
		# information in species for bg sequences
		push(@species_suffixes,$species_suffix);
		

		@vs_tmp=();   # all pairs of coordinates for sub-sequence extraction for each sequence
		@vs2_tmp=();  # off1,l1,off2,l2,off3,l3,off4,l4 for each seq
		my $tmpc;
		my $s_tmp;
		my $e_tmp;
		my $pos_missing=0;

		$s_tmp=$c1c-$wex_l2+1;  # always as we don't have info about length of c1 exons
		if($sc-$c1c-1>=$wint_l2){
			$e_tmp=$c1c+$wint_l2; $tmpc=$sc-$wint_l2;
		}else{
			$pos_missing=$wint_l2-($sc-$c1c-1);
			if($pos_missing>($w_l-1)/2){$pos_missing=($w_l-1)/2;}
			$e_tmp=$sc-1+$pos_missing; $tmpc=$c1c+1-$pos_missing;
		}
		push(@vs_tmp,$s_tmp,$e_tmp);
		push(@vs2_tmp,0,$e_tmp-$s_tmp+1);   # off set here is always 0

		$s_tmp=$tmpc;
		if($ec-$sc+1>=$wex_l2){
			$e_tmp=$sc+$wex_l2-1; $tmpc=$ec-$wex_l2+1; 
		}else{
			$pos_missing=$wex_l2-($ec-$sc+1);
			if($pos_missing>($w_l-1)/2){$pos_missing=($w_l-1)/2;}
			$e_tmp=$ec+$pos_missing; $tmpc=$sc-$pos_missing;
		}
		push(@vs_tmp,$s_tmp,$e_tmp);
		push(@vs2_tmp,$s_tmp-($sc-$wint_l2),$e_tmp-$s_tmp+1);

		$s_tmp=$tmpc;
		if($c2c-$ec-1>=$wint_l2){
			$e_tmp=$ec+$wint_l2; $tmpc=$c2c-$wint_l2;
		}else{
			$pos_missing=$wint_l2-($c2c-$ec-1);
			if($pos_missing>($w_l-1)/2){$pos_missing=($w_l-1)/2;}
			$e_tmp=$c2c-1+$pos_missing; $tmpc=$ec+1-$pos_missing;
		}
		push(@vs_tmp,$s_tmp,$e_tmp);
		push(@vs2_tmp,$s_tmp-($ec-$wex_l2+1),$e_tmp-$s_tmp+1);

		$s_tmp=$tmpc;
		$e_tmp=$c2c+$wex_l2-1;  # always because we don't have info about length of c2 exon
		push(@vs_tmp,$s_tmp,$e_tmp);
		push(@vs2_tmp,$s_tmp-($c2c-$wint_l2),$e_tmp-$s_tmp+1);

		$seq_cntr++;
		$grps[$seq_cntr]=$grp_id;
		my $grp_pos=$gids_select{$grp_id};
		$n_per_grp[$grp_pos]++;
		for(my $k=0;$k<@stats;$k++){$stats[$k][$seq_cntr]=0;}
		for(my $i=0;$i<4;$i++){   # order of sequences in $fh_tmp is always: first seq for ex i, second seq for ex i, third seq for ex i, fourth seq for ex i, first seq for ex i+1, second seq for ex i+1 ....
			if($skip_c1 && $i==0){next;}
			if($skip_c2 && $i==3){next;}
			
			my $offset=$i*$seq_l;  # correction for the 4 regions to be analyzed
			if($strv eq "+"){ # and first to fourth is from upstream -> downstream on strand of genes
				$s_tmp=$vs2_tmp[$i*2];              # start=offset
				$e_tmp=$s_tmp+$vs2_tmp[$i*2+1]-1;   # end=start+length-1
				print $fh_tmp "".$vs_tmp[$i*2]."\t".$vs_tmp[$i*2+1]."\t$scfv\t$strv\t$grp_id\t$seq_cntr\t${seq_cntr}_${s_tmp}_$offset\t$species_suffix\n";   # the last information ($offset+$s_tmp) is the offset wrt array  @stats (first dimension), which later needs to be considered when updated statistics from motif hits 
			}else{
				$s_tmp=$ext_seq_l-$vs2_tmp[(3-$i)*2+1]-$vs2_tmp[(3-$i)*2];   # start=offset    but offset is recomputed for strand- as offset_strand- = total_region_l - seq_l - offset_strand+
				                                                             # with total_region_l = $ext_seq_l
				$e_tmp=$s_tmp+$vs2_tmp[(3-$i)*2+1]-1;                        # end=start+length-1
				print $fh_tmp "".$vs_tmp[(3-$i)*2]."\t".$vs_tmp[(3-$i)*2+1]."\t$scfv\t$strv\t$grp_id\t$seq_cntr\t${seq_cntr}_${s_tmp}_$offset\t$species_suffix\n";
			}

			# the data matrix will contain first for each pos of the sliding window and case the number of covered positions (=length of sliding window, with exceptions when sequences are shorter then $ext_seq_l)
			my $sum=0;

#print "s_tmp=$s_tmp  e_tmp=$e_tmp  offs=$offset\n";
			for(my $k=$s_tmp-$w_l+1;$k<=$e_tmp;$k++){
				if($k<=$s_tmp){$sum++}
				if($k+$w_l-1>$e_tmp){$sum--}
				if($k<0){next};	if($k>$seq_l-1){last}
				$stats[$k+$offset][$seq_cntr]+=$sum;
#print "k=$k  stats[".($k+$offset)."]+=".$sum."\n";

			}
		}
	}
}
close($fh);


#for(my $j=0;$j<@{$stats[0]};$j++){for(my $i=0;$i<@stats;$i++){
#	print "stats   $i  $j  $stats[$i][$j]\n";	
#}}

if($we_need_to_infer_bgpwms){
	my $j=0;
	for(my $i=0;$i<@regions_bgpwms;$i+=5){
		print $fh_tmp "$regions_bgpwms[$i]\t$regions_bgpwms[$i+1]\t$regions_bgpwms[$i+2]\t$regions_bgpwms[$i+3]\t$regions_bgpwms[$i+4]\tNA\tINFER_BG_PWM\t".$species_suffixes[$j++]."\n";
	}
}
@regions_bgpwms=();
@species_suffixes=();
close($fh_tmp);

# retrieve sequences
my %fastas=();
my $trash;
if(@fasta_split>1){  # user has specified data from several species = several FASTA files
	my ($fh,$col_ns_aref,$col_ids_aref,$comments)=open_fct_file($tmpfile_n,[],$MYCOMMAND);close($fh);  # only to get all column names from sequence description file
	my ($fh_tmp2,$tmpfile2_n) = tempfile( DIR => ".", UNLINK => 1);
	print $fh_tmp2 join("\t",@$col_ns_aref)."\tSEQ\n";  # write out header

	for(my $i=1;$i<@fasta_split;$i++){
		my @fs2=split(":",$fasta_split[$i]);  # species_id:/path/to/fasta
		my ($fh_tmp3,$tmpfile3_n) = tempfile( DIR => ".", UNLINK => 1);close($fh_tmp3);		
		$cmd="matt get_rows $tmpfile_n SPECIES]".$fs2[0]."[ > $tmpfile3_n";
		$trash=`$cmd`; die "Retrieving sequences failed (1) $trash" if $?!=0;
		$cmd="matt get_seqs $tmpfile3_n START END SCAFFOLD STRAND $fs2[1] | matt add_cols $tmpfile3_n -";
		$trash=`$cmd`; die "Retrieving sequences failed (2) $trash" if $?!=0;
		$cmd="matt add_rows $tmpfile2_n $tmpfile3_n";
		$trash=`$cmd`; die "Retrieving sequences failed (3) $trash" if $?!=0;
		unlink($tmpfile3_n);
	}
	
	move($tmpfile2_n,$tmpfile_n);
	
}else{  # all exons/introns come from the same species
	$cmd = "matt get_seqs $tmpfile_n START END SCAFFOLD STRAND $fasta | matt add_cols $tmpfile_n -";
	$trash=`$cmd`; die "Retrieving sequences failed (5) $trash" if $?!=0;
}

# learn bg pwms; one for each group
my @bg_pwm_fns=();
my @seqs_fns=();
if($we_need_to_infer_bgpwms){
	for(my $i=0;$i<$ngrps;$i++){
		my $cmp="matt get_rows $tmpfile_n SEQ_ID]INFER_BG_PWM[ GROUP]".$gids_select[$i]."[ | get_cols - SEQ";
		my @seqs=`$cmd`; die "Retrieving sequences failed: $!" if $?!=0; 
		chomp(@seqs); shift(@seqs);
		# learn bg model 
		my ($bgpwm_aref)=learn_hmm0_as_pwm_from_seqs(\@seqs,$max_motif_l,$minprob,\@alph);
		# create temprary file for bg model
		(my $fh_tmp_bgpwm,$bg_pwm_fns[$i]) = tempfile( DIR => ".", UNLINK => 1, SUFFIX => ".tab") or die "$!";close($fh_tmp_bgpwm);
		# write bg model into file
		write_pwm($bgpwm_aref,$bg_pwm_fns[$i]);
	}
	
	# delete sequences for learing bg models as these should not be used for motif search
	my ($fh_tmp,$fn_tmp) = tempfile( DIR => ".", UNLINK => 1, SUFFIX => ".tab") or die "$!";close($fh_tmp);
	my $cmd = "matt get_rows $tmpfile_n !SEQ_ID]INFER_BG_PWM[ > $fn_tmp";
	my $trash=`$cmd`; die "$!" if $?!=0;
	move($fn_tmp,$tmpfile_n) or die "$!";
	
	# put sequences for motif search into different files, one for each group
	for(my $i=0;$i<$ngrps;$i++){
		# create temprary file for bg model
		(my $fh_tmp_bgpwm,$seqs_fns[$i]) = tempfile( DIR => ".", UNLINK => 1, SUFFIX => ".tab") or die "$!";close($fh_tmp_bgpwm);
		my $cmd="matt get_rows $tmpfile_n GROUP]".$gids_select[$i]."[ > ".$seqs_fns[$i]."";
		my $trash=`$cmd`;  die "Error during separating sequences per group: $!" if $?!=0;
	}
}


##########
## here starts motif search
##########
# matrix with statistics
# columns: pos of the sliding window
# rows   : number of cases (skipped exons or retained introns) 
my @stats2=();for(my $i=0;$i<4*$seq_l;$i++){push(@stats2,[])};
my @datacov=();

# get data coverage
for(my $i=0;$i<4*$seq_l;$i++){for(my $j=0;$j<@grps;$j++){my $v=$stats[$i][$j];if(!defined($v)){$v=0}
	#print "raw counts $i $j $v $grps[$j] $gids_select{$grps[$j]}\n";
	$stats2[$i][ $gids_select{$grps[$j]} ]+=$v;
}}
# normalize data coverage and save for output
for(my $j=0;$j<$ngrps;$j++){for(my $i=0;$i<4*$seq_l;$i++){
	my $v=$stats2[$i][$j]/($n_per_grp[$j]*$w_l);
	push( @datacov , $v );
}}
@stats2=();for(my $i=0;$i<4*$seq_l;$i++){push(@stats2,[])};

#print "length(stats2)=".scalar(@stats2)."\n";

my @out=();   # in the end contains three values per index:  data coverage <TAB> enrichment score <TAB> p value
# go over motifs
my @motif_ns=();
($fh,$col_ns_aref,$col_ids_aref,$comments)=open_fct_file($f2,[$c_t,$c_n,$c_e_f,$c_thresh,$c_bgmodel],$MYCOMMAND);
($t,$n,$f_e,$thresh,$bgmodel)=($col_ids_aref->[0],$col_ids_aref->[1],$col_ids_aref->[2],$col_ids_aref->[3],$col_ids_aref->[4]);
my @hits;
my $exit_status;
my @motifnames_output=();
while(<$fh>){$_=clean_line_leave_quotes($_); if(length($_)==0 || substr($_,0,1) eq "#"){next;} my @fs=split_line($_); if(!defined($chosen_motifs{$fs[$n]})){next;} push(@motifnames_output,$fs[$n]);

	push(@motif_ns,$fs[$n]);
	
	if($fs[$t] eq "REGEXP"){
			@hits=`matt get_regexp_hits $tmpfile_n SEQ SEQ_ID '$fs[$f_e]'`;$exit_status=$?;
			#print "matt get_regexp_hits $tmpfile_n SEQ SEQ_ID '$fs[$f_e]'";
	}else{
		if($fs[$bgmodel] eq "INFER_BGPWM"){  # inferred bg pwm; one for each group
			@hits=`matt get_pwm_hits $seqs_fns[0] SEQ SEQ_ID $fs[$f_e] $fs[$thresh] $bg_pwm_fns[0]`;$exit_status=$?;die "Error while searching for hits of motif $fs[$n]: $!" if $exit_status=$?!=0;
			for(my $i=1;$i<$ngrps;$i++){
				my @hits_tmp=`matt get_pwm_hits $seqs_fns[$i] SEQ SEQ_ID $fs[$f_e] $fs[$thresh] $bg_pwm_fns[$i]`;$exit_status=$?;die "Error while searching for hits of motif $fs[$n]: $!" if $exit_status=$?!=0;
				shift(@hits_tmp);
				@hits=(@hits,@hits_tmp);
			}
		}elsif($fs[$bgmodel] eq "NA"){ # no bg pwm
			@hits=`matt get_pwm_hits $tmpfile_n SEQ SEQ_ID $fs[$f_e] $fs[$thresh]`;$exit_status=$?;
		}else{  # one user specified bg pwm
			@hits=`matt get_pwm_hits $tmpfile_n SEQ SEQ_ID $fs[$f_e] $fs[$thresh] $fs[$bgmodel]`;$exit_status=$?;
		}
	}
	die "Error while searching for hits of motif $fs[$n]: $!" if $exit_status=$?!=0;
	shift(@hits);  # remove header
	chomp(@hits);

	# read hit statistics
	my $extension_mode=0;
	my ($seq_cntr,$offset_local,$offset_global,$s_tmp,$e_tmp);
	for(my $i=0;$i<4*$seq_l;$i++){for(my $j=0;$j<@grps;$j++){$stats2[$i][$j]=0;}}
	for(my $i=0;$i<@hits;$i++){
		unless($extension_mode){
			@fs=split("\t",$hits[$i]);
			($seq_cntr,$offset_local,$offset_global)=split("_",$fs[0]);
			$s_tmp=$fs[1]-1;   # first pos is encoded by 1  -> needs to be changed for starting index = 0
			$e_tmp=$fs[2]-1;
		}

		if($i<@hits-1){   # we have yet another hit
			my @fs2=split("\t",$hits[$i+1]);   # $fs2[1]-1 = start2   $fs2[2]-1 = end2  $fs2[0] = SEQ_ID

			if($fs2[1]-1<=$e_tmp){
				my ($seq_cntr2,$offset_local2,$offset_global2)=split("_",$fs2[0]);

				if($seq_cntr==$seq_cntr2 && $offset_local==$offset_local2 && $offset_global==$offset_global2){   # overlapping hit
					$extension_mode=1;
					my $e_tmp2=$fs2[2]-1;
					if($e_tmp2>$e_tmp){ $e_tmp=$e_tmp2; }  # extend region covered by motifs   strange case: |s......|s2...e2|....e|  second motif inside first motif
					next;
				}else{
					$extension_mode=0;
				}
			}else{
					$extension_mode=0;
			}
		}

		my $sum=0;
#print "offs_l=$offset_local  offs_g=$offset_global  s_tmp=$s_tmp  e_tmp=$e_tmp\n"; 
		for(my $k=$s_tmp-$w_l+1;$k<=$e_tmp;$k++){
			if($k<=$s_tmp){$sum++}
			if($k+$w_l-1>$e_tmp){$sum--}
			if($k+$offset_local<0){next}; if($k+$offset_local>$seq_l-1){last}
			$stats2[$k+$offset_local+$offset_global][$seq_cntr]+=$sum;
#print "".($k+$offset_local+$offset_global)."  sum=$sum\n";
		}
	}

	# delete huge array
	@hits=();

sub get_test_stats{
	my $stats_aref=$_[0];
	my $grps_aref=$_[1];
	my $gids_select_href=$_[2];
	my $enrichments_aref=$_[3];
	my $norm_consts_aref=$_[4];
	my $diffs_aref=$_[5];
	my $ns_aref=$_[6];
	my $permute=$_[7];

	my $npos=scalar(@$stats_aref);
	my $ngrps=scalar(keys %$gids_select_href);
	my $nobs=scalar(@{$grps_aref});

	# re-setting data structures
	for(my $i=0;$i<$npos;$i++){for(my $j=0;$j<$ngrps;$j++){
		$enrichments_aref->[$i][$j]=0;
		$norm_consts_aref->[$i][$j]=0;
	}}

	my $grps_aref_2=[];  # contains permuted or un-permuted group ids for observations
	# summation of summands for each group and each position of the sliding window
	for(my $i=0;$i<$npos;$i++){

		if($permute){   # for position i we first check which observations are available at this position
		                # and then permute their group ids
		    my @grpids_tmp=();
		    # get group ids of available observations at position i of sliding window
			for(my $j=0;$j<$nobs;$j++){
				$grps_aref_2->[$j]=$grps_aref->[$j];
				if($ns_aref->[$i][$j]==0){next}
				push(@grpids_tmp,$grps_aref->[$j]);		
			}
			
			# permute available grpids
			shuffle_elements_in_aref(\@grpids_tmp);
			
			# set permuted grp ids
			my $c2=0;
			for(my $j=0;$j<$nobs;$j++){
				if($ns_aref->[$i][$j]==0){next}
				$grps_aref_2->[$j]=$grpids_tmp[$c2++];
			}
			
		}else{
			$grps_aref_2=$grps_aref;   # we use the original group ids without permuting them
		}
		
		for(my $j=0;$j<$nobs;$j++){
			my $nc=$ns_aref->[$i][$j];
#if(!defined($nc)){print "ns->[$i][$j] is undef\n"}
			if($nc==0){next;} # no sequence coverage at this pos in the range of 1 to Lmax  (a sequence shorter than Lmax)
			my $v=$stats_aref->[$i][$j];
			my $grp=$gids_select_href->{$grps_aref_2->[$j]};
			$enrichments_aref->[$i][$grp]+=$v;
			$norm_consts_aref->[$i][$grp]+=$nc;
		}
	}

	# normalization according to the number of summands for each group and position of the sliding window
	for(my $i=0;$i<$npos;$i++){for(my $j=0;$j<$ngrps;$j++){ 
		my $nc=$norm_consts_aref->[$i][$j];
		unless($nc==0){
			$enrichments_aref->[$i][$j]/=$nc;
		}else{
			$enrichments_aref->[$i][$j]="NA";  # no data coverage here at all in this group
		}
	}}

	# computation of differences between enrichment scores of all groups vs reference group (which is always the last group)
	for(my $i=0;$i<$npos;$i++){for(my $j=0;$j<$ngrps-1;$j++){
		my $v1=$enrichments_aref->[$i][$j];
		my $v2=$enrichments_aref->[$i][$ngrps-1];  # reference group
		if($v1 eq "NA" || $v2 eq "NA"){
			$diffs_aref->[$i][$j]="NA";
		}else{
			$diffs_aref->[$i][$j]= $v1 - $v2 ;
		}
	}}

return();
}

	my @enrichment_scores=();     # to be plotted
	my @diffs=();                 # for p-value estimation
	my @norm_consts=();
	for(my $i=0;$i<$ngrps;$i++){push(@enrichment_scores,[]); push(@norm_consts,[]); if($i<$ngrps-1){push(@diffs,[]);}}


#print "length(stats2)=".scalar(@stats2)."\n"; die;
#for(my $i2=0;$i2<@stats;$i2++){for(my $j2=0;$j2<@{$stats[$i2]};$j2++){my $v2=$stats[$i2][$j2]; if(defined($v2)){print "$v2\t"}else{print "undef"}}print "\n"}


	# enrichment_scores contains enrichment scores to be plotted
	# diffs contains differences of enrichment scores for each group and position of sliding window wrt. reference group (which is always last group)
	get_test_stats(\@stats2,\@grps,\%gids_select,\@enrichment_scores,\@norm_consts,\@diffs,\@stats,0);

	# save for output
	my $out_c=0;
	for(my $j=0;$j<$ngrps;$j++){for(my $i=0;$i<4*$seq_l;$i++){
		my $v=$diffs[$i][$j];if(!defined($v)){$v="NA";}
		$out[$out_c].="\t$datacov[$out_c]\t$enrichment_scores[$i][$j]\t$v";
		$out_c++;
		#print "$i, $j  :  $out[$out_c-1]\n";
	}}

	
	if($pval<1 || $fdr<1){
	
		my @pvals=();
		my @diffs2=();
		my @enrichment_scores2=();
		my @n_successful_perms=();
		for(my $i=0;$i<$ngrps;$i++){push(@enrichment_scores2,[]); if($i<$ngrps-1){push(@n_successful_perms,[]); push(@diffs2,[]);push(@pvals,[]);}}

		# permutation test
		for(my $p=0;$p<$nperms;$p++){
			get_test_stats(\@stats2,\@grps,\%gids_select,\@enrichment_scores2,\@norm_consts,\@diffs2,\@stats,1);
			for(my $j=0;$j<$ngrps-1;$j++){for(my $i=0;$i<@stats;$i++){
				my $v1=$diffs[$i][$j];  # original
				my $v2=$diffs2[$i][$j]; # with perumted group ids
				if($v1 ne "NA" && $v2 ne "NA"){
					$n_successful_perms[$i][$j]++;
					if(abs($v2) >= abs($v1)){$pvals[$i][$j]++}
				}
			}}
		}

		# compute p values and save for output
		$out_c=0;
		for(my $j=0;$j<$ngrps;$j++){for(my $i=0;$i<4*$seq_l;$i++){
			my $v=$pvals[$i][$j];
			if(!defined($v)){$v=0;}
			if(!defined($n_successful_perms[$i][$j])){$out[$out_c++].="\tNA";next;}  # regions not covered by any sequence of at least one group or values for refrence group
			$out[$out_c++].="\t".(($v+1)/($n_successful_perms[$i][$j]+1));
		}}
		@pvals=();
	}else{
		$out_c=0;
		for(my $j=0;$j<$ngrps;$j++){for(my $i=0;$i<4*$seq_l;$i++){
			$out[$out_c++].="\t1";
		}}
	}

}# while


# output file for statistics which will be then turned into plots with R
# create temporary file; will be deleted automatically 
my ($fh_r,$rfile_n) = tempfile( DIR => ".", UNLINK => 0);
foreach my $o (@out){
	my @fs=split("\t",$o);
	print $fh_r join("\t",@fs[1..(scalar(@fs)-1)])."\n";
}
close($fh_r);

# output motif names into file
my ($fh_r2,$rfile_n2) = tempfile( DIR => ".", UNLINK => 0); print $fh_r2 join("\n",@motifnames_output)."\n"; close($fh_r2);

my $cwd=getcwd;
chomp(my $mattversion=`matt --version`);
# output information for plotting under R
$cmd="$scr_abspath/../utilities/plot_rnamaps.r $scr_abspath/.. $out_dir $rfile_n $wex_l $wint_l $w_l $mode $ex_or_int_view $nperms pval=$pval fdr=$fdr $f2 $rfile_n2 ".join(",",@gids_select)." ".join(",",@n_per_grp)." $ymax $mattversion";

print `$cmd`;

# delete file with bg model
foreach my $f (@bg_pwm_fns){
	unlink($f);
}

print "\n\n$cmd\n\n";


if($pval<1){
	print "\n\nIf you want to re-generate the motif RNA map with a different threshold for the p value, repeat the call above where you need to change the value of the p value.\n";
}
if($fdr<1){
	print "\n\nIf you want to re-generate the motif RNA map with a different threshold for the FDR, repeat the call above where you need to change the value of the FDR.\n";
}


exit(0); # success
__END__
