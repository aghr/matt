#!/usr/bin/env perl
use strict;
use warnings;

use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.


my $MYCOMMAND="get_regexp_hits";

# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "seq_analysis\t$MYCOMMAND\t: \tget REGEXP hits";
        exit(1);
}


if(!defined($ARGV[0]) || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/ || @ARGV<4 || @ARGV>5){
	print "\nmatt $MYCOMMAND <TABLE> <C_SEQ> <C_ID> <SEARCH_PATTERN> [<SEARCH_MODE>]\n\n";
	print "   ... search hits of Perl regular expression SEARCH_PATTERN in genomic sequences\n\n";
	print "   <TABLE>          : tab-separated table with sequences\n";
	print "   <C_SEQ>          : name of column with sequences\n";
	print "   <C_ID>           : name of column with sequence ids\n";
	print "   <SEARCH_PATTERN> : e.g., AGGC, GUCCA, Hello, \'ACCG|GCCA\' or any Perl regexp.\n";
	print "                      It might be necessary to put the pattern into \' \'.\n";
	print "   <SEARCH_MODE>    : single (default; seqs as given), or double (seqs as given and their rc)\n\n";
	print "   Output: table describing hits (case insensitive) with columns SEQ_ID, START, END, STRAND, HIT to STDOUT\n";
	print "           Always START <= END; and both are always defined wrt. to sequences as given, not to their RCs.\n";
	print "           Sequence positions start with 1.\n";
	print "\n";
	exit(1);
}

# arguments 
my ($fn,$seq_col,$id_col,$search_string,$search_mode)=@ARGV;
unless($search_mode){$search_mode="single";}
if($search_mode ne "single" && $search_mode ne "double"){die "$MYCOMMAND: SEARCH_MODE given as $search_mode must be either single or double."}


# read in feature-column table with sequences 
my ($fh,$col_ns_aref,$col_ids_aref,$comments)=open_fct_file($fn,[$id_col,$seq_col],$MYCOMMAND);

# print new header
print "SEQ_ID\tSTART\tEND\tSTRAND\tHIT\n";

while(<$fh>){
	$_=clean_line($_);
	if(length($_)==0 || substr($_,0,1) eq "#"){next;}
	my @fs=split_line($_);
	
	my $seq_id=$fs[ $col_ids_aref->[0] ];
	my $seq=$fs[ $col_ids_aref->[1] ];
	
	my @hits=();
	my @starts=();
	my @ends=();
	my @ids=();
	my @strands=();
	my $regexp="(?=(".$search_string."))";
	
	while ($seq =~ /$regexp/gi){	my $s=pos($seq)+1; # positions of hits should start with 1 not with 0
					my $e=$s+length($1)-1;
					print "$seq_id\t$s\t$e\t+\t$1\n";
	}
	if($search_mode eq "double"){
		$seq=rvcmplt($seq);
		while ($seq =~ /$regexp/gi){	my $e=length($seq)-pos($seq)-1;
						my $s=$e-length($1)+1;
						print "$seq_id\t$s\t$e\t-\t$1\n";
		}
	}
}

close($fh);


exit(0); # success
__END__