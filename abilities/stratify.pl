#!/usr/bin/env perl
use strict;
use warnings;

use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.


my $MYCOMMAND="stratify";

# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "extras\t$MYCOMMAND\t:     \tstratify data in table";
        exit(1);
}


if(!defined($ARGV[0]) || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/ || @ARGV!=5){
        print "\nmatt $MYCOMMAND <TABLE> <C_STRATIFY> <C_GROUP> <REF_GROUP> <MAX_DEV>\n\n";
        print "   ... stratify ONE data set wrt. to a reference data set. Involves randomnization,\n";
	print "       i.e., repeated runs might give different stratified data sets.\n\n";
        print "   <TABLE>      : tab-separated table\n";
        print "   <C_STRATIFY> : name of column with numerical feature according to which stratification should be done\n";
        print "   <C_GROUP>    : name of column with group IDs; MUST contain only two groups!\n";
        print "                  One of the groups is the refrence group, the other group will get stratified.\n";
        print "   <REF_GROUP>  : group ID of reference group. The other group will be sampled down while trying to make\n";
        print "                    their distribution of feature <C_STRATIFY> similar to those of the refrence group\n";
        print "   <MAX_DEV>    : the algorithm tries to find for each element of reference group one element of the other\n";
        print "                    group with a similar value in C_STRATIFY. MAX_DEV is the maximal deviation (+-) allowed for\n";
        print "                    a element of the other group to be picked. If for one element of the reference group there\n";
        print "                    is no element in the other group with similar C_STRATIFY value, this element will be dropped.\n";
        print "                    Hence, you might try several different values for MAX_DEV until you get a acceptable result.\n";
        print "\n";
        print "   Output: a table like TABLE with the down-sampled data sets\n";
        print "\n";
        exit(1);
}


my ($fn,$cn_strat,$cn_group,$ref_group,$max_dev)=@ARGV;

my ($fh,$col_ns_aref,$col_ids_aref,$comments) = open_fct_file($fn,[$cn_strat,$cn_group],$MYCOMMAND);

# values of reference group
my @vs_ref=();
my @lines_ref=();
# values of others
my @vs_nonref=();
my @lines_nonref=();


my %groups=();
my @fs;
while(<$fh>){
	$_=clean_line($_);
	if(length($_)==0 || substr($_,0,1) eq "#"){next;}
	my @fs=split_line($_);

	$groups{ $fs[ $col_ids_aref->[1] ] }=1;
	
	if( $fs[ $col_ids_aref->[1] ] eq $ref_group ){
		push(@vs_ref,$fs[ $col_ids_aref->[0] ]);
		push(@lines_ref,join("\t",@fs));
	}else{
		push(@vs_nonref,$fs[ $col_ids_aref->[0] ]);
		push(@lines_nonref,join("\t",@fs));
	}
}
close($fh);

my @groups=keys %groups;
if(@groups != 2){die "matt stratify only works properly if the input table contains exactly two groups, but $fn contains ",(scalar @groups)," groups in column ",$cn_group,". You could sub-select from the table rows corresponding to only two groups with get_rows.";}

#print "l(vs_ref)=".scalar(@vs_ref)."\n";
#print "l(ls_ref)=".scalar(@lines_ref)."\n";
#print "l(vs_nonref)=".scalar(@vs_nonref)."\n";
#print "l(ls_nonref)=".scalar(@lines_nonref)."\n";

# permute order
my $id_tmp;
my @ids_tmp=();my @ids_tmp_tmp=0..(@vs_ref-1);
for(my $i=0;$i<@vs_ref;$i++){$id_tmp=int(rand(@ids_tmp_tmp));push(@ids_tmp,$ids_tmp_tmp[$id_tmp]);splice @ids_tmp_tmp, $id_tmp, 1;}
@vs_ref=@vs_ref[@ids_tmp];@lines_ref=@lines_ref[@ids_tmp];
@ids_tmp=();@ids_tmp_tmp=0..(@vs_nonref-1);
for(my $i=0;$i<@vs_nonref;$i++){$id_tmp=int(rand(@ids_tmp_tmp));push(@ids_tmp,$ids_tmp_tmp[$id_tmp]);splice @ids_tmp_tmp, $id_tmp, 1;}
@vs_nonref=@vs_nonref[@ids_tmp];@lines_nonref=@lines_nonref[@ids_tmp];


#print "l(vs_ref)=".scalar(@vs_ref)."\n";
#print "l(ls_ref)=".scalar(@lines_ref)."\n";
#print "l(vs_nonref)=".scalar(@vs_nonref)."\n";
#print "l(ls_nonref)=".scalar(@lines_nonref)."\n";

#for(my $i=0;$i<20;$i++){print "$vs_nonref[$i]\n";print "$lines_nonref[$i]\n";}

#my $count=0;

my $v_ref;
print join("\t",@$col_ns_aref)."\n";
for(my $i=0;$i<@vs_ref;$i++){
	$v_ref=$vs_ref[$i];
	for(my $j=0;$j<@vs_nonref;$j++){
		if(abs($v_ref-$vs_nonref[$j])<$max_dev){
			print $lines_ref[$i]."\n";
			print $lines_nonref[$j]."\n";
			#print "$i->$j: $v_ref->$vs_nonref[$j]\n";
			
			# delete this value from non-ref values
			splice @vs_nonref, $j, 1;
			splice @lines_nonref, $j, 1;
			
			last;
		}else{
			#print "$i->non hit: $v_ref->$vs_nonref[$j]\n";
		}
	}
}


exit(0); # success
__END__