#!/usr/bin/env perl
use strict;
use warnings;
use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.


my $MYCOMMAND="chg_labs";

# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "manipulation\t$MYCOMMAND\t:   \tchange labels";
        exit(1);
}

if(!defined($ARGV[0]) || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/ || @ARGV<3){
        print "\nmatt $MYCOMMAND <TABLE> <C_WITH_LABS> <LAB1_ORIG>:<LAB1_NEW> [<LAB2_ORIG>:<LAB2_NEW> ... ]\n\n";
	print "   ... change labels (e.g. group IDs) in a column\n\n";
	print "   <TABLE>                : tab-separated table\n";
	print "   <C_WITH_LABS>          : name of column with labels to be changed\n";
	print "   <LAB1_ORIG>:<LAB1_NEW> : a pair colon-separated of the original label and the new\n";
	print "                              label into which it should be changed, e.g., bg3:BG\n";
	print "   Output : TABLE with changed labels written to STDOUT\n";
	print "\n";
        exit(1);
}


my $fn=$ARGV[0];
my $cn=$ARGV[1];
my %new_lab=();
for(my $i=2;$i<@ARGV;$i++){my @fs=split(":",$ARGV[$i]);$new_lab{$fs[0]}=$fs[1];}

my ($fh,$col_ns_aref,$col_ids_aref,$comments)=open_fct_file($fn,[$cn],$MYCOMMAND);
my $c_id=$col_ids_aref->[0];

# OUTPUT all into temp file
print $comments;
print join("\t",@{$col_ns_aref})."\n";

while(<$fh>){
	$_=clean_line($_);
	if(length($_)==0){next}
	if(substr($_,0,1) eq "#"){print "$_\n";next;}
	my @fs=split_line($_);
	if(defined($new_lab{$fs[$c_id]})){$fs[$c_id]=$new_lab{$fs[$c_id]};}
	print join("\t",@fs)."\n";
}

close($fh); 


exit(0); # success
__END__