#!/usr/bin/env perl
use strict;
use warnings;
use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.


my $MYCOMMAND="chk_nls";

# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "import_table\t$MYCOMMAND\t:  \tcheck newlines in table";
        exit(1);
}

if(!defined($ARGV[0]) || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/ || @ARGV<1){
        print "\nmatt $MYCOMMAND <TABLE>\n\n";
        print "   ... visualizes the different newline charcters in a table.\n";
        print "       Useful if you have problems with your table due to Windows/old Mac newlines.\n\n";
        print "   <TABLE>  : tab-separated table\n\n";
        print "   Output to STDOUT: the first 10 lines of table making explicit newline characters.\n\n";
        print "   Note: two different newline encodings exists: '\\n' = line feed (LF) and '\\r' = carriage return (CR).\n";
        print "         Unix/Mac use LF (ignoring old-style Mac semantics), Windows uses the combination CRLF.\n";
        print "         In text editors, \\r sometimes appears as -M or \^M\n\n";
        print "   According to the newlines used in your file, you might want to:\n";
        print "   * delete all CR      : > perl -p -e 's/\\r//g' < table.tab > out_table.tab      (table.tab from Windows, you work under Unix)\n";
        print "   * change CR for LF   : > perl -p -e 's/\\r/\\n/g' < table.tab > out_table.tab    (table.tab from old-style Mac, you work under Unix)\n";
        print "   * delete all LF      : > perl -p -e 's/\\n//g' < table.tab > out_table.tab      (only here for completeness)\n";
        print "   * change LF for CR   : > perl -p -e 's/\\n/\\r/g' < table.tab > out_table.tab    (table.tab from Unix, you work under old MacOS)\n";
        print "   * change LF for CRLF : > perl -p -e 's/\\n/\\r\\n/g' < table.tab > out_table.tab  (table.tab from Unix, you want it to work under Windows)\n";
        print "   * change CR for CRLF : > perl -p -e 's/\\r/\\r\\n/g' < table.tab > out_table.tab  (table.tab from old MacOS, you want it to work under Windows)\n";
        print "\n";
        exit(1);
}

open(my $fh,$ARGV[0]) or die "$!";
my $c=0;
while(<$fh>){$c++; if($c>10){last;};
	
	my $line=$_;
	$line =~ s/\r/[CR]/g;
	$line =~ s/\n/[LF]/g;
	
	#my @fs=split("\r",$_,-1);
	#for(my $i=0;$i<@fs;$i++){
	#	my @fs2=split("\n",$fs[$i],-1);
	#	for(my $j=0;$j<@fs2;$j++){
	#		if($i==0 && $j==0 && length($fs2[$j])==0){
	#			$line.="LF";
	#		}else{
	#			$line.="...LF";
	#		}
	#	}
	#	if($i==0 && length($fs[$i])==0){
	#			$line.="CR";
	#	}else{
	#			$line.="...CR";
	#	}
	#}
	
	print "$line\n";
}

close($fh);

exit(0); # success
__END__