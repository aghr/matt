#!/usr/bin/env perl
use strict;
use warnings;
use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.


my $MYCOMMAND="def_cats";

# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "manipulation\t$MYCOMMAND\t:   \tdefine categories";
        exit(1);
}

if(!defined($ARGV[0]) || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/ || @ARGV<3){
        print "\nmatt $MYCOMMAND <TABLE> <OUTPUT_COLUMN_NAME> '<GRP_LAB1>=<GRP_DEF1>' ['<GRP_LAB2>=<GRP_DEF2>' ...]\n\n";
	print "   ... define groups of rows (e.g., groups of exons), assign a group label to each row.\n";
	print "       according to the group it belongs to.\n\n";
	print "   <TABLE>                : tab-separated table with columns describing features, like length etc.\n";
	print "   <OUTPUT_COLUMN_NAME>   : name of column with group labels\n";
	print "   <GRP_LAB>=<GRP_DEF>    : a pair of a group label and the corresponding group definition.\n";
	print "                            You need to put the entire expression in '...'\n";
	print "   <GRP_LAB>                Group lables should contain only letters, numbers and under score, e.g. up_regulated.\n";
	print "   <GRP_DEF>                Group definition where you exploit the capabilities and syntax of the get_rows commands.\n";
	print "                            You might have a look to the help message of get_rows.\n";
	print "                            Example assuming columns LENGTH and DPSI exist in TABLE (generated column has name GROUP):\n";
	print "                            matt $MYCOMMAND TABLE GROUP 'reg_up_short=LENGTH[0,100] DPSI[15,100]' 'reg_down_long=LENGTH[500,5000] DPSI[-100,-15]'\n";
	print "\n";
	print "   Output : a table consisting only of column <OUTPUT_COLUMN_NAME> which has the same number of rows than TABLE to STDOUT\n";
	print "            In each row it has the group label of the cooresponding group of this row. If group definitions overlap,\n";
	print "            the label of the group definition which applies to the row and which is listed first in the Matt call\n";
	print "            will be output for this row. If no group definition applies, the NA will be output\n";
	print "\n";
        exit(1);
}


sub get_colid{
	my $colns_aref=$_[0];
	my $cn=$_[1];
	my $id=-1;
	my $c=0;
	for(my $i=0;$i<@$colns_aref;$i++){if($colns_aref->[$i] eq $cn){$c++;$id=$i;}}
	if($c>1){die "Column $cn found several times in table";}
	if($id==-1){die "Column $cn not found in table";}
return($id);
}


my $fn=$ARGV[0];
my $cn_out=$ARGV[1];

my ($fh,$col_ns_aref,$col_ids_aref,$comments)=open_fct_file($fn,[],$MYCOMMAND);
close($fh);

my @cat=();
my @negation=();
my @constr_col_ids=();
my @constr_type=();
my @minmax=();
my @cats=();

my ($arg,$col_n,$neg_aref,$min_aref,$max_aref,$cats_href,$colids_aref,$type_aref,$minmax_aref,$col_ns_aref_tmp);
for(my $i=2; $i<@ARGV; $i++){
	my @fs=split("=",$ARGV[$i]);
	$cat[$i-2]=$fs[0];
	@fs=split(/\s+/,join("=",@fs[1..(@fs-1)]));
	
	$neg_aref=[];
	$cats_href=[];
	$colids_aref=[];
	$type_aref=[];
	$minmax_aref=[];

	for(my $j=0;$j<@fs;$j++){
		$arg=$fs[$j];
		# numeric constraint
		if( $arg =~ /(.*)\[(.+)\]/ ){
			$col_n=$1;
			my @mins_maxs=split(",",$2);

			if(substr($col_n,0,1) eq "!"){$neg_aref->[$j]=1;$col_n=substr($col_n,1);}else{$neg_aref->[$j]=0;}
			$colids_aref->[$j]=get_colid($col_ns_aref,$col_n);
			$type_aref->[$j]="n";

			if(@mins_maxs % 2){die "$MYCOMMAND: number of given values in numerical constraint $arg is not even."}
			for(my $k=0;$k<@mins_maxs;$k+=2){
				my $min=$mins_maxs[$k];
				my $max=$mins_maxs[$k+1];
				if($min>$max){die "$MYCOMMAND: MIN ($min) > MAX ($max) for contraint $arg.";}
				if(!is_numeric($min) || !is_numeric($max)){die "$MYCOMMAND: MIN and/or MAX of constraint $arg are not numeric.";}
			}
			$minmax_aref->[$j]=[@mins_maxs];
			next;
		}

		# cardinal constrain
		if($arg =~ /(.*)\](.+)\[/){
			$col_n=$1;
			my @fs2=split(",",$2);
			if(substr($col_n,0,1) eq "!"){$neg_aref->[$j]=1;$col_n=substr($col_n,1);}else{$neg_aref->[$j]=0;}
			$colids_aref->[$j]=get_colid($col_ns_aref,$col_n);
			$type_aref->[$j]="c";

			my $href={};
			if(@fs2==2 && -e $fs2[0]){ # input is table from which we take all elements in specified column
				($fh,$col_ns_aref_tmp,$col_ids_aref,$comments)=open_fct_file($fs2[0],[$fs2[1]],$MYCOMMAND);
				while(<$fh>){$_=clean_line($_);if(length($_)==0 || substr($_,0,1) eq "#"){next;}my @fs3=split_line($_);
					$href->{$fs3[$col_ids_aref->[0]]}=1;				
				}
				close($fh);
			}else{ # input is a list of values	
				foreach my $v (@fs2){$href->{$v}=1;}
			}
			$cats_href->[$j]=$href;
			next;
		}
	
		# constraint for checking for empty entries
		if($arg =~ /(.*)\]\[/){
			$col_n=$1;
			if(substr($col_n,0,1) eq "!"){$neg_aref->[$j]=1;$col_n=substr($col_n,1);}else{$neg_aref->[$j]=0;}
			$colids_aref->[$j]=get_colid($col_ns_aref,$col_n);
			$type_aref->[$j]="e";
			next;
		}

		die "$MYCOMMAND: I do not understand constraint $arg.";
	}
	
	$negation[$i-2]=$neg_aref;
	$constr_col_ids[$i-2]=$colids_aref;
	$constr_type[$i-2]=$type_aref;
	$minmax[$i-2]=$minmax_aref;
	$cats[$i-2]=$cats_href;
}

($fh,$col_ns_aref,$col_ids_aref,$comments)=open_fct_file($fn,[],$MYCOMMAND);
print "$cn_out\n";
while(<$fh>){
	$_=clean_line($_);
	if(length($_)==0){print "#\n";next;} 
	if(substr($_,0,1) eq "#"){print "#\n";next;}
	my @fs=split_line($_);
	
	my $one_cat_matched=0;
	CATS:
	for(my $i=0;$i<@cat;$i++){
		
		if($one_cat_matched==1){last;}
		
		for(my $j=0;$j<@{$constr_type[$i]};$j++){

			my $v=$fs[ $constr_col_ids[$i]->[$j] ];
		
			# numeric constraint
			if($constr_type[$i]->[$j] eq "n" ){
				if(!defined($v) || $v eq "NA"){next CATS;}
				my $v_in_user_defined_intervals=0;
				my @mins_maxs=@{$minmax[$i]->[$j]};
				for(my $k=0;$k<@mins_maxs;$k+=2){
					# value is in one of the user defined min;max intervals
					if($mins_maxs[$k]<=$v && $v<=$mins_maxs[$k+1]){$v_in_user_defined_intervals=1;last;}
				}
				if( $negation[$i]->[$j] &&  $v_in_user_defined_intervals){next CATS;}
				if(!$negation[$i]->[$j] && !$v_in_user_defined_intervals){next CATS;}
			}

			# cardinal constraint
			if($constr_type[$i]->[$j] eq "c"){
				if($negation[$i]->[$j]){
					if(defined($cats[$i]->[$j]->{$v})){next CATS;}
				}else{
					if(!defined($cats[$i]->[$j]->{$v})){next CATS;}
				}
			}

			# empty constraint
			if($constr_type[$i]->[$j] eq "e"){
				if($negation[$i]->[$j]){
					if($v eq ""){next CATS;}
				}else{
					if($v ne ""){next CATS;}
				}
			}
		}
		
		print "$cat[$i]\n";
		$one_cat_matched=1;
	}
	
	if($one_cat_matched==0){ print "NA\n";}
}


close($fh);

exit(0); # success
__END__