#!/usr/bin/env perl
use strict;
use warnings;
use File::Temp qw(tempfile);
use File::Copy qw(move);
use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.


# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "manipulation\tadd_val\t:    \tadd column with fixed value";
        exit(1);
}

my $MYCOMMAND="add_val";

if(!defined($ARGV[0]) || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/ || @ARGV!=3){
        print "\nmatt $MYCOMMAND <TABLE> <C_NAME> <VAL>\n\n";
        print "   ... add a column with constant values to right end of TABLE\n\n";
	print "   <TABLE>  : tab-separated table\n";
	print "   <C_NAME> : column name of new columns\n";
	print "   <VAL>    : column will contain this value across all rows of TABLE\n\n";
        print "   Output : table TABLE with addtional column C_NAME will be printed to screen\n\n";
        exit(1);
}


my ($file_n,$col_n,$val)=@ARGV[0..2];

open(my $fh,"<".$file_n) or die "Sorry, you requested add_val but: I cannot open file $file_n.";

my $found_header=0;
while(<$fh>){
	$_=clean_line($_);
	# remove empty line
	if(length($_)==0){next;}
	if(substr($_,0,1) eq "#"){print "$_\n"; next;}
	
	if($found_header==0){
		if(/$col_n/){die "Sorry, you requested add_val but: there is already a column $col_n in file $file_n.";}
		print "$_\t$col_n\n";
		$found_header=1;
		next;
	}
	
	print "$_\t$val\n";
}

close($fh);

exit(0); # success
__END__
