#!/usr/bin/env perl
use strict;
use warnings;
use File::Copy;
use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.
use File::Temp qw(tempfile);

my $MYCOMMAND="cmpr_features";
my @INTRON_FEATURES=qw/ INTRON_LENGTH UPEXON_MEDIANLENGTH DOEXON_MEDIANLENGTH RATIO_UPEXON_INTRON_LENGTH RATIO_DOEXON_INTRON_LENGTH INTRON_GCC UPEXON_GCC DOEXON_GCC RATIO_UPEXON_INTRON_GCC RATIO_DOEXON_INTRON_GCC SF1_HIGHESTSCORE_3SS INTRON_5SS_20INT10EX_GCC INTRON_3SS_20INT10EX_GCC MAXENTSCR_HSAMODEL_5SS MAXENTSCR_HSAMODEL_3SS DIST_FROM_MAXBP_TO_3SS SCORE_FOR_MAXBP_SEQ PYRIMIDINECONT_MAXBP POLYPYRITRAC_OFFSET_MAXBP POLYPYRITRAC_LEN_MAXBP POLYPYRITRAC_SCORE_MAXBP BPSCORE_MAXBP NUM_PREDICTED_BPS MEDIAN_DIST_FROM_BP_TO_3SS MEDIAN_SCORE_FOR_BPSEQ MEDIAN_PYRIMIDINECONT MEDIAN_POLYPYRITRAC_OFFSET MEDIAN_POLYPYRITRAC_LEN MEDIAN_POLYPYRITRAC_SCORE MEDIAN_BPSCORE MEDIAN_TR_LENGTH MEDIAN_INTRON_NUMBER INTRON_MEDIANRANK INTRON_MEDIANRELATIVERANK INTRON_MEDIANRELATIVERANK_3BINS INTRON_MEDIANRELATIVERANK_5BINS INTRON_MEDIANRELATIVERANK_10BINS PROP_FIRST_INTRON PROP_LAST_INTRON PROP_INTERNAL_INTRON PROP_INTRON_IN_UTR INTRON_COOCCURS_WITH_OTHER_INTRONS MEDIAN_INTRON_INTRON_COOCCURRENCE_NUMBER_OVER_ALL_TRS /;
my @EXON_FEATURES=qw/ EXON_LENGTH UPEXON_MEDIANLENGTH DOEXON_MEDIANLENGTH RATIO_UPEXON_EXON_LENGTH RATIO_DOEXON_EXON_LENGTH UPINTRON_MEDIANLENGTH DOINTRON_MEDIANLENGTH RATIO_UPINTRON_EXON_LENGTH RATIO_DOINTRON_EXON_LENGTH EXON_GCC UPINTRON_GCC UPEXON_GCC DOINTRON_GCC DOEXON_GCC RATIO_UPEXON_EXON_GCC RATIO_UPINTRON_EXON_GCC RATIO_DOINTRON_EXON_GCC RATIO_DOEXON_EXON_GCC SF1_HIGHESTSCORE_3SS_UPINTRON SF1_HIGHESTSCORE_3SS_DOINTRON UP_5SS_20INT10EX_GCC GCC_3SS_20INT10EX GCC_5SS_20INT10EX DO_3SS_20INT10EX_GCC MAXENTSCR_HSAMODEL_UPSTRM_5SS MAXENTSCR_HSAMODEL_3SS MAXENTSCR_HSAMODEL_5SS MAXENTSCR_HSAMODEL_DOWNSTRM_3SS DIST_FROM_MAXBP_TO_3SS_UPINTRON SCORE_FOR_MAXBP_SEQ_UPINTRON PYRIMIDINECONT_MAXBP_UPINTRON POLYPYRITRAC_OFFSET_MAXBP_UPINTRON POLYPYRITRAC_LEN_MAXBP_UPINTRON POLYPYRITRAC_SCORE_MAXBP_UPINTRON BPSCORE_MAXBP_UPINTRON NUM_PREDICTED_BPS_UPINTRON MEDIAN_DIST_FROM_BP_TO_3SS_UPINTRON MEDIAN_SCORE_FOR_BPSEQ_UPINTRON MEDIAN_PYRIMIDINECONT_UPINTRON MEDIAN_POLYPYRITRAC_OFFSET_UPINTRON MEDIAN_POLYPYRITRAC_LEN_UPINTRON MEDIAN_POLYPYRITRAC_SCORE_UPINTRON MEDIAN_BPSCORE_UPINTRON DIST_FROM_MAXBP_TO_3SS_DOINTRON SCORE_FOR_MAXBP_SEQ_DOINTRON PYRIMIDINECONT_MAXBP_DOINTRON POLYPYRITRAC_OFFSET_MAXBP_DOINTRON POLYPYRITRAC_LEN_MAXBP_DOINTRON POLYPYRITRAC_SCORE_MAXBP_DOINTRON BPSCORE_MAXBP_DOINTRON NUM_PREDICTED_BPS_DOINTRON MEDIAN_DIST_FROM_BP_TO_3SS_DOINTRON MEDIAN_SCORE_FOR_BPSEQ_DOINTRON MEDIAN_PYRIMIDINECONT_DOINTRON MEDIAN_POLYPYRITRAC_OFFSET_DOINTRON MEDIAN_POLYPYRITRAC_LEN_DOINTRON MEDIAN_POLYPYRITRAC_SCORE_DOINTRON MEDIAN_BPSCORE_DOINTRON MEDIAN_TR_LENGTH MEDIAN_EXON_NUMBER EXON_MEDIANRANK EXON_MEDIANRELATIVERANK EXON_MEDIANRELATIVERANK_3BINS EXON_MEDIANRELATIVERANK_5BINS EXON_MEDIANRELATIVERANK_10BINS PROP_FIRST_EXON PROP_LAST_EXON PROP_INTERNAL_EXON PROP_EXON_IN_UTR EXON_COOCCURS_WITH_OTHER_EXONS MEDIAN_EXON_EXON_COOCCURRENCE_NUMBER_OVER_ALL_TRS /;

# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "super\t$MYCOMMAND\t: \tfeature comparison";
        exit(1);
}

if(@ARGV<5 || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/){
    	print "\nmatt $MYCOMMAND <TABLE> -a <C_a1> <C_a2> <C_a3>... -b <C_b1> <C_b2> <C_b3>... [-points] [-outliers] ...\n";
    	print "      ... [-bins <N> or -quants <P1,P2,P3...,PN> or -quantss <P1,P2,P3...,PN>] [-o <OUTPUT_DIR>] [-p <PVAL>] [-colors]\n\n";
	print "   Compares (boxplot, Kruskal-Wallis test) features given by -a for all events (rows)\n";
	print "   binned into 3-10 bins according to one feature given by -b at a time. Outputs a summary PDF report,\n";
	print "   all boxplot graphics, and a table with results of all statistical tests.\n\n";
    	print "   Application: For exons/introns, compare features like PSI or dPSI wrt. their sequence/gene features\n";
    	print "   extracted by Matt. In this case, all -b features are numerical features Matt can extract for exons/introns.\n\n";
    	print "   For exons, call $MYCOMMAND like:\n";
    	print "\nmatt $MYCOMMAND <TABLE> -a <C_a1> <C_a2> <C_a2>... -mattexon <C_START> <C_END> <C_CHR> <C_STRAND> <C_GENEID> ...\n"; 
    	print "   ... <GTF> <FASTA> <SPECIES> <MAXLEN_BP_ANALYSIS> [-f <FIELD>]\n\n";
	print "   For introns, call $MYCOMMAND like:\n";
    	print "\nmatt $MYCOMMAND <TABLE> -a <C_a1> <C_a2> <C_a2>... -mattintron <C_START> <C_END> <C_CHR> <C_STRAND> <C_GENEID> ...\n"; 
    	print "   ... <GTF> <FASTA> <SPECIES> <MAXLEN_BP_ANALYSIS> [-f <FIELD>]\n\n";
    	print "\n";
    	print "   <TABLE>         : tab-separated table\n";
    	print "   -a <C_a1> <C_a2> <C_a3> : columns of TABLE with features you want boxplots / comparisons for.\n";
    	print "   -b <C_b1> <C_b2> <C_b3> : columns with features according to which events (rows of TABLE) should be binned.\n";
    	print "                             If feature is discrete, it is assumed to contain group ids and binning is done according to them.\n"; # XXX discrete
    	print "   -points         : Adds data points to the boxplots\n";
    	print "   -outliers       : Adds outlier data points to the boxplots\n";
    	print "   -bins <N>       : Number of equidistant bins. Default -b 3\n"; 
    	print "   -quants <P1,P2,P3...,PN> : Definition of bins by quantiles. Bin 1 would be P1<=x<P2, bin 2 P2<=x<P3, last bin PN-1<=x<=PN.\n";
    	print "                              All Ps must be >=0 and <=1. For 5 not-equidistant bins, you could use e.g. -quants 0,0.1,0.3,0.6,0.9,1\n";
    	print "                              You can leave out some data points at the beginning and end, e.g. -quants 0.05,0.1,0.3,0.6,0.9,0.95\n";
    	print "   -quantss <P1,P2,P3...,PN>: Allows you to define overlapping bins, and to leave out parts of the data.\n";
    	print "                              N must be even, and pairs of values define one bin, e.g., -quantss 0.05,0.3,0.2,0.4,0.7,0.9\n";
    	print "                              defines three intervals: [0.05,0.3) [0.2,0.4) [0.7,0.9]. The last bin is [], all others are [).\n";
    	print "   -o <OUTPUT_DIR> : output folder. Default is -o ./\n";
	print "   -p <PVAL>       : Significant results highlighted in the report are those with a p-value <= PVAL. Default is -p 0.05\n";
	print "   -colors         : Colors of the box plots; when omitted all boxes appear gray.\n";
    	print "                       You must specify one color for each bin have specified.\n";
    	print "                       For available colors see: http://www.stat.columbia.edu/~tzheng/files/Rcolor.pdf\n";
    	print "                       Example: if you chose 3 bins -colors:blue,blue,yellow .\n";
 	print "\n";
    	print "   <C_START>, <C_END>, <C_CHR>, <C_STRAND>, <C_GENEID> : names of columns of TABLE with the specified information on exons/introns\n";
    	print "   <GTF>           : a GTF file describing the gene structure; gene IDs in field gene_id (to change this default use argument -f <FIELD>)\n";
    	print "                       of GTF must match those in column C_GENEID of TABLE. The GTF should also contain information on\n";
    	print "                       gene type (conding, non-coding). It's recommended to use ENSEMBL GTFs\n";
    	print "   <FASTA>         : FASTA file with all genome sequences. Chromosome IDs must match those in column C_CHR in TABLE\n";
    	print "   <SPECIES>       : choose one from Hsap, Ptro, Mmul, Mmus, Rnor, Cfam, Btau.\n";
	print "                       Branch point features from SVM-BPfinder are only available for above listed species.\n";
	print "                       If your species is different but close to one listed species, you could use the closest one to get BP features.\n";
    	print "   <MAXLEN_BP_ANALYSIS> : length of the 3'-ends of introns scanned for branch point analysis, e.g., 150 in general should work OK.\n";
    	print "   -f FIELD        : Gene ids in TABLE and GTF must coincide. Often GTF contain in the last column different fields like gene_id or gene_name.\n";
    	print "                        FIELD is the field identifier to be used for extracting gene ids. Default is -f gene_id\n";
    	print "\n";
    	print "   Output: All output files will be put into folder OUTPUT_DIR. You'll find summary.pdf containing the report of the comparison,\n";
    	print "           but also a table with all statistics per feature across bins and all box plots as graphics files for later use.\n";
    	print "\n";
    	print "   Hint: For more information on exon features (and related arguments) see help message for matt get_efeatures.\n";
    	print "   Hint: To extract gene ids from GTF, you could use Matt's command retr_geneids. Add them to TABLE with Matt's command add_cols.\n";
    	print "\n";
exit(1);
}

my ($fn)=$ARGV[0]; unless(-e $fn){die "Cannot find input file $fn."}

# XXX always colors
my @featsa=();
my @featsb=();
my $colors="";
my $gid_search_pattern="gene_id";
my $pvalthresh=0.05;
my $outd="./";
my @bins=(0,0.333,0.333,0.666,0.666,1);  # three equidistant bins
my ($gtf,$fasta,$evttype,$cstart,$cend,$cchr,$cstr,$sp,$bplen,$cgid);
my $points="points:false";
my $outliers="outliers:false";
OUTER:for(my $i=1;$i<@ARGV;$i++){
	if($ARGV[$i] =~ /-colors:(.+)/){$colors=$1;next;}
	if($ARGV[$i] eq "-o"){$outd=$ARGV[$i+1];$i++;next;}
	if($ARGV[$i] eq "-f"){$gid_search_pattern=$ARGV[$i+1];$i++;next;}
	if($ARGV[$i] eq "-p"){$pvalthresh=$ARGV[$i+1];$i++;next;}
	if($ARGV[$i] eq "-points"){$points="points:true";next;}
	if($ARGV[$i] eq "-outliers"){$outliers="outliers:true";next;}
	if($ARGV[$i] eq "-a"){
		for(my $j=$i+1;$j<@ARGV;$j++){
			if($ARGV[$j] =~ /^-quants|^-colors|^-o|^-f|^-p|^-b|^-matt/){$i=$j-1;next OUTER}
			push(@featsa,$ARGV[$j]);
		}
	}
	if($ARGV[$i] eq "-b"){
		for(my $j=$i+1;$j<@ARGV;$j++){
			if($ARGV[$j] =~ /^-quants|^-colors|^-o|^-f|^-p|^-b|^-matt/){$i=$j-1;next OUTER}
			push(@featsb,$ARGV[$j]);
			if($j==@ARGV-1){last OUTER} # in case no other arguments are given and to avoid 'Do not understand argument' error
		}
	}
	if($ARGV[$i] =~ /^-matt/){
		if($ARGV[$i] eq "-mattexon"){$evttype="exon"}elsif($ARGV[$i] eq "-mattintron"){$evttype="intron"}else{die "Do not understand argument $ARGV[$i].\n"}
		($cstart,$cend,$cchr,$cstr,$cgid,$gtf,$fasta,$sp,$bplen)=@ARGV[($i+1)..($i+9)];
		$i+=9; next;
	}
	if($ARGV[$i] eq "-bins"){
		if($ARGV[$i+1]<2){die "Argument -bin N: N should be >=2 but it isn't.\n"}
		@bins=(0);
		for(my $j=1;$j<$ARGV[$i+1];$j++){push(@bins,$j/$ARGV[$i+1],$j/$ARGV[$i+1])}
		push(@bins,1);
		$i++; next;
	}
	if($ARGV[$i] eq "-quants"){
		@bins=();
		my @fs=split(",",$ARGV[$i+1]);
		for(my $j=0;$j<@fs;$j++){
			if($j==0 || $j==@fs-1){
				push(@bins,$fs[$j]);
			}else{
				push(@bins,$fs[$j],$fs[$j]);
			}
			if($j>0 && $fs[$j-1]>=$fs[$j]){die "Values of argument -quants must be numerically increasing.\n"}
		}
		$i++;
		next;
	}
	if($ARGV[$i] eq "-quantss"){
		@bins=split(",",$ARGV[$i+1]);
		if(@bins%2!=0){die "Number of values for argument -quantss must be even.\n"}
		for(my $j=1;$j<@bins;$j+=2){
			if($bins[$j-1]>=$bins[$j]){die "Pairs of values P_i & P_i+1 of argument -quantss defining a bin must be P_i<P_i+1.\n"}
		}
		$i++;
		next
	}
	die "Do not understand argument $ARGV[$i]\n";
}

# argument checks
if(!is_numeric($pvalthresh) || $pvalthresh<0 || $pvalthresh>1){die "PVAL must be numeric and 0<=PVAL<=1.\n"}
if($colors ne "" && !are_all_valid_R_colors($colors,$scr_abspath)){die "Check the colors; at least one is not a valid R color.\n";}
if($evttype){
	if(!defined($cstart) || !defined($cend) || !defined($cchr) || !defined($cstr) || !defined($cgid) || !defined($gtf) || !defined($fasta) || !defined($sp) || !defined($bplen)){die "You specified -mattexon or -mattintron but could not find all necessary arguments <C_START> <C_END> <C_CHR> <C_STRAND> <C_GENEID> <GTF> <FASTA> <SPECIES> <MAXLEN_BP_ANALYSIS>. Please check syntax.\n"}
	unless(-e $gtf){die "Cannot open file $gtf.\n"}
	unless(-e $fasta){die "Cannot open file $fasta.\n"}
	unless($sp =~ /Hsap|Ptro|Mmul|Mmus|Rnor|Cfam|Btau/){die "Species $sp does not fit to any of Hsap, Ptro, Mmul, Mmus, Rnor, Cfam, Btau.\n"}
	if(!is_numeric($bplen) || $bplen<10){die "Argument bplen should be >= 10.\n"}
}
if(@featsa==0){die "You have specied no -a features but you need to speciy at least one.\n"}
# are all -a features in table?
my ($fh,$col_ns_aref,$col_ids_aref,$comments)=open_fct_file($fn,[@featsa],$MYCOMMAND); close($fh);
if(!$evttype && @featsb==0){die "You have specied no -b features but you need to speciy at least one.\n"}
# are all -b features in table?
if(!$evttype){ ($fh,$col_ns_aref,$col_ids_aref,$comments)=open_fct_file($fn,[@featsb],$MYCOMMAND); close($fh); }
# is quants sorted and all values >0 and <1 ?
for(my $i=0;$i<@bins;$i++){
	if($bins[$i]<0 || $bins[$i]>1){die "All values in -quants or -quantss should be >=0 and <=1.\n"}
}
unless($colors){
	my @colors=(); for(my $i=0;$i<@bins/2;$i++){push(@colors,"lightgrey")};
	$colors=join(",",@colors); 
}
{my @fs=split(",",$colors); if(@fs<@bins/2){die "Given number of colors is smaller than number of defined bins.\n"}}
if(@bins<4){die "Check arguments -quants / -quantss. The number of defined bins must be >=2.\n"}


#######
# MAIN
#######
# create output dir
unless(-e $outd){mkdir($outd);}

my ($cmd,$ret);

# create temporary file with -a and -b features only, to keep RAM usage in R as small as possible
my $tmpfile_n = "$outd/000_extracted_features.tab";

if($evttype){
	my ($fh_tmp2,$tmpfile_n2) = tempfile( DIR => $outd, UNLINK => 1); close($fh_tmp2);
	copy($fn,$tmpfile_n2) or die "Copy failed: $!";
	if($evttype eq "exon"){
		$cmd="matt get_efeatures $fn $cstart $cend $cchr $cstr $cgid $gtf $fasta $sp $bplen -notrbts -f $gid_search_pattern | matt add_cols $tmpfile_n2 -"; 
		$ret=`$cmd`; die "$!\n$ret\n" if $?!=0;

		$cmd="matt get_rows $tmpfile_n2 EXON_FOUND_IN_GTF]yes[ | matt get_cols - ".join(" ",@featsa,@EXON_FEATURES)." > $tmpfile_n";
		$ret=`$cmd`; die "$!\n$ret\n" if $?!=0;
		@featsb=@EXON_FEATURES;
	}

	if($evttype eq "intron"){
		$cmd="matt get_ifeatures $fn $cstart $cend $cchr $cstr $cgid $gtf $fasta $sp $bplen -notrbts -f $gid_search_pattern | matt add_cols $tmpfile_n2 -";
		$ret=`$cmd`; die "$!\n$ret" if $?!=0;
		
		$cmd="matt get_rows $tmpfile_n2 INTRON_FOUND_IN_GTF]yes[ | matt get_cols - ".join(" ",@featsa,@INTRON_FEATURES)." > $tmpfile_n";
		$ret=`$cmd`; die "$!\n$ret\n" if $?!=0;
		@featsb=@INTRON_FEATURES;
	}
}else{
	$cmd="matt get_cols $fn ".join(" ",@featsa,@featsb)." > $tmpfile_n";
	$ret=`$cmd`; die "$!\n$ret\n" if $?!=0;
}


# call R and proceed within R
chomp(my $mattversion=`matt --version`);
$tmpfile_n=~s/$outd\///; # delete outd as in generate_summary_cmpr_features.r we change into outdir and work from there
$cmd = "$scr_abspath/../utilities/generate_summary_cmpr_features.r $scr_abspath/.. $outd $tmpfile_n \"".join(",",@featsa)."\" \"".join(",",@featsb)."\" \"".join(",",@bins)."\" $mattversion $pvalthresh colors:$colors $points $outliers";

print "$cmd\n";

$ret=`$cmd`;die "$!" if $?!=0;

print "\nSummary PDF successfully created.\n\n";
print "File with all features necessary to re-create the summary PDF is: $tmpfile_n\n";
print "To re-create the summary PDF, you may simply re-run the command from above.\n";
print "Change colors by changing argument 'colors:'.\n";
print "Add/remove points in boxplots with argument 'points:'.\n";
print "Show/don't show outliers in boxplots with argument 'outliers:'.\n\n";

exit(0); # success
__END__