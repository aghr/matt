#!/usr/bin/env perl
use strict;
use warnings;
use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.


my $MYCOMMAND="get_match";

# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "retrieval\t$MYCOMMAND\t:  \tfind ID match";
        exit(1);
}

if(!defined($ARGV[0]) || @ARGV < 5 || @ARGV > 6){
	print "\nmatt $MYCOMMAND <TABLE> <C1_KEY> <TABLE_KEY_VAL> <C2_KEY> <C_VALS> [<NOT_FOUND_TAG>]\n\n";
	print "   ... retrieve matches from a key-value table.\n\n";
	print "   <TABLE>         : tab-separated table which contains keys in one column\n";
	print "   <C1_KEY>        : name of column of TABLE with keys\n";
	print "   <TABLE_KEY_VAL> : tab-separated table which contains key-value pairs\n";
	print "   <C2_KEY>        : name of column of TABLE_KEY_VAL with keys (same as in C1_KEY of TABLE)\n";
	print "                       This column should contain unique keys and each key only one time.\n";
	print "   <C_VALS>        : name of column of TABLE_KEY_VAL with values\n";
	print "                     Can be a comma-separated list of columns.\n";
	print "   <NOT_FOUND_TAG> : if given, this value will be output for cases where no match could be found.\n\n";
	print "   What will happen: table TABLE_KEY_VAL defines a mapping from keys to values\n";
	print "                     Using this mapping, a column will be generated for TABLE which contains\n";
	print "                     the values from the mapping for the keys from C1_KEY of TABLE\n\n";
	print "   Output: Columns as defined by C_VALS to STDOUT. They contain in the same order as keys in\n";
	print "   C1_KEY of TABLE the corresponding values, or NOT_FOUND_TAG (by default empty string) for un-matched keys\n";
	print "\n";
        exit(1);
}


# get mapping key -> val
my %val=();
my ($fh,$col_ns_aref,$col_ids_aref,$comments)=open_fct_file($ARGV[2],[$ARGV[3],split(",",$ARGV[4])],$MYCOMMAND);
my @c_vals=@{$col_ids_aref};
my $c_key=shift(@c_vals);
while(<$fh>){
	$_=clean_line($_);
	if(length($_)==0 || substr($_,0,1) eq "#"){next}
	my @fs=split_line($_);
	$val{$fs[$c_key]}=join("\t",@fs[@c_vals]);
}
close($fh);



($fh,$col_ns_aref,$col_ids_aref,$comments)=open_fct_file($ARGV[0],[$ARGV[1]],$MYCOMMAND);
$c_key=$col_ids_aref->[0];

# header of output column
$ARGV[4]=~s/,/\t/g;
print $ARGV[4]."\n";
my @unmatched_output=();foreach my $tmp (@c_vals){if(@ARGV==6){push(@unmatched_output,$ARGV[5])}else{push(@unmatched_output,"")}}
my $unmatched_output=join("\t",@unmatched_output);

while(<$fh>){
	$_=clean_line($_);
	if(length($_)==0 || substr($_,0,1) eq "#"){print "#\n";next;}
	my @fs=split_line($_);
	my $val=$val{ $fs[$c_key] };
	if(defined($val)){print "$val\n";}else{print "$unmatched_output\n";}
}
close($fh);

exit(0); # success
__END__