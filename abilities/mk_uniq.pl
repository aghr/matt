#!/usr/bin/env perl
use strict;
use warnings;

use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.


my $MYCOMMAND="mk_uniq";

# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "extras\t$MYCOMMAND\t:      \tmake table unique wrt. rows";
        exit(1);
}

if(@ARGV<2 || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/){
    	print "\nmatt $MYCOMMAND <TABLE> <CNAME1> [<CNAME2> ...]\n";
    	print "   ... make table unique wrt. rows by omitting copies of rows.\n";
    	print "   Each row is identified by its values in columns CNAME1, [CNAME2,...].\n";
    	print "   Rows having identical entries in these columns are assumed to be copies.\n";
    	print "\n";
    	print "   <TABLE>      : tab-separated table\n";
    	print "   <CNAME1>     : column name\n";
    	print "   <CNAME2 ...> : potentially more column names\n";
    	print "\n";
    	print "   Output: TABLE where copies of lines have been omitted\n";
    	print "\n";
exit(1);
}


my ($fh,$col_ns_aref,$col_ids_aref,$comments)=open_fct_file(shift(@ARGV),[@ARGV],$MYCOMMAND);

print $comments;
print join("\t",@{$col_ns_aref})."\n";

# ids of requested columns
my @col_ids=@{$col_ids_aref};
my @fs;
my $row_key;
my %seen=();
while(<$fh>){
	$_=clean_line($_);
	if(length($_)==0 || substr($_,0,1) eq "#"){next;}
	my @fs=split_line($_);
	
	$row_key=join(",",@fs[@col_ids]);
	unless($seen{$row_key}){print "$_\n";}
	$seen{$row_key}=1;
}
close($fh);

exit(0); # success
__END__