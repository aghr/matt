#!/usr/bin/env perl
use strict;
use warnings;

use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";

my $MYCOMMAND="extr_scafids";

# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "extras\t$MYCOMMAND\t: \textract scaffold ids";
        exit(1);
}


if(!defined($ARGV[0]) || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/ || @ARGV!=2){
        print "\nmatt t$MYCOMMAND <FILE> <FILE_TYPE>\n\n";
        print "   ... extract the scaffold/sequence IDs.\n\n";
        print "   <FILE>      : FASTA, FASTQ, GTF, GFF file\n";
	print "   <FILE_TYPE> : choose one of: FASTA, FASTQ, GTF (for GTF and GFF)\n\n";
	print "   Output: a table with one column SCAFFOLD_IDS to STDOUT\n";
	print "\n";
        exit(1);
}


my ($file,$type)=@ARGV;

if($type ne "GTF" && $type ne "FASTA"  && $type ne "FASTQ"){die "Given file type $type unknown."}

##
# main
##
open(my $fh,"<".$file) or die "Cannot open $ARGV[0]: $!";
my %scafids;
my $first_char;
my @fs;
my $line;
my ($h,$s,$l,$q);
while(1){
	if(eof($fh)){last;}
	if($type eq "FASTA"){
		$line=<$fh>;
		$first_char=substr($line,0,1);
		if($first_char eq ";"){next;}
		if($first_char eq ">"){
			chomp($line);
			@fs=split(" ",substr($line,1));
			$scafids{$fs[0]}=1;
		}
		next;
	}
	if($type eq "FASTQ"){
		chomp($h=<$fh>);chomp($s=<$fh>);chomp($l=<$fh>);chomp($q=<$fh>);
		@fs=split(" ",substr($h,1));
		$scafids{$fs[0]}=1;
		next;
	}
	if($type eq "GTF"){
		$line=<$fh>;
		$first_char=substr($line,0,1);
		if($first_char eq "#"){next;}
		chomp($line);
		@fs=split("\t",$line);
		$scafids{$fs[0]}=1;
		next;
	}
}
close($fh);

print "SCAFFOLD_IDS\n";
foreach my $id (sort keys %scafids){
	print "$id\n";
}

exit(0); # success
__END__