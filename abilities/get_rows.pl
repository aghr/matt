#!/usr/bin/env perl
use strict;
use warnings;

use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.


my $MYCOMMAND="get_rows";

# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "retrieval\t$MYCOMMAND\t:   \tretrieve rows";
        exit(1);
}


if(!defined($ARGV[0]) || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/ || @ARGV<2){
	print "\nmatt $MYCOMMAND <TABLE> <CONSTRAINT1> <CONSTRAINT2> ...\n\n";
        print "   ... retrieve rows from table.\n\n";
        print "   <TABLE>       : tab-separated text table\n"; 
	print "   <CONSTRAINT*> : only rows satisfying these constraints will be retrieved\n\n";
	print "   Three different types of constraints can be defined plus their negation.\n";
	print "   * constrint on columns with numeric values <C_NAME>[MIN1,MAX1,MIN2,MAX2,...]\n";
	print "     value in column C_NAME must be in interval [MIN1;MAX1] OR [MIN2;MAX2] OR ... if defined.\n";
	print "   * constraint on columns with categorical values <C_NAME>]VAL1,VAL2,VAL3,...[\n";
        print "     value in column C_NAME must be one of VAL1, VAL2, VAL3, ...\n";
        print "   * constraint on columns with categorical values <C_NAME>]<TABLE2>,<C_NAME2>[\n";
        print "     value in column C_NAME must be one if the values in column C_NAME2 of table TABLE2\n";
        print "   * constraint on empty cells: <C_NAME>][ will output rows where the entry in C_NAME is empty.\n";
        print "        Empty means equal to the empty string. A cell containing blanks is not empty.\n";
        print "   * equality: <C_NAME1>==<C_NAME2>==<C_NAME3>... will output rows with identical entries in \n";
        print "     columns C_NAME1, C_NAME2, ...\n";
        print "   * Negation: put a ! in front of the column name, e.g., '!LENGTH[50,100]' will output rows\n";
	print "     with value in column LENGTH being <50 or >100.\n";
        print "     You need to put negative constraints into '...'\n\n";
        print "   Output to STDOUT: table TABLE reduced to the selected rows\n\n";
        print "Example: matt $MYCOMMAND t1.tab LENGTH[50,10] GROUP]a,b[ \'!NAME]t2.tab,SURNAME[\'\n";
        print "Extracts rows with value in column LENGTH from 50 to 100, and value in column GROUP either a or b,\n";
        print "and whose values in column NAME are NOT in column SURNAME of table t2.tab\n\n";
        exit(1);
}


my $fn = shift(@ARGV);

# column names of columns with constraint
my @constr_col_ns=();
# n or c
my @constr_type=();
# both constraint arrays will have length @constr_col_ns with empty values in the middle
my @n_constr=();
my @c_constr=();
my @empty_constr=();  # checks if cell is empty
my @negation=();
my @equal_constr=();
my ($min,$max,$vs,$href,$arg,$col_n);
my @fs;
my ($fh,$col_ns_aref,$col_ids_aref,$comments);
my $cid;

for(my $i=0; $i<@ARGV; $i++){
	$arg=$ARGV[$i];

	# numeric constraint
	if( $arg =~ /(.*)\[(.+)\]/ ){
		$col_n=$1;
		my @mins_maxs=split(",",$2);
		if(substr($col_n,0,1) eq "!"){push(@negation,1);$col_n=substr($col_n,1);}else{push(@negation,0);}
		push(@constr_col_ns,$col_n);
		$constr_type[$i]="n";

		if(@mins_maxs % 2){die "$MYCOMMAND: number of given values in numerical constraint $arg is not even."}
		for(my $j=0;$j<@mins_maxs;$j+=2){
			my $min=$mins_maxs[$j];
			my $max=$mins_maxs[$j+1];
			if($min>$max){die "$MYCOMMAND: MIN ($min) > MAX ($max) for contraint $arg.";}
			if(!is_numeric($min) || !is_numeric($max)){die "$MYCOMMAND: MIN and/or MAX of constraint $arg are not numeric.";}
		}
		$n_constr[$i]=[@mins_maxs];
		next;
	}

	# cardinal constrain
	if($arg =~ /(.*)\](.+)\[/){
		$col_n=$1;$vs=$2;
		if(substr($col_n,0,1) eq "!"){push(@negation,1);$col_n=substr($col_n,1);}else{push(@negation,0);}
		push(@constr_col_ns,$col_n);
		$constr_type[$i]="c";
		@fs=split(",",$vs);
		$href={};
		if(@fs==2 && -e $fs[0]){ # input is table from which we take all elements in specified column
			($fh,$col_ns_aref,$col_ids_aref,$comments)=open_fct_file($fs[0],[$fs[1]],$MYCOMMAND);
			$cid=$col_ids_aref->[0];
			while(<$fh>){$_=clean_line($_);if(length($_)==0 || substr($_,0,1) eq "#"){next;}my @fs=split_line($_);
				$href->{$fs[$cid]}=1;				
			}
			close($fh);
		}else{ # input is a list of values	
			foreach my $v (@fs){$href->{$v}=1;} 
		}
		$c_constr[$i]=$href;
		next;
	}
	
	# constraint for checking for empty entries
	if($arg =~ /(.*)\]\[/){
		$col_n=$1;
		if(substr($col_n,0,1) eq "!"){push(@negation,1);$col_n=substr($col_n,1);}else{push(@negation,0);}
		push(@constr_col_ns,$col_n);
		$constr_type[$i]="e";
		$c_constr[$i]=[];
		next;
	}

	# constraint for equality
	if($arg =~ /==/){
		$col_n=$arg;
		if(substr($col_n,0,1) eq "!"){push(@negation,1);$col_n=substr($col_n,1);}else{push(@negation,0);}
		$constr_type[$i]="eq";
		my @tmp=split("==",$col_n);
		push(@constr_col_ns,@tmp);
		$n_constr[$i]=scalar(@tmp); # number of columns of this equality constraint
		next;
	}

	die "$MYCOMMAND: I do not understand constraint $arg.";
}

($fh,$col_ns_aref,$col_ids_aref,$comments)=open_fct_file($fn,[@constr_col_ns],$MYCOMMAND);

print $comments;
print join("\t",@$col_ns_aref)."\n";


my $v;
ROW : 
 while(<$fh>){
	$_=clean_line($_);
	if(length($_)==0 || substr($_,0,1) eq "#"){next;}
	my @fs=split_line($_);
	
	# check constraints
	my $pos_in_col_ids_aref=-1;
	for(my $i=0; $i<@constr_type;$i++){  # go over constraints

		$pos_in_col_ids_aref++;
		$v=$fs[ $col_ids_aref->[$pos_in_col_ids_aref] ];
		
		# numeric constraint
		if($constr_type[$i] eq "n"){
			
			# value is NA or empty
			if($v eq "NA" || $v eq ""){next ROW;}

			my $v_in_user_defined_intervals=0;
			for(my $j=0;$j<@{$n_constr[$i]};$j+=2){
				my $min=$n_constr[$i]->[$j];
				my $max=$n_constr[$i]->[$j+1];
				
				# value is in one of the user defined min;max intervals
				if($min<=$v && $v<=$max){$v_in_user_defined_intervals=1;}
			}
			
			if( $negation[$i] &&  $v_in_user_defined_intervals){next ROW;}
			if(!$negation[$i] && !$v_in_user_defined_intervals){next ROW;}
		}

		# cardinal constraint
		if($constr_type[$i] eq "c"){
			if($negation[$i]){
				if(defined($c_constr[$i]->{$v})){next ROW;}
			}else{
				if(!defined($c_constr[$i]->{$v})){next ROW;}
			}
		}

		# empty constraint
		if($constr_type[$i] eq "e"){
			if($negation[$i]){
				if($v eq ""){next ROW;}
			}else{
				if($v ne ""){next ROW;}
			}
		}
		
		# equality constraint
		if($constr_type[$i] eq "eq"){
			my $all_equal=1;
			my $n_cols=$n_constr[$i];
			if($n_cols>1){
				for(my $j=0;$j<$n_cols-1;$j++){
					if($fs[$col_ids_aref->[$j]] ne $fs[$col_ids_aref->[$j+1]]){$all_equal=0;last;}
				}
			}
			
			if($negation[$i]){
				if($all_equal){next ROW;}
			}else{
				unless($all_equal){next ROW;}
			}
			$pos_in_col_ids_aref+=$n_cols-1; # add to $i number of columns of this equality constraint (-1) because all these columns are in @$col_ids_aref 
		}
	}

	print join("\t",@fs)."\n";
}

close($fh);

exit(0); # success
__END__
