#!/usr/bin/env perl
use strict;
use warnings;
use File::Temp qw(tempfile);

use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.


my $MYCOMMAND="test_regexp_enrich";

# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "seq_analysis\t$MYCOMMAND\t: \ttest REGEXP enrichment";
        exit(1);
}

if(@ARGV<8 || @ARGV>9 || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/){
    print "\nmatt $MYCOMMAND <TABLE_SEQS> <C_SEQS> <C_DATASET> <SEARCH_STRAND> <TABLE_REGEXPS> ...\n";
    print "                     ... <C_REGEXP_NAMES> <C_REGEXPS> <MODE> <N_ITERATIONS>\n\n";
    print "   ... compare pairs of sequence data sets wrt. the number of Perl regular-expression hits in these sets (significant enrichment or depletion).\n\n";
    print "   <TABLE_SEQS>      : tab-separated table containing sequences to be used\n";
    print "   <C_SEQS>          : name of column with sequences (can be of variable length)\n";
    print "   <C_DATASET>       : name of column containing information (IDs) about the groups the sequences belong to\n";
    print "                         In addition, you need to specify which groups of sequences you want to compare!\n";
    print "                         E.g., DATASET[fg1,bg1,fg2,bg2] will compare groups fg1 vs bg1, and fg2 vs bg2. The order is always foreground background,\n";
    print "                         forground background... . You can specify as many pairs of groups as you want.\n";
    print "   <SEARCH_STRAND>   : single -> search on sequences as given\n";
    print "                         double1 -> search also on rev-complement and positions of hits are wrt. each strand\n";
    print "                         double2 -> like double1 but hit positions are wrt. forward strand (sequences as given)\n";
    print "   <TABLE_REGEXPS>   : a table containing information in Perl regular expression to be used for the enrichment analysis\n";
    print "   <C_REGEXP_NAMES>  : name of column of <TABLE_REGEXPS> with short name of regular expression\n";
    print "                         All regular expressions with the same name will be tested together (as a group) and hit positions\n";
    print "                         will be reported only once, i.e., if two hits are the same position, this position is reported only once.\n";
    print "                         You can also sub-select regular expressions from TABLE_REGEXPS with: C_REGEXPS_NAMES[name1,name2,name3] and\n";
    print "                         only regular expressions with these names will be considered.\n";
    print "   <C_REGEXPS>       : name of column of <TABLE_REGEXPS> which contains the regular expressions\n";  #XXX
    print "   <MODE>            : can be either \'quant\' or a integer\n";
    print "                         * if <MODE>=quant: enrichment is defined wrt. ratio of normalized hit counts (quantitatively)\n";
    print "                         * if <MODE>=<INTEGER>: enrichment is defined as ratio of sequences with status YES/NO (having hit/having no hit)\n";
    print "                           where a sequence is YES if it has at least <INTEGER> hits.\n";
    print "   <N_ITERATIONS>    : number of iterations of permutation test used for estimating p-values, e.g., 10000 or more\n\n";
    print "   Output: a table with all results (number of hits, test statistics, p values) is written to STDOUT.\n";
    print "           P value encoding: p value <= 0.05/0.01/0.001 : */**/***\n";
    print "\n";
    exit(1);
}

my ($fn_seq,$cn_seq,$cn_ds,$strand_mode,$fn_kms,$cn_kmn,$cn_kms,$mode,$N)=@ARGV;
unless($strand_mode eq "single" || $strand_mode eq "double1" || $strand_mode eq "double2"){die "Please supply information on search locus: single or double1, double2?.";}

my @kmrs_perl_search_exps=();
my @kmrs_names=();
my $cmd;

# get regexp-group names
if($cn_kmn =~ /(.+)\[(.+?)\]/){
	$cn_kmn=$1;
	@kmrs_names=split(",",$2);
}else{
	$cmd="matt col_uniq $fn_kms $cn_kmn | matt get_cols - ${cn_kmn}";
	#print "$cmd\n";
	my @fs=split("\n",`$cmd`);
	@kmrs_names=@fs[1..(@fs-1)];
}

# for each group
foreach my $name (@kmrs_names){
	$cmd="matt get_rows $fn_kms $cn_kmn]${name}[ | matt get_cols - $cn_kms";
	#print "$cmd\n";
	my @fs=split("\n",`$cmd`);
	
	# make reg. exps unique
	my %hash=();foreach my $f (@fs[1..@fs-1]){$hash{$f}=1;}@fs=keys(%hash);

	push(@kmrs_perl_search_exps,\@fs);
}

# for each group get joined cell contents of all columns from kmer description table
$cmd="matt get_colnms $fn_kms | matt get_cols - COL_NAMES";
my @fs=split("\n",`$cmd`);
if($cn_kmn eq "NAME"){
	for(my $i=0;$i<@fs;$i++){if($fs[$i] eq "NAME"){$fs[$i]="NAME_2";}}
}
my $kmr_descr_header=join("\t",@fs[1..(@fs-1)]);

my @kmr_descr=();
foreach my $n (@kmrs_names){
	$cmd="matt get_rows $fn_kms $cn_kmn]${n}[";
	#print "$cmd\n";
	@fs=split("\n",`$cmd`);
	@fs=@fs[1..(@fs-1)];

	my @joined_cells=();
	foreach my $line (@fs){
		my @fs2=split("\t",$line,-1);
		for(my $i=0; $i<@fs2;$i++){
			unless($joined_cells[$i]){$joined_cells[$i]=[];}
			push(@{$joined_cells[$i]},$fs2[$i]);
		}
	}
	
	for(my $i=0;$i<@joined_cells;$i++){ $joined_cells[$i]=join(",",@{$joined_cells[$i]});}
	push(@kmr_descr,join("\t",@joined_cells));
}


$cn_ds=~/(.+)\[(.+?)\]/;
$cn_ds=$1;
unless($2){die "$MYCOMMAND: you need to specify pairs of data sets (fg1,bg1,fg2,bg2,...) but you gave $cn_ds.";}
my @datasets=split(",",$2);
if( @datasets % 2){die "$MYCOMMAND: you need to specify pairs of data sets (fg1,bg1,fg2,bg2,...) but you gave $cn_ds.";}

# check if data sets are present in table
$cmd="matt col_uniq $fn_seq $cn_ds";  
@fs=split("\n",`$cmd`);  die "$cmd\n$!" if $?!=0;
foreach my $dataset (@datasets){my $found=0; for(my $i=0;$i<@fs;$i++){if($fs[$i]=~/^$dataset\t/){$found=1;last;}} if($found==0){die "Group $dataset is not present in file $fn_seq in column $cn_ds\n"}}

#output header
print "NAME"
	."\tSEARCH_ON_STRAND"
	."\tMODE"
	."\tBG_MODEL"
	."\tDATASETNAME_FG"
	."\tNSEQS_FG"
	."\tNHITS_ALL_FG"
	."\tNHITS_START_FG"
	."\tNHITS_INTERNAL_FG"
	."\tNHITS_END_FG"
	."\tNSEQS_WITHHITS_ALL_FG"
	."\tNSEQS_WITHHITS_START_FG"
	."\tNSEQS_WITHHITS_INTERNAL_FG"
	."\tNSEQS_WITHHITS_END_FG"
	."\tTESTSTAT_ALL_FG"
	."\tTESTSTAT_START_FG"
	."\tTESTSTAT_INTERNAL_FG"
	."\tTESTSTAT_END_FG"
	."\tDATASETNAME_BG"
	."\tNSEQS_BG"
	."\tNHITS_ALL_BG"
	."\tNHITS_START_BG"
	."\tNHITS_INTERNAL_BG"
	."\tNHITS_END_BG"
	."\tNSEQS_WITHHITS_ALL_BG"
	."\tNSEQS_WITHHITS_START_BG"
	."\tNSEQS_WITHHITS_INTERNAL_BG"
	."\tNSEQS_WITHHITS_END_BG"
	."\tTESTSTAT_ALL_BG"
	."\tTESTSTAT_START_BG"
	."\tTESTSTAT_INTERNAL_BG"
	."\tTESTSTAT_END_BG"
	."\tENRICHMENT_ALL_FG_VS_BG"
	."\tENRICHMENT_FIRST_FG_VS_BG"
	."\tENRICHMENT_INTERNAL_FG_VS_BG"
	."\tENRICHMENT_END_FG_VS_BG"
	."\tN_ITS_PERMUTATIONTEST"
	."\tPVALUE_ALL"
	."\tPVALUE_FIRST"
	."\tPVALUE_INTERNAL"
	."\tPVALUE_END"
	."\tSIGNIFICANT_RESULT_ALL"
	."\tSIGNIFICANT_RESULT_FIRST"
	."\tSIGNIFICANT_RESULT_INTERNAL"
	."\tSIGNIFICANT_RESULT_END"
	."\t$kmr_descr_header"
	."\n";


my (@fg,@bg,@nhits_fg,@nhits_bg,@l_fg,@l_bg);
my (@teststat_fg,@teststat_bg);
my $seq_tmp;
my $nhits;
my $kmer_search_exp;
# for testing if the hit positions differ between fg and bg
# therefor each seq gets divided into the first 1/3, the middle 1/3 and the last 1/3
# and we count the hits in these regions and compare them, i.e.,
# first 1/3 fg vs bg, middle 1/3 fg vs bg, last 1/3 fg vs bg
my @hits;
my @nseqs_with_hits; 
my @nhits;
my @teststats;
my @teststats_diffs;
my @teststats_sums;
my @enrichments;
my @Ns_more_extreme;
my @pvals;
my @sig_levels;
my @relations;
my $hits_aref_tmp;
my $N_its;

for(my $i=0; $i<@datasets;$i+=2){
	$cmd="matt get_rows $fn_seq $cn_ds]${datasets[$i]}[ | matt get_cols - $cn_seq";
	#print "$cmd\n";
	my @fs=split("\n",`$cmd`);
	@fg=@fs[1..@fs-1];
	
	$cmd="matt get_rows $fn_seq $cn_ds]${datasets[$i+1]}[ | matt get_cols - $cn_seq";
	#print "$cmd\n";
	@fs=split("\n",`$cmd`);
	@bg=@fs[1..@fs-1];
	#print scalar(@fg)." ".scalar(@bg)."\n";

	for(my $j=0;$j<@kmrs_names;$j++){
		
		# get hits and seq lengths from fg data
		(@nhits_fg,@nhits_bg,@l_fg,@l_bg)=((),(),(),());
		@hits=([],[],[],[],[],[],[],[]);  # order all hits fg, hits first 1/3 fg, hits middle 1/3 fg, hits last 1/3 fg, same for bg
		($hits_aref_tmp,$kmer_search_exp)=get_regexp_hits_for_seqs(\@fg,$kmrs_perl_search_exps[$j],$strand_mode);
		for(my $k=0;$k<@fg;$k++){
			 $hits[0]->[$k]=0; $hits[1]->[$k]=0; $hits[2]->[$k]=0; $hits[3]->[$k]=0;
			 my $seq_len=length($fg[$k]);
			 my $seq_len_1=$seq_len/3;
			 my $seq_len_2=2*$seq_len/3;
			 $l_fg[$k]=$seq_len;
			 $nhits=0;
	  	 	 foreach my $hit_pos (@{$hits_aref_tmp->[$k]}){
		 		$nhits++;
				if($hit_pos < $seq_len_1){$hits[1]->[$k]+=1;}elsif($hit_pos < $seq_len_2){$hits[2]->[$k]+=1;}else{$hits[3]->[$k]+=1;}
			 }
			 
			 $hits[0]->[$k]=$nhits;
		}

		# get hits and seq lengths from bg data
		($hits_aref_tmp,$kmer_search_exp)=get_regexp_hits_for_seqs(\@bg,$kmrs_perl_search_exps[$j],$strand_mode);
		for(my $k=0;$k<@bg;$k++){
			 $hits[4]->[$k]=0; $hits[5]->[$k]=0; $hits[6]->[$k]=0; $hits[7]->[$k]=0;
			 my $seq_len=length($bg[$k]);
			 my $seq_len_1=$seq_len/3;
			 my $seq_len_2=2*$seq_len/3;
			 $l_bg[$k]=$seq_len;
			 $nhits=0;
			 foreach my $hit_pos (@{$hits_aref_tmp->[$k]}){
			 	$nhits++;
    				if($hit_pos < $seq_len_1){$hits[5]->[$k]+=1;}elsif($hit_pos < $seq_len_2){$hits[6]->[$k]+=1;}else{$hits[7]->[$k]+=1;}
    			 }

			 $hits[4]->[$k]=$nhits;
		}
		@nseqs_with_hits=(0,0,0,0,0,0,0,0); # number of sequences with hits; 0: across entire seq in fg, 1: across first 1/3 in fg, 2: across middle 1/3 fg, across last 1/3 in fg, .. same with bg
		@nhits=(0,0,0,0,0,0,0,0);
		for(my $l=0;$l<@hits;$l++){
			for(my $k=0;$k<@{$hits[$l]};$k++){
				$nhits[$l]+=$hits[$l]->[$k];
				$nseqs_with_hits[$l]+= $hits[$l]->[$k] > 0 ? 1 : 0;
		}}

		# normalize counts 
		if($mode eq "quant"){
			for(my $k=0;$k<@fg;$k++){$hits[0]->[$k]=$hits[0]->[$k]/$l_fg[$k];}
			for(my $l=1;$l<4;$l++){for(my $k=0;$k<@fg;$k++){$hits[$l]->[$k]=$hits[$l]->[$k]/($l_fg[$k]/3);}}
			for(my $k=0;$k<@bg;$k++){$hits[4]->[$k]=$hits[4]->[$k]/$l_bg[$k];}
			for(my $l=5;$l<8;$l++){for(my $k=0;$k<@bg;$k++){$hits[$l]->[$k]=$hits[$l]->[$k]/($l_bg[$k]/3);}}
		}else{
			for(my $l=0;$l<8;$l++){for(my $k=0;$k<@{$hits[$l]};$k++){$hits[$l]->[$k] = $hits[$l]->[$k] >= $mode ? 1 : 0;}}
		}

		# get test statistics
		for(my $l=0;$l<8;$l++){$teststats[$l]=get_mean_from_aref($hits[$l]);}
		
		# get test stat diffs
		for(my $l=0;$l<4;$l++){$teststats_diffs[$l]=abs( $teststats[$l]-$teststats[$l+4]);}
				
		# get enrichments
		for(my $l=0;$l<4;$l++){
			if($teststats[$l+4]>0){
				$enrichments[$l]=$teststats[$l]/$teststats[$l+4];
			}else{
				$enrichments[$l]="NA";
			}
		}
		
		# do permutation test
		@Ns_more_extreme=(0,0,0,0);
		my @ids=0..(@fg+@bg-1);
		$N_its=0;
		for(my $n=0;$n<$N;$n++){
			my $id;
			@teststats_sums=(0,0,0,0,0,0,0,0);
			shuffle_elements_in_aref(\@ids);
			for(my $k=0;$k<@ids;$k++){
				$id=$ids[$k];
				if($k<@fg){
					if($id<@fg){
						for(my $l=0;$l<4;$l++){$teststats_sums[$l]+=$hits[$l]->[$id];}
					}else{
						for(my $l=0;$l<4;$l++){$teststats_sums[$l]+=$hits[$l+4]->[$id-@fg];}
					}
				}else{
					if($id<@fg){
						for(my $l=0;$l<4;$l++){$teststats_sums[$l+4]+=$hits[$l]->[$id];}
					}else{
						for(my $l=0;$l<4;$l++){$teststats_sums[$l+4]+=$hits[$l+4]->[$id-@fg];}
					}
				}
			}
			
			for(my $l=0;$l<4;$l++){
				$Ns_more_extreme[$l]+=abs( ($teststats_sums[$l]/@fg) - ($teststats_sums[$l+4]/@bg) ) >= $teststats_diffs[$l] ? 1 : 0;  
			}
			$N_its++;
			
			if($n>0 && ($n % 10000==0 || $n==1000)){
				# break out here already if p-value too larger
				my $break=0;
				if($n>1000){
					for(my $l=0;$l<4;$l++){
						if( ($Ns_more_extreme[$l]+1)/($N_its+1)>0.1 ){$break++;}
					}
				}else{
					for(my $l=0;$l<4;$l++){
						if( ($Ns_more_extreme[$l]+1)/($N_its+1)>0.4 ){$break++;}
					}
				}
				
				if($break==4){
					last;
				}
			}
		}

		# compute p values, significance levels, relations
		for(my $l=0;$l<4;$l++){
			$pvals[$l]= ($Ns_more_extreme[$l]+1)/($N_its+1);
			if($pvals[$l]<=0.001){$sig_levels[$l]="***";
			}elsif($pvals[$l]<=0.01){$sig_levels[$l]="**";
			}elsif($pvals[$l]<=0.05){$sig_levels[$l]="*";
			}else{$sig_levels[$l]="";}
			
			if($sig_levels[$l] ne ""){
				if($teststats[$l]<$teststats[$l+4]){$sig_levels[$l]=$datasets[$i]."<".$datasets[$i+1]." ".$sig_levels[$l];
				}else{ $sig_levels[$l]=$datasets[$i].">".$datasets[$i+1]." ".$sig_levels[$l] } 
			}
		}

		# output
		print "$kmrs_names[$j]"
      		      ."\t$strand_mode"
		      ."\t$mode"
		      ."\tNA"
		      ."\t$datasets[$i]"
		      ."\t".scalar(@fg)
		      ."\t".$nhits[0]
		      ."\t".$nhits[1]
		      ."\t".$nhits[2]
		      ."\t".$nhits[3]
		      ."\t".$nseqs_with_hits[0]
		      ."\t".$nseqs_with_hits[1]
		      ."\t".$nseqs_with_hits[2]
		      ."\t".$nseqs_with_hits[3]
		      ."\t".join("\t",@teststats[0..3])
		      ."\t$datasets[$i+1]"
		      ."\t".scalar(@bg)
		      ."\t".$nhits[4]
		      ."\t".$nhits[5]
		      ."\t".$nhits[6]
		      ."\t".$nhits[7]
		      ."\t".$nseqs_with_hits[4]
		      ."\t".$nseqs_with_hits[5]
		      ."\t".$nseqs_with_hits[6]
		      ."\t".$nseqs_with_hits[7]
		      ."\t".join("\t",@teststats[4..7])
		      ."\t".join("\t",@enrichments)
		      ."\t$N_its"
		      ."\t".join("\t",@pvals)
		      ."\t".join("\t",@sig_levels)
		      ."\t".$kmr_descr[$j]
		      ."\n";
	}
}	


exit(0); # success
__END__