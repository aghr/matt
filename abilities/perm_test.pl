#!/usr/bin/env perl
use strict;
use warnings;
use Scalar::Util qw(looks_like_number);

use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.

#######
## Main
#######

my $MYCOMMAND="perm_test";
my $START_TIME_SECS=time();

# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "math\t$MYCOMMAND\t: \ttest differences between groups";
        exit(1);
}

if(!defined($ARGV[0]) || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/ || @ARGV<5){
        print "\nmatt $MYCOMMAND <TABLE> <C_GROUP> <N_REPEATS/HOURS> [-equalgrpsizes] ...\n";
        print "                     -num <C_NAME1> <C_NAME2>...\n";
        print "                     -card <C_NAME1> <C_NAME2>...\n";
        print "                     -cooc <C_NAME1> <C_NAME2>...\n\n";
	print "   ... apply a parmutation test for testing significance of differences of features between groups\n\n";
	print "   <TABLE>           : tab-separated table containing features of at least two groups\n";
	print "   <C_GROUP>         : name of column which contains group IDs\n";
	print "                         This function works only for comparing two groups.\n";
	print "                         When C_GROUP contains more than two distinct group IDs, you need.\n";
	print "                         to sub-select those IDs for which you want to run the comparision.\n";
	print "                         E.g., DATASET[a,b] -> group a and b will be compared.\n";
	print "                         The test statistics = differences are computed as grp_a - grp_b.\n";
	print "   <N_REPEATS/HOURS> : number of iterations;  The larger, the better the estimates of the p-value.\n";
	print "                         For a quick test 1.000 or 10.000, for accurate p-values 100.000 or more.\n";
	print "                         When you add \"h\" at the end of the number, the number gets interpreted as hours\n";
	print "                         and the test stops after this time has elapsed, e.g., 20h -> the test does as many\n";
	print "                         iterations as possible in 20 hours.\n";
	print "     -equalgrpsizes  : if the number of data points in both groups is different, the test will be done using\n";
	print "                         equal-sized groups which is accomplished by down-sampling data points from the larger group\n";
	print "   -num [<C_NAME>]   : names of columns with NUMERICAL variables whose difference between groups should be tested\n";
	print "   -card [<C_NAME>]  : names of columns with CATEGORIAL variables whose difference between groups should be tested\n";
	print "   -cooc [<C_NAME>]  : names of columns which contain information on co-occurrences. \n";
	print "                         E.g., if TABLE describes introns, than column IS_IN_GENE might contain gene IDs.\n";
	print "                         These gene IDs get interpreted as cluster IDs and all introns which occur in the same gene (cluster),\n";
	print "                         i.e., which have the same gene (cluster) ID in this column, co-occur with each other.\n";
	print "                         One intron might occur in several clusters, i.e., the column with cluster IDs might contain a\n";
	print "                         colon (:) separated list of cluster IDs.\n";
	print "\n";
        exit(1);
}

my ($fn,$c_grp,$ns)=@ARGV[0..2];

my @GRP_VALS_ORDERED=(); # length 2; diffs between groups get computed as $GRP_VALS_ORDERED[0] - $GRP_VALS_ORDERED[1]
my %grp_vals=(); 
my $group_selection=0;
if($c_grp =~ /\[(.*)\]$/){
	foreach my $val (split(",",$1)){
		push(@GRP_VALS_ORDERED,$val);
		$grp_vals{$val}=1;
	}
	$c_grp=substr($c_grp,0,length($c_grp)-2-length($1));
	$group_selection=1;
}

my $n_grp_ok=0;
for my $line (`matt col_uniq $fn $c_grp`){my @fs=split("\t",$line);if($grp_vals{$fs[0]}){$n_grp_ok++;}}
if($n_grp_ok!=2){die "matt perm_test works only for exactly two groups. You have either selected more or less than 2 groups or group ids which not occur in file $fn.";}


my $mode="iterations";
my $N_SECS="";
if(substr($ns,length($ns)-1,1) eq "h"){$mode="time";$ns=substr($ns,0,length($ns)-1);$N_SECS=$ns*60*60;}

my @cs_card=();
my @cs_num=();
my @cs_cooc=();
my $arg_chk;
my $equalgrpsizes=0;
for(my $i=3;$i<@ARGV;$i++){
	if($ARGV[$i] eq "-num"){$arg_chk="num";next;}
	if($ARGV[$i] eq "-card"){$arg_chk="card";next;}
	if(substr($ARGV[$i],0,5) eq "-cooc"){$arg_chk="cooc";next;}
	if($ARGV[$i] eq "-equalgrpsizes"){$equalgrpsizes=1;$arg_chk="";next;}

	if($arg_chk eq "card"){push(@cs_card,$ARGV[$i]);next;}
	if($arg_chk eq "num"){push(@cs_num,$ARGV[$i]);next;}
	if($arg_chk eq "cooc"){push(@cs_cooc,$ARGV[$i]);next;}
	die "Given arguments cannot be interpreted (file $fn). To which group belongs the feature $ARGV[$i]?";
}

my ($fh,$col_ns_aref,$col_ids_aref,$comments)=open_fct_file($fn,[$c_grp,@cs_card,@cs_num,@cs_cooc],$MYCOMMAND);


my @card_vals=();for(my $i=0;$i<@cs_card;$i++){push(@card_vals,[]);}
my @num_vals=();for(my $i=0;$i<@cs_num;$i++){push(@num_vals,[]);}
my %grp_poss=();  # for each grp a ref to a array where we store all rows which correspond to this grp
my @cooc_vals=();for(my $i=0;$i<@cs_cooc;$i++){push(@cooc_vals,[]);}

my $pos_cntr=-1;
while(<$fh>){
	$_=clean_line($_);
	if(length($_)==0 || substr($_,0,1) eq "#"){next;}
	my @fs=split_line($_);
	my $grp=$fs[$col_ids_aref->[0]];

	my $add=0;
	if($group_selection){
	if($grp_vals{$grp}){$add=1;}
	}else{$grp_vals{$grp}=1;$add=1;}

	if($add){
		if(!defined($grp_poss{$grp})){$grp_poss{$grp}=[];}
		push(@{$grp_poss{$grp}},++$pos_cntr);

		for(my $i=0;$i<@cs_card;$i++){push(@{$card_vals[$i]},$fs[$col_ids_aref->[$i+1]]);}
		for(my $i=0;$i<@cs_num;$i++){push(@{$num_vals[$i]},$fs[$col_ids_aref->[$i+1+@cs_card]]);}
		for(my $i=0;$i<@cs_cooc;$i++){push(@{$cooc_vals[$i]},$fs[$col_ids_aref->[$i+1+@cs_card+@cs_num]]);}
	}
}
close($fh);

# determine co-occurrence statistics for each co-occurrence column
my @int_coocurs_yes_no=();
my @int_median_conum=();
my @grp_poss_cluster=();
for(my $i=0;$i<@cooc_vals;$i++){
	push(@int_coocurs_yes_no,[]);
	push(@int_median_conum,[]);
	push(@grp_poss_cluster,{});
	
	my @trs=@{$cooc_vals[$i]};
	foreach my $grp (keys %grp_poss){
		$grp_poss_cluster[$i]->{$grp}=[];
		my @ids=@{$grp_poss{$grp}};
		my %tr2nis=();
		my %tr2poss=();
		for(my $j=0;$j<@ids;$j++){
			my $id=$ids[$j];
			my @fs=split(":",$trs[$id]);
			foreach my $tr (@fs){
				$tr2nis{$tr}++;
				if(!defined($tr2poss{$tr})){$tr2poss{$tr}="";}
				$tr2poss{$tr}.="$id:";
			}
		}
		push(@{$grp_poss_cluster[$i]->{$grp}},values(%tr2poss));   # rememeber which rows correspond to itrons which in grp occur in same tr
		
		for(my $j=0;$j<@ids;$j++){
			my $id=$ids[$j];
			my @fs=split(":",$trs[$id]);
			my $coocurs_yes_no="DOES_NOT_COOCUR";
			my @conums=();
			my $n_tmp;
			foreach my $tr (@fs){
				$n_tmp=$tr2nis{$tr}-1;
				push(@conums,$n_tmp);
				if($n_tmp>0){$coocurs_yes_no="DOES_COOCUR";}
			}
			$int_coocurs_yes_no[$i]->[$id]=$coocurs_yes_no;
			$int_median_conum[$i]->[$id]=get_median_from_aref(\@conums);
		}
	}
}

############
## subs
############
sub get_proportions{
	my $grps_href=$_[0];
	my $vals_aref=$_[1];
	my $sample_space_aref=$_[2];

	my %counts=();
	my $grp;
	my $val;
	my %sums=();
	foreach my $grp (keys %{$grps_href}){
	foreach my $i (@{$grps_href->{$grp}}){
		$val=$vals_aref->[$i];
		
		if($val eq "" || $val eq "NA"){next;}
		
		if(!defined($counts{$grp})){$counts{$grp}={};$sums{$grp}=0;}
		if(!defined($counts{$grp}->{$val})){$counts{$grp}->{$val}=0;}
		
		$counts{$grp}->{$val}++;
		$sums{$grp}++;
	}}
	
	my %diffs=();
	my %pval_cnts_lt=();
	my %pval_cnts_gt=();
	foreach $val (@{$sample_space_aref}){
		foreach $grp (keys %counts){
			unless(defined($counts{$grp}->{$val})){$counts{$grp}->{$val}=0;}  # make sure we have all values in all groups
			$counts{$grp}->{$val}/=$sums{$grp};
			$counts{$grp}->{$val}=[$counts{$grp}->{$val}];
		}
		$diffs{$val}=$counts{$GRP_VALS_ORDERED[0]}->{$val}->[0]-$counts{$GRP_VALS_ORDERED[1]}->{$val}->[0];
		$pval_cnts_lt{$val}=0;
		$pval_cnts_gt{$val}=0;
	}

return([\@GRP_VALS_ORDERED,$sample_space_aref,\%counts,\%diffs,\%pval_cnts_lt,\%pval_cnts_gt]);
}


sub get_means{
	my $grps_href=$_[0];
	my $vals_aref=$_[1];

	my %sums=();
	my $grp;
	my $val;
	my %ns=();

	foreach my $grp (keys %{$grps_href}){
	foreach my $i (@{$grps_href->{$grp}}){
		$val=$vals_aref->[$i];
		
		if($val eq "" || $val eq "NA"){next;}

		if(!defined($sums{$grp})){$sums{$grp}={};$sums{$grp}->{"mean"}=0;$ns{$grp}=0;}

		$sums{$grp}->{"mean"}+=$val;
		$ns{$grp}++;
	}}
	
	my %diffs=();
	foreach $grp (keys %sums){
		$sums{$grp}->{"mean"}/=$ns{$grp};
		$sums{$grp}->{"mean"}=[$sums{$grp}->{"mean"}];
	}
	$diffs{"mean"}=$sums{$GRP_VALS_ORDERED[0]}->{"mean"}->[0]-$sums{$GRP_VALS_ORDERED[1]}->{"mean"}->[0];
	my %pval_cnts_lt=("mean"=>0);
	my %pval_cnts_gt=("mean"=>0);

return([\@GRP_VALS_ORDERED,["mean"],\%sums,\%diffs,\%pval_cnts_lt,\%pval_cnts_gt]);
}


sub get_stats{
	my @card_vals=@{$_[0]};
	my @num_vals=@{$_[1]};
	my @int_coocurs_yes_no=@{$_[2]};
	my @int_median_conum=@{$_[3]};
	my @card_vals_sample_space=@{$_[4]};
	my %grp_poss=%{$_[5]};
	my @grp_poss_cooc=@{$_[6]};
	my $stats_orig_aref=$_[7];

#foreach my $grp (keys %grp_poss){
#print "$grp ".scalar( @{$grp_poss{$grp}} ).": ";
#foreach my $v (@{$grp_poss{$grp}}){print "$v ";}
#print "\n";
#}
#print "\n";
#exit;

	my @stats=();
	my $stats_cntr=-1;
	for(my $i=0;$i<@card_vals;$i++){
		$stats[++$stats_cntr]=get_proportions(\%grp_poss,$card_vals[$i],$card_vals_sample_space[$i]);
	}

	for(my $i=0;$i<@num_vals;$i++){
		$stats[++$stats_cntr]=get_means(\%grp_poss,$num_vals[$i]);
	}

	for(my $i=0;$i<@int_coocurs_yes_no;$i++){
#foreach my $grp (keys $grp_poss_cooc[$i]){
#	print "$grp ".scalar(@{$grp_poss_cooc[$i]->{$grp}})." ".$grp_poss_cooc[$i]->{$grp}->[-1]."\n";
#}
		$stats[++$stats_cntr]=get_proportions($grp_poss_cooc[$i],$int_coocurs_yes_no[$i],["DOES_COOCUR","DOES_NOT_COOCUR"]);
		my $stats_tmp=get_means($grp_poss_cooc[$i],$int_median_conum[$i]);
		# merge both into one object
		foreach my $grp (keys %{$stats[$stats_cntr]->[2]}){
			$stats[$stats_cntr]->[2]->{$grp}->{"mean of MEDIAN_COOC_NUM_OVER_TRS"}=$stats_tmp->[2]->{$grp}->{"mean"};
		}
		$stats[$stats_cntr]->[3]->{"mean of MEDIAN_COOC_NUM_OVER_TRS"}=$stats_tmp->[3]->{"mean"};
		$stats[$stats_cntr]->[1]=["DOES_COOCUR","DOES_NOT_COOCUR","mean of MEDIAN_COOC_NUM_OVER_TRS"];
		my $pval_cnts_lt={};$pval_cnts_lt->{"DOES_COOCUR"}=0;$pval_cnts_lt->{"DOES_NOT_COOCUR"}=0;$pval_cnts_lt->{"mean of MEDIAN_COOC_NUM_OVER_TRS"}=0;
		$stats[$stats_cntr]->[4]=$pval_cnts_lt;
		my $pval_cnts_gt={};$pval_cnts_gt->{"DOES_COOCUR"}=0;$pval_cnts_gt->{"DOES_NOT_COOCUR"}=0;$pval_cnts_gt->{"mean of MEDIAN_COOC_NUM_OVER_TRS"}=0;
		$stats[$stats_cntr]->[5]=$pval_cnts_gt;
	}
	
	# copy values to $stats_orig_aref
	if(defined($stats_orig_aref) && @{$stats_orig_aref}==@stats){
		my $tmp_val;
		for(my $i=0;$i<@stats;$i++){
			foreach my $val (@{$stats[$i]->[1]}){
			foreach my $grp (@{$stats[$i]->[0]}){
				$tmp_val=$stats[$i]->[2]->{$grp}->{$val}->[0];
				push(@{$stats_orig_aref->[$i]->[2]->{$grp}->{$val}},$tmp_val); # add value
			}}
			$stats_orig_aref->[$i]->[3]=$stats[$i]->[3]; # replace diffs
		}
		return($stats_orig_aref);
	}
	
return(\@stats);
}



##################
## main 
##################
my @all_poss=([]);  # array with all pos
my @min_grp_sizes=(9**9**9);
foreach my $grp_id (keys %grp_poss){
	my $tmp_grp_size=scalar(@{$grp_poss{$grp_id}});
	if($tmp_grp_size<$min_grp_sizes[0]){$min_grp_sizes[0]=$tmp_grp_size;}
	push(@{$all_poss[0]},@{$grp_poss{$grp_id}});
}
for(my $i=0; $i<@grp_poss_cluster;$i++){
	$min_grp_sizes[$i+1]=9**9**9;
	$all_poss[$i+1]=[];
	foreach my $grp (keys %{$grp_poss_cluster[$i]}){
		my $tmp_grp_size=scalar(@{$grp_poss_cluster[$i]->{$grp}});
		if($tmp_grp_size<$min_grp_sizes[$i+1]){$min_grp_sizes[$i+1]=$tmp_grp_size;}
		push(@{$all_poss[$i+1]},@{$grp_poss_cluster[$i]->{$grp}});
	}
}


# get sample space for all cardinal features
my @sample_space=();
for(my $i=0; $i<@card_vals;$i++){my %tmp_h=(); foreach my $tmp_v (@{$card_vals[$i]}){ $tmp_h{$tmp_v}=1; } push(@sample_space,[ sort(keys %tmp_h) ]);  }

my @stats_orig=();
my @grp_poss_cooc;
unless($equalgrpsizes){
		@grp_poss_cooc=();
		for(my $i=0;$i<@grp_poss_cluster;$i++){
			$grp_poss_cooc[$i]={};
			foreach my $grp (keys %{$grp_poss_cluster[$i]}){
				my $ids_aref=[];
#print "$grp: ".scalar(@{$grp_poss_cluster[$i]->{$grp}})."\n";
				foreach my $tmpv (@{$grp_poss_cluster[$i]->{$grp}}){push(@{$ids_aref},split(":",$tmpv));}
				$grp_poss_cooc[$i]->{$grp}=uniq_from_aref($ids_aref); # each intron should count only once
		}}
		@stats_orig=@{ get_stats(\@card_vals,\@num_vals,\@int_coocurs_yes_no,\@int_median_conum,\@sample_space,\%grp_poss,\@grp_poss_cooc) };
}

#foreach my $grp (@{$stats_orig[0]->[0]}){
#print "$grp\n";
#	foreach my $val (keys %{$stats_orig[0]->[2]->{$grp}}){
#		print "   $val: ".$stats_orig[0]->[2]->{$grp}->{$val}->[0]."\n";
#	}
#}

my ($start,$end);
my %grp_poss_tmp;
my @stats;
my @stats_tmp;
my $tmp_diff;
my $orig_diff;

my $n=0;my $time_diff;
while(1){
	$n++;
	$time_diff=time()-$START_TIME_SECS;
	if($mode eq "iterations"){if($n>$ns){$n--;last;}
	}else{if($time_diff>$N_SECS){$n--;last;}}

	# here in case of equal group sizes, we down-sample to the minimal group size in each group within each groups (without permutations)
	# and re-compute the statistics per group
	if($equalgrpsizes){

		%grp_poss_tmp=();
		foreach my $grp (keys %grp_poss){
			shuffle_elements_in_aref($grp_poss{$grp});  # shuffle positions within each group
			$grp_poss_tmp{$grp}=[@{$grp_poss{$grp}}[0..($min_grp_sizes[0]-1)]];
		}
		@grp_poss_cooc=();
		for(my $i=0;$i<@grp_poss_cluster;$i++){
			$grp_poss_cooc[$i]={};
			foreach my $grp (keys %{$grp_poss_cluster[$i]}){
				my $ids_aref=[];
				my @to_shuffle=@{$grp_poss_cluster[$i]->{$grp}};
				shuffle_elements_in_aref(\@to_shuffle);
				@to_shuffle=@to_shuffle[0..($min_grp_sizes[$i+1]-1)];
				foreach my $tmpv (@to_shuffle){push(@{$ids_aref},split(":",$tmpv));}
				$grp_poss_cooc[$i]->{$grp}=uniq_from_aref($ids_aref); # each intron should count only once
		}}
		
		@stats_orig=@{ get_stats(\@card_vals,\@num_vals,\@int_coocurs_yes_no,\@int_median_conum,\@sample_space,\%grp_poss_tmp,\@grp_poss_cooc,\@stats_orig) };
	}

	# here we are doing the permutations and re-computation of statistics
	# in case of equal group sizes, we down-sample to the minimal group size in each group
	shuffle_elements_in_aref($all_poss[0]);
	%grp_poss_tmp=();
	$start=0;
	foreach my $grp (keys %grp_poss){
		unless($equalgrpsizes){$end=$start+scalar(@{$grp_poss{$grp}})-1;}else{$end=$start+$min_grp_sizes[0]-1;}
		$grp_poss_tmp{$grp}=[@{$all_poss[0]}[$start..$end]];
		$start=$end+1;
	}
	@grp_poss_cooc=();
	for(my $i=0;$i<@grp_poss_cluster;$i++){
		$grp_poss_cooc[$i]={};
		$start=0;
		my @to_shuffle=@{$all_poss[$i+1]};
		shuffle_elements_in_aref(\@to_shuffle);
		foreach my $grp (keys %{$grp_poss_cluster[$i]}){
#print "$grp\n";
			unless($equalgrpsizes){$end=$start+scalar(@{$grp_poss_cluster[$i]->{$grp}})-1;}else{$end=$start+$min_grp_sizes[$i+1]-1;}
			my $ids_aref=[];
#print "$start $end\n";
			foreach my $tmpv (@to_shuffle[$start..$end]){push(@{$ids_aref},split(":",$tmpv));}
			$grp_poss_cooc[$i]->{$grp}=uniq_from_aref($ids_aref); # each intron should count only once
			$start=$end+1;
#print scalar(@{$grp_poss_cooc[$i]->{$grp}})."\n";
	}}
	@stats=@{ get_stats(\@card_vals,\@num_vals,\@int_coocurs_yes_no,\@int_median_conum,\@sample_space,\%grp_poss_tmp,\@grp_poss_cooc) };
	for(my $i=0;$i<@stats;$i++){
		foreach my $val (@{$stats[$i]->[1]}){
#print "$val\n";
			$tmp_diff=$stats[$i]->[3]->{$val};
			$orig_diff=$stats_orig[$i]->[3]->{$val};
#print "tmp_diff: $tmp_diff\n";
#print "orig_diff: $orig_diff\n";
#print "cnt_lt: ".$stats_orig[$i]->[4]->{$val}."\n";
#print "cnt_gt: ".$stats_orig[$i]->[5]->{$val}."\n";

			if($tmp_diff<=$orig_diff){$stats_orig[$i]->[4]->{$val}++;}   # P(diff_tmp<=diff_orig or diff_sampled)
			if($tmp_diff>=$orig_diff){$stats_orig[$i]->[5]->{$val}++;}   # P(diff_tmp>=diff_orig or diff_sampled)
		}
	}
}


##########
## Output
##########
my $time_hours=floor( $time_diff/(60*60) );
my $time_min=floor( ($time_diff-$time_hours*60*60)/(60) );
my $datestring = localtime();

print "\nPERMUTATION TEST\n################\n";
print "Date:             : $datestring\n";
print "Input table       : $fn\n";
print "Iterations        : ".$n."\n";
print "Groups            : ";foreach my $grp (sort(keys %grp_poss)){print "$grp (".scalar(@{$grp_poss{$grp}}).")  "}print "\n";
print "Equal group sizes : ";if($equalgrpsizes){print "yes with ",$min_grp_sizes[0]," elements per group";}else{print "no";}print "\n";
print "Elapsed time      : ".$time_hours."h ".$time_min."min\n\n";
print "Results (*/**/*** -> p value <= 0.05/0.01/0.001):\n#################################################\n";

my ($str1,$str2,$pv1,$pv2,@sig_levels,$max,$g1,$g2,$val1,$val2,$diff,$feature_name);
my ($pre,$post);
my $n_pos_cntr=-1;
my $type;
my $grp_comp_str_empty;
my $grp_comp_str_nonempty;
for(my $i=0;$i<(@card_vals+@num_vals+@cooc_vals);$i++){
	
	@stats_tmp=@{$stats_orig[$i]};
	if($i<@card_vals){
		$feature_name="$cs_card[$i]\t";
		$str2.="* ".$cs_card[$i]." (cardinal)\n";
		$pre="prop.";
		$type="cardinal";
	}
	if(@card_vals<=$i && $i<@card_vals+@num_vals){
		$feature_name="$cs_num[$i-@card_vals]\t";
		$str2.="* ".$cs_num[$i-@card_vals]." (numerical)\n";
		$pre="mean";
		$type="numeric";
	}
	if(@card_vals+@num_vals<=$i){
		$feature_name="$cs_cooc[$i-@card_vals-@num_vals]\t";
		$str2.="* ".$cs_cooc[$i-@card_vals-@num_vals]." (co-occurrence)\n";
		$pre="";
		$type="cooc";
	}

	@sig_levels=(0);
	foreach my $val (@{$stats_tmp[1]}){
		$n_pos_cntr++;
		$g1=$stats_tmp[0]->[0];
		$g2=$stats_tmp[0]->[1];

#print "$g1 $val: ".scalar(@{$stats_tmp[2]->{$g1}->{$val}}).": ";foreach my $v (@{$stats_tmp[2]->{$g1}->{$val}}){print "$v ";}
#print "\n";
#print "$g2 $val: ".scalar(@{$stats_tmp[2]->{$g2}->{$val}}).": ";foreach my $v (@{$stats_tmp[2]->{$g2}->{$val}}){print "$v ";}
#print "\n";

		$val1=get_mean_from_aref( $stats_tmp[2]->{$g1}->{$val} ); # we have always only one value with the exception when we are
		$val2=get_mean_from_aref( $stats_tmp[2]->{$g2}->{$val} ); # the test was run with equalgrpsizes
		$diff=$val1-$val2;
		
		$grp_comp_str_empty= (" " x length("$g1 < $g2"));
		if($type eq "numeric"){
			if($val1<$val2){$grp_comp_str_nonempty="$g1 < $g2";}else{$grp_comp_str_nonempty="$g1 > $g2";}
		}
		
		$pv1=($stats_tmp[4]->{$val}+1)/($n+1); # A Note on the Calculation of Empirical P Values from Monte Carlo Procedures, B. V. North, D. Curtis, and P. C. Sham
		$pv2=($stats_tmp[5]->{$val}+1)/($n+1);

		$post="";
		if($pv1<=0.05 || $pv2<=0.05 ){push(@sig_levels,1);$post=" (*)";}
		if($pv1<=0.01 || $pv2<=0.01){push(@sig_levels,2);$post=" (**)";}
		if($pv1<=0.001 || $pv2<=0.001){push(@sig_levels,3);$post=" (***)";}

		if($i<@card_vals || $i>=@card_vals+@num_vals){$str2.="   Value: $val  $post\n";}
		$str2.="     ".$pre." in $g1: ".$val1."\n";
		$str2.="     ".$pre." in $g2: ".$val2."\n";
		$str2.="      diff      : ".$diff."\n";
		$str2.="      P(<=diff) : ".$pv1." (ratio: ".($stats_tmp[4]->{$val}+1)."/".($n+1).")\n";
		$str2.="      P(>=diff) : ".$pv2." (ratio: ".($stats_tmp[5]->{$val}+1)."/".($n+1).")\n";
	}
	$str2.="\n";

	$max=get_max_from_aref(\@sig_levels);
	if($max==3){$str1.="***  ".$grp_comp_str_nonempty."  ".$feature_name."\n";}
	if($max==2){$str1.="**   ".$grp_comp_str_nonempty."  ".$feature_name."\n";}
	if($max==1){$str1.="*    ".$grp_comp_str_nonempty."  ".$feature_name."\n";}
	if($max==0){$str1.="     ".$grp_comp_str_empty."  ".$feature_name."\n";}
}

$str1.="\n\n";
print $str1;
print "Details:\n########\n";
print $str2;

exit(0); # success
__END__