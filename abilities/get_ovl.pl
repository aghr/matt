#!/usr/bin/env perl
use strict;
use warnings;

use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.


my $MYCOMMAND="get_ovl";

# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "retrieval\t$MYCOMMAND\t:    \toverlap of sets";
        exit(1);
}

if(!defined($ARGV[0]) || scalar(@ARGV) % 2 !=0 || @ARGV<4){
	print "\nmatt $MYCOMMAND <TABLE1> <C_ID1> <TABLE2> <C_ID2> [<TABLE> <C_ID>]*\n\n";
	print "   ... retrieve IDs of rows which occur in several tables.\n\n";
	print "   <TABLE*> : tab-separated tables\n";
	print "   <C_ID*>  : column in tables which contain some kind of ID\n\n";
	print "   Output to STDOUT: a table with one column OVERLAP containing the IDs of rows\n";
	print "                     occurring in all tables in the specified columns. If IDs occur several\n";
	print "                     times in one file, they will get unifies. No ID is reported twice.\n\n";
        exit(1);
}


my %ids;
my %cnts=();
my @fs;
my $cid;
my $id;
for(my $i=0;$i<@ARGV;$i+=2){
	my ($fh,$col_ns_aref,$col_ids_aref,$comments)=open_fct_file($ARGV[$i],[$ARGV[$i+1]],$MYCOMMAND);
	$cid=$col_ids_aref->[0];
	%ids=();
	while(<$fh>){
		$_=clean_line($_);
		if(length($_)==0 || substr($_,0,1) eq "#"){next;}
		my @fs=split_line($_);
		$id=$fs[$cid];
		if(length($id)==0){next;}
		$ids{ $id }=1;
	}
	close($fh);
	foreach $id (keys %ids){$cnts{$id}+=1;}
}


print "OVERLAP\n";
foreach $id (keys %cnts){
	if($cnts{$id}==@ARGV/2){print "$id\n";}
}


exit(0); # success
__END__