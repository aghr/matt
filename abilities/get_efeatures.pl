#!/usr/bin/env perl
use strict;
use warnings;
use File::Temp qw(tempfile);

use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.


my $MYCOMMAND="get_efeatures";

# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "seq_analysis\t$MYCOMMAND\t:   \tget exon features";
        exit(1);
}

# I use two codes for
# 1: encoding if a features should be included in the summary report with graphics
#    if there is a blank at the last positions: plot this featured in summary report  else: should not be plotted
# 2: encoding if feature is BP feature and needs to be excluded from results table if user has defined species = NA
#    if last or second last symbol is . -> it is a BP feature  otherwise: not
my @feature_expl=(
	"EXON_ID - unique exon id like <gene_id>:<start>,<end>,<scaffold>,<strand>" # 0
	,"GENE_BIOTYPE - if available (coding, noncoding, pseudo, rest)"
	,"EXON_FOUND_IN_GTF - (yes,no)",
	,"EXON_LENGTH "
	,"UPEXON_MEDIANLENGTH - median length of up-stream exon "
	,"DOEXON_MEDIANLENGTH - median length of down-stream exon "
	,"RATIO_UPEXON_EXON_LENGTH - median up-stream exon length / exon length "
	,"RATIO_DOEXON_EXON_LENGTH - median down-stream exon length / exon length "
	,"UPINTRON_MEDIANLENGTH - median length of up-stream introns "
	,"DOINTRON_MEDIANLENGTH - median length of down-stream introns "
	,"RATIO_UPINTRON_EXON_LENGTH - median up-stream intron length / exon length "
	,"RATIO_DOINTRON_EXON_LENGTH - median down-stream intron length / exon length " # 11
	,"EXON_GCC - GC content of entire exon sequence " # 12
	,"UPINTRON_GCC - GC content of entire up-stream intron sequence "
	,"UPEXON_GCC - GC content of entire up-stream exon sequence "
	,"DOINTRON_GCC - GC content of entire down-stream intron sequence "
	,"DOEXON_GCC - GC content of entire down-stream exon sequence "
	,"RATIO_UPEXON_EXON_GCC - UPEXON_GCC / EXON_GCC "
	,"RATIO_UPINTRON_EXON_GCC - UPINTRON_GCC / EXON_GCC "
	,"RATIO_DOINTRON_EXON_GCC - DOINTRON_GCC / EXON_GCC "
	,"RATIO_DOEXON_EXON_GCC - DOEXON_GCC / EXON_GCC "
	,"SF1_HIGHESTSCORE_3SS_UPINTRON - highest score of a SF1 positon weight matrix trained with human data in the last 150 nt 3 prime intron positons of up-stream intron "
	,"SF1_HIGHESTSCORE_3SS_DOINTRON - highest score of a SF1 positon weight matrix trained with human data in the last 150 nt 3 prime intron positons of down-stream intron "
	,"LEN_SEQ_UPINTRON_USED_FOR_BP_ANALYSIS - actual length of up-stream intron used for extracting BP features"    # highest SF1 score and BP analysis
	,"LEN_SEQ_DOINTRON_USED_FOR_BP_ANALYSIS - actual length of down-stream intron used for extracting BP features" 	# highest SF1 score and BP analysis
	,"UP_5SS_20INT10EX_GCC - GC content of up-stream 5ss sequence (20int+10ex positions) "
	,"GCC_3SS_20INT10EX - GC content of 3ss sequence (20int+10ex positions) "
	,"GCC_5SS_20INT10EX - GC content of 5ss sequence (20int+10ex positions) "
	,"DO_3SS_20INT10EX_GCC - GC content of down-stream 3ss sequence (20int+10ex positions) "	
	,"MAXENTSCR_HSAMODEL_UPSTRM_5SS - maximum entropy score of 5ss of up-stream exon using a model trained with human splice sites "
	,"MAXENTSCR_HSAMODEL_3SS - maximum entropy score of 3ss using a model trained with human splice sites "
	,"MAXENTSCR_HSAMODEL_5SS - maximum entropy score of 5ss using a model trained with human splice sites "
	,"MAXENTSCR_HSAMODEL_DOWNSTRM_3SS - maximum entropy score of 3ss of down-stream exon using a model trained with human splice sites "
	,"DIFF_MAXENTSCR_HSAMODEL_3SS_VS_DOWNSTRM_3SS - maximum entropy score of 3ss minus maximum entropy score of downstream 3ss "
	,"DIFF_MAXENTSCR_HSAMODEL_5SS_VS_UPSTRM_5SS - maximum entropy score of 5ss minus maximum entropy score of upstream 5ss "
	,"DIST_FROM_MAXBP_TO_3SS_UPINTRON - distance to 3ss of best precited BP. "
	,"SEQ_MAXBP_UPINTRON - sequence of best predicted BP."
	,"SCORE_FOR_MAXBP_SEQ_UPINTRON - BP sequence score of best predicted BP. "
	,"PYRIMIDINECONT_MAXBP_UPINTRON - Pyrimidine content between the BP adenine and the 3 prime splice site for best BP. "
	,"POLYPYRITRAC_OFFSET_MAXBP_UPINTRON - Polypyrimidine track offset relative to the BP adenine for best BP. "
	,"POLYPYRITRAC_LEN_MAXBP_UPINTRON - Polypyrimidine track length for best BP. "
	,"POLYPYRITRAC_SCORE_MAXBP_UPINTRON - Polypyrimidine track score for best BP. "
	,"BPSCORE_MAXBP_UPINTRON - SVM classification score of best BP. "
	,"NUM_PREDICTED_BPS_UPINTRON - number of all predicted BPs which have a positive BP score. "
	,"MEDIAN_DIST_FROM_BP_TO_3SS_UPINTRON - like DIST_FROM_MAXBP_TO_3SS but median over top-3 predicted BPs. "
	,"SEQ_BPS_UPINTRON - comma-separated list of sequences of top-3 predicted BPs."
	,"MEDIAN_SCORE_FOR_BPSEQ_UPINTRON - like SCORE_FOR_MAXBP_SEQ but median over top-3 predicted BPs. "
	,"MEDIAN_PYRIMIDINECONT_UPINTRON - like PYRIMIDINECONT_MAXBP but median over top-3 predicted BPs. "
	,"MEDIAN_POLYPYRITRAC_OFFSET_UPINTRON - like POLYPYRITRAC_OFFSET_MAXBP but median over top-3 predicted BPs. "
	,"MEDIAN_POLYPYRITRAC_LEN_UPINTRON - like POLYPYRITRAC_LEN_MAXBP but median over top-3 predicted BPs. "
	,"MEDIAN_POLYPYRITRAC_SCORE_UPINTRON - like POLYPYRITRAC_SCORE_MAXBP but median over top-3 predicted BPs. "
	,"MEDIAN_BPSCORE_UPINTRON - like BPSCORE_MAXBP but median over top-3 predicted BPs. "
	,"DIST_FROM_MAXBP_TO_3SS_DOINTRON - distance to 3ss of best precited BP. "
	,"SEQ_MAXBP_DOINTRON - sequence of best predicted BP"
	,"SCORE_FOR_MAXBP_SEQ_DOINTRON - BP sequence score of best predicted BP. "
	,"PYRIMIDINECONT_MAXBP_DOINTRON - Pyrimidine content between the BP adenine and the 3 prime splice site for best BP. "
	,"POLYPYRITRAC_OFFSET_MAXBP_DOINTRON - Polypyrimidine track offset relative to the BP adenine for best BP. "
	,"POLYPYRITRAC_LEN_MAXBP_DOINTRON - Polypyrimidine track length for best BP. "
	,"POLYPYRITRAC_SCORE_MAXBP_DOINTRON - Polypyrimidine track score for best BP. "
	,"BPSCORE_MAXBP_DOINTRON - SVM classification score of best BP. "
	,"NUM_PREDICTED_BPS_DOINTRON - number of all predicted BPs which have a positive BP score. "
	,"MEDIAN_DIST_FROM_BP_TO_3SS_DOINTRON - like DIST_FROM_MAXBP_TO_3SS but median over top-3 predicted BPs. "
	,"SEQ_BPS_DOINTRON - comma-separated list of sequences of top-3 predicted BPs."
	,"MEDIAN_SCORE_FOR_BPSEQ_DOINTRON - like SCORE_FOR_MAXBP_SEQ but median over top-3 predicted BPs. "
	,"MEDIAN_PYRIMIDINECONT_DOINTRON - like PYRIMIDINECONT_MAXBP but median over top-3 predicted BPs. "
	,"MEDIAN_POLYPYRITRAC_OFFSET_DOINTRON - like POLYPYRITRAC_OFFSET_MAXBP but median over top-3 predicted BPs. "
	,"MEDIAN_POLYPYRITRAC_LEN_DOINTRON - like POLYPYRITRAC_LEN_MAXBP but median over top-3 predicted BPs. "
	,"MEDIAN_POLYPYRITRAC_SCORE_DOINTRON - like POLYPYRITRAC_SCORE_MAXBP but median over top-3 predicted BPs. "
	,"MEDIAN_BPSCORE_DOINTRON - like BPSCORE_MAXBP but median over top-3 predicted BPs. " # 68
	,"EXON_FOUND_IN_THESE_TRS - transcript ids where exon was found in" # 69
	,"MEDIAN_TR_LENGTH - median length of transcripts the exon occurs in "
	,"MEDIAN_EXON_NUMBER - ... of transcripts where exon was found in "
	,"EXON_MEDIANRANK - median rank of all occurrences of this exon (rank = ordinal number of exon in transcript)"
	,"EXON_MEDIANRELATIVERANK - relative rank = rank / number of all exons in transcript, is between 0 and 1 "
	,"EXON_MEDIANRELATIVERANK_3BINS - median bin into which EXON_MEDIANRELATIVERANK falls when binning 0-1 into 3 bins "
	,"EXON_MEDIANRELATIVERANK_5BINS - similar to EXON_MEDIANRELATIVERANK_3BINS with 5 bins "
	,"EXON_MEDIANRELATIVERANK_10BINS - similar to EXON_MEDIANRELATIVERANK_3BINS with 10 bins "
	,"NTRS_ALL_FOR_GENE - number of transcripts of gene where the exon was found in "
	,"NTRS_WITH_EXON - number of transcripts where exon was found"
	,"NTRS_WITH_EXON_AS_FIRST_EXON - number of transcripts where exon was found as first exon"
	,"PROP_FIRST_EXON - NTRS_WITH_EXON_AS_FIRST_EXON / NTRS_WITH_EXON "
	,"NTRS_WITH_EXON_AS_LAST_EXON - number of transcripts where exon was found as last exon"
	,"PROP_LAST_EXON - NTRS_WITH_EXON_AS_LAST_EXON / NTRS_WITH_EXON "
	,"NTRS_WITH_EXON_AS_INTERNAL_EXON - number of transcripts where exon was found as internal intron"
	,"PROP_INTERNAL_EXON - NTRS_WITH_EXON_AS_INTERNAL_EXON / NTRS_WITH_EXON "
	,"NTRS_WITH_EXON_IN_UTR - number of transcripts where exon was found and overlaps with a UTR"
	,"PROP_EXON_IN_UTR - NTRS_WITH_EXON_IN_UTR / NTRS_WITH_EXON "
	,"EXON_COOCCURS_WITH_OTHER_EXONS - Does this exon co-occurs with other exons of the same exon set? (yes, no)"
	,"EXON_COOCCURS_WITH_THESE_EXONS - EXON_ID of co-occurring exons"
	,"MEDIAN_EXON_EXON_COOCCURRENCE_NUMBER_OVER_ALL_TRS - median number of co-occurring EXON over all transcripts this exon was found in" # 89	
	,"SEQ_UPEXON" # 90
	,"SEQ_UPINTRON"
	,"SEQ_UPINTRON_USED_FOR_BP_ANALYSIS"
	,"SEQ_EXON"
	,"SEQ_DOINTRON"
	,"SEQ_DOINTRON_USED_FOR_BP_ANALYSIS"
	,"SEQ_DOEXON"
	,"SEQ_UP_5SS_20INT10EX"
	,"SEQ_UP_5SS_6INT3EX"
	,"SEQ_3SS_20INT10EX"
	,"SEQ_3SS_20INT3EX"
	,"SEQ_5SS_20INT10EX"
	,"SEQ_5SS_6INT3EX"
	,"SEQ_DO_3SS_20INT10EX"
	,"SEQ_DO_3SS_20INT3EX"
);


if(@ARGV==1 && $ARGV[0] eq "explain"){
	print "\nOverview of exon features:\n\n";
	my $c=0;
	foreach my $f (@feature_expl){$c++;if($c<10){$c.=".  ";}elsif($c<100){$c.=". "}else{$c.="."}print "   $c $f\n";}
	print "\n";
exit(0);
}

if(@ARGV<9 || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/){
   	print "\nmatt $MYCOMMAND <TABLE> <C_START> <C_END> <C_CHR> <C_STRAND> <C_GENEID> <GTF> <FASTA> <SPECIES> [<MAXLEN_BP_ANALYSIS>] [-notrbts] [-f <FIELD>] [<FILE:SCORENAME>]*\n\n";
    	print "   ... determine features for exons given in TABLE\n\n";
	print "   <C_START>        : name of column with start coordinates of exons\n";
	print "   <C_END>          : name of column with end coordinates of exons\n";
	print "   <C_CHR>          : name of column with scaffold/chromosome ID for exons (IDs must be identical to those in GTF and FASTA)\n";
	print "   <C_STRAND>       : name of column with strand (+, -) information for exons\n";
	print "   <C_GENEID>       : name of column which contains IDs of genes the exons belong to. If TABLE does not contain a column with gene ids,\n";
	print "                        the corresponding genes the exons belong to can be determined automatically.\n";
	print "                        In this case, use Matt's command retr_geneids on TABLE and add extracted gene ids to TABLE first.\n";
	print "   <GTF>            : GTF file describing transcripts for all genes.\n";
	print "                        The GTF should contain information on UTRs and transcript biotypes (like GTFs from Ensembl).\n";
	print "   <FASTA>          : FASTA file containing sequences for all chromosomes / scaffolds\n";
	print "                      If you set FASTA to NA (not available), then all but sequence-related features will be extracted.\n";
	print "   <SPECIES>        : chose one from Hsap, Ptro, Mmul, Mmus, Rnor, Cfam, Btau, NA (for all other species)\n";
	print "                        Branch point features are only available for above listed species.\n";
	print "                        If your species is different but close to one listed species, you could use the clostest one to get BP features.\n";
	print "<MAXLEN_BP_ANALYSIS>: maximal length of the 3' intron ends considered for feature HUMANSF1_HIGHESTSCORE_3SS\n";
	print "                        and BP analysis features (can be omitted; default length = 150)\n";
	print "                        Important: the 20 first 5' intron positions will be never used for the BP analysis.\n";
	print "   -notrbts         : no transcript_biotypes; if given, all transcripts in <GTF> will be considered as they are.\n";
	print "                        If NOT given, <GTF> must contain a transcript_biotype field for all transcripts as for example\n";
	print "                        the GTF files from Ensembl. Transcripts then get classified according to their biotype into\n";
	print "                        coding, noncoding, pseudo, rest. A gene is coding if it contains at least one coding tr and all\n";
	print "                        other transcripts of a coding gene will be neglected. A gene is noncoding/pseudo if all its trs are\n";
	print "                        noncoding/pseudo. Transcripts classified as rest will be neglected.\n";
	print "                        column containing the information of the biotype of the gene the exon was found in.\n";
	print "                        Infos on biotypes: http://www.ensembl.org/Help/Faq?id=468 . You can add the field transcript_biotype to the <GTF>\n";
	print "                        by yourself and it might contain as well these values: noncoding, coding, pseudo, rest.\n";
	print "   -f FIELD         : gene ids in TABLE and GTF must coincide. Often GTF contain in the last column different fields like gene_id or gene_name.\n";
	print "                      When you specify -f FIELD, gene ids will be taken from field FIELD from GTF. Default is gene_id.\n";
	print "   <FILE:SCORENAME> : if given, file FILE should contain transcript ids. The output then\n";
	print "                        contains a column SCORENAME which contains for each exon e of gene g\n";
	print "                        the proportion of transcripts from FILE from gene g containing e\n";
	print "                        of all transcripts from gene g overlapping e\n";
	#print "   -fs              : if given, only chosen features will be output. E.g., -fs EXON_LENGTH,EXON_GCC\n";
	print "\n   Output: a table with columns containing the extracted features to STDOUT\n\n";
	print "   To get an explanation of extracted features run: matt get_efeatures explain\n\n";
	print "Used external tools:\n";
	print "   1. for maximum entropy scores\n";
	print "      Maximum entropy modeling of short sequence motifs with applications to RNA splicing signals\n";
	print "      Yeo et al., 2003, DOI: 10.1089/1066527041410418\n";
	print "   2. for extraction of branch point features and classification of branch points\n";
	print "      Genome-wide association between branch point properties and alternative splicing\n";
	print "      Corvelo et al., 2010, DOI: 10.1371/journal.pcbi.1001016\n";
	print "      http://regulatorygenomics.upf.edu/Software/SVM_BP\n\n";
exit(1);
}

my ($seqfeat_file,$cn_start,$cn_end,$cn_chr,$cn_strand,$cn_gname,$gtf_file,$fasta_file,$species)=@ARGV;

unless($species=~/Hsap|Ptro|Mmul|Mmus|Rnor|Cfam|Btau|NA/){die "Wrong SPECIES argument; you have specified: $species.";}

my @col_ns=($cn_start,$cn_end,$cn_chr,$cn_strand,$cn_gname);

my $int_3ss_len=150; # default value
my $notrbts=0;  # no transcript biotypes
my @feature_names=();
my @feature_tid_hrefs=();
my @feature_counts_arefs=();
my $gid_search_term="gene_id";
if(@ARGV>9){
	for(my $i=9;$i<@ARGV;$i++){
		my @fs=split(":",$ARGV[$i]);   # e.g. Hsa19_trids_coding_trs.txt:CODING_SCORE
		
		if(scalar(@fs)==1){
			if(is_numeric($fs[0])){$int_3ss_len=$fs[0];next;}
			if($fs[0] eq "-notrbts"){$notrbts=1;next;}
			if($fs[0] eq "-f"){$gid_search_term=$ARGV[$i+1];$i++;next;}
			die "Cannot interpret argument $ARGV[$i].";
		}
		if(scalar(@fs)>2){die "Cannot interpret argument $ARGV[$i].";}
		
		push(@feature_names,$fs[1]);
		my $href={};
		open(my $fh,"<".$fs[0]) or die "Cannot open file $fs[0].";
		while(<$fh>){
			$_=clean_line($_);
			if(length($_)==0 || substr($_,0,1) eq "#"){next;}
			$href->{ $_ }=1;  # should only contain tr ids; one per line
		}
		close($fh);
		push(@feature_tid_hrefs,$href);
		push(@feature_counts_arefs,[]);
	}
}

if($gid_search_term eq ""){die "Gene id search term defined by argument -f FIELD must not be empty."}

##
# main
##

my ($fh,$col_ns_aref,$col_ids_aref,$comments) = open_fct_file($seqfeat_file,[@col_ns],$MYCOMMAND);
my $col_start=$col_ids_aref->[0];
my $col_end=$col_ids_aref->[1];
my $col_chr=$col_ids_aref->[2];
my $col_strand=$col_ids_aref->[3];
my $col_gname=$col_ids_aref->[4];

my @fs;
my $seq;

# input
my (@estarts,@eends,@echrs,@estrands, @egnames,@eids);
my $eid;
my $N_exons=0;

# read exons
while(<$fh>){
	$_=clean_line($_);
	if(length($_)==0 || substr($_,0,1) eq "#"){next;}
	@fs=split_line($_);
	push(@estarts,$fs[$col_start]);
	push(@eends,$fs[$col_end]);
	push(@echrs,$fs[$col_chr]);
	push(@estrands,$fs[$col_strand]);
	push(@egnames,$fs[$col_gname]);
	$eid=join(",",$fs[$col_start],$fs[$col_end],$fs[$col_chr],$fs[$col_strand],$fs[$col_gname]);
	push(@eids,$eid);
	$N_exons++;	
}
close($fh);

# check GTF is gene ids are really in $gid_search_term 
my %gids_tmp=();
open(my $gtf,$gtf_file) or die "$gtf_file: $!";
while(<$gtf>){
	$_=clean_line_leave_quotes($_);
	@fs=split_line($_);
	if(length($_)==0 || substr($_,0,1) eq "#" || @fs!=9){next;}
	my $gid="";
	if($fs[8]=~/$gid_search_term \"(.*?)\"/){$gid=$1;}if($gid eq ""){next;}
	$gids_tmp{$gid}=1;
}
close($gtf);

my $N_gid_hits=0;
my $emax=100; if($emax>@egnames){$emax=@egnames;}
for(my $i=0; $i<$emax; $i++){
	if($gids_tmp{$egnames[$i]}){$N_gid_hits++;}
}

if($N_gid_hits<$emax/2){die "Please check argument -f FIELD. It looks like only less than 50 of 100 checked gene ids from TABLE could be found in field $gid_search_term in GTF.";}


# get gtf content in hash: content for each combination of chr and strand
my (%exons,%utrs,%gid2trs);
open($gtf,$gtf_file) or die "$!";
my %trbt=();  # transcript biotype -> if user has not specified -notrbt each transcript must have a field transcript_biotype
              # we delete all noncoding, pseudo, rest transcripts from coding genes -> and store for the remaining trs the information that they occur in a coding gene  
	      # a noncoding gene must contain only noncoding trs -> and we store for these that they occur in a noncoding gene
	      # same for pseudo genes
	      # rest genes get neglected
my %gbt=();   # gene biotype
my $trbt;
my $gbt;
while(<$gtf>){
	$_=clean_line_leave_quotes($_);
	@fs=split_line($_);
	if(length($_)==0 || substr($_,0,1) eq "#" || @fs!=9){next;}
	my $gid="";
	if($fs[8]=~/$gid_search_term \"(.*?)\"/){$gid=$1;}if($gid eq ""){next;}
	my $tid="";
	if($fs[8]=~/transcript_id \"(.*?)\"/){$tid=$1;}if($tid eq ""){next;}
	unless($notrbts){
		if($fs[8]=~/transcript_biotype \"(.*?)\"/){ $trbt=$1; 
		}elsif($fs[8]=~/gene_biotype \"(.*?)\"/){   $trbt=$1;
		}else{ if(!defined($trbt{$tid})){die "No transcript_biotype nor gene_biotype for transcript $tid.";}}
		
		$trbt=get_type_from_ensembl_tr_biotype($trbt);
		if($trbt eq "undefined"){die "Unknown transcript_biotype ($trbt) for transcript $tid. Try running get_efatures with argument -notrbts .";}
		if($trbt eq "rest"){next;}
		$trbt{$tid}=$trbt;
		$gbt=$gbt{$gid};
		if(!defined($gbt)){$gbt{$gid}=$trbt;
		}else{
			if($trbt eq "coding"){$gbt{$gid}=$trbt; # we always set coding
			}elsif($trbt eq "noncoding"){
				if($gbt ne "coding"){$gbt{$gid}=$trbt;}
			}else{
				if($gbt ne "coding" && $gbt ne "noncoding"){$gbt{$gid}=$trbt;} # pseudo
			}
		}
	}

	if(!defined($gid2trs{$gid})){$gid2trs{$gid}={};}
	$gid2trs{$gid}->{$tid}=1;  							# remember that trs with tid comes from gene gid

	if($fs[2]=~ /exon/i){
		unless($exons{$tid}){$exons{$tid}="";} # -> aryref which contains refrences to arys containg start/ends of exons for each tr
		# start / end
		$exons{$tid}.=join("\t",$fs[3],$fs[4])."\n";
	}
	if($fs[2]=~ /utr/i){
		unless($utrs{$tid}){$utrs{$tid}="";}
		# start / end
		$utrs{$tid}.=join("\t",$fs[3],$fs[4])."\n";
	}
}
close($gtf);

# clean gtf: delete transcripts which are not coding from coding genes
#my $ndel=0;
#my $ntrs=0;
unless($notrbts){
	foreach my $gid (keys %gid2trs){
		$gbt=$gbt{$gid};
		if($gbt ne "coding"){next;}
		foreach my $tid (keys %{$gid2trs{$gid}}){
			$trbt=$trbt{$tid};
			if($gbt eq "coding" && $trbt ne "coding"){delete($gid2trs{$gid}->{$tid});}
		}
	}
}

my %already_worked_on_tr=();
my (@exon_starts,@exon_ends,@utr_starts,@utr_ends,@sorted_pos);
my ($gid,$estart,$eend,$estrand,$echr);

my @gbt=(); # gene biotype for gene in which intron occurs
my @erank=();
my @erelrank=();
my @seqs=();       # all seqs we need to retrieve from fasta
my @upex_ids=();   # all the following arrays contain ids to @seqs at pos i for exon i 
my @doex_ids=();
my @ex_ids=();    # id of exon i
my @doint_ids=();
my @upint_ids=();
my @up20int10ex_ids=();  # 5 ss
my @u20int10ex_ids=();   # 3 ss
my @d20int10ex_ids=();   # 5 ss
my @do20int10ex_ids=();  # 3 ss
my @ntrs_in_utr=();
my @ntrs_is_firstex=();
my @ntrs_is_lastex=();
my @ntrs_is_internalex=();
my @ntrs_withex=(); # number of trs where intron was found in
my @ntrs_all=();
my @n_exons=();  # contains for each exon an array with number of exons of all trs the exon was found in
my @feature_counts;
my @this_exon_occurs_in_trs=();
my %tid2eids=();
my @trlengths=();  # contains lengths of transcripts the exon occurs in
my @tr_ids=(); # contains transcript ids where exon was found in
for(my $j=0;$j<@feature_counts_arefs;$j++){$feature_counts[$j]=0;}

my %pos=();
my $seq_counter=-1;
sub add_to_seqs{
	my $seqs_aref=$_[0];
	my $c_ref=$_[1]; # seqs counter
	my $s=$_[2]; # start
	my $e=$_[3]; # end
	my $chr=$_[4]; # chr
	my $str=$_[5]; # strand
	my $pos_href=$_[6];
	my $where_to_add_pos_aref=$_[7];
	
	my $tmp_key=$s.",".$e.",".$chr.",".$str;
	# first time we come across this coordinate
	unless(defined($pos_href->{$tmp_key})){$pos_href->{$tmp_key}=++$$c_ref;push(@{$seqs_aref},"$s\t$e\t$chr\t$str");}
	push(@{$where_to_add_pos_aref}, $pos{$tmp_key});
return;
}

my ($s1,$s2,$s3,$s4,$e1,$e2,$e3,$e4,$idx);
for(my $i=0; $i<@estarts;$i++){
	$gid="";$gid=$egnames[$i];  # $gid might be "" in rare cases where we don't have a gene id for a ASE
	unless($notrbts){$gbt[$i]=$gbt{$gid};}
	$estart=$estarts[$i];
	$eend=$eends[$i];
#if($estart!=2577745 && $eend!=2578140){next;}
	
	$estrand=$estrands[$i];
	$echr=$echrs[$i];
	$eid=join(",",$estart,$eend,$echr,$estrand,$gid);
	$ntrs_in_utr[$i]=0;
	$ntrs_is_firstex[$i]=0;
	$ntrs_is_lastex[$i]=0;
	$ntrs_withex[$i]=0;
	$ntrs_is_internalex[$i]=0;
	$ntrs_all[$i]=0;
	$n_exons[$i]=[];
	$this_exon_occurs_in_trs[$i]=[];
	for(my $j=0;$j<@feature_counts;$j++){$feature_counts[$j]=0;}
	$tr_ids[$i]="";
	$ex_ids[$i]=[];
	$u20int10ex_ids[$i]=[];
	$d20int10ex_ids[$i]=[];
	$upex_ids[$i]=[];
	$upint_ids[$i]=[];
	$doex_ids[$i]=[];
	$doint_ids[$i]=[];
	$up20int10ex_ids[$i]=[];
	$do20int10ex_ids[$i]=[];
	# add seqs
	add_to_seqs(\@seqs,\$seq_counter,$estart,$eend,$echr,$estrand,\%pos,$ex_ids[$i]); # exon
	if($estrand eq "+"){$s1=$estart-20; $e1=$estart+9; $s2=$eend-9; $e2=$eend+20;}
	else{$s1=$eend-9; $e1=$eend+20; $s2=$estart-20; $e2=$estart+9;}  
	add_to_seqs(\@seqs,\$seq_counter,$s1,$e1,$echr,$estrand,\%pos,$u20int10ex_ids[$i]); # 20int10ex 3 ss; from these we extract later seqs for max ent score
	add_to_seqs(\@seqs,\$seq_counter,$s2,$e2,$echr,$estrand,\%pos,$d20int10ex_ids[$i]); # 20int10ex 5 ss; from these we extract later seqs for max ent score
	
	# collect this information across all trs
	my @exon_ranks=();
	my @exon_relranks=();
	my @tr_lengths_tmp=();
	
	# go over each tr of that gene
	foreach my $tid (keys %{$gid2trs{$gid}}){
		$ntrs_all[$i]++;
		if(!defined($already_worked_on_tr{$tid})){  # sort exons and utrs wrt. start
			@exon_starts=();@exon_ends=();
			foreach my $line (split("\n",$exons{$tid})){@fs=split("\t",$line);push(@exon_starts,$fs[0]);push(@exon_ends,$fs[1]);}
			@sorted_pos = sort { $exon_starts[$a] <=> $exon_starts[$b] } 0 .. $#exon_starts;
			@exon_starts=@exon_starts[@sorted_pos];@exon_ends=@exon_ends[@sorted_pos];
			my $tmp_str="";
			for(my $j=0;$j<@exon_starts;$j++){$tmp_str.="$exon_starts[$j]\t$exon_ends[$j]\n";}
			$exons{$tid}=$tmp_str;
			
			if($utrs{$tid}){
				@utr_starts=();@utr_ends=();
				foreach my $line (split("\n",$utrs{$tid})){@fs=split("\t",$line);push(@utr_starts,$fs[0]);push(@utr_ends,$fs[1]);}
				@sorted_pos = sort { $utr_starts[$a] <=> $utr_starts[$b] } 0 .. $#utr_starts;
				@utr_starts=@utr_starts[@sorted_pos];@utr_ends=@utr_ends[@sorted_pos];
				$tmp_str="";
				for(my $j=0;$j<@utr_starts;$j++){$tmp_str.="$utr_starts[$j]\t$utr_ends[$j]\n";}
				$utrs{$tid}=$tmp_str;
			}
		}

		@exon_starts=();@exon_ends=();@utr_starts=();@utr_ends=();
		foreach my $line (split("\n",$exons{$tid})){
			@fs=split("\t",$line);
			push(@exon_starts,$fs[0]);
			push(@exon_ends,$fs[1]);
		}
		
		# collect all up/do-stream exon pairs of overlapping introns
		# if we have more than 1 overlapping intron -> we select at the end the most up/do-stream exons and negelct all others
		my @upex_starts_tmp=();
		my @upex_ends_tmp=();
		my @doex_starts_tmp=();
		my @doex_ends_tmp=();
		my $isfirstex=0;
		my $islastex=0;
		my $isinutr=0;
		my $isinternalex=0;
		
		#print $istrand." ".$exon_starts[0]."<".$istart." && ".$iend."<".$exon_ends[@exon_ends-1]."   ".scalar(@exon_starts)."=".scalar(@exon_ends);
		if($exon_starts[0]<$eend && $estart<$exon_ends[@exon_ends-1]){  # this trs overlaps the exon
			my $found_overlapping_exon=0;
			my $rank_for_plusstrand="";# exon_relranks  exon_ranks
			my $rank_for_negstrand="";
			# coming from up-stream
			for(my $j=0; $j<@exon_starts;$j++){
				if($j==0 && $estart-$exon_ends[$j]<=1){$rank_for_plusstrand=1;$isfirstex=1;last;}
				if($j<@exon_starts-1 && $exon_ends[$j]<$estart && $estart-$exon_ends[$j+1]<=1){ # last up-stream exon before exon
						$rank_for_plusstrand=$j+2;					# with $estart-$exon_ends[$j+1] we tackle complex cases like
						push(@upex_starts_tmp,$exon_starts[$j]);			# 1-10.....20-30......40-50  and we search for a exon 31-35.
						push(@upex_ends_tmp,$exon_ends[$j]);                            # in this case, the up-stream exon is 1-10 not 20-30
						last;
				}
			}
			# coming from down-stream
			for(my $j=@exon_starts-1; $j>-1;$j--){
				if($j==@exon_starts-1 && $exon_starts[$j]-$eend<=1){$rank_for_negstrand=1;$islastex=1;last;}
				if($j>0 && $exon_starts[$j]>$eend && $exon_starts[$j-1]-$eend<=1){ # first do-stream exon after exon
						$rank_for_negstrand=@exon_starts-$j+1;
						push(@doex_starts_tmp,$exon_starts[$j]);
						push(@doex_ends_tmp,$exon_ends[$j]);
						last;
				}
			}

			unless($isfirstex || $islastex){$isinternalex=1;}
			my $rank=$rank_for_plusstrand;
			if($estrand eq "-"){
				my $islastex_tmp=$islastex;
				if($isfirstex){$islastex=1;}else{$islastex=0;}
				if($islastex_tmp){$isfirstex=1;}else{$isfirstex=0;}
				$rank=$rank_for_negstrand;
				my @tmpa=@upex_starts_tmp;
				@upex_starts_tmp=@doex_starts_tmp;
				@doex_starts_tmp=@tmpa;
				@tmpa=@upex_ends_tmp;
				@upex_ends_tmp=@doex_ends_tmp;
				@doex_ends_tmp=@tmpa;
			}
			push(@exon_ranks,$rank);
			push(@exon_relranks,$rank/(@exon_starts));
						
			$found_overlapping_exon=1;  # we always find the exon in an overlapping tr because we don't search anymore for overlapping exons

			if($found_overlapping_exon){
				$ntrs_withex[$i]++;
				# collect feature count
				for(my $j=0;$j<@feature_counts;$j++){if(defined($feature_tid_hrefs[$j]->{$tid})){$feature_counts[$j]++;}}
				push(@{$n_exons[$i]},scalar(@exon_ends));  # save number of exons of this trs
				push(@{$this_exon_occurs_in_trs[$i]},$tid);
				if(!defined($tid2eids{$tid})){$tid2eids{$tid}=$eid;}else{$tid2eids{$tid}.=":".$eid;}
				# check if exon overlaps with utr (only check if we actually have a annotated utr and if the exon does not yet overlap with a utr in another tr)
				if(defined($utrs{$tid})&& !$isinutr){foreach my $line (split("\n",$utrs{$tid})){@fs=split("\t",$line);if($fs[0]<$eend && $fs[1]>$estart){$isinutr=1;}}}
				push(@tr_lengths_tmp,$exon_ends[@exon_ends-1]-$exon_starts[0]);  # end of last exon - start of first exon (here we neglect UTRs));
				if($tr_ids[$i] eq ""){$tr_ids[$i]="$tid"}else{$tr_ids[$i].=":$tid";}
			}

			# we do the summation here in order not to count several times if by chance in a tr we have several overlapping exons
			$ntrs_is_firstex[$i]+=$isfirstex;
			$ntrs_is_lastex[$i]+=$islastex;
			$ntrs_is_internalex[$i]+=$isinternalex;
			$ntrs_in_utr[$i]+=$isinutr;
		}else{
			#print "\t NO\n";
		}

#print "$tid: upex starts - ends:\n";
#for(my $ii=0;$ii<@upex_starts_tmp;$ii++){print "$upex_starts_tmp[$ii]-$upex_ends_tmp[$ii]\n";}


		# we found at least one overlapping exon -> take as up/do-stream exon the most up/do-stream exon 
		# and corredponing up/do-stream introns
		# and sss
		if(@upex_starts_tmp>0){
			if($estrand eq "+"){
				$idx=get_idx_of_min_element_aref(\@upex_starts_tmp);
				$s1=$upex_starts_tmp[$idx]; $e1=$upex_ends_tmp[$idx]; # exon		
				$s2=$e1+1; $e2=$estart-1; # intron
				$s3=$e1-9; $e3=$e1+20; # 20int10ex up 5ss 
			}else{
				$idx=get_idx_of_max_element_aref(\@upex_ends_tmp);
				$s1=$upex_starts_tmp[$idx]; $e1=$upex_ends_tmp[$idx]; # exon		
				$s2=$eend+1; $e2=$s1-1; # intron
				$s3=$s1-20; $e3=$s1+9; # 20int10ex up 5ss
			}
			# up exon
			add_to_seqs(\@seqs,\$seq_counter,$s1,$e1,$echr,$estrand,\%pos,$upex_ids[$i]);
			# up intron
#print "upintron: $s2-$e2,$echr,$estrand\n";
			add_to_seqs(\@seqs,\$seq_counter,$s2,$e2,$echr,$estrand,\%pos,$upint_ids[$i]);
			# up 5ss
			add_to_seqs(\@seqs,\$seq_counter,$s3,$e3,$echr,$estrand,\%pos,$up20int10ex_ids[$i]);
		}
		if(@doex_starts_tmp>0){
			if($estrand eq "+"){
				$idx=get_idx_of_max_element_aref(\@doex_ends_tmp);
				$s1=$doex_starts_tmp[$idx];	$e1=$doex_ends_tmp[$idx]; # exon		
				$s2=$eend+1; $e2=$s1-1; # intron
				$s3=$s1-20; $e3=$s1+9; # do 20int10ex
			}else{
				$idx=get_idx_of_min_element_aref(\@doex_starts_tmp);
				$s1=$doex_starts_tmp[$idx];	$e1=$doex_ends_tmp[$idx]; # exon		
				$s2=$e1+1; $e2=$estart-1; # intron
				$s3=$e1-9; $e3=$e1+20; # do 20int10ex
			}
			# do exon
			add_to_seqs(\@seqs,\$seq_counter,$s1,$e1,$echr,$estrand,\%pos,$doex_ids[$i]);
			# do intron
			add_to_seqs(\@seqs,\$seq_counter,$s2,$e2,$echr,$estrand,\%pos,$doint_ids[$i]);
			# do 5ss
			add_to_seqs(\@seqs,\$seq_counter,$s3,$e3,$echr,$estrand,\%pos,$do20int10ex_ids[$i]);
		}
		$already_worked_on_tr{$tid}=1;
	}# for over trs

	if(@exon_ranks>0){push(@erank,get_median_from_aref(\@exon_ranks));}else{push(@erank,"NA");}
	if(@exon_relranks>0){push(@erelrank,get_median_from_aref(\@exon_relranks));}else{push(@erelrank,"NA");}
	if(@tr_lengths_tmp>0){push(@trlengths,get_median_from_aref(\@tr_lengths_tmp));}else{push(@trlengths,"NA");}

	# store counts for user specified features if we have any
	for(my $j=0;$j<@feature_counts;$j++){push(@{$feature_counts_arefs[$j]},$feature_counts[$j]);}		
}

# clear memory
%exons=();%pos=();%utrs=();%gid2trs=();%already_worked_on_tr=();%gids_tmp=();


# get intron and exon sequences
my @lens=();
# create temporary file; will be deleted automatically at the end of script or if error occurs
my ($fh_tmp,$tmpfile_n) = tempfile( DIR => ".", UNLINK => 1);
print $fh_tmp "START\tEND\tCHR\tSTRAND\n";
for(my $i=0;$i<@seqs;$i++){
	print $fh_tmp "$seqs[$i]\n";
	my @fs=split("\t",$seqs[$i]);
	push(@lens,$fs[1]-$fs[0]+1);
}
close($fh_tmp);

my @gcc=();
my $cmd;
my @sss3=(); my @sss5=(); my $cmd_fh;
my @max_sf1_scores_up=();
my @seqs_bp_analysis_up=();
my @max_sf1_scores_do=();
my @seqs_bp_analysis_do=();
if($fasta_file eq "NA"){@seqs=();}
if($fasta_file ne "NA"){
	my $cmd="matt get_seqs $tmpfile_n START END CHR STRAND $fasta_file";
	open(my $cmd_fh,"$cmd |");<$cmd_fh>;
	my $i=-1;
	while(<$cmd_fh>){
	if(substr($_,0,1) eq "#"){print $_;next;}
		chomp($_);
		$seqs[++$i]=$_;  # replace seq defs by seqs
		# print "$i\t$_\tgcc=";  # XXX
		$gcc[$i]=get_gcc_from_str($_);
		# print "$gcc[$i]\n";    # XXX
	}
	close($cmd_fh);
	unlink($tmpfile_n);
	if($i+1 != @seqs){die "Could not extract all necessary sequences ($seqfeat_file) from FASTA ($fasta_file).";}

	# get max ent score for splice sites
	@sss3=();my $ids_tmp_aref=[];foreach my $tmp_aref (@u20int10ex_ids,@do20int10ex_ids){
		push(@$ids_tmp_aref,@$tmp_aref);
	}
	# make unique ids
	$ids_tmp_aref=uniq_from_aref($ids_tmp_aref);
	($fh_tmp,$tmpfile_n) = tempfile( DIR => ".", UNLINK => 1);
	print $fh_tmp "3SS_SEQ\n";foreach my $seq_tmp (@seqs[@$ids_tmp_aref]){print $fh_tmp substr($seq_tmp,0,23)."\n";}close($fh_tmp);
	$cmd="matt get_sss $tmpfile_n -3 3SS_SEQ";
	open($cmd_fh,"$cmd |");<$cmd_fh>;
	my $i_tmp=-1;while(<$cmd_fh>){chomp($_);$sss3[ $ids_tmp_aref->[++$i_tmp] ]=$_;}#print "3ss: $ids_tmp[$i_tmp] -> $_\n";}
	unlink($tmpfile_n);

	@sss5=();$ids_tmp_aref=[];foreach my $tmp_aref (@d20int10ex_ids,@up20int10ex_ids){push(@$ids_tmp_aref,@$tmp_aref);}
	($fh_tmp,$tmpfile_n) = tempfile( DIR => ".", UNLINK => 1);
	print $fh_tmp "5SS_SEQ\n";foreach my $seq_tmp (@seqs[@$ids_tmp_aref]){print $fh_tmp substr($seq_tmp,7,9)."\n";}close($fh_tmp);
	$cmd="matt get_sss $tmpfile_n -5 5SS_SEQ";
	open($cmd_fh,"$cmd |");<$cmd_fh>;  
	$i_tmp=-1;while(<$cmd_fh>){chomp($_);$sss5[ $ids_tmp_aref->[++$i_tmp] ]=$_;}# print "5ss: $ids_tmp[$i_tmp] -> $_\n";}
	unlink($tmpfile_n);
	$ids_tmp_aref=[];


	# score of best SF1 hit in the intron sequences
	my $minprob=0.000001;
	my @tmp=read_pwm("$scr_abspath/../data_external/pwm/sf1_human_l7.tab",$MYCOMMAND,$minprob);
	my $pwm_aryref=$tmp[0];   # PWM has length 7
	my $alph_aryref=$tmp[1];
	@tmp=();

	# we search only in the 150 ($int_3ss_len) most down-stream positions of the introns
	# we always leave out the 20 most up-stream positions
	# * if intron length >=170 -> we search in the 150 most down-stream positions
	# * if intron length < 170 -> we search in the intron_length-20 most down-stream positions
	my $tmp_seq;
	for(my $i=0; $i<@ex_ids;$i++){
		$tmp_seq="";
		# find longest intron
		foreach my $id (@{$upint_ids[$i]}){ if(length($seqs[$id])>length($tmp_seq)){$tmp_seq=$seqs[$id];} }
		if(length($tmp_seq)>20){
			$tmp_seq=substr($tmp_seq, 20); # we always neglect the first 20 positions at the 5' end
			# we shorten down to the last positions which do not contain any N
			($tmp_seq = $tmp_seq) =~ s/^.*N//i;
			if(length($tmp_seq)>$int_3ss_len){$tmp_seq=substr($tmp_seq,length($tmp_seq)-$int_3ss_len);}
			# get_pwm_scores_for_seq return also NA if there are symbols in the sequences not previously defined like Ns
			# ignoreSTRINGS -> these NA values are not considered for determination of maximum
			$max_sf1_scores_up[$i]=get_max_from_aref( get_pwm_scores_for_seq( $tmp_seq, $pwm_aryref ) , "ignoreSTRINGS" );
			$seqs_bp_analysis_up[$i]=$tmp_seq;
			next;
		}
		push(@max_sf1_scores_up,"NA");
		push(@seqs_bp_analysis_up,"");
	}



	for(my $i=0; $i<@ex_ids;$i++){
		$tmp_seq="";
		# find longest intron
		foreach my $id (@{$doint_ids[$i]}){ if(length($seqs[$id])>length($tmp_seq)){$tmp_seq=$seqs[$id];} }
		if(length($tmp_seq)>20){
			$tmp_seq=substr($tmp_seq, 20); # we always neglect the first 20 positions at the 5' end
			# we shorten down to the last positions which do not contain any N
			($tmp_seq = $tmp_seq) =~ s/^.*N//i;
			if(length($tmp_seq)>$int_3ss_len){$tmp_seq=substr($tmp_seq,length($tmp_seq)-$int_3ss_len);}
			# get_pwm_scores_for_seq return also NA if there are symbols in the sequences not previously defined like Ns
			# ignoreSTRINGS -> these NA values are not considered for determination of maximum
			$max_sf1_scores_do[$i]=get_max_from_aref( get_pwm_scores_for_seq( $tmp_seq, $pwm_aryref ) , "ignoreSTRINGS" );
			$seqs_bp_analysis_do[$i]=$tmp_seq;
			next;
		}
		push(@max_sf1_scores_do,"NA");
		push(@seqs_bp_analysis_do,"");
	}
}

sub get_bpfs{
	my $seqs_aref=$_[0];
	my $eids_aref=$_[1];
	my $species=$_[2];
	my $int_3ss_len=$_[3];
	my $scr_abspath=$_[4];
	
	# dist23ss bpseq bpseq_scr pyc polypy_off polypy_l polypy_scr bp_scr nbp_pos_scr
	my @ret=([],[],[],[],[],[],[],[],[]);
		
	my %eid2arypos=();my $i;
	($fh_tmp,$tmpfile_n) = tempfile( DIR => ".", UNLINK => 1);
	for($i=0;$i<@{$seqs_aref};$i++){
		if(!defined($eid2arypos{$eids_aref->[$i]})){$eid2arypos{$eids_aref->[$i]}={};} #XXX
		$eid2arypos{$eids_aref->[$i]}->{$i}=1; # same exon might occur in different rows
		if(length($seqs_aref->[$i])==0){next;};
		# seqs need to be lower case and must not contain n (this has been checked already before)
		print $fh_tmp ">$eids_aref->[$i]\n".lc($seqs_aref->[$i])."\n";
	}close($fh_tmp);
	
	$cmd="$scr_abspath/../external_progs/SVM-BPfinder-3M/svm_bpfinder.py -i $tmpfile_n -s $species -l $int_3ss_len";
	open($cmd_fh,"$cmd |");<$cmd_fh>; # header  
	while(<$cmd_fh>){
		# seq_id  agez  ss_dist  bp_seq  bp_scr  y_cont  ppt_off  ppt_len  ppt_scr  svm_scr
		@fs=clean_and_split_line($_);
		$fs[3]=uc($fs[3]); # make BP sequence upper case
		foreach $i (keys %{$eid2arypos{$fs[0]}}){
			unless(defined($ret[0]->[$i])){for(my $j=0;$j<8;$ret[$j++]->[$i]=[]){}}  # if these features are the first for exon i
			for(my $j=0;$j<8;push(@{$ret[$j++]->[$i]},$fs[$j+1])){}                  # add features to those of exon i
		}
	}
	unlink($tmpfile_n);%eid2arypos=();

	# we reduce infos to those of the topN=3 BPs if we actually have more BPs
	my $topN=3;
	for($i=0;$i<@{$seqs_aref};$i++){unless(defined($ret[0]->[$i])){next;}
		my $idxs=get_idxs_of_desc_ordering_aref($ret[7]->[$i]);
		# from all BPs get the number of those with score > 0
		my $n_bp_with_pos_score=0;
		foreach my $j (@{$idxs}){if($ret[7]->[$i]->[$j]>0){$n_bp_with_pos_score++;}else{last;}}
		$ret[8]->[$i]=[$n_bp_with_pos_score];  # save
#print "idxs for $i:\n";
#foreach my $tmptmp (@$idxs){print "$tmptmp ";}print "\n";
#print "len(idxs): ".(min(scalar(@{$idxs}),$topN)-1)."\n";
#		# reduction to at most topN BPs if more are available
		my @idxs=@{$idxs}[0..(min(scalar(@{$idxs}),$topN)-1)];
#print "idxs for $i:\n";
#foreach my $tmptmp (@idxs){print "$tmptmp ";}print "\n";
#		# we always have to sort as final output later depends on sorted entries
		for(my $j=0;$j<8;$j++){$ret[$j]->[$i]=[ @{$ret[$j]->[$i]}[@idxs] ];}
	}
return(\@ret);
}

# branch point analysis
my $bpfs_up_aref=[];my $bpfs_do_aref=[];
if($species ne "NA" && $fasta_file ne "NA"){
	# up intron
	# $bpfs_up_aref contains: ss_dist  bp_seq  bp_scr  y_cont  ppt_off  ppt_len  ppt_scr  svm_scr number_bps_with_score_>0
	$bpfs_up_aref=get_bpfs(\@seqs_bp_analysis_up,\@eids,$species,$int_3ss_len,$scr_abspath);
	# di intron
	$bpfs_do_aref=get_bpfs(\@seqs_bp_analysis_do,\@eids,$species,$int_3ss_len,$scr_abspath);
}

#for(my $ii=0;$ii<@$bpfs_up_aref;$ii++){
#print "$ii ".scalar(@{$bpfs_up_aref->[$ii]})."\n";
#for(my $j=0;$j<@{$bpfs_up_aref->[$ii]};$j++){
#unless(defined($bpfs_up_aref->[$ii]->[$j])){print "$ii $j: undef\n";next;}
#for(my $jj=0;$jj<@{$bpfs_up_aref->[$ii]->[$j]};$jj++){
#unless(defined($bpfs_up_aref->[$ii]->[$j]->[$jj])){print "$ii $j $jj: undef\n";next;}
#print "$ii $j $jj: ".$bpfs_up_aref->[$ii]->[$j]->[$jj]."\n";
#}}}

#exit;

# OUTPUT OF FEATURES
my @header_out=();
if($species ne "NA" && $fasta_file ne "NA"){for(my $i=0; $i<@feature_expl;$i++){($header_out[$i])=split(" ",$feature_expl[$i]);}}
if($species eq "NA" && $fasta_file ne "NA"){
	for(my $i=0; $i<@feature_expl;$i++){
		unless($feature_expl[$i] =~ /\.( |)$/){  # BP features which are here unavailable have end in . or .blank
			($header_out[$i])=split(" ",$feature_expl[$i]);
		}
	}
}
if($fasta_file eq "NA"){  # same output features regardless of species
	my @feature_expl_tmp=@feature_expl[(0..11,69..89)];
	for(my $i=0; $i<@feature_expl_tmp;$i++){($header_out[$i])=split(" ",$feature_expl_tmp[$i]);}
}
for(my $j=0;$j<@feature_names;$j++){push(@header_out,"NTRS_".$feature_names[$j]);push(@header_out,"PROP_".$feature_names[$j]);}
print join("\t",@header_out)."\n";


my ($tmp1,$tmp2,$tmp3,$tmp4,$tmp5,$bin,%tmp,$exon_not_found,@tmp_strs);
for(my $i=0; $i<$N_exons;$i++){
	$exon_not_found=0;if(@{$this_exon_occurs_in_trs[$i]}==0){$exon_not_found=1;}

	# EXON_ID
	$eid=$eids[$i];
	print $eid;

	# GENE_BIOTYPE
	unless($notrbts){
		if(defined($gbt[$i])){print "\t$gbt[$i]";}else{print "\t";}
	}else{print "\t";}

	# EXON_FOUND_IN_GTF
	if($exon_not_found){print "\tno"}else{print "\tyes";}

	# EXON_LENGTH
	$tmp1=$eends[$i]-$estarts[$i]+1;
	print "\t$tmp1";

	# UPEXON_MEDIANLENGTH
	$tmp2=get_median_from_aref( [ @lens[@{$upex_ids[$i]}] ] );  # returns NA if @{$upex_ids[$i]} is empty array
	print "\t$tmp2";

	# DOWNEXON_MEDIANLENGTH
	$tmp3=get_median_from_aref( [ @lens[@{$doex_ids[$i]}] ] );  # returns NA if @{$doex_ids[$i]} is empty array 
	print "\t$tmp3";

	# RATIO_UPEXON_EXON_LENGTH
	if($tmp2 ne "NA" && $tmp1 ne "NA" && $tmp1>0){print "\t".($tmp2/$tmp1);}else{print "\tNA";}
	
	# RATIO_DOWNEXON_EXON_LENGTH
	if($tmp3 ne "NA" && $tmp1 ne "NA" && $tmp1>0){print "\t".($tmp3/$tmp1)."";}else{print "\tNA";}
	
	# UPINTRON_MEDIANLENGTH
	$tmp2=get_median_from_aref( [ @lens[@{$upint_ids[$i]}] ] );  # returns NA if @{$upex_ids[$i]} is empty array
	print "\t$tmp2";

	# DOWNINTRON_MEDIANLENGTH
	$tmp3=get_median_from_aref( [ @lens[@{$doint_ids[$i]}] ] );  # returns NA if @{$doex_ids[$i]} is empty array 
	print "\t$tmp3";

	# RATIO_UPINTRON_EXON_LENGTH
	if($tmp2 ne "NA" && $tmp1 ne "NA" && $tmp1>0){print "\t".($tmp2/$tmp1)."";}else{print "\tNA";}
	
	# RATIO_DOWNINTRON_EXON_LENGTH
	if($tmp3 ne "NA" && $tmp1 ne "NA" && $tmp1>0){print "\t".($tmp3/$tmp1)."";}else{print "\tNA";}

	# EXON_GCC
	if(@gcc>0){
		$tmp1=$gcc[$ex_ids[$i]->[0]];print "\t".$tmp1;

		# UPINTRON_GCC   # gcc of NNNNN is NA
		$tmp2=get_median_from_aref([ @gcc[@{$upint_ids[$i]}] ],"ignoreSTRINGS"); print "\t".$tmp2."";
	
		# UPEXON_GCC
		$tmp3=get_median_from_aref([ @gcc[@{$upex_ids[$i]}] ],"ignoreSTRINGS"); print "\t".$tmp3."";
	
		# DOINTRON_GCC
		$tmp4=get_median_from_aref([ @gcc[@{$doint_ids[$i]}] ],"ignoreSTRINGS"); print "\t".$tmp4."";
	
		# DOEXON_GCC
		$tmp5=get_median_from_aref([ @gcc[@{$doex_ids[$i]}] ],"ignoreSTRINGS"); print "\t".$tmp5."";
	
		# RATIO_UPEXON_EXON_GCC
		if($tmp3 ne "NA" && $tmp1 ne "NA" && $tmp1>0){print "\t".($tmp3/$tmp1)."";}else{print "\tNA";}
	
		# RATIO_UPINTRON_EXON_GCC
		if($tmp2 ne "NA" && $tmp1 ne "NA" && $tmp1>0){print "\t".($tmp2/$tmp1)."";}else{print "\t"."NA";}
	
		# RATIO_DOINTRON_EXON_GCC
		if($tmp4 ne "NA" && $tmp1 ne "NA" && $tmp1>0){print "\t".($tmp4/$tmp1)."";}else{print "\tNA";}

		# RATIO_DOEXON_EXON_GCC
		if($tmp5 ne "NA" && $tmp1 ne "NA" && $tmp1>0){print "\t".($tmp5/$tmp1)."";}else{print "\tNA";}
	}

	# HUMANSF1_HIGHESTSCORE_3SS_UPINTRON
	if(@max_sf1_scores_up>0){print "\t".$max_sf1_scores_up[$i]."";}

	# HUMANSF1_HIGHESTSCORE_3SS_DOINTRON
	if(@max_sf1_scores_do){print "\t".$max_sf1_scores_do[$i]."";}

	# LEN_SEQ_UPINTRON_USED_FOR_BP_ANALYSIS
	if(@seqs_bp_analysis_up>0){
		$tmp1=$seqs_bp_analysis_up[$i];
		if(defined($tmp1)){ print "\t".length( $tmp1 ).""; }else{ print "\tNA"; }
	}

	# LEN_SEQ_DOINTRON_USED_FOR_BP_ANALYSIS
	if(@seqs_bp_analysis_do>0){
		$tmp1=$seqs_bp_analysis_do[$i];
		if(defined($tmp1)){ print "\t".length( $tmp1 ).""; }else{ print "\tNA"; }
	}
	
	# UP_5SS_20INT10EX_GCC
	if(@gcc>0){print "\t".get_median_from_aref([ @gcc[@{$up20int10ex_ids[$i]}] ],"ignoreSTRINGS")."";}
	
	# 3SS_20INT10EX_GCC
	if(@gcc>0){print "\t".@gcc[@{$u20int10ex_ids[$i]}]."";}
	
	# 5SS_20INT10EX_GCC
	if(@gcc>0){print "\t".@gcc[@{$d20int10ex_ids[$i]}]."";}
	
	# DO_3SS_20INT10EX_GCC
	if(@gcc>0){print "\t".get_median_from_aref([ @gcc[@{$do20int10ex_ids[$i]}] ],"ignoreSTRINGS")."";}

	# MAXENTSCORE_BURGEHUMANMODEL_UPSTREAM_5SS
	my $me_up5ss="NA";
	if(@sss5>0){
		$me_up5ss=get_median_from_aref([ @sss5[@{$up20int10ex_ids[$i]}] ],"ignoreSTRINGS");
		print "\t$me_up5ss";
	}

	# MAXENTSCORE_BURGEHUMANMODEL_3SS	  # print @a[@b]   takes last element of @b as index and prints from @a the corresponding element
	my $me_3ss="NA";
	if(@sss3>0){
		$me_3ss=@sss3[@{$u20int10ex_ids[$i]}];
		print "\t$me_3ss"; # prints empty string if @b=()
	}
	
	# MAXENTSCORE_BURGEHUMANMODEL_5SS
	my $me_5ss="NA";
	if(@sss5>0){
		$me_5ss=@sss5[@{$d20int10ex_ids[$i]}];
		print "\t$me_5ss";
	}
	
	# MAXENTSCORE_BURGEHUMANMODEL_DOWNSTREAM_3SS
	my $me_do3ss="NA";
	if(@sss3>0){
		$me_do3ss=get_median_from_aref([ @sss3[@{$do20int10ex_ids[$i]}] ],"ignoreSTRINGS");
		print "\t$me_do3ss";
	}
	
	# MAXENTSCORE_BURGEHUMANMODEL_3SS - MAXENTSCORE_BURGEHUMANMODEL_DOWNSTREAM_3SS
	if(@sss3>0){
		if(is_numeric($me_3ss) && is_numeric($me_do3ss)){print "\t".($me_3ss-$me_do3ss)."";}else{print "\tNA";}
	}
	
	# MAXENTSCORE_BURGEHUMANMODEL_5SS - MAXENTSCORE_BURGEHUMANMODEL_UPSTREAM_5SS
	if(@sss5>0){
		if(is_numeric($me_5ss) && is_numeric($me_up5ss)){print "\t".($me_5ss-$me_up5ss)."";}else{print "\tNA";}
	}

	# output BP feature only if species was ne NA
	if($species ne "NA" && @{$bpfs_up_aref}>0 && @{$bpfs_do_aref}>0){
		
		unless(defined($bpfs_up_aref->[0]->[$i])){ # we do not have branch point features for this intron
				print "\tNA\t\tNA\tNA\tNA\tNA\tNA\tNA\tNA\tNA\t\tNA\tNA\tNA\tNA\tNA\tNA";
		}else{
			# branch point features for branch point with max score -> is always first entry in arrays because the are sorted according to decreasing BP score
			for(my $j=0;$j<8;print "\t".$bpfs_up_aref->[$j++]->[$i]->[0].""){}
			# number of BPs with score > 0
			print "\t".$bpfs_up_aref->[8]->[$i]->[0]."";		
			# median bp features for topN BPs
			for(my $j=0;$j<8;$j++){
				if($j != 1){
					print "\t".get_median_from_aref( $bpfs_up_aref->[$j]->[$i] ).""
				}else{
					print "\t".join(",",@{$bpfs_up_aref->[$j]->[$i]} ).""
				}
			}
		}
	
		unless(defined($bpfs_do_aref->[0]->[$i])){ # we do not have branch point features for this intron
				print "\tNA\t\tNA\tNA\tNA\tNA\tNA\tNA\tNA\tNA\t\tNA\tNA\tNA\tNA\tNA\tNA";
		}else{
			# branch point features for branch point with max score -> is always first entry in arrays because the are sorted according to decreasing BP score
			for(my $j=0;$j<8;print "\t".$bpfs_do_aref->[$j++]->[$i]->[0].""){}
			# number of BPs with score > 0
			print "\t".$bpfs_do_aref->[8]->[$i]->[0]."";		
			# median bp features for topN BPs
			for(my $j=0;$j<8;$j++){
				if($j != 1){
					print "\t".get_median_from_aref( $bpfs_do_aref->[$j]->[$i] ).""
				}else{
					print "\t".join(",",@{$bpfs_do_aref->[$j]->[$i]} ).""
				}
			}
		}
	}

	# EXON_FOUND_IN_THESE_TRS
	print "\t".join(":",sort(split(":",$tr_ids[$i])))."";
	
	# MEDIAN_TR_LENGTH
	print "\t$trlengths[$i]";

	# MEDIAN_EXON_NUMBER
	print "\t".get_median_from_aref($n_exons[$i])."";

	# EXON_MEDIANRANK
	print "\t$erank[$i]";   # is NA if intron not found in any tr
	
	# EXON_MEDIANRELATIVERANK
	print "\t$erelrank[$i]";  # is NA if intron not found in any tr
	
	# EXON_MEDIANRELATIVERANK_3BINS
	$bin="NA";
	if($erelrank[$i] ne "NA"){ $bin=int( $erelrank[$i] / 0.3333333333333333 ) + 1;if($bin==4){$bin=3;} }
	print "\t$bin";
	
	# EXON_MEDIANRELATIVERANK_5BINS
	$bin="NA";
	if($erelrank[$i] ne "NA"){ $bin=int( $erelrank[$i] / 0.2 ) + 1;if($bin==6){$bin=5;} }
	print "\t$bin";
	
	# INTRON_MEDIANRELATIVERANK_10BINS
	$bin="NA";
	if($erelrank[$i] ne "NA"){ $bin=int( $erelrank[$i] / 0.1 ) + 1;if($bin==11){$bin=10;} }
	print "\t$bin";

	# NTRS_ALL_FOR_GENE
	print "\t$ntrs_all[$i]";

	# NTRS_WITH_EXON
	$tmp1=$ntrs_withex[$i];
	print "\t$tmp1";
	
	# NTRS_WITH_EXON_AS_FIRST_EXON
	$tmp2=$ntrs_is_firstex[$i];
	print "\t$tmp2";
	
	# PROP_FIRST_EXON
	if($tmp1>0){print "\t".($tmp2/$tmp1)."";}else{print "\tNA";}
	
	# NTRS_WITH_EXON_AS_LAST_EXON
	$tmp2=$ntrs_is_lastex[$i];
	print "\t$tmp2";
	
	# PROP_LAST_EXON
	if($tmp1>0){print "\t".($tmp2/$tmp1)."";}else{print "\tNA";}
	
	# NTRS_WITH_EXON_AS_INTERNAL_EXON
	$tmp2=$ntrs_is_internalex[$i];
	print "\t$tmp2";
	
	# PROP_INTERNAL_EXON
	if($tmp1>0){print "\t".($tmp2/$tmp1)."";}else{print "\tNA";}
	
	# NTRS_WITH_EXON_IN_UTR
	$tmp2=$ntrs_in_utr[$i];
	print "\t$tmp2";
	
	# PROP_EXON_IN_UTR
	if($tmp1>0){print "\t".($tmp2/$tmp1)."";}else{print "\tNA";}

	my @cnts_co_exs=();
	my $cooccurs_with_other_exons="no";
	%tmp=();
	foreach my $tid (@{$this_exon_occurs_in_trs[$i]}){
		my %tmptmp=();
		foreach my $eid_tmp (split(":",$tid2eids{$tid})){
			if($eid_tmp ne $eid){$tmp{$eid_tmp}=1; $tmptmp{$eid_tmp}=1;$cooccurs_with_other_exons="yes";}
		}
		push(@cnts_co_exs,scalar(keys %tmptmp));
	}
	if($exon_not_found){$cooccurs_with_other_exons="NA";}  # exon not found in any tr

	# EXON_COOCCURS_WITH_OTHER_EXONS
	print "\t$cooccurs_with_other_exons";
	
	# EXON_COOCCURS_WITH_THESE_EXONS
	print "\t".join(":",sort(keys %tmp))."";

	# MEDIAN_EXON_EXON_COOCCURRENCE_NUMBER_OVER_ALL_TRS 
	print "\t".get_median_from_aref(\@cnts_co_exs)."";  # is NA if @cnts_co_introns is empty

	if(@seqs>0){
		# SEQ_UPEXON
		print "\t".get_longest_str_from_aref([ @seqs[@{$upex_ids[$i]}] ])."";
	
		# SEQ_UPINTRON
		print "\t".get_longest_str_from_aref([ @seqs[@{$upint_ids[$i]}] ])."";
	
		# SEQ_UPINTRON_USED_FOR_BP_ANALYSIS
		print "\t".$seqs_bp_analysis_up[$i]."";
	
		# SEQ_EXON
		print "\t".$seqs[$ex_ids[$i]->[0]]."";

		# SEQ_DOINTRON
		print "\t".get_longest_str_from_aref([ @seqs[@{$doint_ids[$i]}] ])."";

		# SEQ_DOINTRON_USED_FOR_BP_ANALYSIS
		print "\t".$seqs_bp_analysis_do[$i]."";

		# SEQ_DOEXON
		print "\t".get_longest_str_from_aref([ @seqs[@{$doex_ids[$i]}] ])."";

		# SEQ_UP_5SS_20INT10EX
		print "\t".join(",",@{uniq_from_aref([ @seqs[@{$up20int10ex_ids[$i]}] ])})."";

		# SEQ_UP_5SS_6INT3EX
		print "\t".join(",",@{ substrs(uniq_from_aref([ @seqs[@{$up20int10ex_ids[$i]}] ]),7,9) })."";

		# SEQ_3SS_20INT10EX
		@tmp_strs=@seqs[@{$u20int10ex_ids[$i]}];
		if(@tmp_strs>0){print "\t".$tmp_strs[0]."";}else{print "\t";}

		# SEQ_3SS_20INT3EX
		if(@tmp_strs>0){print "\t".substr($tmp_strs[0],0,23)."";}else{print "\t";}

		# SEQ_5SS_20INT10EX
		@tmp_strs=@seqs[@{$d20int10ex_ids[$i]}];
		if(@tmp_strs>0){print "\t".$tmp_strs[0]."";}else{print "\t";}

		# SEQ_5SS_6INT3EX
		if(@tmp_strs>0){print "\t".substr($tmp_strs[0],7,9)."";}else{print "\t";}

		# SEQ_DO_3SS_20INT10EX
		print "\t".join(",",@{uniq_from_aref([ @seqs[@{$do20int10ex_ids[$i]}] ])})."";

		# SEQ_DO_3SS_20INT3EX
		print "\t".join(",",@{ substrs(uniq_from_aref([ @seqs[@{$do20int10ex_ids[$i]}] ]),0,23) });  # no last TAB
	}

	# counts and proportions for user specified features
	my $str_tmp="\t";
	for(my $j=0;$j<@feature_counts;$j++){
		$tmp1=$ntrs_withex[$i];
		$tmp2=$feature_counts_arefs[$j]->[$i];

		# count
		if($exon_not_found){$str_tmp.="NA\t";
		}else{$str_tmp.="$tmp2\t";}

		# proportion
		if($tmp1>0 && !$exon_not_found){$str_tmp.=$tmp2/$tmp1."\t";}else{$str_tmp.="NA\t";}
	}
	# omit last tab
	print substr($str_tmp,0,length($str_tmp)-1);

	print "\n";
}



exit(0); # success
__END__
