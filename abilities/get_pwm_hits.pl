#!/usr/bin/env perl
use strict;
use warnings;

use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.


my $MYCOMMAND="get_pwm_hits";

# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "seq_analysis\t$MYCOMMAND\t:    \tget hits of motif PWM";
        exit(1);
}


if(!defined($ARGV[0]) || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/ || @ARGV<5 || @ARGV>7){
	print "\nmatt $MYCOMMAND <TABLE> <C_SEQ> <C_ID> <PWM> <THRESH> [SEARCH_MODE] [<PWM_BG> or INFER_BGPWM]\n\n"; 
	print "   ... search for hits with a motif PWM in genomic sequences\n\n";
	print "   <TABLE>       : tab-separated table with sequences\n";
	print "   <C_SEQ>       : name of column with sequences\n";
	print "   <C_ID>        : name of column with sequence IDs\n";
	print "   <PWM>         : file with motif PWM (text file, see help of command get_pwm)\n";
	print "   <THRESH>      : hits get reported only if their score >= THRESH\n";
	print "                   Interpretation of THRESH depends on background model.\n";
	print "                    * without bg PWM, THRESH applies to log-PWM-score\n";
	print "                    * with bg PWM, THRESH applies to likelihood ratio (not to log-ratio!)\n";
	print "                      e.g., THRESH=10: hits are reported if they are 10 times more likely\n";
	print "                      under the motif PWM than under the bg PWM\n";
	print "   <SEARCH_MODE> : single (default; seqs as given), or double (seqs as given and their rc)\n\n";
	print "   <PWM_BG>      : a path to a file with a background PWM.\n";
	print "                     If you set <PWM_BG>=INFER_BGPWM, then a BG model (homogeneous Markov model order 0)\n";
	print "                     gets inferred from all seqs given in column SEQ_COLUMN of TABLE. The alphabe for\n";
	print "                     this model is taken from foregound PWM.\n\n";
	print "   Output: a table describing hits with columns SEQ_ID, START, END, STRAND, HIT, LOGSCORE to STDOUT\n";
	print "           Always START <= END; and both are always defined wrt. the sequences as given, not wrt. their RCs.\n";
	print "           Sequence positions start with 1.\n";
	print "           SCORE is log-PWM score or log likelihood ratio.\n";
	print "\n";
	exit(1);
}

# arguments 
my ($fn,$seq_col,$id_col,$fn_pwm,$thresh)=@ARGV;
my ($search_mode,$bg_pwm);
if(@ARGV>5){
	if($ARGV[5] eq "single" || $ARGV[5] eq "double"){$search_mode=$ARGV[5];
	}else{$bg_pwm=$ARGV[5];}
}
if(@ARGV>6){$bg_pwm=$ARGV[6];}

unless($search_mode){$search_mode="single";}
unless($bg_pwm){$bg_pwm="no_bg_pwm_crazy_code_afkjhauqwejkqw";
}else{
	if($bg_pwm ne "INFER_BGPWM" && ! -f $bg_pwm){die "$MYCOMMAND: argument PWM_BG is $bg_pwm but must be either omitted, or a file or INFER_BGPWM."}
}


################


# get pwm
my $minprob=0.001;
my($pwm_aref,$symbs_aref,$comments_pwm)=read_pwm($fn_pwm,$MYCOMMAND,$minprob);

my ($fh,$col_ns_aref,$col_ids_aref,$comments);

unless($bg_pwm eq "no_bg_pwm_crazy_code_afkjhauqwejkqw"){
	my ($pwm2_aref,$symbs_aref,$comments_pwm);
	if(-f $bg_pwm){
		($pwm2_aref,$symbs_aref,$comments_pwm)=read_pwm($bg_pwm,$MYCOMMAND,$minprob);
	}else{
		# get seqs
		($fh,$col_ns_aref,$col_ids_aref,$comments)=open_fct_file($fn,[$id_col,$seq_col],$MYCOMMAND);
		my @seqs=();
		while(<$fh>){
			$_=clean_line($_);
			if(length($_)==0 || substr($_,0,1) eq "#"){next;}
			my @fs=split_line($_);
			push(@seqs,$fs[ $col_ids_aref->[1] ]);}
		close($fh);
		
		($pwm2_aref)=learn_hmm0_as_pwm_from_seqs(\@seqs,scalar(@$pwm_aref),$minprob,$symbs_aref);
		@seqs=();
	}

	$pwm_aref=subtract_pwms($pwm_aref,$pwm2_aref);
	$thresh=log($thresh);
}


# read in feature-column table with sequences 
($fh,$col_ns_aref,$col_ids_aref,$comments)=open_fct_file($fn,[$id_col,$seq_col],$MYCOMMAND);

# print new header
print "SEQ_ID\tSTART\tEND\tSTRAND\tHIT\tLOGSCORE\n";

my ($seq,$seq_id,$score);
my @fs;
my $scores_aref;
my $motif_len=scalar(@$pwm_aref);
while(<$fh>){
	$_=clean_line($_);
	if(length($_)==0 || substr($_,0,1) eq "#"){next;}
	my @fs=split_line($_);
	
	$seq_id=$fs[ $col_ids_aref->[0] ];
	$seq=$fs[ $col_ids_aref->[1] ];

	# score for each possible position of motif pwm in seq
	$scores_aref=get_pwm_scores_for_seq($seq,$pwm_aref);
		
	for(my $i=0;$i<@$scores_aref;$i++){
		$score=$scores_aref->[$i];
		if($score ne "NA" && $score>=$thresh){
			print $seq_id."\t".($i+1)."\t".($i+$motif_len)."\t+\t".substr($seq,$i,$motif_len)."\t".$score."\n";
		}
	}
	
	if($search_mode eq "double"){
		$seq=rvcmplt($seq);
		$scores_aref=get_pwm_scores_for_seq($seq,$pwm_aref);			
		for(my $i=0;$i<@$scores_aref;$i++){
			$score=$scores_aref->[$i];
			if($score ne "NA" && $score>=$thresh){
				print $seq_id."\t".(length($seq)-$i-$motif_len)."\t".(length($seq)-$i-1)."\t-\t".substr($seq,$i,$motif_len)."\t".$score."\n";
			}
		}	
	}
}

close($fh);


exit(0); # success
__END__