#!/usr/bin/env perl
use strict;
use warnings;
use Scalar::Util qw(looks_like_number);

use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.


my $MYCOMMAND="row_calc";

# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "math\t$MYCOMMAND\t:  \tapply calculations to rows";
        exit(1);
}

if(!defined($ARGV[0]) || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/ || @ARGV<2){
        print "\nmatt $MYCOMMAND <TABLE> <CMD_1> [<CMD_2> ...]\n\n";
	print "   ... apply calculations on column entries along rows\n\n";
	print "   <TABLE> : tab-separated table\n";
	print "   <CMD_*> : each command defines calculations in form of a mathematical formula\n";
	print "               Results per line are output in form of a new column.\n";
	print "               If the table has columns with names START and END and you\n";
	print "               want to determine the difference and get the resulting values\n";
	print "              in a new column with name DIFF then the CMD should be \'DIFF=START-END\'\n";
	print "              Blanks, space characters are NOT allowed inside CMD.\n";
	print "              More examples: \'LENGTH=END-START+1\' \'LOG_FC=log(CONC_1/CONC_2)\'\n";
	print "   IMPORTANT: put CMD in \"command\" or \'command\'\n\n";
	print "   Output: a new table with columns as defined by commands.\n";
	print "\n";
        exit(1);
}

my $fn=$ARGV[0];
my @cmds=@ARGV[1..$#ARGV];

my ($fh,$col_ns_aref,$col_ids_aref,$comments)=open_fct_file($fn,[],$MYCOMMAND);

# get header and remove header from commands
my @header=();
my @fs;
my @cmds_orig=@cmds;
for(my $i=0; $i<@cmds;$i++){@fs=split("=",$cmds[$i]);push(@header,$fs[0]);$cmds[$i]=join("=",@fs[1..$#fs]);}
# output header
print join("\t",@header)."\n";

my @ret;
my $result;
while(<$fh>){
	$_=clean_line($_);
	if(length($_)==0 || substr($_,0,1) eq "#"){next;}
	my @fs=split_line($_);

	my @cmds_exec=@cmds;
	# go over columns
	for(my $i=0;$i<@fs;$i++){
		my $tmpv=$col_ns_aref->[$i];
		#go over commands and do substitution (substitute column name by value in this column)
		for(my $j=0;$j<@cmds_exec;$j++){
			my $found=1;
			while($found){
				$found=0;
				if($cmds_exec[$j]=~/$tmpv/){
					# make sure $tmpv does not contain a sustring of a true column name, e.g. tmpv=DIST and there is a column name DELTA_DIST then DIST in DELTA_DIST should not be substituted
					unless($cmds_exec[$j]=~/\w$tmpv/ || $cmds_exec[$j]=~/$tmpv\w/){
						$cmds_exec[$j] =~ s/$tmpv/$fs[$i]/; 
						$found=1;
					}
				}
			}
	}}
	
	# go over all expressions and change -- -> + ; ++ -> + ; +- -> - ; -+ -> -  (this might occur e.g. in C=abs(A-B) and if B has negative value; then the expression would get C=abs(-5--6) and woudln't work, rather this expression must be C=abs(-5+6))
	for(my $j=0;$j<@cmds_exec;$j++){
			$cmds_exec[$j] =~ s/\-\-/\+/g;
			$cmds_exec[$j] =~ s/\+\+/\+/g;
			$cmds_exec[$j] =~ s/\+\-/\-/g;
			$cmds_exec[$j] =~ s/\-\+/\-/g;
	}
		
	@ret=();
	for(my $i=0;$i<@cmds_exec;$i++){
		$result=eval($cmds_exec[$i]);
		if(!defined($result)){die "Calculation of command ".$cmds_orig[$i]." failed. Check that this command contains only correct column names and no blanks.";}
		push(@ret,$result);
	}
	
	print join("\t",@ret)."\n";
}
close($fh);

exit(0); # success
__END__
