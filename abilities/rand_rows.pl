#!/usr/bin/env perl
use strict;
use warnings;
use File::Temp qw(tempfile);
use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.


my $MYCOMMAND="rand_rows";

# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "retrieval\t$MYCOMMAND\t:  \tget random rows";
        exit(1);
}


if(!defined($ARGV[0]) || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/ || @ARGV<2){
        print "\nmatt $MYCOMMAND <TABLE> <N> [<SEED>] [-r]\n\n";
        print "   ... randomly draw N rows from a table\n\n";
        print "   <TABLE>  : tab-separated text table\n"; 
        print "   <N>      : number of desired rows\n";
        print "   <SEED>   : seed for random number generator (any long integer)\n";
        print "   -r       : if given, rows are drawn with replacement\n";
        print "\n   Output to STDOUT: table TABLE reduced to the drawn rows\n\n";
        print "\n";
        exit(1);
}


my $fn=shift(@ARGV);
my $N=shift(@ARGV);
if($N<0){die "$MYCOMMAND: given N ($N) is smaller then 0!";}
my $seed=undef;
my $repl=undef;
for(my $i=0;$i<@ARGV;$i++){
	if($ARGV[$i] eq "-r"){$repl=1;next}
	if(is_numeric($ARGV[$i])){$seed=$ARGV[$i];next}
	die "Argument $ARGV[$i] unknown\n";
}
if(defined($seed) && $seed<=0){die "Seed $seed is not greater zero\n";}



# create temporary file; will be deleted automatically at the end of script or if error occurs
my ($fh_tmp,$tmpfile_n) = tempfile( DIR => ".", UNLINK => 1);

my ($fh,$col_ns_aref,$col_ids_aref,$comments)=open_fct_file($fn,[],$MYCOMMAND);
if($N<=0){print $comments; print join("\t",@{$col_ns_aref})."\n";exit 0;}
print $fh_tmp $comments;
print $fh_tmp join("\t",@{$col_ns_aref})."\n";

my @ids=();
my @fs;
my $id=-1;
while(<$fh>){
	print $fh_tmp $_;
	$_=clean_line($_);
	if(length($_)==0 || substr($_,0,1) eq "#"){next;}
	push(@ids,++$id);
}
close($fh);
close($fh_tmp);

if(defined($seed)){srand($seed);my $trash=rand();}

my %rows_output=();
if(!defined($repl)){
	shuffle_elements_in_aref(\@ids);
	for(my $i=0; $i<$N && $i<@ids; $i++){
		$rows_output{$ids[$i]}++;
	}
}else{
	for(my $i=0; $i<$N; $i++){
		$rows_output{$ids[rand @ids]}++;
	}
}


($fh,$col_ns_aref,$col_ids_aref,$comments)=open_fct_file($tmpfile_n,[],$MYCOMMAND);
print $comments;
print join("\t",@{$col_ns_aref})."\n";
$id=-1;
while(<$fh>){
	$_=clean_line($_);
	if(length($_)==0 || substr($_,0,1) eq "#"){next;}
	$id++;
	my $n=$rows_output{$id};
	if(defined($n)){
		for(my $i=0;$i<$n;$i++){print "$_\n";}
	}
}
close($fh);


exit(0); # success
__END__