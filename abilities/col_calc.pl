#!/usr/bin/env perl
use strict;
use warnings;
use Scalar::Util qw(looks_like_number);

use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.


my $MYCOMMAND="col_calc";

# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "math\t$MYCOMMAND\t:  \tapply calculations to columns";
        exit(1);
}

if(!defined($ARGV[0]) || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/ || @ARGV<3 || @ARGV>5){
        print "\nmatt $MYCOMMAND <TABLE> <CALCULATION> <C_VALUES> [<C_GROUPS>] [-ignoreNA]\n\n";
	print "   ... apply calculations to values per group\n\n";
	print "   <TABLE>       : tab-separated table\n";
	print "   <CALCULATION> : calculation which you want to apply\n";
	print "                     available are:\n";
	print "                     * min : get the minimal value per column\n";
	print "                     * max : get the maximal value per column\n";
	print "                     * absmin: get value whose abs value is minimal per column\n";
	print "                     * absmax: get value whose abs value is maximal per column\n";
	print "                     * sum : calculate the sum over all values per column\n";
	print "                     * abssum : calculate the sum over all abs values per column\n";
	print "                     * mean: calculate mean\n";
	print "                     * median: calculate median\n";
	print "                     * sd: calculate standard deviation\n";
	print "                     * ns: get number of values\n";
	print "   <C_VALUES>    : names of columns with values; comma-separated list if you want to\n";
	print "                     apply calculations on more than one column at once\n";
	print "   <C_GROUPS>    : comma-separated list of names of columns with group identifier\n";
	print "                     If you specify more than one column, groups are defined by the \n";
	print "                     cross-product of the groups of each column.\n";
	print "                     E.g., if column DATASET contains a, b, c and column DAY 1, 3, 5,\n";
	print "                     then you will apply calculation for each group combination, i.e.,\n";
	print "                     a1, a3, a5, b1, b3, b5, c1, c3, c5\n";
	print "                     Without specifying C_GROUPS, calculations apply to entire selected columns.\n";
	print "   -ignoreNA     : NA values will be neglected\n\n";
	print "   IMPORTANT     : the group ids in <C_GROUPS> must not contain the character '&'\n\n";
	print "   Output: a new table with result columns; rows correspond to group (combinations).\n";
	print "\n";
        exit(1);
}

my ($fn,$cmd,$cvs,$ignoreNA,$cgs)=@ARGV;
if($ignoreNA && $ignoreNA ne "-ignoreNA"){
	if($cgs && $cgs eq "-ignoreNA"){$cgs=$ignoreNA;$ignoreNA="-ignoreNA";}
	unless($cgs){$cgs=$ignoreNA;$ignoreNA=0;}
}
if($ignoreNA && $ignoreNA ne "-ignoreNA"){die "Something with arguments seems wrong. Check syntax and spelling of argument -ignoreNA.";}

unless($cmd=~/^min$|^max$|^absmin$|^absmax$|^sum$|^abssum$|^mean$|^median$|^sd$|^ns$/){die "Calculation $cmd unknown.\n"}

if($ignoreNA && $ignoreNA eq "-ignoreNA"){$ignoreNA="ignoreSTRINGS"}else{$ignoreNA=undef}
my $prefix=uc($cmd);
my $abs=undef;
if($cmd=~/abssum|absmin|absmax/){$abs="abs";$cmd=~s/^abs//;}else{$abs=undef}

my @cvs=split(",",$cvs);
my @cgs=();if($cgs){@cgs=split(",",$cgs);}
my ($fh,$col_ns_aref,$col_ids_aref,$comments)=open_fct_file($fn,[@cvs,@cgs],$MYCOMMAND);

# group column ids
my @gcids=();for(my $i=@cvs;$i<@cvs+@cgs;$i++){push(@gcids,$col_ids_aref->[$i])}
# value column ids
my @vcids=();for(my $i=0;$i<@cvs;$i++){push(@vcids,$col_ids_aref->[$i])}


my @res;
for(my $i=0;$i<@cvs;$i++){$res[$i]={}}

while(<$fh>){$_=clean_line($_);if(length($_)==0 || substr($_,0,1) eq "#"){next;}my @fs=split_line($_);
	my $gid=join("&",@fs[@gcids]);
	unless($gid){$gid="&";}# no group columns given -> we create an artificial group id
	for(my $i=0;$i<@vcids;$i++){
		my $cid=$vcids[$i];
		if(!defined($res[$i]->{$gid})){$res[$i]->{$gid}=[];}
		push(@{$res[$i]->{$gid}},$fs[$cid]);
	}
}

close($fh);

# output new header
my @output=@cgs; # no output if no group columns defined
foreach my $gn (@cvs){push(@output,"${gn}_$prefix");}
print join("\t",@output)."\n";

my @gids=sort keys %{$res[0]};
# output results
foreach my $gid (@gids){
	# recover single group ids
	my @fs=split("&",$gid);
	@output=@fs;  # no output if no group columns given
	
	for(my $i=0;$i<@cvs;$i++){
		if($cmd eq "mean"){
			push(@output,get_mean_from_aref($res[$i]->{$gid},$ignoreNA));
			next;
		}
		if($cmd eq "median"){			
			push(@output,get_median_from_aref($res[$i]->{$gid},$ignoreNA));
			next;
		}
		if($cmd eq "ns"){
			if($ignoreNA){
			     my $n=0;
			     foreach my $c (@{$res[$i]->{$gid}}){if(is_numeric($c)){$n++}}
			     push(@output,$n);
			}else{push(@output,scalar(@{$res[$i]->{$gid}}));}
			next;
		}
		if($cmd eq "sum"){
			push(@output,get_sum_from_aref($res[$i]->{$gid},$ignoreNA,$abs));
			next;
		}
		if($cmd eq "sd"){
			push(@output,get_sd_without_minus_1_from_aref($res[$i]->{$gid},$ignoreNA));
			next;
		}
		if($cmd eq "min"){
			my ($min,$max)=get_min_max_from_aref($res[$i]->{$gid},$ignoreNA,$abs);
			push(@output,$min);
			next;
		}
		if($cmd eq "max"){
			my ($min,$max)=get_min_max_from_aref($res[$i]->{$gid},$ignoreNA,$abs);
			push(@output,$max);
			next;
		}
	}
	print join("\t",@output)."\n";
}


exit(0); # success
__END__