#!/usr/bin/env perl
use strict;
use warnings;

use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.


my $MYCOMMAND="col_uniq";

# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "retrieval\t$MYCOMMAND\t:   \tunique values in columns";
        exit(1);
}


if(!defined($ARGV[0]) || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/ || @ARGV<2){
	print "\nmatt $MYCOMMAND <TABLE> <C1_NAME> [<C2_NAME> ...]\n\n";
        print "   ... determines distinct values and their frequencies in column C_NAME.\n\n";
        print "   <TABLE>       : tab-separated text table\n"; 
	print "   <C1_NAME>     : column with categorical data\n";
	print "   <C2_NAME> ... : potentially more columns with categorical data\n\n";
        print "   Output to STDOUT: table with distinct values in column C1_NAME and threir frequencies.\n";
        print "                     If several column names given, then table with distinct values in these\n";
        print "                     columns as they appear in TABLE.\n\n";
        exit(1);
}

# fct file
my $fn=shift(@ARGV);
# column name
my @cns=@ARGV;

my ($fh,$col_ns_aref,$col_ids_aref,$comments)=open_fct_file($fn,[@cns],$MYCOMMAND);

my @col_ids=@{$col_ids_aref};
my @col_ns=@{$col_ns_aref};
my %freqs=();
my @fs;
while(<$fh>){
	$_=clean_line($_);
	if(length($_)==0 || substr($_,0,1) eq "#"){next;}
	@fs=split_line($_);
	$freqs{ join("\t",@fs[@col_ids]) }++;
}
close($fh);

print join("\t",@col_ns[@col_ids])."\tFREQ\n";
foreach my $val (sort {lc $a cmp lc $b} keys %freqs){print "$val\t".$freqs{$val}."\n";}

exit(0); # success
__END__