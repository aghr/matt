#!/usr/bin/env perl
use strict;
use warnings;
use File::Temp qw(tempfile);

use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.


my $MYCOMMAND="prnt_tab";

# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "extras\tprnt_tab\t:     \tprint table nicely";
        exit(1);
}


if(!defined($ARGV[0]) || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/ || @ARGV<1){
        print "\nmatt prnt_tab <TABLE> [-w/-W <COLWIDTH>] [-s <COLSPAN=3>] [-n <N_COLUMNS>]\n\n";
	print "   ... print a table nicely aligned to screen.\n\n";
	print "   -w <COLWIDTH> : only the first COLWIDTH characters of each column are printed\n";
	print "   -W <COLWIDTH> : only the first COLWIDTH characters of each column are printed\n";
	print "                   If the entire column is smaller then COLWIDTH, it will be printed\n";
	print "                   using less positions than COLWIDTH. This is slower.\n";
	print "   <COLSPAN>     :  number of blanks (space) printed between columns\n";
	print "   <N_COLUMNS>   :  print only the first N_COLUMNS columns\n\n";
	print "   Output: a nicely aligned but truncated table to STDOUT\n\n";
	print "   IMPORTANT: the resulting table being truncated should not be piped-back to matt\n\n";
	print "   Hint: In order not to see the full table at once, call (quit with 'q'): matt $MYCOMMAND <TABLE> ... | less -S\n\n";
        exit(1);
}

#################################################


# fct file
my $fn=$ARGV[0];
my ($width,$span,$n_col) = (6,3,"all");
my $mode="simple";  # all columns are of equal size


for(my $i=1; $i<@ARGV;$i+=2){

	my $cr=$ARGV[$i];
	my $v=$ARGV[$i+1];

	if($cr eq "-w"){$width=$v;next;}
	if($cr eq "-W"){$width=$v;$mode="adaptive";next;}  # columns narrower then $width will be narrower
	if($cr eq "-s"){$span=$v;next;}
	if($cr eq "-n"){$n_col=$v;next;}
	
	die "prnt_tab: argument $cr unknown\n";
}



my @BLANKS=("");
for(my $i=1;$i<=max($width,$span);$i++){$BLANKS[$i]=" ".$BLANKS[$i-1];}

sub trunc_fields {
	my ($aref,$widths_aref)=@_;
	my $tmpstr;
	for(my $i=0;$i<@{$aref};$i++){
		$tmpstr=substr($aref->[$i],0,$widths_aref->[$i]);
		$tmpstr.=$BLANKS[ $widths_aref->[$i]-length($tmpstr) ];
		$aref->[$i]=$tmpstr;
	}
return($aref);
}

#################################################
my ($fh,$col_ns_aref,$col_ids_aref,$comments);
my @widths=();
my @fs;
my ($fh_tmp,$tmpfile_n);

if($mode eq "adaptive"){  # determine max width of each column
	($fh,$col_ns_aref,$col_ids_aref,$comments)=open_fct_file($fn,[],$MYCOMMAND);
	($fh_tmp,$tmpfile_n) = tempfile( DIR => ".", UNLINK => 1);

	
	print $fh_tmp $comments;
	print $fh_tmp join("\t",@{$col_ns_aref})."\n";
	for(my $i=0;$i<@$col_ns_aref;$i++){$widths[$i]=-1;}
	while(<$fh>){
		print $fh_tmp $_;
		$_=clean_line($_);
		if(length($_)==0 || substr($_,0,1) eq "#"){next;}
		@fs=split_line($_);
		for(my $i=0;$i<@fs;$i++){
			if(length($fs[$i])>$widths[$i]){$widths[$i]=length($fs[$i]);}
		}
	}
	close($fh);
	close($fh_tmp);
	$fn=$tmpfile_n;
}


($fh,$col_ns_aref,$col_ids_aref,$comments)=open_fct_file($fn,[],$MYCOMMAND);

if($mode eq "simple"){  # each column should have same width
	for(my $i=0;$i<@$col_ns_aref;$i++){$widths[$i]=$width;}
}else{
	for(my $i=0;$i<@$col_ns_aref;$i++){if($widths[$i]>$width){$widths[$i]=$width;}}
}


# for outputting columns
if($n_col eq "all"){$n_col=@{$col_ns_aref};}
my @col_ids_out=0..($n_col-1);

# truncate header
$col_ns_aref=trunc_fields($col_ns_aref,\@widths);
prnt_line(\*STDOUT,trunc_fields($col_ns_aref,\@widths),$BLANKS[$span],\@col_ids_out);

($fh,$col_ns_aref,$col_ids_aref,$comments)=open_fct_file($fn,[],$MYCOMMAND);
while(<$fh>){
	$_=clean_line($_);
	if(length($_)==0){print "\n";next;}
	if(substr($_,0,1) eq "#"){print "$_\n";next;}
	prnt_line(\*STDOUT, trunc_fields([ split("\t",$_,-1) ],\@widths), $BLANKS[$span], \@col_ids_out);
}

exit(0); # success
__END__