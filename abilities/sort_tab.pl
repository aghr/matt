#!/usr/bin/env perl
use strict;
use warnings;
use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.


my $MYCOMMAND="sort_tab";

# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "extras\t$MYCOMMAND\t:     \tsort table";
        exit(1);
}

if(!defined($ARGV[0]) || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/ || @ARGV<2){
	print "\nmatt $MYCOMMAND <TABLE> <C_NAME> [-d]\n\n";
	print "   ... sort rows of table according to entries in column C_NAME\n\n";
	print "   <TABLE>  : tab-separated table\n";
	print "   <C_NAME> : column according to which table should be sorted\n";
	print "                If all entries in column C_NAME are numeric, sorting\n";
	print "                will be numerical else lexical\n";
	print "   -d       : if given, sort decreasingly\n\n";
	print "   Output: sorted table to STDOUT\n";
	print "\n";
        exit(1);
}



my ($fn,$cn,$decr)=@ARGV;

my @vals=();
my ($fh,$col_ns_aref,$col_ids_aref,$comments)=open_fct_file($fn,[$cn],$MYCOMMAND);


my $all_numeric=1;
my $cid=$col_ids_aref->[0];
my $v;
my @vs;
my @fs;
my @lines;
while(<$fh>){
	$_=clean_line($_);
	if(length($_)==0 || substr($_,0,1) eq "#"){next;}
	my @fs=split_line($_);

	$v=$fs[$cid];
	unless(is_numeric($v)){$all_numeric=0;}
	push(@vs,$v);
	push(@lines,$_);
}
close($fh);


my @ids;
if($all_numeric){
	if(!defined($decr)){@ids = sort { $vs[$a] <=> $vs[$b] } 0 .. $#vs;
	}else{@ids = sort { $vs[$b] <=> $vs[$a] } 0 .. $#vs;}
}else{
	if(!defined($decr)){@ids = sort { $vs[$a] cmp $vs[$b] } 0 .. $#vs;
	}else{@ids = sort { $vs[$b] cmp $vs[$a] } 0 .. $#vs;}
}


print $comments;
print join("\t",@$col_ns_aref)."\n";
foreach $v (@ids){print $lines[$v]."\n";}


exit(0); # success
__END__