#!/usr/bin/env perl
use strict;
use warnings;
use Cwd qw(abs_path);
use File::Copy;
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.
use File::Temp qw(tempfile);
use File::Copy;

my $MYCOMMAND="test_cisbp_enrich";

# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "super\t$MYCOMMAND\t: \ttest CISBP motif enrichment";
        exit(1);
} 

if(@ARGV<3 || @ARGV>10 || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/){
    	print "\n   $MYCOMMAND <TABLE_WITH_SEQS> <COLUMN_SEQS> <COLUMN_DATASET> [<N_ITERATIONS_PERMTEST>] [-p <RBP_LIST>] [-m <MODE>] [-t <THRESHOLD>]\n";
    	print "\n";
    	print "   Use this function if you want to compare two sequence sets for enrichment of binding motifs of RBPs\n";
    	print "   from the CISBP-RNA data base (build 0.6).\n";
    	print "   CISBP-RNA contains binding models (PWMs). One PWM might correspond to several RBPs when they have\n";
    	print "   binding domains in common. The output table contains the PWM ids. Use them and the Search-By-Identifier\n";
    	print "   on http://cisbp-rna.ccbr.utoronto.ca to get more information on certain binding motifs and corresponding RBPs.\n";
    	print "\n";
    	print "   <TABLE_WITH_SEQS>  : table containing sequences (fg and bg sets)\n";
    	print "   <COLUMN_SEQS>      : column containing sequences\n";
    	print "   <COLUMN_DATASET>   : column containing group ids defining sets of sequences\n";
    	print "                          In addition, you need to specify which groups of sequences you want to compare,\n";
    	print "                          like, DATASET[fg1,bg1,fg2,bg2]. The order is always foreground background forground background ...\n";
    	print "   <N_ITERATIONS>     : number of iterations of permutation test used for estimating the p-values; default=10000\n";
    	print "   -p <RBP_LIST>      : sub-select RBPs from all CISBP-RNA RBPs, e.g., -p fox,esrp,sf3b1\n";
    	print "   -m <MODE>          : q (quant, is default) -> enrichment is tested with ratio of normalized hit counts per set\n";
    	print "                        <INTEGER> (e.g., 3)   -> enrichment is tested with ratio of YES/NO sequences meaning\n";
    	print "                                                 they have hits or not and a sequnence is YES if it has at least INTEGER hits\n";
    	print "   -t <THRESHOLD>     : threshold on ratio Prob_rbp(hit)/Prob_bg(hit) for predicting hits; default is 10 meaning hits are\n";
    	print "                           predicted at positions which are 10 times more likely being a hit of the RBP then being background.\n";
    	print "                           Should be chosen conservatively to avoid false positives.\n";
    	print "\n";
    	print "   For searching RBPs call: matt $MYCOMMAND <SEARCH_STRING>\n";
    	print "                            and <SEARCH_STRING> could be, e.g., sf3b1 or pum to get pum1 and pum2\n\n";
    	print "   For printing all CISBP-RNA information call:\n\t   matt $MYCOMMAND print_all_info\n\n";
    	print "   If you publish results from this analysis, please cite:\n";
    	print "   * Nature. 2013 Jul 11;499(7457):172-7. doi: 10.1038/nature12311\n";
    	print "\n";
exit(1);
}

my $cisbp_info_file="$scr_abspath/../data_external/cisbp_rna/cisbprna_info.tab";

# search for RBPs
if(@ARGV==1 && $ARGV[0] eq "print_all_info"){
	my $info_file="$scr_abspath/../data_external/cisbp_rna/RBP_Information_all_motifs.txt";
	open(my $fh,$info_file) or die "Cannot find CISBP-RNA info file $info_file.";
	while(<$fh>){print $_;}
	exit;
}

if(@ARGV==1){
	open(my $fh,$cisbp_info_file) or die "Cannot find CISBP-RNA info file $cisbp_info_file.";
	my $header=<$fh>;
	chomp($header);
	my @ids=(0,1,2,8,9);
	my @header=split("\t",$header);
	my @ids_search=();
	for(my $i=0;$i<@header;$i++){if($header[$i]=~/GENES_IN_/ || $header[$i]=~/MOST_FREQ_GENE/){push(@ids_search,$i);}}
	my %cols=();
	my @lines=();
	my $push_line;
	my @fs;
	while(<$fh>){chomp;@fs=split("\t",$_,-1);
		$push_line=0;
		foreach my $id (@ids_search){
			if($fs[$id]=~ /$ARGV[0]/){
				$cols{$id}=1;
				$push_line=1;
			}
		}
		if($push_line){push(@lines,$_);}
	}
	
	@ids=(@ids,keys(%cols));
	print join("\t",@header[@ids])."\n";
	foreach my $line (@lines){@fs=split("\t",$line,-1);print join("\t",@fs[@ids])."\n";}
	
	exit;
}

my ($fn_seqs,$cn_seqs,$cn_ds)=@ARGV;
my $N_its=10000;
my $rbp_subselection="";
my $thresh=10;
my $mode="q";
for(my $i=3;$i<@ARGV;$i++){
	if($ARGV[$i] eq "-m"){$mode=$ARGV[$i+1];$i++;next;}
	if($ARGV[$i] eq "-t"){$thresh=$ARGV[$i+1];$i++;next;}
	if($ARGV[$i] eq "-p"){$rbp_subselection=$ARGV[$i+1];$i++;next;}
	if(is_numeric($ARGV[$i])){$N_its=$ARGV[$i];next}
	die "Problems with extracting agruments: do not understand argument $ARGV[$i]. Please check syntax.";
}
if(!is_numeric($N_its)){die "Problems with extracting arguments: number of iterations given as $N_its is not numeric. Please check syntax.";}
if(!is_numeric($thresh)){die "Problems with extracting arguments: threshold for prediction of hits given as $thresh is not numeric. Please check syntax.";}
unless($mode eq "q" || is_numeric($mode)){die "Problems with extracting arguments: mode given with argument -m is not q nor i. Please check syntax.";}
if($mode eq "q"){$mode="quant";}

my $rbp_patterns="";
if($rbp_subselection){$rbp_patterns=$rbp_subselection;$rbp_patterns =~ s/,/|/g;}

# get all pwm ids
# create temporary file; will be deleted automatically at the end of script or if error occurs
my ($fh_tmp,$tmpfile_n) = tempfile( DIR => ".", UNLINK => 1);
my $cmd="matt get_rows $cisbp_info_file TYPE]pwm[ > $tmpfile_n";
my $trash=`$cmd`;

my $line_number_1=1;
if($rbp_patterns){
	$line_number_1=0;
	open(my $fh,$tmpfile_n);open(my $fh2,">".$tmpfile_n."tmp");
	my $header=<$fh>;print $fh2 $header;
	while(<$fh>){my @fs=split("\t",$_,-1); my $teststr=join("\t",@fs[10..(@fs-1)]);if($teststr =~/$rbp_patterns/){print $fh2 $_;$line_number_1++;} }
	close($fh);close($fh2);
	move($tmpfile_n."tmp",$tmpfile_n);
}

if($line_number_1>0){
	$cmd="matt rm_cols $tmpfile_n PERCENT_BINDINGENERGY_REQUESTED PERCENT_BINDINGENERGY_REACHED DELTA_PERCENT_BINDINGENERGY NKMERS_FOR_THRESHOLD MOST_LIKELY_KMER";
	$trash=`$cmd`;die "$!" if $?!=0;

	# adapt path to PWM files
	open(my $fh,"<".$tmpfile_n) or die "Cannot open temporary file $tmpfile_n.";
	my $header=<$fh>;chomp($header);
	my @header=split("\t",$header,-1);
	my $idx = first_index_exact("FILE_REGEXP",\@header);
	my $idx2 = first_index_exact("LOGPWMSCORE_THRESHOLD",\@header);
	$header[$idx2]="THRESHOLD_RATIO_PROBFG_vs_PROBBG";
	my @lines=(join("\t",@header));
	while(<$fh>){chomp;my @fs=split("\t",$_,-1);
		$fs[$idx]=$scr_abspath."/../data_external/cisbp_rna/".$fs[$idx];
		$fs[$idx2]=$thresh;  # default 10
		push(@lines,join("\t",@fs));
	}
	close($fh);
	open($fh,">".$tmpfile_n) or die "Cannot open $tmpfile_n for writing.";
	foreach my $line (@lines){print $fh $line."\n";}
	close($fh);

	my $cn_thresh="THRESHOLD_RATIO_PROBFG_vs_PROBBG";
	$cmd="matt test_pwm_enrich $fn_seqs $cn_seqs $cn_ds single $tmpfile_n ID FILE_REGEXP $cn_thresh INFER_BGPWM $mode $N_its";
	#print $cmd."\n";
	my @out_pwms=split("\n",`$cmd`);

	# this is all for plotting PWM profiles for significant enrichments
	my @col_nms=qw(SIGNIFICANT_RESULT_ALL SIGNIFICANT_RESULT_FIRST SIGNIFICANT_RESULT_INTERNAL SIGNIFICANT_RESULT_END FILE_REGEXP NAME BG_MODEL THRESHOLD_RATIO_PROBFG_vs_PROBBG);
	my @col_ids=();
	my @header_tmp=split("\t",$out_pwms[0],-1);
	foreach my $col_nm (@col_nms){
		my $index = first_index_exact($col_nm,\@header_tmp);
		if($index==-1){die "$MYCOMMAND: failed to extract column name $col_nm from header from test_pwm_enrich.";}
		push(@col_ids,$index);
	}
	my @col_order_out=0..(@header_tmp-1);
	foreach my $col_nm (reverse(qw(NAME MOST_FREQ_GENE SIGNIFICANT_RESULT_ALL SIGNIFICANT_RESULT_FIRST SIGNIFICANT_RESULT_INTERNAL SIGNIFICANT_RESULT_END PVALUE_ALL PVALUE_FIRST PVALUE_INTERNAL PVALUE_END))){
		my $index = first_index_exact($col_nm,\@header_tmp);
		if($index==-1){die "$MYCOMMAND: failed to extract column name $col_nm from header from test_pwm_enrich.";}
		unshift(@col_order_out,$index);  # add index at first position
	}
	@col_order_out=@{uniq_from_aref(\@col_order_out)};

	my %sig_hits_infos=();
	my $is_header=1;
	foreach my $line (@out_pwms){my @tmp_line=split("\t",$line,-1);
		print join("\t",@tmp_line[@col_order_out])."\n";
		if($is_header){$is_header=0;next;}

		# do we have found significant enrichment?
		if($tmp_line[$col_ids[0]] || $tmp_line[$col_ids[1]] || $tmp_line[$col_ids[2]] || $tmp_line[$col_ids[3]]){
			$sig_hits_infos{ join("\t",@tmp_line[@col_ids[4..7]]) }=1;
		}
	}
	
	# plot PWM profiles for all significant hits
	my @sig_hits_infos=keys %sig_hits_infos;
	if(@sig_hits_infos>0){
		my ($fh_tmp,$tmpfile_n) = tempfile( DIR => ".", UNLINK => 1);
		print $fh_tmp "FILE\tNAME\tBG\tTHRESH\n";
		foreach my $infos (@sig_hits_infos){print $fh_tmp $infos."\n";}
		close($fh_tmp);
		
		$cn_ds =~ /(.+)\[(.+)\]/;
		my $cn_ds_tmp=$1."[".join(",",@{uniq_from_aref([split(",",$2)])})."]";
		$cmd="matt get_pwm_prof $fn_seqs $cn_seqs CRL $tmpfile_n NAME FILE THRESH BG $cn_ds_tmp";
		$trash=`$cmd`;print "$MYCOMMAND: error during generation of PWM profiles $!. You might generate these profiles by yourself later with matt get_pwm_prof. This error does not affect the final result.\n" if $?!=0;
	}
}

$cmd="matt get_rows $cisbp_info_file TYPE]regexp[ > $tmpfile_n";
$trash=`$cmd`;die "$!" if $?!=0;

my $line_number_2=1;
if($rbp_patterns){
	$line_number_2=0;
	open(my $fh,$tmpfile_n);open(my $fh2,">".$tmpfile_n."tmp");
	my $header=<$fh>;print $fh2 $header;
	while(<$fh>){my @fs=split("\t",$_,-1); my $teststr=join("\t",@fs[10..(@fs-1)]);if($teststr =~/$rbp_patterns/){print $fh2 $_;$line_number_2++;} }
	close($fh);close($fh2);
	move($tmpfile_n."tmp",$tmpfile_n);
}

if($line_number_2>0){
	$cmd="matt rm_cols $tmpfile_n PERCENT_BINDINGENERGY_REQUESTED PERCENT_BINDINGENERGY_REACHED DELTA_PERCENT_BINDINGENERGY NKMERS_FOR_THRESHOLD MOST_LIKELY_KMER";

	# adapt column name
	open(my $fh,"<".$tmpfile_n) or die "Cannot open temporary file $tmpfile_n.";
	my $header=<$fh>;chomp($header);
	my @header=split("\t",$header,-1);
	my $idx2 = first_index_exact("LOGPWMSCORE_THRESHOLD",\@header);
	$header[$idx2]="THRESHOLD_RATIO_PROBFG_vs_PROBBG";
	my @lines=(join("\t",@header));
	while(<$fh>){chomp;push(@lines,$_);}
	close($fh);
	open($fh,">".$tmpfile_n) or die "Cannot open $tmpfile_n for writing.";
	foreach my $line (@lines){print $fh $line."\n";}
	close($fh);

	$trash=`$cmd`;die "$!" if $?!=0;
	$cmd="matt test_regexp_enrich $fn_seqs $cn_seqs $cn_ds single $tmpfile_n ID FILE_REGEXP $mode $N_its";
	my @out_regexps=split("\n",`$cmd`);

	# this is all for plotting REGEXP profiles for significant enrichments
	my @col_nms=qw(SIGNIFICANT_RESULT_ALL SIGNIFICANT_RESULT_FIRST SIGNIFICANT_RESULT_INTERNAL SIGNIFICANT_RESULT_END FILE_REGEXP NAME BG_MODEL THRESHOLD_RATIO_PROBFG_vs_PROBBG);
	my @col_ids=();
	my @header_tmp=split("\t",$out_regexps[0],-1);
	foreach my $col_nm (@col_nms){
		my $index = first_index_exact($col_nm,\@header_tmp);
		if($index==-1){die "$MYCOMMAND: failed to extract column name $col_nm from test_pwm_enrich.";}
		push(@col_ids,$index);
	}
	my @col_order_out=0..(@header_tmp-1);
	foreach my $col_nm (reverse(qw(NAME MOST_FREQ_GENE SIGNIFICANT_RESULT_ALL SIGNIFICANT_RESULT_FIRST SIGNIFICANT_RESULT_INTERNAL SIGNIFICANT_RESULT_END PVALUE_ALL PVALUE_FIRST PVALUE_INTERNAL PVALUE_END))){
		my $index = first_index_exact($col_nm,\@header_tmp);
		if($index==-1){die "$MYCOMMAND: failed to extract column name $col_nm from header from test_pwm_enrich.";}
		unshift(@col_order_out,$index);  # add index at first position
	}
	@col_order_out=@{uniq_from_aref(\@col_order_out)};

	my %sig_hits_infos=();
	my $is_header=1;
	foreach my $line (@out_regexps){my @tmp_line=split("\t",$line,-1);
		if($is_header){$is_header=0; if($line_number_1==0){print join("\t",@tmp_line[@col_order_out])."\n";}   next;}
		print join("\t",@tmp_line[@col_order_out])."\n";
		# do we have found significant enrichment?
		if($tmp_line[$col_ids[0]] || $tmp_line[$col_ids[1]] || $tmp_line[$col_ids[2]] || $tmp_line[$col_ids[3]]){
			$sig_hits_infos{ join("\t",@tmp_line[@col_ids[4..5]]) }=1;
		}
	}
	
	# plot REGEXP profiles for all significant hits
	my @sig_hits_infos=keys %sig_hits_infos;
	if(@sig_hits_infos>0){
		my ($fh_tmp,$tmpfile_n) = tempfile( DIR => ".", UNLINK => 1);
		print $fh_tmp "REGEXP\tNAME\n";
		foreach my $infos (@sig_hits_infos){print $fh_tmp $infos."\n";}
		close($fh_tmp);
		
		$cn_ds =~ /(.+)\[(.+)\]/;
		unless($2){die "$MYCOMMAND: you need to specify pairs of data sets (fg1,bg1,fg2,bg2,...) but you gave $cn_ds.";}
		my $cn_ds_tmp=$1."[".join(",",@{uniq_from_aref([split(",",$2)])})."]";
		$cmd="matt get_regexp_prof $fn_seqs $cn_seqs CRL $tmpfile_n $cn_ds_tmp";
		$trash=`$cmd`;print "$MYCOMMAND: error during generation of REGEXP profiles $!. You might generate these profiles by yourself later with matt get_regexp_prof. This error does not affect the final result.\n" if $?!=0;
	}
	
}

if($line_number_1+$line_number_2==0){
	print "Did not find any RBP in CISBP-RNA matching your sub-selection $rbp_subselection\n";
}


exit(0); # success
__END__
