#!/usr/bin/env perl
use strict;
use warnings;
use File::Copy;
use File::Temp qw(tempfile);
use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.

# which fastq-dump porgram do we use?
my $fastq_dump_prog;
my $check=`which fastq-dump`;
if($?==0){   # is installed
	chomp($check);
	$fastq_dump_prog=$check;
}else{
	die "sra-toolkit seems not to be installed. Cannot find fastq-dump";
}

my $MYCOMMAND="retr_rnaseq";

# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "extras\t$MYCOMMAND\t:  \tretrieve RNAseq data from GEO";
        exit(1);
}


if(!defined($ARGV[0]) || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/ || @ARGV<1 || @ARGV>7){
        print "\nmatt $MYCOMMAND <TABLE> [-fasta] [-keepsra] [-o <OUTDIR>] [-p <N_PROCS>]\n\n";
        print "   ... retrieves RNAseq SRA archives from GEO, and extracts reads from them.\n\n";
        print "   <TABLE>      : tab-/blank-/comma-/semicolon-separated table without header.\n";
        print "                    * first column:  GEO accession number of SRA archive containing RNAseq data, e.g., SRR3532927\n";
        print "                    * second column: (if available) stem of file names for downloaded data, e.g., KD1, WT1 (no blanks in stems!)\n";
        print "                      If second column ommited, files will be named after accession number.\n";
        print "   -fasta       : by default, FASTQ files will be extracted. If you give -fasta, then FASTA files will be extracted.\n";
        print "   -keepsra     : by default, downloaded SRA archives will be deleted. If you give -keepsra, they will be kept.\n";
        print "   -o <OUTDIR>  : by default, all files will be put in current working directory. If you give -o OUTDIR, all files will\n";
        print "                    be stored in folder OUTDIR.\n";
        print "   -p <N_PROCS> : by default, one process will be started and all SRA archives will be downloaded sequentially. If you\n";
        print "                    specify -p N_PROCS, N_PROCS parallel processes will download achrives and extract reads.\n";
        print "\n";
        print "   Output: all files into current working directory or OUTDIR if speficied, and a table with information on all\n";
        print "             downloaded RNAseq data sets into dataset_info.tab in OUTDIR. If, by chance, files with identical file names\n";
        print "             already exist in OUTDIR, a file with random file name will be saved, and you need to solve the naming issue later.";
        print "\n";
        print "   Note: fastq-dump and prefetch from the SRA toolkit must be available through PATH variable. If errors occur during extracting\n";
        print "           FASTQ/FASTA files from SRA archives, you might have to install a newer version of fastq-dump.\n";
        print "\n";
        exit(1);
}


my $fn=shift(@ARGV);
my $fasta=0;
my $keepsra=0;
my $odir=".";
my $N=1;
for(my $i=0;$i<@ARGV;$i++){
	if($ARGV[$i] eq "-o"){$odir=$ARGV[++$i];next;}
	if($ARGV[$i] eq "-p"){$N=$ARGV[++$i];next;}
	if($ARGV[$i] eq "-fasta"){$fasta=1;next;}
	if($ARGV[$i] eq "-keepsra"){$keepsra=1;next;}
	
	die "Do not understand argument $ARGV[$i].";
}

unless(is_numeric($N)){die "Number of processes $N is not numeric."}
if($N<1){$N=1;}

unless(-e $odir){mkdir($odir);}


my @acc_ids=();
my %acc_ids=(); 
my @f_names=();
my %f_names=();

open(my $fh,$fn) or die "$!";
while(<$fh>){if(substr($_,0,1) eq "#"){next} $_=clean_line($_);if(length($_)==0){next};
	my @fs=split(/\s+|,|;/,$_);
	if($acc_ids{$fs[0]}){next;} # replicate of previous SRA id
	push(@acc_ids,$fs[0]);
	$acc_ids{$fs[0]}=1;
	if(@fs>1){push(@f_names,$fs[1]);$f_names{$fs[1]}++;}else{push(@f_names,"")}
}
close($fh);

# do we have any file name twice?
my $tmp_str='';
foreach my $f_name (keys %f_names){
	if($f_names{$f_name}>1){$tmp_str.="$f_name\n"}
}
if($tmp_str ne ''){
	die "Each file name stem given in input table must be unique, but the following stems occurred several times.\n$tmp_str\n";
}

# download first SRA archive in the hope we download all reference files for it to such that the parallel processes won't need to download the references again which usually causes issues
# it's a bad hack because it also assumes that all SRAs come from the same species = need the same reference files
my $cmd="prefetch --max-size 104857600 --output-file $odir/".($acc_ids[0]).".sra ".($acc_ids[0])."";
print STDERR `$cmd`;
if($?){
# try a second time
	sleep(60);
	print STDERR `$cmd`;
}

# start $N parallel processes
for(my $child=0;$child<$N;$child++){
  my $pid = fork;
  if (not $pid) {  # in one of the child processes  

	my @o=();
	for(my $j=0;$j<@acc_ids;$j++){
		if($j % $N==$child){
			my $sra=$acc_ids[$j];
			my ($root)=$sra=~/(\w{3}\d{3})/;
			my ($type)=$sra=~/(\w{3})/;
			# download SRA archive
			for(my $tries=0;$tries<10;$tries++){
				my $cmd="prefetch --max-size 104857600 --output-file $odir/$sra.sra $sra";
				print STDERR `$cmd`;
				my $retval=$?;
				if($retval){
					if($tries==9){die "Matt error: Could not download SRA with id $sra.\nCall:$cmd\nTry to run matt retr_rnaseq with -p 1\n";}
					sleep 60;
				}else{last}
			}

			# extract RNAseq reads
			# https://edwards.sdsu.edu/research/fastq-dump/
			#$cmd="fastq-dump --outdir $odir --gzip --readids --dumpbase --skip-technical --split-files";
			$cmd="$fastq_dump_prog --outdir $odir --gzip --readids --dumpbase --skip-technical --split-files";
			if($fasta){$cmd.=" --fasta 0";}
			$cmd.=" $odir/$sra.sra";
			print STDERR `$cmd`;
			if($?){
				die "Matt error: Could not extract FASTX from SRA with id $sra. This command failed: $cmd \n";
			}

			# remove SRA archive
			unless($keepsra){
				unlink("$odir/$sra.sra");
			}
			
			# count reads in files
			my $f_name=$f_names[$j];
			opendir (my $dir, $odir) or die $!;
			while (my $file = readdir($dir)) {
				my @fs=split("_",$file);
				if(lc($fs[0]) eq lc($sra)){

					open(my $fh,"gunzip -c $odir/$file |") or die "Can’t open pipe to $odir/$file: $!";
					my @ls=();
					my $i=0;my $c=0;
					if($fasta){
						while(<$fh>){$i++;if($i==2){$ls[$c++]=length($_)-1;$i=0;}}
					}else{
						while(<$fh>){$i++;if($i==2){$ls[$c++]=length($_)-1;}; if($i==4){$i=0;}}
					}
					close($fh);
					@ls = sort { $a <=> $b } @ls;
					
					# rename if necessary
					if($f_name ne ""){
						$fs[0]=$f_name;
						my $f_name_new="$odir/".join("_",@fs);
						if(-e $f_name_new){ # a file with this name already exists
							do{
								$fs[0]=rand_string(10);
								$f_name_new="$odir/".join("_",@fs);
							}while(-e $f_name_new);			
						}
						move("$odir/$file",$f_name_new) or die "$!";
					}
					push(@o,"$sra\t".join("_",@fs)."\t".scalar(@ls)."\t$ls[0]\t".$ls[(@ls*0.25)-1]."\t".$ls[(@ls*0.5)-1]."\t".$ls[(@ls*0.75)-1]."\t".$ls[@ls-1]."\n");
				}
    		} # while-file
			closedir($dir);
		}# if-j
	} # for-j
	
	# create temporary file; will not be deleted automatically at the end of script or if error occurs
	my ($fh_tmp,$tmpfile_n) = tempfile( TEMPLATE => "dataset_info_XXXXXXXXX", DIR => "$odir", UNLINK => 0, SUFFIX => ".tab");
	print $fh_tmp "SRA_ID\tFILE\tN_READS\tMIN_L\t25_QUARTILE_L\tMEDIAN_L\t75_QUARTILE_L\tMAX_L\n";
	print $fh_tmp join("",@o);
	close($fh_tmp);
    exit;
  } # end child
} # for-child

# wait until all childreen have finished
for (1 .. $N) {
   wait();
}



# unify temprary info tables from childreen into dataset_info.tab
opendir (my $dir, $odir) or die $!;
my @fs=();
my @o=();  # contains contents of all dataset_info files
my $h;
while (my $file = readdir($dir)) {
	if(substr($file,0,12) eq "dataset_info"){
		push(@fs,"$odir/$file");
		open($fh,"$odir/$file") or die "$!";$h=<$fh>;
		while(<$fh>){push(@o,$_);}
		close($fh);
	}
}
closedir($dir);

# delete all dataset_info files
foreach my $f (@fs){unlink($f);}

# generate new dataset_info file with all cumulative info
open($fh,">$odir/dataset_info.tab") or die "$!";
print $fh $h;
print $fh join("",@o);
close($fh);


exit(0); # success
__END__
