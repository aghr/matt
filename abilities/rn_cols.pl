#!/usr/bin/env perl
use strict;
use warnings;
use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.


my $MYCOMMAND="rn_cols";

# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "manipulation\t$MYCOMMAND\t:    \trename columns";
        exit(1);
}

if(!defined($ARGV[0]) || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/ || @ARGV<2){
        print "\nmatt $MYCOMMAND <TABLE> <C_NAME_OLD1>:<C_NAME_NEW1> [<C_NAME_OLD2>:<C_NAME_NEW2> ...]\n\n";
	print "   ... change names of columns of given table\n\n";
	print "   <TABLE>                     : tab-separated table\n";
	print "   <C_NAME_OLD*>:<C_NAME_NEW*> : a pair of column names separated by colon.\n";
	print "                                 The column C_NAME_OLD gets changed to C_NAME_NEW\n\n";
	print "   Output : TABLE with changed column names gets written to STDOUT\n";
	print "\n";
        exit(1);
}


my $fn=$ARGV[0];
my @old_cnms=();
my @new_cnms=();
for(my $i=1;$i<@ARGV;$i++){my @fs=split(":",$ARGV[$i]);push(@old_cnms,$fs[0]);push(@new_cnms,$fs[1]);}

my ($fh,$col_ns_aref,$col_ids_aref,$comments)=open_fct_file($fn,[@old_cnms],$MYCOMMAND);

# rename columns
for(my $i=0;$i<scalar(@{$col_ids_aref});$i++){
	$col_ns_aref->[ $col_ids_aref->[$i] ]=$new_cnms[$i];
}

# OUTPUT all into temp file
print $comments;
print join("\t",@{$col_ns_aref})."\n";

while(<$fh>){
	$_=clean_line($_);
	if(length($_)==0 || substr($_,0,1) eq "#"){next;}
	print "$_\n";
}

close($fh); 

exit(0); # success
__END__
