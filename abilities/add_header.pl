#!/usr/bin/env perl
use strict;
use warnings;
use File::Copy;
use File::Temp qw(tempfile);
use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.

my $MYCOMMAND="add_header";


# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "manipulation\t$MYCOMMAND\t: \tadd column names";
        exit(1);
}


#####
sub print_die_message{
	my $out=$_[0];
	print "Table check did not finish with success. Table is not correctly formatted. Here are information about lines which don't have the correct number of fields:\n\n";
	my @fs=split("\n",$out);
	print $fs[0]."\n";
	foreach my $f (@fs[1..(@fs-1)]){my @fs2=split("\t",$f); $fs2[0]--; print join("\t",@fs2)."\n";}
}




if(!defined($ARGV[0]) || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/ || @ARGV<2){
	print "\nmatt $MYCOMMAND <TABLE> <COL_NAME1> [<COL_NAME2> <COL_NAME3> ...]\n\n";
	print "   ... add to the top of a table a new row with column names.\n";
	print "       Can be used to add column names to tables which don't contain column\n";
	print "       names yet, e.g., like BED files. Having added column names, this command\n";
	print "       will call chk_tab on on the table to check if it's correctly formatted\n";
	print "       to be used with Matt.\n\n";
	print "   <TABLE>     : tab-separated table yet without column names and without comment lines\n";
	print "   <COL_NAME*> : new column names separated by blanks. You need to provide as many\n";
	print "                 column names as TABLE has columns.\n\n";
	print "   Output : no output will be writte to screen but the original TABLE (file) will be changed.\n\n";
	exit(1);
}

my $orig_fn=shift(@ARGV);
open (my $fh,$orig_fn) or die "$!"; my $h=<$fh>; close($fh); open($fh,$orig_fn);
my @cols=split("\t",$h);

# create temporary file; will be deleted automatically at the end of script or if error occurs
my ($fh_tmp,$tmpfile_n) = tempfile( DIR => ".", UNLINK => 1);

if(@ARGV != @cols){die "You have specified ".scalar(@ARGV)." column names, but looks like the table has ".scalar(@cols)." columns. You may need to specify the correct number of column names or comment out lines at the top of the table if they don't really belong to the data of the table.\n"}

print $fh_tmp join("\t",@ARGV)."\n";
while(<$fh>){print $fh_tmp clean_line($_)."\n";}
close($fh_tmp);

my $cmd="matt chk_tab $tmpfile_n";
my $return=`$cmd`; die print_die_message($return) if $?!=0;

move($tmpfile_n, $orig_fn);

exit(0); # success
__END__
