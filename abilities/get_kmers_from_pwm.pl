#!/usr/bin/env perl
use strict;
use warnings;

use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.

my $MYCOMMAND="get_kmers_from_pwm";

# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "extras\t$MYCOMMAND\t: \tget top-N kmers from PWM";
        exit(1);
}


if(!defined($ARGV[0]) || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/ || @ARGV>4 || @ARGV<3){
	print "\nmatt $MYCOMMAND <TABLE> <N/P> <TIME_IN_MINS> [<PWM_NAME>]\n\n";
	print "   ... get the most likely kmers from a position weight matrix model (PWM)\n\n";
	print "   <TABLE>        : tab-separated table describing PWM in matt format (see also get_pwm)\n";
	print "   <N/P>          : if N/P is an integer larger or equal 1: the N most likely kmers will be returned\n";
	print "                      If it is a real number smaller then 1, then the most likely kmers which together\n";
	print "                      account for P (e.g., 0.1) of the binding specificity will be returned.\n";  
	print "                      The idea here is that all kmers together account for 100% of the binding\n";
	print "                      specificity.\n";
	print "   <TIME_IN_MINS> : maximum time in minutes, e.g., 1 = 1 min\n";
	print "   <PWM_NAME>     : can be omitted; if given this name will be used as PWM name in the output table\n";
	print "                      If not given, the PWM name will be taken from the file name <TABLE>.\n\n";
	print "   Output: a table column ID, KMER, PWM_LOGSCORE, CUMULATIVE_PERCENT_BINDING, PWM_NAME descrinbing the kmers.\n\n";
	print "   IMPORTANT: uses a DP algorithm to solve this problem. Can be very RAM and time consuming for large N/P.\n";
	print "\n";
        exit(1);
}


my ($file,$N,$max_ms)=@ARGV;
my $mode="P";   # percent
if($N>=1){$mode="N";}

my $pwm_name="";
if(@ARGV==4){
	$pwm_name=$ARGV[3];
}else{
	$pwm_name=$file;
	if($pwm_name =~ /\./){
        	$pwm_name=~ /(.+?)\./;
        	$pwm_name=$1;
	}
	if($pwm_name =~ /\//){
        	$pwm_name=~ /.*\/(.+)/;
        	$pwm_name=$1;
	}
}

##
# main
##
my ($pwm_aref,$alph_aref)=read_pwm($file);

# $pwm_aref  -> length = positions
# at each position hash with mapping letter -> log-prob
my $L=scalar(@$pwm_aref);
my $A=scalar(@$alph_aref);

# sample kmers
my @borders=();
my $sum;
for(my $l=0;$l<$L;$l++){
	$sum=0;
	$borders[$l]=[];
	foreach my $letr (@$alph_aref){
		$sum+=exp( $pwm_aref->[$l]->{$letr} );
		push(@{$borders[$l]},$sum);
	}
	$borders[$l]->[scalar(@$alph_aref)-1]=1;
}

my %kmers=();
my @kmers;
my @scores;
my @idx;
# we want to consider at most the top 100 kmers
# this theshold is the lower threshold on kmer scores when doing the complete kmer search for the top 100 kmers
my $score_lower_thresh="";

my $N_samples=(($A-2)**($L));
if($N_samples>1000000){$N_samples=1000000;}
my $break_out=0;
my $start_time = time();
while(1){
	#print "sampling $N_samples samples\n";
	for(my $i=0;$i<$N_samples;$i++){
		my $kmer="";
		for(my $l=0;$l<$L;$l++){
			my $shot=rand();
			for(my $j=0;$j<@$alph_aref;$j++){
				if($shot<=$borders[$l]->[$j]){$kmer.=$alph_aref->[$j];last;}
			}
		}
		unless($kmers{$kmer}){$kmers{$kmer}=get_pwm_scores_for_seq($kmer,$pwm_aref)->[0];}
	}


	# sort sampled kmers decreasingly
	@kmers=keys(%kmers);
	@scores=values(%kmers);
	@idx = sort { $scores[$b] <=> $scores[$a] } 0 .. $#scores;
	@scores=@scores[@idx];
	@kmers=@kmers[@idx];

	#print "\n\nSAMPLED ".scalar(@scores)."\n\n";
	#print "ID\tKMER\tPWM_LOGSCORE\n";
	#for(my $i=0;$i<20;$i++){
	#	print "".($i+1)."\t$kmers[$i]\t$scores[$i]\n";
	#}
	#for(my $i=@scores-20;$i<@scores;$i++){
	#	print "".($i+1)."\t$kmers[$i]\t$scores[$i]\n";
	#}

	if($mode eq "N"){
		if($N<@scores*0.25){$score_lower_thresh=$scores[$N-1];$break_out=1;}
	}
	$sum=0;
	if($mode eq "P"){
		for(my $i=0;$i<@scores*0.25;$i++){
			$sum+=exp($scores[$i]);
			if($sum>$N){$score_lower_thresh=$scores[$i];$break_out=1;}
		}
	}
	
	if($break_out){last;
	}else{$N_samples=$N_samples*2;}

	if(time() - $start_time > $max_ms*60){die "Tried hard but could not find a good lower bound to extract the requested kmers within the specified maximum minutes $max_ms.;"}
}


sub get_kmers{
	my $kmer_prefix=$_[0];
	my $score_prefix=$_[1];
	my $pos=$_[2];
	my $L=$_[3];
	my $A=$_[4];
	my $aref_pwm=$_[5];
	my $aref_alph=$_[6];
	my $aref_kmers=$_[7];
	my $aref_scores=$_[8];
	my $score_lower_thresh=$_[9];

	if($pos<$L){
		foreach my $letr (@$aref_alph){
			my $score_updated=$score_prefix+$aref_pwm->[$pos]->{$letr};
			
			if($score_updated < $score_lower_thresh){next;}
			
			get_kmers($kmer_prefix.$letr
				  ,$score_updated
				  ,$pos+1
				  ,$L
				  ,$A
				  ,$aref_pwm
				  ,$aref_alph
				  ,$aref_kmers
				  ,$aref_scores
				  ,$score_lower_thresh
				);
		}
	}else{
		push(@$aref_kmers,$kmer_prefix);
		push(@$aref_scores,$score_prefix);
	}
}


@kmers=();
@scores=();
#$score_lower_thresh= -1 * 9**9**9;
get_kmers("",0,0,$L,$A,$pwm_aref,$alph_aref,\@kmers,\@scores,$score_lower_thresh);

@idx = sort { $scores[$b] <=> $scores[$a] } 0 .. $#scores;
@scores=@scores[@idx];
@kmers=@kmers[@idx];


print "ID\tKMER\tPWM_LOGSCORE\tCUMULATIVE_PERCENT_BINDING\tPWM_NAME\n";

$sum=exp($scores[0]);
for(my $i=0;$i<@scores;$i++){
	print "".($i+1)."\t$kmers[$i]\t$scores[$i]\t$sum\t$pwm_name\n";
	if($mode eq "N" && $i==($N-1)){last;}
	
	if($mode eq "P" && $sum>$N){last;}
	if($i<@scores-1){$sum+=exp($scores[$i+1]);}
}

exit(0); # success
__END__