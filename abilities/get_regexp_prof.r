#!/usr/bin/env Rscript
args <- commandArgs(trailingOnly = TRUE)

#rm(list=ls())
#setwd("/home/crg/temp")
#args<-c("mics.fct","SEQ_UP","RL","SEQ_DOWN","LR","3","sf3b1_regulated]TRUE,FALSE[")


MYCOMMAND<-"get_regexp_prof";

# return short description
if(length(args)==1 && args[1]=="code:001"){
      str<-paste("seq_analysis\t",MYCOMMAND,"\t: \tget profiles of REGEXP hits",sep="")
      cat(str)
      quit(save = "no", status = 0)
}

if(length(args)<4 || args[1]=="help" || args[1]=="-help" || args[1]=="--help" || args[1]=="?"){
	cat("\nmatt ",MYCOMMAND," <TABLE> <C_SEQ1> <TYPE1> [<C_SEQ2> <TYPE2> ...] <REGEXP_TABLE> [<C_CLASS>]\n\n");
	cat("   ... search hits with Perl regular expressions in genomic sequences and plot positional distribution\n\n");
	cat("   <TABLE>     : tab-separated table with sequences\n");
	cat("   <C_SEQ1>    : name of column with sequences\n");
	cat("   <TYPE1>     : type of visualization of cumulative distribution\n");
	cat("                   * LR : left->right increasing  (standard visualization)\n");
	cat("                   * RL : right->left increasing\n");
	cat("                   * CLR: like LR but all sequences get length-normalized to length = 100\n");
	cat("                   * CRL: like RL but all sequences get length-normalized to length = 100\n");
	cat("   <C_SEQ2>... : you can specify several columns with sequences, e.g., which could contain up-stream and down-stream\n");
	cat("                 sequences of exons\n");
	cat("   <TYPE2>...  : for each column you'll need to specify the way how cumulative distributions get plotted\n");
	cat("<REGEXP_TABLE> : name of file with tab-separated table with information in regular expressions. It must contain a header with\n");
	cat("                   column names NAME and REGEXP, and must not contain comments. Each line contains a search string (which can\n");
	cat("                   be a PERL regular expression) and the name of this pattern.\n");
	cat("                   IMPORTANT: <REGEXP_TABLE> can be an integer; in this case it defines the length of k-mers over A,C,G,T and\n");
	cat("                     all k-mers of this length get tested. Resonable choices are 3-5; remember there are 4^LENGTH different k-mers\n");
	cat("   <C_CLASS>   : sequences are treated in groups defined by values in this column\n");
	cat("                  If you give this parameter as <C_CLASS>[val1,val2,...] only sequnces with values val1, val2, ...\n");
	cat("                  will be considered and the last group is considered being the reference group; maximum differences\n");
	cat("                  for ranking results will be determined wrt. this group.\n");
	cat("                  If <C_CLASS> is omitted, then sequences get treated as coming all from the same group.\n");
	cat("\n");
	cat("   Output: a folder which contains a website with the following results.\n");
	cat("        * positional distributions considering positions of all hits\n");
	cat("          Distributions for all groups get plotted into the same diagram. Distributions for sequences from different\n");
	cat("          columns (C_SEQ1, C_SEQ2,...) get plotted into different diagrams.\n");
	cat("        * cumulative positional distributions considering positions of all hits\n");
	cat("            for <TYPE>=LR: cumulative distributions increase from left to right\n");
	cat("            for <TYPE>=RL: cumulative distributions increase from right to left\n");
	cat("        * positional distributions considering ONLY the very first position per sequences\n");
	cat("            for <TYPE>=LR: first position = left-most position\n");
	cat("            for <TYPE>=RL: first position = right-most position\n");
	cat("        * cumulative positional distributions considering ONLY the very first positions\n");
	cat("            Again affected by <TYPE> parameter.\n");
	cat("\n");
	
	quit(save = "no", status = 0)
}

#################
## FUNCTIONS
#################
get_kmers<-function(K){
  alph<-c("A","C","G","T")
  n<-length(alph)
  ret<-rep(alph,each=n^(K-1))
  if(K>1){
    for(k in 2:K){
      ret<-paste(ret,rep(alph,each=n^(K-k)),sep="")
  }}

return(ret)
}


plot_dens<-function(data.lists,l.maxs,kmer,dist.args,xlab.txt,cdist=FALSE){
  # turn positions into lines
  y.max<-0
  y.max.diff<-0
  class_vs<-names(data.lists[[1]])
  class_ref<-class_vs[length(class_vs)]
  Ns<-list()
  for(c in 1:length(data.lists)){
    denss<-list()
    Ns[[c]]<-list()
    for(class in rev(class_vs)){
      Ns[[c]][[class]]<-length(data.lists[[c]][[class]])
      if(Ns[[c]][[class]]!=0){
	denss[[class]]<-density(data.lists[[c]][[class]],adjust=0.25,from=1,to=l.maxs[c],bw=l.maxs[c]/10)
      }else{
	denss[[class]]<-list(x=seq(1,l.maxs[c],length.out=512),y=rep(0,512))
      }
      if(Ns[[c]][[class]]!=0 & cdist=="TRUE"){
	if(dist.args[c] == "LR"){ denss[[class]][["y"]]<-cumsum(denss[[class]][["y"]])/sum(denss[[class]][["y"]]) }
	if(dist.args[c] == "RL"){ denss[[class]][["x"]]<-rev(denss[[class]][["x"]]); denss[[class]][["y"]]<-cumsum(rev(denss[[class]][["y"]]))/sum(denss[[class]][["y"]])}
      }
      y.max<-max(c(y.max,max(denss[[class]][["y"]])))
      
      # last class in class_vs is reference class for computing y.max.diff
      y.diff.tmp<-denss[[class]][["y"]]-denss[[class_ref]][["y"]]
      if(max(abs(y.diff.tmp))>abs(y.max.diff)){ids.tmp<-which(abs(y.diff.tmp)==max(abs(y.diff.tmp)));y.max.diff<-y.diff.tmp[ids.tmp[1]]}
    }
    if(length(class_vs)==1){
      if(cdist==FALSE){y.max.diff<-max(denss[[class]][["y"]])-min(denss[[class]][["y"]])
      }else{
        # maximum distance to diagonal
        ys<-denss[[class]][["y"]]; ys.n<-length(ys)
	y.max.diff <- ys - (1:ys.n)/ys.n
	ids.tmp<-which(abs(y.max.diff)==max(abs(y.max.diff)))
	y.max.diff<-y.max.diff[ids.tmp[1]]
      }
    }
    data.lists[[c]]<-denss
  }
  
  # 11 distinctive colors
  cols<-colors()[c(11,31,27,33,50,91,117,139,24,7,83)];cols<-cols[1:length(data.lists[[1]])];names(cols)<-names(data.lists[[1]])
  span<-10
  if(y.max==0){y.max<-1}
  plot(x=1,type="n",xlim=c(1,sum(l.maxs)+(length(l.maxs)-1)*span),ylim=c(0,y.max),main=kmer,xlab=xlab.txt,ylab="",axes=FALSE,cex.lab=1.4)
  if(cdist=="TRUE"){mtext("Cumulative density",side=2,line=1,at=y.max/2,cex=1.5)}else{mtext("Density",side=2,line=1,at=y.max/2,cex=1.5)}
  x.max.running<-0
  x.at<-c()
  x.labels<-c()
  for(c in 1:length(data.lists)){
    x.labels.toadd<-c(1,round(l.maxs[c]/2),l.maxs[c])
    x.at<-c(x.at,x.labels.toadd+x.max.running)
    if(dist.args[c] == "RL"){x.labels.toadd<-rev(x.labels.toadd)}
    x.labels<-c(x.labels,x.labels.toadd)
    mtext(names(data.lists)[c],side=1,line=2,at=x.labels.toadd[2]+x.max.running,cex=0.7)
    if(cdist==FALSE){
      legend(x=x.max.running+1,y=y.max*1.05,legend=Ns[[c]],text.col=cols[1:length(Ns[[c]])],bty="n",text.font=2,horiz=TRUE)
    }
    for(class in names(data.lists[[c]])){
      lines(x=data.lists[[c]][[class]][["x"]] +x.max.running ,y=data.lists[[c]][[class]][["y"]],lwd=3,col=cols[class])
    }
    x.max.tmp<-max(data.lists[[c]][[class]][["x"]])
    x.max.running<-x.max.running + x.max.tmp + span
  }

  axis(1,at=x.at,labels=x.labels)
  if(names(data.lists[[1]])[1]!="fhjaswqpo2987myxasqwe978812332wzrw"){
    legend.labs<-names(data.lists[[1]])
    if(cdist=="FALSE"){legend(x="topright",legend=legend.labs,col=cols,lwd=2,bty="n")
    }else{
      if(dist.args[length(dist.args)]=="LR"){legend(x="bottomright",legend=legend.labs,col=cols,lwd=2,bty="n")}else{legend(x="topright",legend=legend.labs,col=cols,lwd=2,bty="n")}
    }
  }
return(y.max.diff)
}



####################
## MAIN
####################
fn<-args[1]

offset<-2
if(length(args) %% 2 == 0){offset<-1} # no C_CLASS given
col_nms<-c()
dist_args<-c()
normalize<-c()
dist_args_orig<-c()
for(i in 2:(length(args)-offset)){
  if(i%%2==0){
    col_nms<-c(col_nms,args[i]);
  }else{
    if(!is.element(args[i],c("RL","LR","CRL","CLR"))){stop("visualization type ",args[i]," is not one of LR, RL, CLR, CLR\n")}
    norm_flag<-"NO"
    dist_args_orig<-c(dist_args_orig,args[i])
    if(args[i] == "CRL"){args[i]<-"RL";norm_flag<-"YES"}
    if(args[i] == "CLR"){args[i]<-"LR";norm_flag<-"YES"}
    dist_args<-c(dist_args,args[i])
    normalize<-c(normalize,norm_flag)
  }
}

# length of kmers or file name containing per line one k-mer or PERL search pattern
K<-args[length(args)-offset+1]

# read data
d<-read.table(file=fn,header=TRUE,sep="\t",check.names=FALSE,as.is=TRUE)

# class variable
c_class<-NA
class_vs<-c("fhjaswqpo2987myxasqwe978812332wzrw")
if(length(args) %% 2 != 0){c_class<-args[length(args)]}

if(!is.na(c_class)){
  if(length(grep("\\[",c_class))>0){   # C_CLASS]val1,val2[
    class_vs<-strsplit( substr(c_class,regexpr("\\[",c_class)+1,nchar(c_class)-1), "," )[[1]]
    c_class<-substr(c_class,1,regexpr("\\[",c_class)-1)
  }else{
    class_vs<-unique(d[,c_class])
  }
}

# reduce table to those lines corresponding to requested classes
classes<-NA
if(!is.na(c_class)){
  d<-d[is.element(d[,c_class],class_vs),]
  classes<-as.character(d[,c_class])
}else{
  classes<-rep(class_vs,nrow(d))  # set class of all data points to fhjaswqpo2987myxasqwe978812332wzrw
}

if(!is.na(c_class)){
  N.seqs<-tapply(d[,c_class],d[,c_class],length)
}else{
  N.seqs<-c(nrow(d))
  names(N.seqs)[1]<-"all"
}
data_info<-paste(names(N.seqs),"(",N.seqs,")",collapse=", ")
col_info<-paste(col_nms,"(",dist_args_orig,")",collapse=", ")


# reduce table to those columns for which the k-mer analysis should be done
if(length(col_nms)>1){
  d<-d[,col_nms]
}else{
  d<-as.matrix(d[,col_nms],ncol=1,dimnames=list(NULL,col_nms))
  colnames(d)<-col_nms
}


# check if K is a file name
if(suppressWarnings(!is.na(as.numeric(K)))=="FALSE"){
  # K is string and cannot be changed into a number
  ls.tmp<-readLines(K,-1)
  kmers<-c()
  kmers.names<-c()
  tmp2<-strsplit(ls.tmp[1],"\t")[[1]]
  if(sum(is.element(tmp2,c("REGEXP","NAME")))!=2){stop("Given table describing REGEXPS does not contain header REGEXP<TAB>NAME\n")}
  for(i in 2:length(ls.tmp)){
    tmp<-strsplit(ls.tmp[i],"\t")[[1]]
    if(tmp2[1]=="REGEXP"){
      kmers[length(kmers)+1]<-tmp[1]
      kmers.names[length(kmers.names)+1]<-tmp[2]
    }else{
      kmers[length(kmers)+1]<-tmp[2]
      kmers.names[length(kmers.names)+1]<-tmp[1]
    }
  }
    output_dir<-"./regexp_profiles"
}else{
  K<-as.numeric(K)
  kmers<-get_kmers(K)
  kmers.names<-kmers
  output_dir<-paste("./",K,"mer_profiles",sep="")
}

# create directory for plots
if(!dir.exists(output_dir)){dir.create(output_dir, showWarnings = TRUE, recursive = FALSE)}


y.diffs<-matrix(NA,ncol=5,nrow=length(kmers),dimnames=list(NULL,c("KMER_NAME","DENSITY_ALL","CDENSITY_ALL","DENSITY_FIRST","CDENSITY_FIRST")))


for(j in 1:length(kmers)){
  kmer<-kmers[j]
  kmer.name<-kmers.names[j]
  
  m.poss<-list()
  m.starts<-list()
  l.maxs<-c()
  for(c in 1:ncol(d)){
  
    # for each column new list
    m.poss[[c]]<-list(); names(m.poss)[c]<-colnames(d)[c]
    m.starts[[c]]<-list(); names(m.starts)[c]<-colnames(d)[c]

    # for each class new list
    for(class in class_vs){m.poss[[c]][[class]]<-list();m.starts[[c]][[class]]<-c(NA)}
    # max length of seq in this column
    seq.ls<-sapply(d[,c],nchar)
    l.max<-max(seq.ls)
    if(normalize[c]=="YES"){l.max<-100}

    d.tmp<-cbind(1:nrow(d),d[,c])
    colnames(d.tmp)<-c("ID","SEQ")
    fn.tmp<-"tmpfile_ajh2198721klas98xsmas178aska.tab"
    #if(c==1){fn.tmp<-"1.tmp"}else{fn.tmp<-"2.tmp"}
    write.table(d.tmp,file=fn.tmp,quote=FALSE,sep="\t",row.names=FALSE)
    cmd<-paste("matt get_regexp_hits ",fn.tmp," SEQ ID '",kmer,"' single",sep="")
    hits<-read.table(pipe(cmd),header=TRUE,sep="\t",as.is=TRUE)
    unlink("tmpfile_ajh2198721klas98xsmas178aska.tab")    
    
    for(id in unique(hits[,"SEQ_ID"])){
      rows<-which(hits[,"SEQ_ID"]==id)
      seq.l<-seq.ls[id]
      class<-classes[id]
      
      match.poss<-hits[rows,"START"]
      
      # seq should be right aligned  -> transform into N_pos which we have to substract from l.max at the end
      if(dist_args[c] == "RL"){match.poss<-seq.l-hits[rows,"END"]}

      # centralized: all hit pos get "normalized" to interval 1..100
      if(normalize[c] == "YES"){match.poss<-match.poss * 100/seq.l}
      
      m.poss[[ c ]][[ class ]][[ length(m.poss[[ c ]][[ class ]]) + 1 ]]<-match.poss
    }
    
    l.maxs[c]<-l.max
    for(l in 1:length(m.poss[[c]])){
      if(dist_args[c] == "RL"){
	m.poss[[c]][[l]]<-lapply(m.poss[[c]][[l]],function(x){l.max-x})
	tmp<-unlist(lapply(m.poss[[c]][[l]],max))
	if(is.null(tmp)){m.starts[[c]][[l]]<-list()}else{m.starts[[c]][[l]]<-tmp}
      }else{
        tmp<-unlist(lapply(m.poss[[c]][[l]],min))
	if(is.null(tmp)){m.starts[[c]][[l]]<-list()}else{m.starts[[c]][[l]]<-tmp}
      }
      tmp<-unlist(m.poss[[c]][[l]])
      if(is.null(tmp)){	m.poss[[c]][[l]]<-list()}else{m.poss[[c]][[l]]<-tmp}
    }
  }

  devices <- c("pdf", "png")
  fac<-0.5
  wid<-10*fac*ncol(d)  # 8.4
  hei<-7*fac*4         # 6.5
  for (i in seq_along(devices)){

    if (devices[i] == "png") {
      ppi <- 100
      png(file = paste(output_dir,"/",kmer.name,".png",sep=""), width = wid * ppi, height = hei * ppi, res = ppi)
    }

    if (devices[i] == "pdf") {
      pdf(file = paste(output_dir,"/",kmer.name,".pdf",sep=""), width = wid, height = hei)
    }
    
    par(mfrow=c(4,1))
    
    # density of all pos
    y.diffs[j,1]<-kmer.name
    y.diffs[j,2]<-plot_dens(m.poss,l.maxs,kmer.name,dist_args,"All occurrences")
    y.diffs[j,3]<-plot_dens(m.poss,l.maxs,"",dist_args,"All occurrences",cdist="TRUE")
    # density of first pos
    y.diffs[j,4]<-plot_dens(m.starts,l.maxs,"",dist_args,"First occurrence")
    y.diffs[j,5]<-plot_dens(m.starts,l.maxs,"",dist_args,"First occurrence",cdist="TRUE")

    graphics.off()
  }
}


write.table(y.diffs,file=paste(output_dir,"/ydiff_scores_of_dens_and_cdens.tab",sep=""),quote=FALSE,sep="\t",row.names=FALSE,col.names=TRUE)

# get date string
date<-strsplit(as.character(Sys.Date())[[1]],"-")[[1]]
months<-c("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Okt","Nov","Dec")
date.str<-paste(months[as.numeric(date[2])]," ",date[3],", ",date[1],sep="")

kmers.ordered.dens.all<-y.diffs[ order(as.numeric(y.diffs[,"DENSITY_ALL"])) ,"KMER_NAME"]
kmers.ordered.cdens.all<-y.diffs[ order(as.numeric(y.diffs[,"CDENSITY_ALL"])) ,"KMER_NAME"]
kmers.ordered.dens.first<-y.diffs[ order(as.numeric(y.diffs[,"DENSITY_FIRST"])) ,"KMER_NAME"]
kmers.ordered.cdens.first<-y.diffs[ order(as.numeric(y.diffs[,"CDENSITY_FIRST"])) ,"KMER_NAME"]

html.table<-matrix(c(
  c("<b>Densities all positions</b>" ,paste("<a href=\"#",kmers.ordered.dens.all,"\">",kmers.ordered.dens.all,"</a><br>",sep=""))
  ,c("<b>Cumulative densities all positions</b>" ,paste("<a href=\"#",kmers.ordered.cdens.all,"\">",kmers.ordered.cdens.all,"</a><br>",sep=""))
  ,c("<b>Densities first positions</b>" ,paste("<a href=\"#",kmers.ordered.dens.first,"\">",kmers.ordered.dens.first,"</a><br>",sep=""))
  ,c("<b>Cumulative densities first positions</b>" ,paste("<a href=\"#",kmers.ordered.cdens.first,"\">",kmers.ordered.cdens.first,"</a><br>",sep=""))
),ncol=4)


ref.group<-class_vs[length(class_vs)]
if(ref.group=="fhjaswqpo2987myxasqwe978812332wzrw"){ref_group="none"}

# create HTML output
sink(paste(output_dir,"/0_all_results.html",sep=""))
cat("<h1>Matt: Positional distributions of hits of REGEXPs</h1>
<p>
<b>Data file:</b>",fn,"<br>
<b>Categories (number of sequences):</b>",data_info,"<br>
<b>Sequence columns selected (plotting parameter):</b>",col_info,"<br>
<b>K or file with information on REGEXPS:</b>",K,"<br>
<b>Date:</b>",date.str,"<br>
</p>
<br>
<h2>Ordered Results</h2>
<a name=\"overview\"></a>
<b>Ordering from top to bottom in each column:</b> corresponds to most negative<br> 
to most positive differences of densities / cumulative densities to<br>
densities / cumulative densities of reference group<br>
<b>Reference group:</b>",ref.group,"<br>
If reference group is <b>none</b> then differences are determined wrt. zero line / diagonal with slope 1.<br><br>
<table cellspacing=\"1\" cellpadding=\"2\">
 <col width=\"130\">
 <col width=\"130\">
 <col width=\"130\">
 <col width=\"130\">
")
for(r in 1:nrow(html.table)){
cat("\n<tr>\n")
cat(paste("<td align=\"center\">",html.table[r,],"</td>",sep="",collaspe="\n"))
cat("</tr>")
}

cat("
</table>
<br><br>
<h2>Plots</h2>
")
cat(paste("<h2>",paste(kmers.names,kmers,sep=" - "),"</h2><a href=\"#overview\">Back to Ordered Results</a><a name=\"",kmers.names,"\"></a><br><img src=\"",kmers.names,".png\"/><br><br><br>",sep="",collapse="\n"))

sink()

quit(status=0) # success
