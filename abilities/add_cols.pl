#!/usr/bin/env perl
use strict;
use warnings;
use File::Temp qw(tempfile);
use File::Copy qw(move);
use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.


# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "manipulation\tadd_cols\t:   \tadd columns to table";
        exit(1);
}

my $MYCOMMAND="add_cols";

if(!defined($ARGV[0]) || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/ || @ARGV<2){
	print "\nmatt $MYCOMMAND <TABLE_TARGET> <TABLE_SOURCE> [<NEW_C_NAME_1> <NEW_C_NAME_2> ...]\n\n";
	print "   ... add columns of TABLE_SOURCE to right end of TABLE_TARGET\n\n";
	print "   <TABLE_TARGET> : tab-separated table\n";
	print "   <TABLE_SOURCE> : tab-separated table\n";
	print "   <NEW_C_NAMES*> : the headers of columns taken from TABLE_SOURCE will be changed\n";
	print "                    to these new column headers before adding them to TABLE_TAGET.\n";
	print "                    If given, you have to specify new column names for all columns to be added.\n";
	print "   IMORTANT       : number of rows of new columns must be identical to number of rowns of TABLE_TARGET.\n";
	print "   NOTE           : TABLE_SOURCE and TABLE_TARGET can be the same file.\n\n";
	print "   Output : no output will be writte to screen but file of TABLE_TARGET gets extended by columns.\n";
	print "\n";
        exit(1);
}


my ($colfeat_file,$colfeat_file_toadd)=@ARGV[0..1];
my @new_colnms=();
if(@ARGV>2){
	@new_colnms=@ARGV[2..(@ARGV-1)];
}

open(my $fh_target,"<".$colfeat_file) or die "Sorry, you requested add_cols but: I cannot open file $colfeat_file. ";
open(my $fh_source,"<".$colfeat_file_toadd) or die "Sorry, you requested add_cols but: I cannot open file $colfeat_file_toadd. ";

# create temporary file; will be deleted automatically at the end of script or if error occurs
my ($fh_tmp,$tmpfile_n) = tempfile( DIR => ".", UNLINK => 1);

my $val_to_add;
my $N_source=0;
my $N_target=0;
my $header_checked=0;
my $added_nothing=1;

while(<$fh_source>){
	$val_to_add=clean_line($_);	
	# comment line 	# remove empty line
	if(length($val_to_add)==0 || substr($val_to_add,0,1) eq "#"){next;}

	$N_source++;

	if($header_checked==0 && @new_colnms>0){
		my @fs=split_line($val_to_add);
		if(@fs != @new_colnms){die "ERROR add_cols: you have specied new column names but their number ",scalar(@new_colnms)," does not fit to the number of columns to be added ",scalar(@fs),"";}
		$val_to_add=join("\t",@new_colnms);
	}

	INNER :
	  while(<$fh_target>){
	  	$_=clean_line($_);
	  	# remove empty line
		if(length($_)==0){next;}
		# comment line
		if(substr($_,0,1) eq "#"){print $fh_tmp "$_\n";next;}


		if($header_checked==0){
			# if there are already columns in target with equal name of at least one column in source
			foreach my $target_col_n (split("\t",$_,-1)){
			foreach my $source_col_n (split("\t",$val_to_add,-1)){
				if($target_col_n eq $source_col_n){
					die "There is already a column $source_col_n in file $colfeat_file. Column names must be unique.\n";
				}
			}}
			$header_checked=1;
		}

		print $fh_tmp "$_\t$val_to_add\n";
		$N_target++;
		last INNER;
	}
	$added_nothing=0;
}

# check if there are more lines in target
while(<$fh_target>){
	$_=clean_line($_);
	# remove empty line
	if(length($_)==0){next;}
	# comment line
	if(substr($_,0,1) eq "#"){print $fh_tmp "$_\n";next;}
	$N_target++;
}

close($fh_source);
close($fh_target);
close($fh_tmp);

if($N_source != $N_target){
	die "Sorry, you requested add_cols but: number of rows of $colfeat_file does not fit to number of rows in $colfeat_file_toadd.";
}

unless($added_nothing){
	move($tmpfile_n,$colfeat_file);
}else{
	unlink($tmpfile_n);
}

exit(0); # success
__END__
