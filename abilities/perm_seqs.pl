#!/usr/bin/env perl
use strict;
use warnings;

use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.


my $MYCOMMAND="perm_seqs";

# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "extras\t$MYCOMMAND\t:    \trandomly permute seqs";
        exit(1);
}


if(!defined($ARGV[0]) || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/ || @ARGV<2){
        print "\nmatt$MYCOMMAND <TABLE> <C_SEQ1> [<C_SEQ2> ...] [-seed <SEED>]\n\n";
        print "   ... randomly permute sequences\n\n";
	print "   <TABLE> : tab-separated table with sequences\n";
	print "   <C_SEQ> : names of columns with sequences\n";
	print "   <SEED>  : any real number to be used as seed for random number generator\n\n";
	print "   Output: a table with columns CSEQ_PERM containing the permuted sequences for all\n";
	print "           columns given by C_SEQ to STDOUT.\n";
	print "\n";
        exit(1);
}


#######
# SUBS
#######
# permutes array in place
sub fisher_yates_shuffle{
	my $seq = $_[0];
	
	my @a=split("",$seq);
	
	my $i = @a; my $j;
	while ( --$i ){
		$j = int rand( $i+1 );
		@a[$i,$j] = @a[$j,$i];	
	}

return(join("",@a));
}


sub perm_string{
        my $s=$_[0];
        my @s=split("",$s);
        my @ret=();

        for(my $i=0;$i<@s;$i++){
                my $idx=int rand(@s-$i);
                push(@ret,$s[$idx]);
                $s[$idx]=$s[@s-$i-1];
        }

return(join("",@ret))
}


######
# MAIN
######
my @cnames=@ARGV[1..(@ARGV-1)];

# set seed
if($ARGV[@ARGV-2] eq "-seed"){srand($ARGV[@ARGV-1]); @cnames=@ARGV[1..(@ARGV-3)];}

# open table
my ($fh,$col_ns_aref,$col_ids_aref,$comments) = open_fct_file($ARGV[0],[@cnames],$MYCOMMAND);


# output header
my @header=();
foreach my $tmp_col (@cnames){push(@header,$tmp_col."_PERM");}
print join("\t",@header)."\n";


while(<$fh>){
	$_=clean_line($_);
	if(length($_)==0 || substr($_,0,1) eq "#"){next;}
	my @fs=split_line($_);
	
	my @ret=();
	foreach my $col_id (@$col_ids_aref){
		#push(@ret,fisher_yates_shuffle($fs[$col_id]));
		push(@ret,perm_string($fs[$col_id]));
	}
	print join("\t",@ret)."\n";
}
close($fh);


exit(0); # success
__END__
