#!/usr/bin/env perl
use strict;
use warnings;
use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.


# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "retrieval\tget_colnms\t: \textract column names";
        exit(1);
}


my $MYCOMMAND="get_colnms";

if(!defined($ARGV[0]) || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/ || @ARGV!=1){
        print "\nmatt $MYCOMMAND <TABLE>\n\n";
        print "   ... prints column names and their IDS.\n\n";
	print "   <TABLE>  : tab-separated table\n\n"; 
        print "   Output to STDOUT: column names and IDs of TABLE\n\n";
        exit(1);
}



my ($fh,$col_ns_aref,$col_ids_aref,$comments) = open_fct_file($ARGV[0],[],$MYCOMMAND);
close($fh);

print "ID\tCOL_NAMES\n";
my $col_id=0;
foreach my $cn (@{$col_ns_aref}){
	$col_id++;
	print "$col_id\t$cn\n";
}

exit(0); # success
__END__