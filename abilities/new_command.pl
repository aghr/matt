#!/usr/bin/env perl
use strict;
use warnings;

use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items; You might want to check out all utility functions available in ..../matt/installation/directory/lib/Utilities.pm
                                              # You might want to add more utility functions into ...../matt/installation/directory/lib/Utilities.pm .
################################
# GENERAL HINT ON EXTENDING MATT
################################
# New commands/scripts can be implemented in any scripting/programming language.
# If you want to use Perl,
# 1.) simply copy this template script and give it a name of the new command
# 2.) keep it in ...../matt/installation/directory/abilities
# 3.) change it so that it implements the new functionality
# 4.) make it executable
# 
# Scripting languages: the new script
#                       1.) should be in ...../matt/installation/directory/abilities
#                       2.) should be executable
#                       3.) must return some basic info on the new command when passed argument code:001 (see below in this script)
#                       4.) implement the functionality you want
# Programming language: You might want to create a program and put it into directory ...../matt/installation/directory/lib .
#                       In addition, you would need to create a new wrapper script which is the interface between your program and the user.
#                       This wrapper script 
#                       1.) should be in ...../matt/installation/directory/abilities
#                       2.) should be executable
#                       3.) must return some basic info on the new command when passed argument code:001 (see below in this script)
#                       4.) pass-by user arguments to your program
# 			5.) call your program
#
# You might want to add external programs to Matt. Put their executables into ...../matt/installation/directory/external_progs .
# If you want to add new data (like data bases of binding sites etc.) to Matt, you might want to put these into ...../matt/installation/directory/data_external .
# If you want to add utility scripts not written in Perl, you might want to place them into ...../matt/installation/directory/utilities .
# If you want to add more Perl utility functions, you might add them to ..../matt/installation/directory/lib/Utilities.pm .
#
#################################

my $MYCOMMAND="template";   # Change here for the name of the new command. Whenever you need to refer to this name, use variable $MYCOMMAND.

# return short description, which is used for the overview screen of all commands when the user types > matt
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "TEMPLATE_CATEGORY\t$MYCOMMAND\t:   \tSHORT_DESCRIPTION";	# this string will be split at tabs and consists of four parts
        exit(1);							# 1.) category of command: change TEMPLATE_CATEGORY to one of: retrieval, manipulation, seq_extraction, seq_analysis, import_table, math, extras, super (see command overview when calling > matt) 
}									# 2.) name of the command: nothing needs to be changes here
									# 3.) spacer: adapt to nicely align commands in overview
									# 4.) change SHORT_DESCRIPTION to a super short description of what commnand does (should be really short) 

# Help message; Will be displayed always then this command is called without any arguments, with the wrong number of arguments, or with help or ?.
if(!defined($ARGV[0]) || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/ || @ARGV!=2){   # adapt for number of expected arguments
	print "\nmatt $MYCOMMAND <TABLE> <ARG1> <ARG2> [ARG3]\n\n";             # the first argument should be a table, <ARG>: a necessary argument, [ARG]: this argument can be neglected
        print "   ... this is a template command and so far does not do anything.\n\n";
        print "   <TABLE>  : tab-separated table\n"; 
	print "   <ARG1>   : .....\n";
	print "   <ARG2>   : .....\n";
	print "   [ARG3]   : .....\n\n";
        print "   Output to STDOUT: table with ....\n\n";                       # most often the output will be a new table written to STDOUT so that users can redirect this output
        exit(1);
}

# e.g., name of file containing a table
my $fn=$ARGV[0];
# e.g., name of a column in this table to which the user wants to apply this command/script
my $cn=$ARGV[1];


# open_fct_file is a utility command making it easer to deal with files containing tables.
# Arguments
# 1.) name of file with tab-separated table
# 2.) a reference to an array containing column names of columns you want to work with later; can be empty = []
# 3.) the name of the calling command = this command/script (does not need to be changed)
# Returns
# 4.) a filehandle, opened, ready to read the first row from the table after the table header = column names
# 5.) a reference to an array containing all column names in the same order as they occur in the table header
# 6.) a reference to an array with IDs of columns given in 2.) 
# 7.) a string (if it goes over several lines with newlines), containing comments which come before the table header (and which need to start with #)
my ($fh,$col_ns_aref,$col_ids_aref,$comments)=open_fct_file($fn,[$cn],$MYCOMMAND);

# id of requested column
my $col_id=$col_ids_aref->[0];

# Very often, you will want to run over the table row-by-row and apply some computations to each row or extract pieces of information from the table.
# This while-loop is the standard way of doing it so
while(<$fh>){
	# In principle, you would need to clean each line, e.g., remove the newline at the end, and split it at tabs.
	# For this, different utility functions are available
	# 1.) clean_line  : deletes any newline characters (LF, CR) and deletes quotes (quotes arround all fields are present when table come from MS Excel text output)
	# 2.) split_line  : splits a string at tabs and returns array with fields
	# 3.) clean_and_split_line    : clean_line + split_line
	# 4.) clean_line_leave_quotes : deletes any newline characters (LF, CR) but keeps quotes
	
	# often you might want to clean the line first
	$_=clean_line($_);
	# check if this line is empty or a comment line
	if(length($_)==0 || substr($_,0,1) eq "#"){next;}   # neglect empty lines or comment lines (hint: it's better if tables do not contain internal comment lines)
	# and split this line at tabs
	my @fs=split_line($_);
	
	# this value of the value in the current row in the requested column (for which the user had specified its column name)
	my $val=$fs[$col_id];
	
	# do more stuff here...
}
close($fh);

# generate output; often this output will be a tab-separated table, which should be written to STDOUT

exit(0); # success
__END__