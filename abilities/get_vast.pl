#!/usr/bin/env perl
use strict;
use warnings;
use File::Temp qw(tempfile);

use Cwd qw(abs_path);
my ($scr_abspath);
BEGIN{
	# is the full path to the script even if it was called through a link
	$scr_abspath=abs_path($0);
	# remove script_name.pl to obtain root directory
	$scr_abspath =~ s/\/\w+\.pl$//;
}
use lib "$scr_abspath/../lib";
use Utilities;  # import default list of items.

my $MYCOMMAND="get_vast";

# return short description
if(defined($ARGV[0]) && $ARGV[0] eq "code:001"){
        print "import_table\t$MYCOMMAND\t: \tget VAST-TOOLS output";
        exit(1);
}


sub remove_NA_from_array{
	my $ary_ref=$_[0];

	my @ret=();
	for(my $i=0;$i<@$ary_ref;$i++){
		if($ary_ref->[$i] eq "NA"){next}
		push(@ret,$ary_ref->[$i]);
	}
return(@ret);
}


sub array_contains_NA{
	my $ary_ref=$_[0];
	for(my $i=0;$i<@$ary_ref;$i++){
		if($ary_ref->[$i] eq "NA"){return(1)}
	}
return(0);
}


if(!defined($ARGV[0]) || $ARGV[0] =~ /help/ || $ARGV[0] =~ /\?/ || @ARGV<1){
	print "\nmatt $MYCOMMAND <TABLE> [<CRITERIUM_1> <VALUE_1>] [<CRITERIUM_2> <VALUE_2>] [-paired] ...\n\n";
	print "   ... extracts all main information from a VAST-TOOLS INCLUSION_FILE (result of vast-tools combine)\n"; 
	print "   Also allows to extract only events which fulfill given criteria.\n\n";
	print "   <TABLE>    : final result table (INCLUSION table) of VAST-TOOLS align\n";
	print "   -minqglob  : extract only events whose PSI values could be estimated across all data sets with a minimum quality flag as specified.\n";
	print "                  Values: N, VLOW, LOW, OK, SOK  (default: VLOW)\n";
	print "   -minqab    : like minqglob but applies only to samples selected via -a and -b option.\n";
	print "                  Values: N, VLOW, LOW, OK, SOK  (default: VLOW)\n";
	print " -IRimbalglob : lower threshold on p values of binomial test testing how imbalanced are the read numbers mapping to both inclusion junctions\n";
	print "                  of introns. If they are very imbalanced (p value small) this might indicate strong bias on PIR computations. [default: -IRimbalglob 0]\n";
	print "   -IRimbalab : like -IRimbalglob but for samples selcted with -a/-b. [default: -IRimbalab 0]\n";
	print "   -complex   : extract only splicing events of a certain type (in column COMPLEX)\n";
	print "                  Default: No restriction. Any type containg IR/Alt3/Alt5 is assumed to be an IR/Alt3/Alt5 event, the rest are SE events.\n";
	print "                  Use comma-separated list to select several values from column COMPLEX, e.g., S,C1,C2,C3,MIC extracts only exons\n";
	print "   -scfds     : extract only events coming from specifies chromosomes/scaffolds\n";
	print "                  Values: comma-separated list like chr1,chr4,chr17 (default: not set = no restriction)\n";  
	print "   -strand    : extract only events from strand - or +\n";
	print "                  Values: - or + (default: not set = no restriction)\n";
	print "   -a and -b  : specify two groups of samples to be compared. Output table will contain dPSI and margin between PSIs of groups A and of group B.\n";
	print "   -paired    :   If given, dPSI will be computed in a paired manner, e.g., with  -a KD1,KD2,KD3 -b CNTR1,CNTR2,CNTR3: mean(KD1-CNTR1, KD2-CNTR2, KD3-CNTR3)\n";
	print "   -minNVal   : PSIs of samples not fulfilling -minqglob, -minqab, -IRimbalglob, -IRimbalab will be considered as being NA (not available).\n";
	print "                  By default reported values like dPSI, minPSI, maxPSI etc. will be NA if one ore more of the PSI values used for the computations are NA.\n";
	print "                  If you specify -minNVal <NUMBER>, e.g., -minNVal 2, then dPSI, minPSI, maxPSI will be computed if at least NUMBER PSI values are not NA.\n";
	print "                  E.g., if PSIs are 30, 40, NA, 50, NA then, with -minNVal 1 or 2 or 3 you'll get minPSI=30, maxPSI=50, meanPSI=50 while with\n";
	print "                  -minNVal 4 all would be NA. If you specify -a/-b then NUMBER applies to the PSIs within group A, and within group B. That means, with three\n";
	print "                  replicates per group and -minNVal 2, you would get minPSI, maxPSI, dPSI comparing group A vs B as long as you have at least 2 valid PSIs per group.\n";
	print "   -minNValb  : like -minNVal but applies to samples of groups -b. If -minNVal is given but -minNValb not, then minNVal applies to group -a and -b.\n";
	print "   -gtf <GTF> : a GTF file with gene annotation from which gene ids should be taken from. Unless given column GENEID will only contain NAs.\n";
	print "   -f <FIELD> : The name of the field in GTF which contains the gene ids. For Ensembl GTFs this is e.g. gene_id. The default is gene_id .\n\n";	
	print "   Output to STDOUT: a new table comprising the first 6 columns of TABLE, and new columns containing extracted information:\n";
	print "   START, END, STRAND, SCAFFOLD (chromosome / scaffold id); START/END correspond to:\n";
	print "   -exon start/end for S, C1, C2, C3\n";
	print "   -intron start/end for IR-S, IR-C\n";
	print "   -in case of Alt3 with ..EXON==|------------|A=|B==Exon.. => one line with A/A and a second with A/B \n";
	print "   -in case of Alt5 with ..EXON==A|=B|------------|==Exon.. => one line with A/A and a second with B/B\n\n";
	print "   More information is for each sample: PSI, Q(uality) of PSI estimation and normalized number of inclusion and exclusion reads as output by VAST-TOOLs\n";
	print "   Further information for each group of samples is: MINPSI, MAXPSI, PSIRANGE, and, if -a and -b are given, dPSI, and the margin between PSI ranges of a and b.\n\n";    
	print "   Example: matt get_vast VTS_OUT.tab -a kd1,kd2 -b ctr1,ctr2 -complex IR,IR-S,IR-C -minqglob LOW -minqab VLOW\n";
	print "   extracts all intron retention events and uses for the computations of MINPSI, MAXPSI, PSIRANGE, dPSI, margin only those PSI values with quality at least VLOW.\n";
	print "   If groups of samples contain at laest one PSI with a lower quality, then all output values will be NA. If you add -minNVal 1 then output values will be computed if\n";
	print "   there is at least one valid PSI per group.\n\n"; 
	print "   Alternative call:\n";
	print "   matt $MYCOMMAND <TABLE> <C_COORD> <C_FullCO> <C_COMPLEX> <C_LENGTH> [ -gtf <GTF> [-f <FIELD>] ]\n\n";
	print "   No filter can be applied here. This version of get_vast is meant to be with already processed vast-tools output files\n";
	print "   where you are interested in extracting information on SCAFFOLD, START, END, STRAND, UPSTRM_EX_BORDER, DOSTRM_EX_BORDER, GENEID\n";
	print "   from vast-tools output\n";
	print "   <TABLE>     : a table of processed vast-tools output\n";
	print "   <C_COORD>   : name of column which contains the vast-tools COORD output; likely COORD\n";
	print "   <C_FullCO>  : name of column which contains the vast-tools FullCO output;likely FullCO\n";
	print "   <C_COMPLEX> : name of column which contains the vast-tools COMPLEX output; likely COMPLEX\n";
	print "   <C_LENGTH>  : name of column which contains the vast-tools LENGTH output; likely LENGTH\n";
	print "\n";
	exit(1);
}


my $gtf="";
my $gid_field="gene_id";
my $mode="normal";  # first get_vast method   # number of arguments is 1 3 5 ... always un-even
if(@ARGV>=5 && @ARGV<=9 && substr($ARGV[1],0,1) ne "-" && substr($ARGV[2],0,1) ne "-" && substr($ARGV[3],0,1) ne "-" && substr($ARGV[4],0,1) ne "-"){
	if(@ARGV==5){
		$mode="reduced";
	}
	if(@ARGV==7 && $ARGV[5] eq "-gtf"){
		$gtf=$ARGV[6];
		$mode="reduced";  # reduced mode -> second get_vast method
	}
	if(@ARGV==9 && $ARGV[5] eq "-gtf" && $ARGV[7] eq "-f"){
		$gtf=$ARGV[6];
		$gid_field=$ARGV[8];
		$mode="reduced";  # reduced mode -> second get_vast method
	}
	if(@ARGV==9 && $ARGV[5] eq "-f" && $ARGV[7] eq "-gtf"){
		$gtf=$ARGV[8];
		$gid_field=$ARGV[6];
		$mode="reduced";  # reduced mode -> second get_vast method
	}
}


# vast-tools INCLUSION_LEVELS output file (results from call vast-tools combine)
my $fn=$ARGV[0];


# vast-tools quality criteria
my %qs;
$qs{"N"}=1;
$qs{"VLOW"}=2;
$qs{"LOW"}=3;
$qs{"OK"}=4;
$qs{"SOK"}=5;

# standard values
my ($minqglob,$minqab,$complex,$minNVal,$minNValb)=("VLOW","VLOW","all",undef,undef);
my ($irimbalglob,$irimbalab)=(0,0);
my @a=();
my @b=();
my %g1_ids=();
my %g2_ids=();
my $g1_defined=0;
my $g2_defined=0;
my ($scfds,$strand)=("","");
my @complexes=split(",",$complex);
my @scfds=split(",",$scfds);
my $paired=0;


if($mode eq "normal"){
	for(my $i=1; $i<@ARGV;$i+=2){

		my $cr=$ARGV[$i];
		my $v=$ARGV[$i+1];

		if($cr eq "-minqglob"){$minqglob=uc($v);unless($qs{$minqglob}){die "Given -minqglob value $minqglob is unknown."}next;}
		if($cr eq "-minqab"){$minqab=uc($v);unless($qs{$minqab}){die "Given -minqab value $minqab is unknown."}next;}
		if($cr eq "-complex"){@complexes=split(",",$v);$complex=$v;  next;}
		if($cr eq "-scfds"){@scfds=split(",",$v);next;}
		if($cr eq "-strand"){$strand=$v;next;}
		if($cr eq "-a"){@a=split(",",$v);$g1_defined=1;next;}
		if($cr eq "-b"){@b=split(",",$v);$g2_defined=1;next;}
		if($cr eq "-minNVal"){$minNVal=$v;next;}
		if($cr eq "-minNValb"){$minNValb=$v;next;}
		if($cr eq "-paired"){$paired=1;$i--;next;}
		if($cr eq "-IRimbalglob"){$irimbalglob=$v;next;}
		if($cr eq "-IRimbalab"){$irimbalab=$v;next;}
		if($cr eq "-gtf"){$gtf=$v; if(!defined($gtf)){die "Found argument -gtf but no GTF file given."} next;}
		if($cr eq "-f"){$gid_field=$v;next;}

		die "get_vast: argument (criterion) $cr unknown.\n";
	}
}else{
	$complex="all";  # no restriction on COMPLEX
	@scfds=();       # no restriction on SCAFFOLD
	$strand="";	 # no restriction on STRAND	
}

my ($fh,$col_ns_aref,$col_ids_aref,$comments);
my @cols_output=();

if($mode eq "normal"){
	
	if($paired && @a!=@b){die "When argument -paired given, the same number of samples must be defined by -a and -b.\n"}
	if(defined($minNVal) && (!is_numeric($minNVal) || $minNVal<0)){die "Number given with argument -minNVal must be numeric and >= 0.\n"}
	if(defined($minNValb) && (!is_numeric($minNValb) || $minNValb<0)){die "Number given with argument -minNValb must be numeric and >= 0.\n"}
	
	unless($minNVal){if($minNValb){die "-minNValb given but -minNVala not given. If -minNValb is given then -minNVal must be specified too.\n"}}
	# set minNValb to minNVal if minNValb not given but minNVal is given
	unless($minNValb){if($minNVal){$minNValb=$minNVal}}
	
	# input file looks like this
	#GENE    EVENT   COORD   LENGTH  FullCO  COMPLEX NTsi_r1 NTsi_r1-Q       SF3B1_r1        SF3B1_r1-Q
	#TSPAN6  HsaEX0067680    chrX:99885756-99885863  108     chrX:99887482,99885756-99885863,99884983        S       94.90   SOK,SOK,SOK,OK,S@2214.02,118.98 97.26   SOK,SOK,SOK,OK,S@2453.87,69.13
	my @col_ns_to_check=("GENE","EVENT","COORD","LENGTH","FullCO","COMPLEX");
	($fh,$col_ns_aref,$col_ids_aref,$comments)=open_fct_file($fn,[@col_ns_to_check],$MYCOMMAND);
	for(my $i=0;$i<@$col_ids_aref;$i++){if($col_ids_aref->[$i]!=$i){die "$MYCOMMAND: given table $fn does not look like vast-tools output (check column order!).";}}

	my $check=0;
	if(@a>0){
		for(my $i=0;$i<@a;$i++){
			my $found=0;
			for(my $j=0;$j<@{$col_ns_aref};$j++){
				if($col_ns_aref->[$j] eq $a[$i]){if($g1_ids{$j}){die "Found sample name $a[$i] more than once. Check -a argument.\n"} $g1_ids{$j}=$i; $found=1;last;}  # we remember order of given samples for -paired
			}
			unless($found){die "Cannot find column $a[$i] as given by -a option in file $fn.";}
		}
		$check++;	
	}

	if(@b>0){
		$check++;
		for(my $i=0;$i<@b;$i++){
			my $found=0;
			for(my $j=0;$i<@{$col_ns_aref};$j++){
				if($col_ns_aref->[$j] eq $b[$i]){if($g2_ids{$j}){die "Found sample name $b[$i] more than once. Check -b arguments.\n"} $g2_ids{$j}=$i;$found=1;last;}				
			}
			unless($found){die "Cannot find column (given by -b) with name $b[$i] in file $fn.";}
		}
	}
	
	# output columns
	# GENE  EVENT   COORD   LENGTH  FullCO  COMPLEX PSI_NTsi_r1   INCLREADSavg_NTsi_r1  EXCLREADSavg_NTsi_r1 PSI_SF3B1_r1 INCLREADSavg_SF3B1_r1  EXCLREADSavg_SF3B1_r1   SCAFFOLD  START   END  STRAND
	@cols_output=();
	for(my $i=0;$i<6;$i++){push(@cols_output,$col_ns_aref->[$i]);}
	# only if we have at least two more columns
	if(@{$col_ns_aref}>7){
		for(my $i=6;$i<@{$col_ns_aref};$i+=2){
			push(@cols_output,"PSI_".$col_ns_aref->[$i]);          
			push(@cols_output,"Q_".$col_ns_aref->[$i]); 
			push(@cols_output,"INCLREADSavg_".$col_ns_aref->[$i]); 
			push(@cols_output,"EXCLREADSavg_".$col_ns_aref->[$i]);
		}
	}else{
		# if there is one more column, keep it
		if(@{$col_ns_aref}==7){
			push(@cols_output,$col_ns_aref->[6]);
		}
	}
}else{
	# mode reduced
	my @col_ns_to_check=@ARGV[1..4];  # COOD, FullCO, COMPLEX, LENGTH
	($fh,$col_ns_aref,$col_ids_aref,$comments)=open_fct_file($fn,[@col_ns_to_check],$MYCOMMAND);
	@cols_output=(); foreach my $coln (@{$col_ns_aref}){push(@cols_output,$coln);}
	
	for(my $i=5; $i<@ARGV;$i+=2){

		my $cr=$ARGV[$i];
		my $v=$ARGV[$i+1];
		
		if($cr eq "-gtf"){$gtf=$v;next;}
		if($cr eq "-f"){$gid_field=$v;next;}

		die "get_vast: argument (criterion) $cr unknown.";
	}
}

# check if gtf file can be opened
if($gtf ne ""){open(my $fh2,$gtf) or die "$gtf: $!"; close($fh2);}


push(@cols_output,"SCAFFOLD","START","END","STRAND","UPSTRM_EX_BORDER","DOSTRM_EX_BORDER");

if($mode eq "normal"){
		push(@cols_output,"MINPSI_ALL","MAXPSI_ALL","PSIRANGE_All","MINPSI_GRPA","MAXPSI_GRPA","PSIRANGE_GRPA","AVGPSI_GRPA","MEDIANPSI_GRPA","MINPSI_GRPB","MAXPSI_GRPB","PSIRANGE_GRPB","AVGPSI_GRPB","MEDIANPSI_GRPB","DPSI_GRPA_MINUS_GRPB","PSIMARGIN_BETWEEN_GRPA_GRPB","PVAL_WILCOX_GRPA_VS_GRPB","GENEID")
}


# create temporary file; will be deleted automatically at the end of script or if error occurs
my ($fh_tmp,$fn_tmp) = tempfile( DIR => ".", UNLINK => 1);


my ($fhw1_tmp,$fnw1_tmp); # fhw1: input for wilcoxon test
if($g2_defined){($fhw1_tmp,$fnw1_tmp) = tempfile( DIR => ".", UNLINK => 1);}

print $fh_tmp join("\t",@cols_output)."\n";

my (@fs,@fs2,$found);
my ($scfd,$start,$end,$my_strand,$upstrm_ex_border,$dostrm_ex_border);
my ($margin,$dpsi);
# go over vast-out file
ASE:
  while(<$fh>){
	$_=clean_line($_);
	if(length($_)==0 || substr($_,0,1) eq "#"){next;}
	my @fs=split_line($_);

    	if($mode eq "normal"){
    		@cols_output=@fs[0..5];
    	}else{
		@cols_output=@fs;
		@fs=("NA","NA",$fs[ $col_ids_aref->[0] ],$fs[ $col_ids_aref->[3] ],$fs[ $col_ids_aref->[1] ],$fs[ $col_ids_aref->[2] ]);  # we don't have GENE nor EVENT nor LENGTH in this mode
    	}

	# check complex
	if($complex ne "all"){
		$found=0;
		foreach my $cmplx (@complexes){if($fs[5] eq $cmplx){$found=1;last;}}
		unless($found){next ASE;}
	}

	@fs2=split(/[:-]/,$fs[2]);
	$scfd=$fs2[0];
	$start=$fs2[1];
	$end=$fs2[2];
	# check scaffold
	if(@scfds>0){
		$found=0;
		foreach my $scf (@scfds){if($scf eq $scfd){$found=1;last;}}
		unless($found){next ASE;}
	}

	@fs2=split(/[:\+,=-]/,$fs[4]);
	# correct for exceptional cases
	# all IR-S and IR-C are exceptions chr22:17128058-17128147=17128553-17128675:+  
	if($fs2[@fs2-1] eq ""){$fs2[@fs2-1]=$fs2[@fs2-2];}
	# another exception  chr1:-55181692+55181816,55182273
	if($fs2[1] eq ""){$fs2[1]=$fs2[2];}


	if($fs2[1]>$fs2[@fs2-1]){$my_strand="-";
	}else{$my_strand="+";}
	# check strand
	if($strand ne ""){
		if($strand eq "+" && $my_strand eq "-"){next ASE;}
		if($strand eq "-" && $my_strand eq "+"){next ASE;}
	}

	$upstrm_ex_border="NA"; $dostrm_ex_border="NA";
	if($fs[5] =~ /IR/i){
		# ___|A--ex1--B|___retained_intron___|C--ex2--D|___  -> A and D get upstrm_ex_border and dostrm_ex_border
		@fs2=split(/[:=\\-]/,$fs[4]);  #fs2[1..4] ex1_start ex1_end ex2_start ex2_end
			if($my_strand eq "+"){
				$upstrm_ex_border=$fs2[1];
				$dostrm_ex_border=$fs2[4];
			}else{
				$upstrm_ex_border=$fs2[2];
				$dostrm_ex_border=$fs2[3];
			}
	}elsif($fs[5] =~ /Alt3/i || $fs[5] =~ /Alt5/i){
		@fs2=split(/[:,\\-]/,$fs[4]);
			if($fs[5] =~ /Alt5/i){
				if($my_strand eq "+"){
					$upstrm_ex_border=$fs2[1];
					$dostrm_ex_border=$fs2[@fs2-1];
				}else{
					$upstrm_ex_border=$fs2[@fs2-2];
					$dostrm_ex_border=$fs2[@fs2-1];
				}
			}
			if($fs[5] =~ /Alt3/i){
				if($my_strand eq "+"){
					$upstrm_ex_border=$fs2[1];
					$dostrm_ex_border=$fs2[@fs2-1];
				}else{
					$upstrm_ex_border=$fs2[2];
					$dostrm_ex_border=$fs2[1];
				}
			}
			if($dostrm_ex_border eq ""){$dostrm_ex_border="NA"}
			if($upstrm_ex_border eq ""){$upstrm_ex_border="NA"}
			
			if($dostrm_ex_border =~ /\+/){
				my @fs2=split("\\+",$dostrm_ex_border);
				if($my_strand eq "+"){
					$dostrm_ex_border=$fs2[0]; foreach my $tmpval (@fs2){if($tmpval>$dostrm_ex_border){$dostrm_ex_border=$tmpval}}
				}else{
					$dostrm_ex_border=$fs2[0]; foreach my $tmpval (@fs2){if($tmpval<$dostrm_ex_border){$dostrm_ex_border=$tmpval}}
				}
			}
			
			if($upstrm_ex_border =~ /\+/){
				my @fs2=split("\\+",$upstrm_ex_border);
				if($my_strand eq "+"){
					$dostrm_ex_border=$fs2[0]; foreach my $tmpval (@fs2){if($tmpval>$dostrm_ex_border){$dostrm_ex_border=$tmpval}}
				}else{
					$dostrm_ex_border=$fs2[0]; foreach my $tmpval (@fs2){if($tmpval<$dostrm_ex_border){$dostrm_ex_border=$tmpval}}
				}
			}	
	}else{  # for skipped exons + mics and retained introns, get next up-/down-stream exon borders
		@fs2=split(/[:,]/,$fs[4]);
		my @fs3=split(/[+]/,$fs2[1]);
		if($my_strand eq "+"){
			$upstrm_ex_border=get_min_from_aref(\@fs3,"ignoreSTRINGS"); # STRINGignore for rare cases where FullCO does not agree with its standard format
		}else{
			$upstrm_ex_border=get_max_from_aref(\@fs3,"ignoreSTRINGS"); # chr1:1685983,1685782+1685642+1685723-1685822,+1685088+1685647
		}
		@fs3=split(/[+]/,$fs2[3]);
		if($my_strand eq "+"){
			$dostrm_ex_border=get_max_from_aref(\@fs3,"ignoreSTRINGS"); # STRINGignore for rare cases where FullCO does not agree with its standard format
		}else{
			$dostrm_ex_border=get_min_from_aref(\@fs3,"ignoreSTRINGS"); # chr1:1685983,1685782+1685642+1685723-1685822,+1685088+1685647
		}
	}

	# recompute start / end for Alt3/Alt5 ASEs
	if($fs[5] =~ /Alt3/i || $fs[5] =~ /Alt5/i){
		@fs2=split(/[:,-]/,$fs[4]);
		if($fs[5] =~ /Alt3/i){
			if(@fs2==3){
					@fs2=split("\\+",$fs2[2]);
			}else{
				if(length($fs2[2])>length($fs2[3])){
					@fs2=split("\\+",$fs2[2]);
				}else{
					@fs2=split("\\+",$fs2[3]);
				}
			}
		}
		if($fs[5] =~ /Alt5/i){
			if(length($fs2[1])>length($fs2[2])){
				@fs2=split("\\+",$fs2[1]);
			}else{
				@fs2=split("\\+",$fs2[2]);
			}
		}

		$start=get_min_from_aref(\@fs2,"ignoreSTRINGS");
		$end=$start+$fs[3];
	}

	if($mode ne "normal"){
		push(@cols_output,$scfd,$start,$end,$my_strand,$upstrm_ex_border,$dostrm_ex_border);
	}else{
	
		my @g1_psis=(); my $g1_n=0;
		my @g2_psis=(); my $g2_n=0;
		my @all_psis=();my $all_n=0;
		my @g1_g2_diffs=();
		
		# if we have more than the first 6 columns GENE	EVENT	COORD	LENGTH	FullCO	COMPLEX	SCAFFOLD	START	END	STRAND
		if(@fs>7){
    			for(my $i=6;$i<@fs;$i+=2){
    				# quality string SOK,SOK,SOK,OK,S@2214.02,118.98  or for IR events: N,OK,NA,0=7=7,0.0949@4.1,25.38
        			@fs2=split(/[,@]/,$fs[$i+1]);
	        		# psi incl_reads_avg excl_reads_avg    # for all samples; even those not selected with -a -b
    	    			push(@cols_output,$fs[$i],$fs2[0],$fs2[@fs2-2],$fs2[@fs2-1]);

				my $psi_1=undef;  my $psi_2=undef;  my $psi_all=$fs[$i];  $all_n++;
				if(defined($g1_ids{$i})){$psi_1=$fs[$i]; $g1_n++;}
				if(defined($g2_ids{$i})){$psi_2=$fs[$i]; $g2_n++;}

				# minq checks
				if(defined($psi_all) && !defined($qs{$fs2[0]}) || $qs{$fs2[0]}<$qs{$minqglob}){$psi_all="NA";}
				# this is one of the -a -b selected samples -> check for minqab
				if(defined($psi_1) && (!defined($qs{$fs2[0]}) || $qs{$fs2[0]}<$qs{$minqab})){$psi_1="NA"}
				if(defined($psi_2) && (!defined($qs{$fs2[0]}) || $qs{$fs2[0]}<$qs{$minqab})){$psi_2="NA"} 

				# check p_IR filter (p value of binomial test for IR testing how imbalanced are the read couts at the two ends of the intron)
				if($fs[5] =~ /IR/i){
					if(defined($psi_all) && (!defined($fs2[@fs2-3]) || $fs2[@fs2-3] eq "" || $fs2[@fs2-3] eq "NA" || $fs2[@fs2-3]<$irimbalglob)){$psi_all="NA"}
					if(defined($psi_1) && (!defined($fs2[@fs2-3]) || $fs2[@fs2-3] eq "" || $fs2[@fs2-3] eq "NA" || $fs2[@fs2-3]<$irimbalab)){$psi_1="NA"}
					if(defined($psi_2) && (!defined($fs2[@fs2-3]) || $fs2[@fs2-3] eq "" || $fs2[@fs2-3] eq "NA" || $fs2[@fs2-3]<$irimbalab)){$psi_2="NA"}
				}

				# get psi value
				if(defined($psi_all)){push(@all_psis,$psi_all)}
				if(defined($psi_1)){$g1_psis[$g1_ids{$i}]=$psi_1}
				if(defined($psi_2)){$g2_psis[$g2_ids{$i}]=$psi_2}  
    			}

			# remove all NAs from PSIs
			@all_psis=remove_NA_from_array(\@all_psis);

			if($paired){
				for(my $j=0;$j<@g1_psis;$j++){
					if($g1_psis[$j] eq "NA" || $g2_psis[$j] eq "NA"){next}
					push(@g1_g2_diffs,$g1_psis[$j]-$g2_psis[$j])
				}
			}

			@g1_psis=remove_NA_from_array(\@g1_psis);
			@g2_psis=remove_NA_from_array(\@g2_psis);
		}

		# if there is one more column after the first 5
		if(@{$col_ns_aref}==7){
			push(@cols_output,$fs[6]);
		}

		push(@cols_output,$scfd,$start,$end,$my_strand,$upstrm_ex_border,$dostrm_ex_border);
		
		# "MINPSI_ALL","MAXPSI_ALL","PSIRANGE_All","MINPSI_GRPA","MAXPSI_GRPA","PSIRANGE_GRPA","MINPSI_GRPB","MAXPSI_GRPB","PSIRANGE_GRPB","MINPSI_REST","MAXPSI_REST","PSIRANGE_REST","DPSI_GRPA_MINUS_GRPB","PSIMARGIN_BETWEEN_GRPA_GRPB"
		my $vtmp1="NA"; my $vtmp2="NA"; my $vtmp3="NA";
		if(!defined($minNVal) && @all_psis==$all_n || defined($minNVal) && @all_psis>=$minNVal){($vtmp1,$vtmp2)=get_min_max_from_aref(\@all_psis); $vtmp3=$vtmp2-$vtmp1;}
		push(@cols_output,$vtmp1,$vtmp2,$vtmp3);
	
		my $vtmp11="NA"; my $vtmp12="NA"; $vtmp3="NA"; my $vtmp4="NA"; my $vtmp5="NA";
		if($g1_defined){
			if(!defined($minNVal) && @g1_psis==$g1_n || defined($minNVal) && @g1_psis>=$minNVal){($vtmp11,$vtmp12)=get_min_max_from_aref(\@g1_psis); $vtmp3=$vtmp12-$vtmp11; $vtmp4=get_mean_from_aref(\@g1_psis); $vtmp5=get_median_from_aref(\@g1_psis)}
		}
		push(@cols_output,$vtmp11,$vtmp12,$vtmp3,$vtmp4,$vtmp5);

		my $vtmp21="NA"; my $vtmp22="NA"; $vtmp3="NA"; $vtmp4="NA"; $vtmp5="NA";
		if($g2_defined){
			if(!defined($minNValb) && @g2_psis==$g2_n || defined($minNValb) && @g2_psis>=$minNValb){($vtmp21,$vtmp22)=get_min_max_from_aref(\@g2_psis); $vtmp3=$vtmp22-$vtmp21; $vtmp4=get_mean_from_aref(\@g2_psis); $vtmp5=get_median_from_aref(\@g2_psis)}
		}
		push(@cols_output,$vtmp21,$vtmp22,$vtmp3,$vtmp4,$vtmp5);

		#"DPSI_GRPA_MINUS_GRPB","PSIMARGIN_BETWEEN_GRPA_GRPB"
		if($vtmp11 eq "NA" || $vtmp12 eq "NA" || $vtmp21 eq "NA" || $vtmp22 eq "NA"){
			push(@cols_output,"NA","NA");
		}else{
			$margin="NA"; $dpsi="NA";
			unless($paired){
				$margin=max($vtmp11-$vtmp22 , $vtmp21-$vtmp12);
				if($margin<0){$margin=0;} # if two PSI intervals are overlapping, it can happen, that margin gets negative -> in this case margin is set to 0
				$dpsi=get_mean_from_aref(\@g1_psis)-get_mean_from_aref(\@g2_psis);
			}else{
				if(@g1_g2_diffs>0){
					$margin=200;
					$dpsi=0;
					my $sign_has_changed=0;
					for(my $j=0;$j<@g1_g2_diffs;$j++){
						$dpsi+=$g1_g2_diffs[$j];
						if(abs($g1_g2_diffs[$j])<$margin){$margin=abs($g1_g2_diffs[$j])}
						if($j>0 && (($g1_g2_diffs[$j-1]<0 && $g1_g2_diffs[$j]>=0) || ($g1_g2_diffs[$j-1]>=0 && $g1_g2_diffs[$j]<0))){$sign_has_changed=1;}
					}
					$dpsi/=@g1_g2_diffs;
					if($sign_has_changed){$margin=0;}
				}
			}
			push(@cols_output,$dpsi,$margin);
		}
		
		push(@cols_output,"NA","NA"); # p value of Wilcox test and gene id
		
		if($g2_defined){
			if(!defined($minNValb) && @g2_psis==$g2_n && !defined($minNVal) && @g1_psis==$g1_n || defined($minNValb) &&  @g2_psis>=$minNValb &&  defined($minNVal) && @g1_psis>=$minNVal){
				print $fhw1_tmp "".scalar(@g1_psis).",".join(",",@g1_psis),",",join(",",@g2_psis)."\n";
			}else{
				print $fhw1_tmp "NA\n";
			}
		}
	}# if mode eq "normal"
    print $fh_tmp join("\t",@cols_output)."\n";
}

close($fh);
close($fh_tmp);
if($g2_defined){close($fhw1_tmp);}

my $cmd;
my @pvals;
if($g2_defined){
	$cmd="$scr_abspath/../utilities/wilcoxon_test.r $fnw1_tmp";
	@pvals=`$cmd`; if($?!=0){die "$!"}
	chomp(@pvals);
}


my @gids=();
if($gtf ne ""){
	$cmd="matt retr_geneids $fn_tmp START END SCAFFOLD STRAND $gtf -f $gid_field -h GENE -m";
	@gids=`$cmd`; die "$!" if $?!=0;
	chomp(@gids);
}


open($fh_tmp,$fn_tmp) or die "$!";
my $i=-1;
while(<$fh_tmp>){if(@gids==0 && @pvals==0){print "$_"; next} chomp; my @fs=split("\t"); $i++;
	
	if(@pvals!=0){
		$fs[$#fs-1]=$pvals[$i];
	}

	if(@gids!=0 && $mode eq "normal"){
		$fs[$#fs]=$gids[$i];
	}

	if(@gids!=0 && $mode eq "reduced"){
		$fs[$#fs+1]=$gids[$i];
	}
		
	print join("\t",@fs)."\n";
}
close($fh_tmp);


exit(0); # success
__END__
