# September 20, 2017

# download complete SQL tables from CISBP-RNA
# wget http://cisbp-rna.ccbr.utoronto.ca/data/0.6/DataFiles/SQLDumps/SQLArchive_cisbp-rna_0.6.zip

# unzip SQLArchive_cisbp-rna_0.6.zip

# unzip motifs.zip

# unzip tfs.zip


echo "
%h=(
'A' => 'A',
'C' => 'C',
'G' => 'G',
'T' => 'T',
'U' => 'T',
'R' => '[A|G]',
'Y' => '[C|T]',
'S' => '[G|C]',
'W' => '[A|T]',
'K' => '[G|T]',
'M' => '[A|C]',
'B' => '[C|G|T]',
'D' => '[A|G|T]',
'H' => '[A|C|T]',
'V' => '[A|C|G]',
'N' => '.',
'.' => '.',
'-' => '.'
);

sub toREGEXP{
	\$str=uc(\$_[0]);
	@lets=split(\"\",\$str);
	for(\$i=0;\$i<@lets;\$i++){
		\$lets[\$i]=\$h{\$lets[\$i]};
	}
return(join(\"\",@lets));
}


%names=();
open(\$fh,\"tfs.sql\");
while(<\$fh>){chomp;
@fs=split(\",\");
if(@fs==7 && \$fs[0]=~ /T....._0\.6/){
\$fs[0]=~s/\(//g;
\$fs[0]=~s/\)//g;
\$fs[0]=~s/'//g;
\$fs[0]=~s/\;//g;
\$fs[4]=~s/\(//g;
\$fs[4]=~s/\)//g;
\$fs[4]=~s/'//g;
\$fs[4]=~s/\;//g;
\$fs[4]=~s/_.*$//g;
\$fs[4]=~s/\..*$//g;
\$fs[4]=~s/\s+//g;
if(length(\$fs[4])<2){\$fs[4]=\"\"}
\$names{\$fs[0]}=uc(\$fs[4]);
}}
close(\$fh);

%out=();
open(\$fh,\"motifs.sql\");
while(<\$fh>){chomp;
@fs=split(\",\");
if(@fs==7 && \$fs[0]=~ /M..._0\.6/){
\$fs[0]=~s/\(//g;
\$fs[0]=~s/\)//g;
\$fs[0]=~s/'//g;
\$fs[0]=~s/\;//g;
\$fs[0]=~s/_0\.6//g;
\$fs[1]=~s/\(//g;
\$fs[1]=~s/\)//g;
\$fs[1]=~s/'//g;
\$fs[1]=~s/\;//g;
\$fs[1]=~s/\s+//g;
\$fs[6]=~s/\(//g;
\$fs[6]=~s/\)//g;
\$fs[6]=~s/'//g;
\$fs[6]=~s/\;//g;
if(length(\$names{\$fs[1]})==0){
	\$out{\$fs[6].\$names{\$fs[1]}}=\"\$fs[0]\t\$fs[6]\t\".toREGEXP(\$fs[6]).\"\tNA\tCISBP-RNA_\".\$fs[0].\"\";
}else{
	\$out{\$fs[6].\$names{\$fs[1]}}=\"\$fs[0]\t\$fs[6]\t\".toREGEXP(\$fs[6]).\"\t\".\$names{\$fs[1]}.\"\t\".\$names{\$fs[1]}.\"_CISBP-RNA_\".\$fs[0].\"\";
}
}}
close(\$fh);

print \"MOTIF_ID\tIUPAC\tREGEXP\tNAME_RBP\tNAME\tTYPE\tTHRESH\tBGMODEL\n\";
foreach \$key (keys %out){print \$out{\$key}.\"\tREGEXP\tNA\tNA\n\";}

"  > run.pl

perl ./run.pl > cisbp_iupac.tab
