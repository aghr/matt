use List::MoreUtils 'first_index';

### needs to be specified
$d=$ARGV[0];
#$d="entiredata_2016_12_09_8_46_am";

open($fh,$d."/RBP_Information_all_motifs.txt") or die "$!\n";
$header=<$fh>;
@header=split("\t",$header);
$pwmid_id = first_index { /Motif_ID/ } @header;
$rbpn_id = first_index { /RBP_Name/ } @header;
$species_id = first_index { /RBP_Species/ } @header;


%pwms;
%all_species;
%most_freq_pname;
while(<$fh>){
	chomp;
	@fs=split("\t",$_,-1);
	$pwm_id=$fs[$pwmid_id];
	$spec=$fs[$species_id];
	$all_species{$spec}=1;
	$all_species{"MOST_FREQ_GENE"}=1;
	$pname=lc($fs[$rbpn_id]);

	$path="$d/pwms/$pwm_id.txt";
	if(-f $path && -s $path > 0){
		unless($pwms{$pwm_id}){$pwms{$pwm_id}={};}
		unless($pwms{$pwm_id}->{$spec}){$pwms{$pwm_id}->{$spec}={};}
		$pwms{$pwm_id}->{$spec}->{$pname}=1;
		$spec="MOST_FREQ_GENE";
		unless($pwms{$pwm_id}->{$spec}){$pwms{$pwm_id}->{$spec}={};}
		$pwms{$pwm_id}->{$spec}->{$pname}++;
	}
}
close($fh);

# get kmers from PWMs for all RBPs and write all this into a table
$append=0;
open($out,">cisbprna_info.tab") or die "$!";
open ($log,">log_threshold_extraction_for_all_pwms.txt")  or die "$!";
#open ($log,'>&', STDOUT );

@pwms=keys %pwms;
@idx = sort { $pwms[$a] cmp $pwms[$b] } 0 .. $#pwms;
@pwms=@pwms[@idx];

mkdir("matt_pwms");

@all_spec= sort keys(%all_species);
# these species should be the first
@specs=qw(MOST_FREQ_GENE Homo_sapiens Mus_musculus Danio_rerio Bos_taurus Drosophila_melanogaster Rattus_norvegicus Gallus_gallus);
for(my $i=0;$i<@specs;$i++){
	my $id=first_index { /$specs[$i]/ } @all_spec;
	my $tmp=$all_spec[$i];
	$all_spec[$i]=$specs[$i];
	$all_spec[$id]=$tmp;
}
@all_spec_2=("MOST_FREQ_GENE");
for(my $i=1;$i<@all_spec;$i++){
	$all_spec_2[$i]="GENES_IN_".$all_spec[$i];
}

# take only the first 8 values
@all_spec=@all_spec[0..7];
@all_spec_2=@all_spec_2[0..7];


print $out "TYPE\tID\tMOTIF_LENGTH\tLOGPWMSCORE_THRESHOLD\tPERCENT_BINDINGENERGY_REQUESTED\tPERCENT_BINDINGENERGY_REACHED\tDELTA_PERCENT_BINDINGENERGY\tNKMERS_FOR_THRESHOLD\tMOST_LIKELY_KMER\tFILE_REGEXP\t".join("\t",@all_spec_2)."\n";
print "TYPE\tID\tMOTIF_LENGTH\tLOGPWMSCORE_THRESHOLD\tPERCENT_BINDINGENERGY_REQUESTED\tPERCENT_BINDINGENERGY_REACHED\tDELTA_PERCENT_BINDINGENERGY\tNKMERS_FOR_THRESHOLD\tMOST_LIKELY_KMER\tFILE_REGEXP\t".join("\t",@all_spec_2)."\n";

for($i=0;$i<@pwms;$i++){
		sleep 0.5;

		$pwm_id=$pwms[$i];
		$pwm_name=$pwms[$i];
		$pwm_name=~ s/_.*$//g;
		$file="matt_pwms/$pwm_id.tab";
		# read in PWM, set min prob to 0.001 and redefine alphabet from A,C,G,U to A,C,G,T
		$cmd="matt get_pwm $d/pwms/$pwms[$i].txt 0.001 A,C,G,T > $file";
		print $log "$cmd\n";
		print $log `$cmd`;

		# adapt p for PWMs with L>=10
		open($fh,"$file") or die "$!";
		$header=<$fh>; # SYMBOL	POS_1	POS_2	POS_3	POS_4	POS_5	POS_6	POS_7
		@fs=split("\t",$header);
		$motif_length=scalar(@fs)-1;

		@pnames=();
		foreach $spec (@all_spec){
			if($spec eq "MOST_FREQ_GENE"){
				my @tmp_ns=keys(%{$pwms{$pwm_id}->{$spec}});
				my @tmp_vs=values(%{$pwms{$pwm_id}->{$spec}});
				@idx = sort { $tmp_vs[$b] cmp $tmp_vs[$a] } 0 .. $#tmp_vs;
				push(@pnames, $tmp_ns[$idx[0]] );				
			}else{
				push(@pnames, join(",",sort(keys(%{$pwms{$pwm_id}->{$spec}}))) );
			}
		}
		
		
		if($motif_length>18){
			print "pwm\t$pwm_name\t$motif_length\tskipped\t$p\tskipped\tskipped\tskipped\tskipped\t$file\t".join("\t",@pnames)."\n";
			print $out "pwm\t$pwm_name\t$motif_length\tskipped\t$p\tskipped\tskipped\tskipped\tskipped\t$file\t".join("\t",@pnames)."\n";
			next;
		}

		if($motif_length>10){$p=0.75;print $log "length PWM=$motif_length; p set to $p\n";}else{$p=0.75;print $log "length PWM=$motif_length; p set to $p\n";}
		close($fh);

		if($append){
			$cmd="matt get_kmers_from_pwm $file $p 10 $pwm_name";
			
		}else{
			$cmd="matt get_kmers_from_pwm $file $p 10 $pwm_name";
			$append=1;
		}
		print $log "$cmd\n";
		$output=`$cmd`;

		@fs=split("\n",$output);
		@fs2=split("\t",$fs[1]);
		$mostlikelykmer=$fs2[1];
		@fs2=split("\t",$fs[@fs-1]);
		
		print $out "pwm\t$pwm_name\t$motif_length\t$fs2[2]\t$p\t$fs2[3]\t".($fs2[3]-$p)."\t".(scalar(@fs)-1)."\t".$mostlikelykmer."\t$file\t".join("\t",@pnames)."\n";
		print "pwm\t$pwm_name\t$motif_length\t$fs2[2]\t$p\t$fs2[3]\t".($fs2[3]-$p)."\t".(scalar(@fs)-1)."\t".$mostlikelykmer."\t$file\t".join("\t",@pnames)."\n";
}


# add kmers handish
@fs=("regexp","R001","NA","NA","NA","NA","NA","NA","NA","[CT]CA[CT](.{0,3}[CT]{0,1}CA[CT]|.{4,23}[CT]CA[CT]){2}","nova","nova","nova","nova","","","","");
print $out join("\t",@fs)."\n";

close($log);
close($out);
