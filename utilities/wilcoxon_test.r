#!/usr/bin/env Rscript
args <- commandArgs(trailingOnly = TRUE)
fileIn<-args[1]  # one Wilcoxon test per line; lines contains different number of numbers; always: <N_VALS_GRP1>,V1_GRP1,V2_GRP1,...,VN_GRP1,V1_GRP2,V2_GRP2,...


cat("PSI_GRPA\tPSI_GRPB\tPVAL_WILCOXON_GRPA_VS_GRPB\n")

vs<-lapply(strsplit(readLines(fileIn),","),function(x){
    if(x[1]=="NA"){return(cat("NA\tNA\tNA\n"))}
    x<-as.numeric(x)
    g1<-NA
    if(x[1]>0){g1<-x[2:(x[1]+1)]}
    g2<-NA
    if(length(x)-1>x[1]){
      if(x[1]>0){
	g2<-x[(x[1]+2):length(x)]
      }else{
	g2<-x[(x[1]+3):length(x)]
      }
    }
    pval<-NA
    if(!is.na(g1) && !is.na(g2)){
      pval<-wilcox.test(g1,g2, alternative = "two.sided", na.action="na.omit",exact=FALSE)$p.value    # exact=FALSE because we have ties in our
      if(is.nan(pval)){pval<-"NA"}
    }
    cat(paste(paste(g1,collapse=","),"\t",paste(g2,collapse=","),"\t",pval,"\n",sep="",collapse=""))
  })
