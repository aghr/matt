#!/usr/bin/env Rscript
args <- commandArgs(trailingOnly = TRUE) # dir input_efeature_table 
cols<-strsplit(args[1],",")[[1]]

areColors <- function(x) {
      sapply(x, function(X) {
          tryCatch(is.matrix(col2rgb(X)), 
                   error = function(e) FALSE)
          })
}

cat( sum(areColors(cols))==length(cols) )

quit(status=0)  # success
