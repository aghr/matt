#!/usr/bin/env perl
use strict;
use warnings;

# for a given alphabet and length,
# this script produces feature-column tables
# decribing PWMs which describe these kmers
# e.g. for A,B,C and length=3 and ACB a feature-column
# table looks like:
#symbol \t pos1 \t pos2 \t pos3
#A \t 1 \t 0 \t 0
#B \t 0 \t 0 \t 1
#C \t 0 \t 1 \t 0

# comma separated string
my @symbols=split(",",$ARGV[0]);
my $len = $ARGV[1];

my @fs=("SYMBOL");
for(my $i=0;$i<$len;$i++){push(@fs,"POS_".($i+1));}
my $header=join("\t",@fs);




construct_and_write_kmer_pwms(0,$len,\@symbols,[],[],$header);



sub construct_and_write_kmer_pwms {
	# depth (start at 0)
	my $d=$_[0];
	# requested kmer length
	my $l=$_[1];
	# reference to array containing symbols
	my $syms_aref=$_[2];
	# reference to array containing as entries columns of feature-column table
	my $cols_aref=$_[3];
	# reference to array of so far added symbols
	my $syms_added_aref=$_[4];
	# header string
	my $h=$_[5];
	
	# break recursion
	if($d==$l+1){
		# output
		my $kmer=join("",@$syms_added_aref);
		open(my $fh,">"."$kmer"."_pwm.fct");
		print $fh "# feature-column table describing the kmer $kmer as Position Weight Matrix. Probabilities are logarithmized with base e.\n";
		print $fh $h."\n";
		my $fields_aref;
		for(my $row=0;$row<@{$cols_aref->[0]};$row++){
			$fields_aref=[];
			for(my $col=0;$col<@{$cols_aref};$col++){
				push(@{$fields_aref},$cols_aref->[$col]->[$row]);
			}
			print $fh join("\t",@$fields_aref)."\n";
		}
		close($fh);
	return();
	}
	
	# add next column
	if($d==0){
		# add SYMBOLS column
		$cols_aref->[$d]=$syms_aref;
		construct_and_write_kmer_pwms($d+1,$l,$syms_aref,$cols_aref,$syms_added_aref,$h);
	}else{
		my $new_col_aref;
		foreach my $sym (@$syms_aref){
			$new_col_aref=[];
			foreach my $sym2 (@$syms_aref){
				if($sym eq $sym2){
					push(@{$new_col_aref},log(1));
					$syms_added_aref->[$d-1]=$sym;
				}else{
					# as log(0) cannot be computed we set this value here to -inf
					push(@{$new_col_aref},-inf);
				}
			}
			$cols_aref->[$d]=$new_col_aref;
			construct_and_write_kmer_pwms($d+1,$l,$syms_aref,$cols_aref,$syms_added_aref,$h);
		}
	}
	
return();
}


exit(0);  # success