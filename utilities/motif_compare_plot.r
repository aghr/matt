#!/usr/bin/env Rscript
args <- commandArgs(trailingOnly = TRUE)


getScriptPath <- function(){
    cmd.args <- commandArgs()
    m <- regexpr("(?<=^--file=).+", cmd.args, perl=TRUE)
    script.dir <- dirname(regmatches(cmd.args, m))
    if(length(script.dir) == 0) stop("can't determine script dir: please call the script with Rscript")
    if(length(script.dir) > 1) stop("can't determine script dir: more than one '--file' argument detected")
    return(script.dir)
}

# read Utilities.r only if it not yet have been called
if(!exists("motif_compare_plot")){
  my.path<-getScriptPath()
  source(paste(my.path,"/Utilities.r",sep="",collapse=""))
}


########
## main
########

motif_compare_plot(args[1],args[2],args[3],args[4],args[5])

quit(status=0)  # success
