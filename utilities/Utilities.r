#rm(list=ls())

# reads PWMs from file in this format:
## Nucleic Acids Res. 2011 Mar; 39(5): 1868–1879.
## Published online 2010 Nov 9. doi:  10.1093/nar/gkq1042
## PMCID: PMC3061054
## Analysis of in situ pre-mRNA targets of human splicing factor SF1 reveals a function in alternative splicing
## Margherita Corioni, Nicolas Antih, Goranka Tanackovic, Mihaela Zavolan, and Angela Krämer1
##
## from supplement
## http://nar.oxfordjournals.org/content/suppl/2010/10/21/gkq1042.DC1/Supplement_NAR_00891-A-2010.pdf
## page 13, table S2: Weight matrix of the binding specificity of SF1
#SYMBOL	POS_1	POS_2	POS_3	POS_4	POS_5	POS_6	POS_7
#A	0.272	0.780	0.174	0.051	0.502	0.755	0.148
#C	0.338	0.058	0.693	0.029	0.072	0.086	0.601
#G	0.137	0.031	0.039	0.030	0.249	0.072	0.099
#T	0.252	0.131	0.094	0.890	0.177	0.087	0.152
readPWM_matt<-function(fn){
    lines<-readLines(fn)
    row.c<-0
    for(li in lines){
      if(substr(li,1,1)=="#"){next;}
      if(substr(li,1,6)=="SYMBOL"){ #header
	header<-strsplit(li,"\t")[[1]]
	pos.nums<-as.numeric(lapply(strsplit(header[-1],"_"),function(x){x[2]}))
	pwm<-matrix(0,ncol=length(header)-1,nrow=4,dimnames=list(c("WILL","BE","SET","LATER"),pos.nums))
	next;
      }
      li<-strsplit(li,"\t")[[1]]
      row.c<-row.c+1
      pwm[row.c,]<-as.numeric(li[-1])
      rownames(pwm)[row.c]<-toupper( li[1] )
    }
    pwm<-pwm[, order(as.numeric(colnames(pwm)))]  # sort column according positons
    pwm<-pwm[order(rownames(pwm)) ,]  # sort column according positons A, C, G, T/U
return(pwm)
}



makeTransparent <- function(..., alpha=0.5) {

    if(alpha<0 | alpha>1) stop("alpha must be between 0 and 1")
    alpha = floor(255*alpha)  
    newColor = col2rgb(col=unlist(list(...)), alpha=FALSE)
    .makeTransparent = function(col, alpha) {rgb(red=col[1], green=col[2], blue=col[3], alpha=alpha, maxColorValue=255)}
    newColor = apply(newColor, 2, .makeTransparent, alpha=alpha)
  return(newColor)
}


  # partSums(1:4) = 1, 3, 6, 10
partSums<-function(vs){
    ret<-vs
    if(length(ret)>1){for(i in 2:length(ret)){ret[i]<-ret[i]+ret[i-1]}}
  ret
}


  
  
######
## plot functions
######

#rm(list=ls())
# visualizes log2( PWM1/PWM2 )
# pwm1: rows -> letters, columns -> positions
# ONLY for ACGT/U pwms
#
# pwm2: rows -> letters, columns -> positions
#
# pwm1 and pwm2 can be filenames or R matrices - need to have rownames A, C, G, T/U
#
# fn.out: output PDF file name
berryplot<-function(pwm1,name1,pwm2,name2,fn.out=NA){

    # returns the log(y) or -log(-y) if y < 0
    transform_yvalues<-function(ys){
      ids<-ys > 2
      ys[ids]<- 2+log(ys[ids])-log(2)
      
      ids<- ys>=0 & ys <= 2
      ys[ids]<- sqrt(ys[ids])* 2/sqrt(2)
      
      ids<- ys<0 & ys >= -2
      ys[ids]<- -sqrt(-ys[ids])* 2/sqrt(2)
      
      ids<-ys < -2
      ys[ids]<- -2 - log(-ys[ids])+log(2)
      
    return(ys)
    }

    if(!is.matrix(pwm1)){   # pwm1 is assumed to be a file and we read in the pwm from it
      pwm.tmp<-readPWM_matt(pwm1)
      pwm1<-pwm.tmp
      rm(pwm.tmp)
    }
    if(nrow(pwm1)!=4){stop("Given PWM1 needs to have 4 rows (nts) and as many columns as positions")}
    rownames(pwm1)<-toupper( rownames(pwm1) )
    pwm1<-pwm1[order(rownames(pwm1)) ,]  # sort column according positons A, C, G, T/U
    
    if(!is.matrix(pwm2)){   # pwm2 is assumed to be a file and we read in the pwm from it
      pwm.tmp<-readPWM_matt(pwm2)
      pwm2<-pwm.tmp
      rm(pwm.tmp)
    }
    if(nrow(pwm2)!=4){stop("Given PWM needs to have 4 rows (nts) and as many columns as positions")}
    rownames(pwm2)<-toupper( rownames(pwm2) )
    pwm2<-pwm2[order(rownames(pwm2)) ,]  # sort column according positons A, C, G, T/U
    
    # check if alphabet is identical
    if(length(which(!is.element(rownames(pwm1),rownames(pwm2))))!=0 || length(which(!is.element(rownames(pwm2),rownames(pwm1))))!=0){
      stop("Alphabets of both PWMs are not the same: ",paste(rownames(pwm1),collapse=",")," <-> ",paste(rownames(pwm2),collapse=","),"\n")
    }

  pwm1[pwm1==0]<- 0.000001
  pwm2[pwm2==0]<- 0.000001
  pwm<-log(pwm1/pwm2)/log(2)
    
  xlim.diff<-0.6666
  ylim.diff<-0.0
  jitter.fac<-0.75
  col.bg<-colors()[245]
  col.axis<-colors()[185]
  cols.acgt<-colors()[c(496,435,148,505)]
  names(cols.acgt)<-rownames(pwm1)

  xs<-jitter( rep(1:ncol(pwm),4), factor=jitter.fac)
  ys<-as.vector(t(pwm))
  
ys<-transform_yvalues(ys)

#print(ys)

# outlier
ys[ys< transform_yvalues(-5)]<- transform_yvalues(-5.5)
ys[ys>  transform_yvalues(5)]<- transform_yvalues(5.5)
  
  pchs<-rep(rownames(pwm),each=ncol(pwm))
  cols<-rep(cols.acgt,each=ncol(pwm))
  cex.let<-3
  ylim.max<-max(2.5,max(ys)+ylim.diff)
  ylim.min<-min(-2.5,min(ys)-ylim.diff)

  if(!is.na(fn.out)){pdf(file=fn.out,height=7,width=ncol(pwm)*1.2)}
    
  par(mar=c(4,5,1,0.5),cex.axis=1.5,cex.lab=2.2,xaxs="i")
  plot(0,0,type="n",xlim=c(1-xlim.diff,ncol(pwm)+xlim.diff),ylim=c(ylim.min,ylim.max),main="",ylab="Log2 ratio of nt probabilities",xlab="Position",axes=FALSE,col.lab=col.axis)
  polygon(x=c(0,0,ncol(pwm)+xlim.diff,ncol(pwm)+xlim.diff),y=c(ylim.min,ylim.max,ylim.max,ylim.min),col=col.bg,border=NA)
  axis(1,at=1:ncol(pwm),1:ncol(pwm),cex.axis=1.8,lwd=2,col=col.axis,col.ticks=col.axis,col.axis=col.axis)

  at<-c(-2,-1,0,1,2)
  if(max(pwm)>4 ){
    at<-c(at,4)
  }
  if(min(pwm)< -4 ){
    at<-c(-4,at)
  }

  labels<-at
  axis(2,at=transform_yvalues(at),labels=labels,lwd=2,cex.axis=1.8,col=col.axis,col.ticks=col.axis,col.axis=col.axis)

  grid(NULL, NA, lwd = 3,lty="solid",col="white")
  abline(h=transform_yvalues(at),lwd=3,col="white")

  abline(h=0,lwd=8,col=colors()[230])
  text(x=(ncol(pwm)+1)/2,y=ylim.max/2,labels=name1,cex=4,col=colors()[340],font=2)
  text(x=(ncol(pwm)+1)/2,y=ylim.min/2,labels=name2,cex=4,col=colors()[340],font=2)
  text(x=xs,y=ys,labels=pchs,col=cols,cex=cex.let,font=2)

  if(!is.na(fn.out)){trash<-dev.off()}
}



freqplot<-function(pwm,fn.out=NA){

   if(!is.matrix(pwm)){   # pwm is assumed to be a file and we read in the pwm from it
    pwm.tmp<-readPWM_matt(pwm)
    pwm<-pwm.tmp
    rm(pwm.tmp)
  }

  if(nrow(pwm)!=4){stop("Given PWM needs to have 4 rows (nts) and as many columns as positions")}
  rownames(pwm)<-toupper( rownames(pwm) )
  pwm<-pwm[order(rownames(pwm)) ,]  # sort column according positons A, C, G, T/U
 
  xlim.diff<-0.6666
  jitter.fac<-0.75
  cex.min<-2.5
  col.bg<-colors()[245]
  col.axis<-colors()[185]
  cols.acgt<-colors()[c(496,435,148,505)]  # A, C, G, T/U
  names(cols.acgt)<-rownames(pwm)
  cols.acgt.tr<-makeTransparent(cols.acgt,alpha=0.70)

  pwm2<-pwm
  cols.mat<-pwm
  lets<-pwm
  for(c in 1:ncol(pwm2)){
    ids<-order(pwm2[,c])
    pwm2[,c]<-partSums(pwm2[ids,c])
    cols.mat[,c]<-cols.acgt.tr[ rownames(pwm2)[ids] ]
    lets[,c]<-rownames(pwm2)[ids]
  }
  pwm2<-rbind(0,pwm2)

  
  if(!is.na(fn.out)){pdf(file=fn.out,height=10,width=ncol(pwm)*1.5)}
  
  par(mar=c(0.05,5,0.8,0.5),cex.axis=1.5,cex.lab=2.2,xaxs="i")
  
  # first plot: nt probabilities as stacked bars per positions
  plot(1,0.5,type="n",axes=FALSE,col.lab=col.axis,main="",ylab="Probability",ylim=c(0,1),xlim=c(1-xlim.diff,ncol(pwm2)+xlim.diff))
  bw<-0.7
  for(c in 1:ncol(pwm2)){
  for(r in 2:nrow(pwm2)){
    rect(xleft=c-bw/2, ybottom=pwm2[r-1,c], xright=c+bw/2, ytop=pwm2[r,c], col=cols.mat[r-1,c],border=FALSE)
    y.width<-pwm2[r,c]-pwm2[r-1,c]
    if(y.width>=0.1){
      if(r<nrow(pwm2)){
	text(x=c,y=pwm2[r-1,c]+(y.width)/2,lets[r-1,c],col="white",cex=3,font=2)
      }else{
	 text(x=c,y=0.9,lets[r-1,c],col="white",cex=5,font=2)
      }
    }
  }}
  axis(2,lwd=2,cex.axis=1.8,col=col.axis,col.ticks=col.axis,col.axis=col.axis)
  
  if(!is.na(fn.out)){trash<-dev.off()}
}


infcontentplot<-function(pwm,name=NA,fn.out=NA){

  if(!is.matrix(pwm)){   # pwm is assumed to be a file and we read in the pwm from it
    pwm.tmp<-readPWM_matt(pwm)
    pwm<-pwm.tmp
    rm(pwm.tmp)
  }

  if(nrow(pwm)!=4){stop("Given PWM needs to have 4 rows (nts) and as many columns as positions")}
  rownames(pwm)<-toupper( rownames(pwm) )
  pwm<-pwm[order(rownames(pwm)) ,]  # sort column according positons A, C, G, T/U
  
  xlim.diff<-0.6666
  jitter.fac<-0.75
  cex.min<-2.5
  col.bg<-colors()[245]
  col.axis<-colors()[185]
  cols.acgt<-colors()[c(496,435,148,505)]  # A, C, G, T/U
  names(cols.acgt)<-rownames(pwm)
  cols.acgt.tr<-makeTransparent(cols.acgt,alpha=0.70)

  # information content in bits per row
  ic <- apply(pwm, 2, function (x) { x[which(x==0)] = 0.000001; 2 + sum(x*log2(x))})

  pwm2<-pwm
  cols.mat<-pwm
  lets<-pwm
  for(c in 1:ncol(pwm2)){
    ids<-order(pwm2[,c])
    pwm2[,c]<-partSums(pwm2[ids,c])
    cols.mat[,c]<-cols.acgt.tr[ rownames(pwm2)[ids] ]
    lets[,c]<-rownames(pwm2)[ids]
  }
  pwm2<-rbind(0,pwm2)
  
  if(!is.na(fn.out)){pdf(file=fn.out,height=10,width=ncol(pwm)*1.5)}
  
  par(mar=c(0.05,5,0.8,0.5),cex.axis=1.5,cex.lab=2.2,xaxs="i")
  
  xs<-jitter( rep(1:ncol(pwm),4), factor=jitter.fac)
  ys<-as.vector(t(pwm)) * ic
  pchs<-rep(rownames(pwm),each=ncol(pwm))
  cols<-rep(cols.acgt,each=ncol(pwm))
  cexs<-ys * 5
  ids<-cexs<cex.min
  cexs[ids]<-cex.min
  fonts<-rep(2,length(cexs))
  fonts[ids]<-1

  par(mar=c(4,5,0.05,0.5),cex.axis=1.5,cex.lab=2.2,xaxs="i")
  plot(0,0,type="n",xlim=c(1-xlim.diff,ncol(pwm)+xlim.diff),ylim=c(-0.1,2.3),main="",ylab="Information content [bit]",xlab="Position",axes=FALSE,col.lab=col.axis)
  polygon(x=c(0,0,ncol(pwm)+xlim.diff,ncol(pwm)+xlim.diff),y=c(-0.2,2.4,2.4,-0.2),col=col.bg,border=NA)
  axis(1,at=1:ncol(pwm),1:ncol(pwm),cex.axis=1.8,lwd=2,col=col.axis,col.ticks=col.axis,col.axis=col.axis)
  axis(2,lwd=2,cex.axis=1.8,col=col.axis,col.ticks=col.axis,col.axis=col.axis)
  grid(NULL, NULL, lwd = 3,lty="solid",col="white")
  text(x=(ncol(pwm)+1)/2,y=1.5,labels=name,cex=4,col=colors()[340],font=2)
  text(x=xs,y=ys,labels=pchs,col=cols,cex=cexs,font=fonts)

  if(!is.na(fn.out)){trash<-dev.off()}
}


freq_and_infcontent_plot <- function(pwm, fn.out=NA){

    if(!is.matrix(pwm)){   # pwm is assumed to be a file and we read in the pwm from it
      pwm.tmp<-readPWM_matt(pwm)
      pwm<-pwm.tmp
      rm(pwm.tmp)
    }

    w<-ncol(pwm)*1.5
    h<-10    
    if(!is.na(fn.out)){pdf(file=fn.out,height=h,width=w)}
  
    par(mfrow=c(2,1))
    
    freqplot(pwm,NA)
    
    infcontentplot(pwm,NA)
   
    if(!is.na(fn.out)){trash<-dev.off()}
}


# pwm1: rows -> letters, columns -> positions
# ONLY for ACGT/U pwms
#
# pwm2: rows -> letters, columns -> positions
#
# pwm1 and pwm2 can be filenames or R matrices - need to have rownames A, C, G, T/U
#
# fn.out: output PDF file name
motif_compare_plot <- function(pwm1,name1,pwm2,name2,fn.out=NA){

    if(!is.matrix(pwm1)){   # pwm1 is assumed to be a file and we read in the pwm from it
      pwm.tmp<-readPWM_matt(pwm1)
      pwm1<-pwm.tmp
      rm(pwm.tmp)
    }
    if(nrow(pwm1)!=4){stop("Given PWM1 needs to have 4 rows (nts) and as many columns as positions")}
    rownames(pwm1)<-toupper( rownames(pwm1) )
    pwm1<-pwm1[order(rownames(pwm1)) ,]  # sort column according positons A, C, G, T/U
    
    if(!is.matrix(pwm2)){   # pwm2 is assumed to be a file and we read in the pwm from it
      pwm.tmp<-readPWM_matt(pwm2)
      pwm2<-pwm.tmp
      rm(pwm.tmp)
    }
    if(nrow(pwm2)!=4){stop("Given PWM needs to have 4 rows (nts) and as many columns as positions")}
    rownames(pwm2)<-toupper( rownames(pwm2) )
    pwm2<-pwm2[order(rownames(pwm2)) ,]  # sort column according positons A, C, G, T/U
    
    if(ncol(pwm1)!=ncol(pwm2)){stop("Both PWMs or all sequences of both groups must be of the same length, but they aren't.")}
    
    w<-ncol(pwm1)*1.5
    h<-25
    if(!is.na(fn.out)){pdf(file=fn.out,height=h,width=w)}
  
    par(mfrow=c(5,1))
    
    freqplot(pwm1,NA)
    
    infcontentplot(pwm1,name1,NA)
    
    berryplot(pwm1,name1,pwm2,name2,NA)
    
    freqplot(pwm2,NA)
    
    infcontentplot(pwm2,name2,NA)
   
    if(!is.na(fn.out)){trash<-dev.off()}
}