#!/usr/bin/env Rscript
args <- commandArgs(trailingOnly = TRUE) # dir input_efeature_table  args<-c("/nfs/users/mirimia/agohr/crg/projects/2016_matt/abilities/..","results","Matt_comp_exons_Zygote_2C_POS_with_efeatures.tab","DATASET2","Matt_comp_exons_Zygote_2C_POS.TABLE","up;ndiff","DATASET]up[;DATASET]ndiff[","0","lightgrey,lightgrey")
dir.matt<-args[1]
dir<-args[2]
fin<-args[3]   ### is matrix with first column containing group IDs and all other columns contain features
col.grps<-args[4]
f.data<-args[5]
grp.names<-unlist(strsplit(args[6],";"))
grp.nums1<-unlist(strsplit(args[7],";")) 
grp.nums2<-unlist(strsplit(args[8],";"))  # after removing exons which could not be found in GTF
keepdups<-args[9]
colors<-strsplit(args[10],",")[[1]]
mattversion<-args[11]
pvalthresh<-as.numeric(args[12])

#############
## internal function for box plotting
plot_internal<-function(d.int, col.vs, colid.grps, cols, feat.n){

  grps<-unique(d.int[,colid.grps])
  max.x.lab.length<-max(nchar(grps))
  xstrech<-max.x.lab.length / 6
  if(xstrech<1){xstrech<-1}

  l<-list()
  for(i in 1:length(grps)){
    grp<-grps[i]
    vs<- d.int[ d.int[,colid.grps]==grp , col.vs ]
    l[[grp]]<-vs[!is.na(vs)]
  }

  # get ylims
  bp<-boxplot(l
     ,xlim=c(0,length(l)+1)
     ,xlab=""
     ,ylab=""
     ,outline=FALSE
     ,at=1:length(l)
     ,boxwex=0.5
     ,lwd=2
     ,axes=FALSE
     ,cex.lab=1.5
     ,range = 1.5
     ,plot=FALSE
      )
  ymin.tmp<-min(bp[[1]][1,],na.rm=TRUE)
  ymax.tmp<-max(bp[[1]][5,],na.rm=TRUE)

  yext<-ymax.tmp-ymin.tmp

  cex.fac<-1
  boxwex.adapt<-0.5
  if(length(grps)<4){cex.fac<-1.5;boxwex.adapt<-0.37}
  if(length(grps)<3){cex.fac<-2;boxwex.adapt<-0.37}
    
  par(mar=c(6*xstrech,5,0.5,0.5))
  boxplot(l
     ,xlim=c(0.5,length(l)+1)
     ,ylim=c(ymin.tmp-yext*0.05,ymax.tmp)
     ,xlab=""
     ,ylab=""
     ,outline=FALSE
     ,at=1:length(l)
     ,boxwex=boxwex.adapt
     ,lwd=2
     ,axes=FALSE
     ,cex.lab=1.5
     ,range = 1.5
     ,col=colors
     ,font.lab=2 
  )

  # get extension of y axis
  ylim.min<-par("usr")[3]
  ylim.max<-par("usr")[4]
  
  Ns<-c()
  for(i in 1:length(l)){Ns<-c(Ns,length(l[[i]]))}
  ns<-gsub("_"," ",grps)
  
  axis(2,cex.axis=1.25*cex.fac)
  axis(1,1:length(l),labels=FALSE,cex.axis=1.25*cex.fac)
  title(ylab=gsub("_"," ",feat.n),font.lab=2,cex.lab=1.2*cex.fac)
  text(x=1:length(ns), y=par()$usr[3]-0.033*(par()$usr[4]-par()$usr[3]),labels=ns, srt=45, adj=1, xpd=TRUE, cex=1.25*cex.fac)
  text(x=1:length(Ns),y=rep(ylim.min+0.0277*(ylim.max-ylim.min),length(Ns)),labels=Ns,cex=1*cex.fac,xpd=TRUE)
}


# rounding
round_internal<-function(x){
  
  if(x>1){x<-round(x,digits=4)
  }else{x<-signif(x,digits=6)}

return(x)
}
#############




#rm(list=ls())
#dir.matt<-"/home/crg/bin2"
#dir<-"/home/crg/crg/projects/2016_lvigevani_sf3b1_inhibitors/analysis_bru_rna/exon_analysis/cmpr_down_vs_ndiff"
#fin<-"exons_with_efeatures.tab"
#col.grps<-"DATASET"

dir.tmp<-getwd()
setwd(dir)
dir.graph<-"summary_graphics"
dir.create(dir.graph)
summary.file<-"summary.tex"
matt<-paste(dir.matt,"matt",sep="/")

# get exon features
ret<-system2(matt,c("get_efeatures","explain"),stdout=TRUE)
# I use two codes for
# 1: encoding if a features should be included in the summary report with graphics
#    if there is a blank at the last positions: plot this featured in summary report  else: should not be plotted
features<-list()
c<-0
for(i in 1:length(ret)){
  feat<-ret[i]
  last.char<-substr(feat,nchar(feat),nchar(feat))
  if(last.char == " "){
    c<-c+1
    # remove blanks and numbers and dot from the beginning
    options(warn=-1)
    for(i in 1:nchar(feat)){
      tmp.char<-substr(feat,i,i)
      if(tmp.char == " " || tmp.char == "." || !is.na(as.numeric(tmp.char))){next;
      }else{
	feat<-substr(feat,i,nchar(feat))
	break
      }
    }
    options(warn=1)
    
    feat<-strsplit(feat,"-")[[1]]
    features[[c]]<-list(name=gsub(" ","",feat[1]),name.latex="",description="")
    features[[c]]$name.latex<-gsub("_"," ",features[[c]][["name"]])
    if(length(feat)>1){
      decr<-paste(feat[-1],collapse="-")
      decr<-sub("^\\s*","",decr,perl=TRUE)
      decr<-sub("\\s*$","",decr,perl=TRUE)
      tmp.char<-substr(decr,nchar(decr),nchar(decr))
      if(tmp.char == "." || tmp.char == ","){decr<-substr(decr,1,nchar(decr)-1);}
      decr<-gsub("_"," ",decr)
      features[[c]]$description<-decr
    }
  }
}


# read in table with features
d<-read.table(fin,sep="\t",header=TRUE,as.is=TRUE)
colid.grps<-which(colnames(d)==col.grps)
if(length(colid.grps)!=1){stop("Did not find column with group IDs = ",col.grps," in table ",fin,"\n")}
grps<-unique(d[,colid.grps])
names(grps)<-grps  # names will be used in plots as xaxis lables and can be short versions of group ids
if(length(grps)<2){stop("Error in generating final summary: input data contains only one group but need at least two.\n")}

# check which features actually are in data table and remove those which aren't
features.tmp<-list()
for(i in 1:length(features)){
  # check if features exists in d
  j<-match(features[[i]]$name,colnames(d))
  if(is.na(j)){next}
  features.tmp[[length(features.tmp)+1]]<-features[[i]]
}
features<-features.tmp


#\\usepackage{selinput}  not istalled on cluster
cat("\\documentclass{scrartcl}
\\usepackage[english]{babel}
\\usepackage{graphicx}
\\usepackage[nottoc]{tocbibind}
\\usepackage{hyperref}
\\hypersetup{colorlinks=true,linktoc=all,linkcolor=blue}
\\setlength\\parindent{0pt}
\\begin{document}
\\title{Comparison of exons grouped into:\\\\",paste(gsub("_","-",grps),sep="",collapse=", "),"}
\\author{}
\\date{\\today\\\\Matt version ",mattversion,"}
\\maketitle
\\addtocontents{toc}{\\protect\\hypertarget{toc}{}}
\\tableofcontents
\\newpage
\\section{Infos}
Visualizations of exon features for different groups of exons.
Each exon occurs in exactly one gene, but might occur in several
transcripts of that gene. Hence, for some features like
the exon length, there is exactly one value for each exon.
For other features, e.g., length of the up-stream exon(s), which could
be different in different transcripts, there might be several 
values for each exon. Consequently, in the latter cases, the
median of these value gets reported.
\\section{Warning: Please read this note carefully}
Please keep in mind that some features might affect other features.
Especially: all branch-point features get extracted from sub-sequences of introns, by standard the last 150 nt at the 3' end of each intron (if you haven't changed this) always neglecting the first 20 nt at their 5' end.
If introns of one set are especially short, i.e., many are shorter then these 150 nt, then the shorter intron length might affect branch-point features.
For example, there might be less branch points found in shorter introns or their distance to the 3' intron ends might be generally shorter simply because of their shorter intron length.
\\section{Notes for publishing results}
The Matt paper:\\
\\emph{Matt: Unix tools for alternative splicing analysis, A. Gohr, M. Irimia, Bioinformatics, 2018, bty606, DOI: 10.1093/bioinformatics/bty606}
\\par
When publishing results wrt. splice site strengths
which you determined for your data using matt,
please cite:\\
\\emph{Maximum entropy modeling of short sequence motifs with applications to RNA splicing signals, Yeo et al., 2003, DOI: 10.1089/1066527041410418}
\\par
When publishing results wrt. branch point features which you
determined for your data with matt,
please cite:\\
\\emph{Genome-wide association between branch point properties and alternative splicing, Corvelo et al., 2010, DOI: 10.1371/journal.pcbi.1001016}
\\par
When publishing results with respect to the binding strength of the human Sf1 splicing factor,
you might refert to where the Sf1 binding motif comes from:\\
\\emph{Analysis of in situ pre-mRNA targets of human splicing factor SF1 reveals a function in alternative splicing, Margherita Corioni, Nicolas Antih, Goranka Tanackovic, Mihaela Zavolan, and Angela Kramer, 2011, DOI:  10.1093/nar/gkq1042}\\\\
The Sf1 binding motif is described in supplement, page 13, table S2: Weight matrix of the binding specificity of SF1.
",file=summary.file,append=FALSE)


# overview table: rows -> feartues, columns -> num values, p values means, medians
N.cols<-length(grps)*4+(length(grps)*(length(grps)-1)*0.5*2)
d.out<-matrix(NA,ncol=N.cols,nrow=length(features),dimnames=list(NULL,rep(NA,N.cols)))

# group overview
cat("\\newpage\\section{Data sets}\n",file=summary.file,append=TRUE)
cat("Input file:\\\\\\verb+",f.data,"+\\par\n",file=summary.file,append=TRUE)
cat("Selection criteria for defining exons groups:\\\\\n",file=summary.file,append=TRUE)
for(i in 1:(length(grp.names))){
  if(i<length(grp.names)){
    cat("\\verb+",grp.names[i],"+: having value \\verb+",grp.names[i],"+ in column \\verb+",col.grps,"+\\\\\n",file=summary.file,append=TRUE)
  }else{
    cat("\\verb+",grp.names[i],"+: having value \\verb+",grp.names[i],"+ in column \\verb+",col.grps,"+\\par\n",file=summary.file,append=TRUE)
  }
}

str<-"yes"
if(keepdups==1){str<-"no"}
cat("Exon duplicates removal: ",str,"\\par\n",file=summary.file,append=TRUE)

cat("Numbers of exons per group before / after neglecting exons which were not found in GTF file (gene annotation). For the comparisons only exons which were found in the gene annotation are used. These numbers might change slightly for each feature if NAs occur.\\par\n",file=summary.file,append=TRUE)
for(i in 1:(length(grp.names))){
  cat(paste("\\verb+",grp.names[i],"+: ",grp.nums1[i]," / ",grp.nums2[i],"\n",sep=""),"\\\\",file=summary.file,append=TRUE)
}

# Overview section
cat("\\newpage\\section{Overview: Features with statistically significant differences (p-val $\\le ",pvalthresh,"$)}\n\\hypertarget{ov}{}\n\\input{ov.tex}\n",file=summary.file,append=TRUE)

## print box plots for all features and all groups
cat("\\newpage\\section{Details: Box plots and statistical assessments for all features}\n",file=summary.file,append=TRUE)
d.out.c<-0
newpage<-""
p.vals<-c()
ov.strs<-c()
ov.links.of.nonsig.results<-c()
for(i in 1:length(features)){
cat(i,"\n")
  d.out.c<-0
  
  j<-which(colnames(d)==features[[i]]$name)

  cat(paste(newpage,"\n\\subsection{",features[[i]]$name.latex,"}\n\\hypertarget{",i,"}{}\n\\hfill{\\small Back to:~\\hyperlink{ov",i,"}{Overview}}~\\textbar~\\hyperlink{toc}{ToC}\\\\\\\\\nMeaning: ",features[[i]]$description,"\\\\\\\\\n",sep="",collapse=""),file=summary.file,append=TRUE)
  if(newpage==""){newpage<-"\\newpage"}

  file<-paste(dir.graph,"/",features[[i]]$name,"_boxplot.pdf",sep="")
  pdf(file=file,width=12,height=8)
  
  plot_internal(d, j, colid.grps, cols, features[[i]]$name)
  
  dev.off()

  cat(paste("\\includegraphics[width=0.8\\textwidth]{",file,"}~\\\\\\\\\n",sep=""),file=summary.file,append=TRUE)
  
  # get means and medians per group
  l<-list()
  for(k in 1:length(grps)){
  
    grp<-grps[k]
    vs<- d[ d[,colid.grps]==grp , j ]
    
    N.NAs<-length(which(is.na(vs)))
    vs<-vs[!is.na(vs)]
    
    d.out.c<-d.out.c+1
    d.out[i, d.out.c ]<-length(vs)
    d.out.c<-d.out.c+1
    d.out[i, d.out.c ]<-N.NAs
    d.out.c<-d.out.c+1
    d.out[i, d.out.c ]<-mean(vs)
    d.out.c<-d.out.c+1
    d.out[i, d.out.c ]<-median(vs)

    colnames(d.out)[(d.out.c-3):d.out.c]<-paste(grp,c("N_DATAPOINTS","N_NA","MEAN","MEDIAN"),sep="_")

    l[[grp]]<-vs
  }

  cat("Significant results from Mann-Whitney U test:\\par",file=summary.file,append=TRUE)
  # pairwise Mann-Whitney-U tests
  pval.min<- 2
  do.noresult.output<-TRUE
  for(k in 1:(length(grps)-1)){for(m in (k+1):length(grps)){
    grp1<-grps[k]
    grp2<-grps[m]
    wt<-wilcox.test(x=l[[k]],y=l[[m]])

    d.out.c<-d.out.c+1
    d.out[i, d.out.c ]<-wt$p.value
    colnames(d.out)[d.out.c]<-paste("PVAL_MANN_WHITNEY_U_TEST_",grp1,"_VS_",grp2,sep="",collpase="")

    if(!is.na(wt$p.value) && wt$p.value<=0.05){
      m1<-mean(l[[k]]);md1<-median(l[[k]])
      m2<-mean(l[[m]]);md2<-median(l[[m]])

      str<-paste("~$\\mbox{  }$~mean: ",round_internal(m1),sep="")
      if(m1<m2){str<-paste(str," $<$ ",round_internal(m2),sep="")}else{if(m1>m2){str<-paste(str," $>$ ",round_internal(m2),sep="")}else{str<-paste(str," $=$ ",round_internal(m2),sep="")}}
      str<-paste(str," , median: ",round_internal(md1),sep="")
      if(md1<md2){str<-paste(str," $<$ ",round_internal(md2),sep="")}else{if(md1>md2){str<-paste(str," $>$ ",round_internal(md2),sep="")}else{str<-paste(str," $=$ ",round_internal(md2),sep="")}}
      cat(paste("$\\bullet$ \\verb+",grp1,"+ vs \\verb+",grp2,"+ : ",round_internal(wt$p.value),"\\\\",str,"\\par",sep="",collpase=""),file=summary.file,append=TRUE)
      if(wt$p.value<pval.min){pval.min<-wt$p.value}
	  do.noresult.output<-FALSE
      str<-"*"
      if(wt$p.value<=0.01){str<-paste(str,"*",sep="")}else{str<-paste(str," ",sep="")}
      if(wt$p.value<=0.001){str<-paste(str,"*",sep="")}else{str<-paste(str," ",sep="")}
      if(m1<m2){str<-paste(str," : ",grp1," < ",grp2,sep="")}else{if(m1>m2){str<-paste(str," : ",grp1," > ",grp2,sep="")}else{str<-paste(str," : ",grp1," = ",grp2,sep="")}}
    }else{
      if(is.na(wt$p.value)){
        str<-"p-value=NA"
        cat(paste("$\\bullet$ \\verb+",grp1,"+ vs \\verb+",grp2,"+ : p value = NA\\par",sep="",collpase=""),file=summary.file,append=TRUE)
        do.noresult.output<-FALSE
      }else{
	str<-paste(grp1," = ",grp2,sep="")
      }
    }
    d.out.c<-d.out.c+1
    d.out[i, d.out.c ]<-str
    colnames(d.out)[d.out.c]<-paste("QUAL_MANN_WHITNEY_U_TEST_",grp1,"_VS_",grp2,sep="",collpase="")
  }}
  
  if(do.noresult.output){
    cat("$\\bullet$ none\\par",file=summary.file,append=TRUE)
  }
  if(pval.min<=pvalthresh){
    p.vals<-c(p.vals,pval.min)
    ov.strs<-c(ov.strs,paste("\\hyperlink{",i,"}{",features[[i]]$name.latex,"}\n\\hypertarget{ov",i,"}{}","~\\\\\\vspace{-0.5cm}\\includegraphics[width=0.445\\textwidth]{",file,"}~\\\\\n",sep="",collapse=""))
  }else{
	  ov.links.of.nonsig.results<-c(ov.links.of.nonsig.results,paste("\\hypertarget{ov",i,"}{}\n",sep="",collapse=""))
  }
}


cat("\\end{document}",file=summary.file,append=TRUE)

cat(paste(ov.links.of.nonsig.results,sep="",collapse="\n"),file="ov.tex",append=FALSE);
# no significant results at all
if(length(p.vals)==0){cat("none",file="ov.tex",append=TRUE)
}else{
	ov.strs<-ov.strs[order(p.vals)]
	for(i in 1:length(ov.strs)){
		cat(ov.strs[i],"\n",file="ov.tex",append=TRUE)
		if(i%%4==0){cat("\\newpage\n",file="ov.tex",append=TRUE)}
	}
}


# write table with all results
d.out<-cbind(unlist(lapply(features,function(x){x$name})),d.out)
colnames(d.out)[1]<-"FEATURE"
write.table(d.out,file="overview_of_features_and_comparisons.tab",row.names=FALSE,quote=FALSE,sep="\t")


# copy latex packages
cmd<-paste("cp ",dir.matt,"/external_progs/latex_packages/*.* .",sep="")
system(cmd)


ret<-system2("pdflatex",c("-interaction=nonstopmode","-halt-on-error","-file-line-error",summary.file))
ret<-system2("pdflatex",c("-interaction=nonstopmode","-halt-on-error","-file-line-error",summary.file))
ret<-system2("pdflatex",c("-interaction=nonstopmode","-halt-on-error","-file-line-error",summary.file))



setwd(dir.tmp)


quit(status=0)  # success
