#!/usr/bin/env Rscript
args <- commandArgs(trailingOnly = TRUE)

getScriptPath <- function(){
    cmd.args <- commandArgs()
    m <- regexpr("(?<=^--file=).+", cmd.args, perl=TRUE)
    script.dir <- dirname(regmatches(cmd.args, m))
    if(length(script.dir) == 0) stop("can't determine script dir: please call the script with Rscript")
    if(length(script.dir) > 1) stop("can't determine script dir: more than one '--file' argument detected")
    return(script.dir)
}

# read Utilities.r only if it not yet have been called
if(!exists("freq_and_infcontent_plot")){
  my.path<-getScriptPath()
  source(paste(my.path,"/Utilities.r",sep="",collapse=""))
}


#pfm <- matrix(data=c(5, 3, 16, 1, 0, 17, 17, 0, 0, 16, 12, 8,
#6, 9, 1, 1, 18,  1, 0, 0, 18,  1, 0, 2,
#2,3, 1, 0, 0, 0, 0, 1, 0, 0, 1, 2,
#5, 3, 0, 16, 0, 0, 1, 17,  0, 1, 5, 6), 
#byrow=TRUE,nrow=4,dimnames=list(c('A','C','G','T')))

#pwm <- apply(pfm, 2, function(x) x/sum(x))

#freq_and_infcontent_plot("/home/crg/crg/projects/2016_matt/data_external/pwm/sf1_human_l7.tab","~/test.pdf")
#freq_and_infcontent_plot(cbind(pwm,pwm,pwm),"~/test2.pdf")
#freq_and_infcontent_plot(pwm[,3:7],"~/test3.pdf")

freq_and_infcontent_plot(args[1],args[2])

quit(status=0)  # success
