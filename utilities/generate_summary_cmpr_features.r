#!/usr/bin/env Rscript
args <- commandArgs(trailingOnly = TRUE)
#args<-c("/home/crg/crg/projects/2016_matt/abilities/..","introns","000_extracted_features.tab","DPSI_GRPA_MINUS_GRPB","INTRON_LENGTH,UPEXON_MEDIANLENGTH,DOEXON_MEDIANLENGTH,RATIO_UPEXON_INTRON_LENGTH,RATIO_DOEXON_INTRON_LENGTH,INTRON_GCC,UPEXON_GCC,DOEXON_GCC,RATIO_UPEXON_INTRON_GCC,RATIO_DOEXON_INTRON_GCC,SF1_HIGHESTSCORE_3SS,INTRON_5SS_20INT10EX_GCC,INTRON_3SS_20INT10EX_GCC,MAXENTSCR_HSAMODEL_5SS,MAXENTSCR_HSAMODEL_3SS,DIST_FROM_MAXBP_TO_3SS,SCORE_FOR_MAXBP_SEQ,PYRIMIDINECONT_MAXBP,POLYPYRITRAC_OFFSET_MAXBP,POLYPYRITRAC_LEN_MAXBP,POLYPYRITRAC_SCORE_MAXBP,BPSCORE_MAXBP,NUM_PREDICTED_BPS,MEDIAN_DIST_FROM_BP_TO_3SS,MEDIAN_SCORE_FOR_BPSEQ,MEDIAN_PYRIMIDINECONT,MEDIAN_POLYPYRITRAC_OFFSET,MEDIAN_POLYPYRITRAC_LEN,MEDIAN_POLYPYRITRAC_SCORE,MEDIAN_BPSCORE,MEDIAN_TR_LENGTH,MEDIAN_INTRON_NUMBER,INTRON_MEDIANRANK,INTRON_MEDIANRELATIVERANK,INTRON_MEDIANRELATIVERANK_3BINS,INTRON_MEDIANRELATIVERANK_5BINS,INTRON_MEDIANRELATIVERANK_10BINS,PROP_FIRST_INTRON,PROP_LAST_INTRON,PROP_INTERNAL_INTRON,PROP_INTRON_IN_UTR,INTRON_COOCCURS_WITH_OTHER_INTRONS,MEDIAN_INTRON_INTRON_COOCCURRENCE_NUMBER_OVER_ALL_TRS","0,0.1,0.1,0.2,0.2,0.3,0.3,0.4,0.4,0.5,0.5,0.6,0.6,0.7,0.7,0.8,0.8,0.9,0.9,1","1.3.0","0.05", "colors:lightgrey,lightgrey,lightgrey,lightgrey,lightgrey,lightgrey,lightgrey,lightgrey,lightgrey,lightgrey","points:false","outliers:false")

dir.matt<-args[1]
outd<-args[2]
fin<-args[3]
featsa<-unlist(strsplit(args[4],",")) # create one boxplot for each feature A, where events (rows) are binned into quantiles for each feature B
featsb<-unlist(strsplit(args[5],",")) # e.g. featsa could be PSI,dPSI and featsb could be LEN,GCC,SSS
bins<-as.numeric(unlist(strsplit(args[6],",")))  # start1,end1,start2,end2,...,startN,endN for N bins
mattversion<-args[7]
pvalthresh<-as.numeric(args[8])
colors<-strsplit(gsub("colors:","",args[9]),",")[[1]]
plotpoints<-args[10]
addoutliers<-args[11]

#############
## internal function
#############
round_internal<-function(x){
  ret<-sapply(as.numeric(x),function(y){
    if(is.na(y)){return(NA)}
    if(y>1){return(round(y,digits=2))
    }else{return(signif(y,digits=3))}
  })
return(ret)
}


getBinStrings<-function(bins){
  bins.strings<-c()
  for(i in seq(1,length(bins),by=2)){
    if(i<length(bins)-1){
      bins.strings<-c(bins.strings,paste("[",bins[i],",",bins[i+1],")",sep=""))
    }else{
      bins.strings<-c(bins.strings,paste("[",bins[i],",",bins[i+1],"]",sep=""))
    }
  }
return(bins.strings)
}


makeTransparent<-function(someColor, alpha=100)
{
  newColor<-col2rgb(someColor)
  apply(newColor, 2, function(curcoldata){rgb(red=curcoldata[1], green=curcoldata[2],
  blue=curcoldata[3],alpha=alpha, maxColorValue=255)})
}

# tests if all elements in x are all numeric, all non-numeric, mixture of both
typeCheck<-function(x){
  if(length(x)==0){return("undef")}
  x<-x[!is.na(x)]
  if(length(x)==0){return("undef")}
  suppressWarnings(ret<-as.numeric(x))
  lNumeric<-length(which(!is.na(ret)))
  lNonnumeric<-length(which(is.na(ret)))
  lOrig<-length(x)
  if(lNumeric==lOrig){return("all_numeric")}
  if(lNonnumeric==lOrig){return("all_nonnumeric")}
  if(lNumeric>0 && lNumeric<lOrig){return("numeric_and_nonnumeric")}  
}


## internal function for box plotting
plot_internal<-function(ys,xs,feata,featb,bins,plotpoints,colors,addoutliers){
  xstrech<-1

  type<-typeCheck(xs)

  l<-list()  # contains values to be box-plotted
  xlabs2<-NA
  if(type=="all_numeric"){
    #xlab<-paste("Inter-quantile bins ",gsub("_"," ",featb))
    xlab<-gsub("_"," ",featb)
    algo.type<-7  # standard type used by R
    if(sum(xs-floor(xs),na.rm=TRUE)==0){algo.type<-3} # SAS method for discontinuous case
    qs<-quantile(xs,probs=bins,na.rm=TRUE,type=algo.type)  # quants never contains 0 and never 1. For defining three equidistant bins, it would be 0.333,0.666
    # check if the interval borders are equal -> can happen when we have many identical values in xs. 
    # e.g. POLYPYRITRAC_OFFSET_MAXBP: 
    # > qs
    # 0%  20%  20%  40%  40%  60%  60%  80%  80% 100% 
    # 1    1    1    1    1    3    3    8    8  117
    # merge inter-quantile bins when they have same borders
    mergeBins<-function(bins){
      Nbins<-length(bins)/2
      if(Nbins>1){
	diffs<-bins[seq(1,length(bins)-1,by=2)]-bins[seq(2,length(bins),by=2)]  # differences between start of intervals i and end of intervals i
	ids<-which(diffs==0)
	if(length(ids)>0){  # at least one bin needs to be merged
	  bins.best<-c()    # bins of maximal length after merging
	  for(id in ids){
	    if(id>1){
	      # there is a previous bin, so we try merging with this one
	      newStartID<-(id-1)*2-1
	      newEndID<-id*2
	      leftPart<-c();  if(newStartID>1){leftPart<-bins[1:(newStartID-1)]}
	      rightPart<-c(); if(newEndID<length(bins)){rightPart<-bins[(newEndID+1):(length(bins))]}
	      mergedBins<-mergeBins(c(leftPart,bins[newStartID],bins[newEndID],rightPart))
	      if(length(mergedBins)>length(bins.best)){bins.best<-mergedBins}
	    }
	    if(id<Nbins){
	      # there is a next bin, so we try merging with this one
	      newStartID<-id*2-1
	      newEndID<-(id+1)*2
	      leftPart<-c();  if(newStartID>1){leftPart<-bins[1:(newStartID-1)]}
	      rightPart<-c(); if(newEndID<length(bins)){rightPart<-bins[(newEndID+1):(length(bins))]}
	      mergedBins<-mergeBins(c(leftPart,bins[newStartID],bins[newEndID],rightPart))
	      if(length(mergedBins)>length(bins.best)){bins.best<-mergedBins}
	    }
	  }# for-id
	  bins<-bins.best
	}
      }
      return(bins)
    }

    qs<-mergeBins(qs)
    xlabs<-getBinStrings(round_internal(as.numeric(gsub("%","",names(qs)))/100))
    xlabs2<-getBinStrings(round_internal(qs))

    for(i in seq(1,length(qs),by=2)){
      if(i<length(qs)-1){
	vs<-ys[ !is.na(xs) & xs>=qs[i] & xs<qs[i+1] ]
      }
      if(i==length(qs)-1){ # last bin
	vs<-ys[ !is.na(xs) & xs>=qs[i] & xs<=qs[i+1] ]
      }
      l[[length(l)+1]]<-vs[!is.na(vs)]
    }
  }else if(type=="all_nonnumeric"){
    xlabs<-unique(xs)
    xlab<-gsub("_"," ",featb)
    for(i in 1:length(xlabs)){
      l[[length(l)+1]]<-ys[!is.na(xs) & xs==xlabs[i]]
     }
     if(length(l)>length(colors)){colors<-c(colors,rep("lightgrey",length(l)-length(colors)))}
  }else{
    stop("Values in column ",featb," are a mixture of numeric and non-numeric. All must be either numeric or non-numeric. If you want to use digits as categories, add an underscore at the end, e.g. 1_ instead of 1.")
  }

  # get ylims
  bp<-boxplot(l
     ,xlim=c(0,length(l)+1)
     ,xlab=""
     ,ylab=""
     ,outline=FALSE
     ,at=1:length(l)
     ,boxwex=0.5
     ,lwd=2
     ,axes=FALSE
     ,cex.lab=1.5
     ,range = 1.5
     ,plot=FALSE
  )
  if(addoutliers=="outliers:true"){
    ymin.tmp<-min(unlist(lapply(l,min)))
    ymax.tmp<-max(unlist(lapply(l,max)))
  }else{
    ymin.tmp<-min(bp[[1]][1,],na.rm=TRUE)
    ymax.tmp<-max(bp[[1]][5,],na.rm=TRUE)
  }

  yext<-ymax.tmp-ymin.tmp

  cex.fac<-1
  boxwex.adapt<-0.5
  if(length(l)<4){cex.fac<-1;boxwex.adapt<-0.37}
  if(length(l)<3){cex.fac<-1;boxwex.adapt<-0.37}

  par(mar=c(6*xstrech,5,0.5,0.5))
  plot(x=0
    ,y=0
    ,type="n"
    ,xlim=c(1-boxwex.adapt,length(l)+boxwex.adapt)
    ,ylim=c(ymin.tmp-yext*0.05,ymax.tmp)
    ,xlab=""
    ,ylab=""
    ,axes=FALSE
  )
  
  lwd<-2 # standard line width for boxplots
  if(plotpoints=="points:true"){
      for(i in 1:length(l)){
        vs<-l[[i]]
        if(addoutliers!="outliers:true"){
	  vs<-vs[vs>=bp[[1]][1,i] & vs<=bp[[1]][5,i]]  # plot only data points inside box
	}
	points(x=rep(i,length(vs))+runif(length(vs),-0.1719,0.1719),y=vs,pch=20,cex=2,col="cornsilk4")
      }
      colors.tmp<-c()
      for(i in 1:length(colors)){
	colors.tmp[i]<-makeTransparent(colors[i],alpha=155)
      }
      colors<-colors.tmp
      lwd<-4   # increase line width for better visibility
  }

  par(mar=c(6*xstrech,5,0.5,0.5))
  boxplot(l
     ,xlim=c(1-boxwex.adapt,length(l)+boxwex.adapt)
     ,ylim=c(ymin.tmp-yext*0.05,ymax.tmp)
     ,xlab=""
     ,ylab=""
     ,outline=FALSE
     ,at=1:length(l)
     ,boxwex=boxwex.adapt
     ,lwd=lwd
     ,axes=FALSE
     ,cex.lab=1.5
     ,range = 1.5
     ,col=colors
     ,font.lab=2 
     ,add=TRUE
  )
  
  # get extension of y axis
  ylim.min<-par("usr")[3]
  ylim.max<-par("usr")[4]

  Ns<-c()
  for(i in 1:length(l)){Ns<-c(Ns,length(l[[i]]))}

  axis(2,cex.axis=1.5*cex.fac)
  axis(1,1:length(l),labels=FALSE,cex.axis=1.25*cex.fac)
  title(ylab=gsub("_"," ",feata),font.lab=2,cex.lab=1.7*cex.fac)
  title(xlab=xlab,font.lab=2,cex.lab=1.7*cex.fac,line=4.7)
  text(x=1:length(xlabs), y=par()$usr[3]-0.033*(par()$usr[4]-par()$usr[3]),labels=xlabs, srt=20, adj=1, xpd=TRUE, cex=1.5*cex.fac)
  text(x=1:length(Ns),y=rep(ylim.min+0.0277*(ylim.max-ylim.min),length(Ns)),labels=Ns,cex=1.25*cex.fac,xpd=TRUE)

  ret<-list(N=length(l))
  mins<-unlist(lapply(l,min))
  maxs<-unlist(lapply(l,max))
  means<-unlist(lapply(l,mean))
  medians<-unlist(lapply(l,median))
  sd<-unlist(lapply(l,sd))
  for(i in 1:length(means)){ret[[paste("stats",i,sep="_")]]<-c(mins[i],maxs[i],means[i],medians[i],sd[i],Ns[i])}

  if(length(l)>1){
    ret[["pval_kruskal"]]<-kruskal.test(l)$p.val
  }else{
    ret[["pval_kruskal"]]<-NA
  }

  if(length(l)>1){
    c<-0
    erg<-pairwise.wilcox.test(unlist(l),unlist(lapply(l,function(x){c<<-c+1;rep(c,length(x))})),p.adjust.method = "BH",alternative="two.sided",exact=FALSE)$p.value
    for(i in 1:ncol(erg)){for(j in i:nrow(erg)){
      ret[[paste("pairwise_pval_wilcox",i,j+1,sep="_")]]<-erg[j,i]
    }}
  }
  
  ret[["bins_definitions"]]<-NA
  if(length(which(!is.na(xlabs2)))==length(xlabs2)){
    ret[["bins_definitions"]]<-paste("\\begin{itemize}\n",paste("\\item\\verb+",paste(xlabs,xlabs2,sep=" = "),"+",collapse="\n"),"\\end{itemize}\n")
  }

return(ret)
}
#############


dir.tmp<-getwd()
setwd(outd)
dir.graph<-"summary_graphics"
dir.create(dir.graph)
summary.file<-"summary.tex"


# read in table with features
d<-read.table(fin,sep="\t",header=TRUE,as.is=TRUE)

# get max number of bins = max(N bins, N categories defined by a discrete feature B)
Nbins<-length(bins)/2
for(feat in featsb){
  type<-typeCheck(d[,feat])
  if(type=="all_nonnumeric"){
    Nbins.tmp<-length(unique(d[,feat]))
    if(Nbins.tmp>Nbins){Nbins<-Nbins.tmp}
  }
  if(type=="numeric_and_nonnumeric"){stop("Values in column ",featb," are a mixture of numeric and non-numeric. All must be either numeric or non-numeric. If you want to use digits as categories, add an underscore at the end, e.g. 1_ instead of 1.")
  }
}

cat("\\documentclass{scrartcl}
\\usepackage[english]{babel}
\\usepackage{graphicx}
\\usepackage[nottoc]{tocbibind}
\\usepackage{hyperref}
\\hypersetup{colorlinks=true,linktoc=all,linkcolor=blue}
\\setlength\\parindent{0pt}
\\setcounter{section}{-1}
\\begin{document}
\\title{Feature analysis}
\\author{}
\\date{\\today\\\\Matt version ",mattversion,"}
\\maketitle
\\addtocontents{toc}{\\protect\\hypertarget{toc}{}}
\\tableofcontents
\\newpage
\\section{One warning and one hint}
Please choose thoroughly the set of events to be included in this feature comparison. Weak patterns may not be visible otherwise; E.g.~when comparing exons/introns and including non-alternatively spliced exons/introns \\footnote{with PSI close to 0 in the vast majority of samples or with PSI close to 100 in the vast majority of samples} then these may spoil patterns of truly alternatively spliced exons/introns in your data.
\\par
The number of data points in equidistant quantile bins may vary considerably if the data are pseudo continuous. For example, the GC content of a set of short DNA sequences, although being a continuous feature, may take only a finit number (20-30) of distinct numeric values. In a real example, this led to more than 800 data points in quantile bin \\verb+[0.65,0.75)+ but less than 400 data points in bin \\verb+[0.75,0.85)+.
\\newpage
\\section{Notes for publishing results}
The Matt paper:\\
\\emph{Matt: Unix tools for alternative splicing analysis, A.~Gohr, M.~Irimia, Bioinformatics, 2018, bty606, DOI: 10.1093/bioinformatics/bty606}\\\\
\\par
When publishing results wrt.~splice site strengths
which you determined with Matt,
please cite:\\
\\emph{Maximum entropy modeling of short sequence motifs with applications to RNA splicing signals, Yeo et al., 2003, DOI: 10.1089/1066527041410418}\\\\
\\par
When publishing results wrt.~branch point features which you
determined with Matt,
please cite:\\
\\emph{Genome-wide association between branch point properties and alternative splicing, Corvelo et al., 2010, DOI: 10.1371/journal.pcbi.1001016}\\\\
\\par
When publishing results wrt.~the binding strength of the human SF1 splicing factor,
please cite where the SF1 binding motif comes from:\\
\\emph{Analysis of in situ pre-mRNA targets of human splicing factor SF1 reveals a function in alternative splicing, M.~Corioni et al., 2011, DOI:  10.1093/nar/gkq1042}\\\\
The SF1 binding motif is described in supplement, page 13, table S2: Weight matrix of the binding specificity of SF1.
",file=summary.file,append=FALSE)


# overview table
# columns: feature A, feature B, pval Kruskal-Wallis mean1, median1, sd1, mean2, median2, sd2..., meanN, medianN, sdN, pval pairwise Kruskal bin1_vs_bin2 ... binN-1_vs_binN
# each row: one combination of a A and B feature
bin_combinations<-c()
for(i in 1:(Nbins-1)){for(j in (i+1):Nbins){bin_combinations<-c(bin_combinations,paste(i,j,sep="_VS_"))}}
d.out<-matrix(NA
  ,ncol=Nbins*6+3+length(bin_combinations)
  ,nrow=length(featsa)*length(featsb)
  ,dimnames=list(NULL,
    c("FEATY","FEATX"
      ,paste(rep(c("MIN_","MAX_","MEAN_","MEDIAN_","SD_","N_"),Nbins),rep(1:Nbins,each=6),sep="")
      ,"KRUSKAL_WALLIS_PVAL"
      ,paste("PAIRWISE_WILCOXRANKSUM_ADJUSTBH_",bin_combinations,sep="")
      )
   )
)

# group overview
cat("\\newpage\\section{Data}\n",file=summary.file,append=TRUE)
cat("Input file:\\\\\\verb+",fin,"+\\par\n",file=summary.file,append=TRUE)
bins.strings<-getBinStrings(bins)
cat("~\\par Quantile bins:\\\\\n",file=summary.file,append=TRUE)
for(i in 1:ceiling(length(bins.strings)/5)){
  cat("\\verb+",paste(bins.strings[((i-1)*5+1):(i*5)],sep="  "),"+\\\\\n",file=summary.file,append=TRUE)
}
cat("~\\par Will be merged automatically if highly similarly distributed data cause empty interval definitions.\\\\\n",file=summary.file,append=TRUE)
 
cat("~\\par A Features:\\\\\n",file=summary.file,append=TRUE)
for(i in featsa){
    cat("\\verb+",i,"+\\\\\n",file=summary.file,append=TRUE)
}
cat("~\\par B Features used for binning data:\\\\\n",file=summary.file,append=TRUE)
for(i in featsb){
    cat("\\verb+",i,"+\\\\\n",file=summary.file,append=TRUE)
}

# Overview section
cat("\\newpage\\section{Overview: Features with statistically significant differences (p-val $\\le ",pvalthresh,"$)}\n\\hypertarget{ov}{}\n\\input{ov.tex}\n",file=summary.file,append=TRUE)

## print box plots for all features and all groups
cat("\\newpage\\section{Box plots and statistical assessments}\n",file=summary.file,append=TRUE)
d.out.c<-0
newpage<-""
p.vals<-c()
ov.strs<-c()
ov.links.of.nonsig.results<-c()
d.out.r<-0
main.link.id<-0
  
for(feata in featsa){
for(featb in featsb){

  main.link.id<-main.link.id+1
  cat(paste(newpage,"\n\\subsection{",gsub("_"," ",feata)," over ",gsub("_"," ",featb),"}\n\\hypertarget{",main.link.id,"}{}\n\\hfill{\\small Back to:~\\hyperlink{ov",main.link.id,"}{Overview}}~\\textbar~\\hyperlink{toc}{ToC}\\\\\\\\\n",sep="",collapse=""),file=summary.file,append=TRUE)
  if(newpage==""){newpage<-"\\newpage"}

  file<-paste(dir.graph,"/",gsub(" ","_",feata),"_",gsub(" ","_",featb),"_boxplot.pdf",sep="")

  d.out.r<-d.out.r+1
  d.out[d.out.r,1:2]<-c(feata,featb)

  if(length(which(is.na(d[,featb])))==nrow(d)){ 
    cat("All data points for this feature are NA.\\\\\n",file=summary.file,append=TRUE)    
    next
  } # only NA values

  pdf(file=file,width=9,height=7)
  ret<-plot_internal(d[,feata],d[,featb],feata,featb,bins,plotpoints,colors,addoutliers)
  dev.off()

  col.tmp<-3
  for(i in 1:ret[["N"]]){d.out[d.out.r,col.tmp:(col.tmp+5)]<-ret[[paste("stats",i,sep="_")]]; col.tmp<-col.tmp+6}
  col.tmp<-which(colnames(d.out)=="KRUSKAL_WALLIS_PVAL")
  d.out[d.out.r,col.tmp]<-ret[["pval_kruskal"]]
  col.tmp<-col.tmp+1
  if(ret[["N"]]>1){
    for(i in 1:(ret[["N"]]-1)){for(j in (i+1):ret[["N"]]){
      d.out[d.out.r,paste("PAIRWISE_WILCOXRANKSUM_ADJUSTBH_",i,"_VS_",j,sep="")]<-ret[[paste("pairwise_pval_wilcox",i,j,sep="_")]]
    }}
  }

  cat(paste("\\includegraphics[width=0.8\\textwidth]{",file,"}\\\\\\\\\n",sep=""),file=summary.file,append=TRUE)

  if(!is.na(ret[["bins_definitions"]])){
    cat(ret[["bins_definitions"]],"~\\\\\\\\\n",file=summary.file,append=TRUE)
  }
  
  pval<-d.out[d.out.r,"KRUSKAL_WALLIS_PVAL"]
  cat("Krusal-Wallis p value: ",round_internal(pval),"\n\\par",file=summary.file,append=TRUE)
  
  if(!is.na(pval) && as.numeric(pval)<=pvalthresh){
    
    for(i in grep("PAIRWISE_WILCOXRANKSUM_ADJUSTBH",colnames(d.out))){
      if(is.na(d.out[d.out.r,i])){next} # can happen when user has continuous and discrete B features and the number of bins is different
      if(as.numeric(d.out[d.out.r,i])<=pvalthresh){
	tmp<-unlist(strsplit(colnames(d.out)[i],"_"))
	c1<-tmp[length(tmp)-2]
	c2<-tmp[length(tmp)]
        cat(paste("$\\bullet$ bin ",c1," vs. bin ",c2," : pairwise Wilcox BH adjust p value = ",round_internal(d.out[d.out.r,i]),"\\\\\n",sep="",collpase=""),file=summary.file,append=TRUE)
        cat(paste("bin ",c1,": mean = ",round_internal(d.out[d.out.r,paste("MEAN_",c1,sep="")]),"~~median = ",round_internal(d.out[d.out.r,paste("MEDIAN_",c1,sep="")]),"\\\\\n",sep="",collpase=""),file=summary.file,append=TRUE)
        cat(paste("bin ",c2,": mean = ",round_internal(d.out[d.out.r,paste("MEAN_",c2,sep="")]),"~~median = ",round_internal(d.out[d.out.r,paste("MEDIAN_",c2,sep="")]),"\\\\\n",sep="",collpase=""),file=summary.file,append=TRUE)
      }
    }

    p.vals<-c(p.vals,as.numeric(pval))
    ov.strs<-c(ov.strs,paste("\\hyperlink{",main.link.id,"}{",gsub("_"," ",feata)," over ",gsub("_"," ",featb),"}\n\\hypertarget{ov",main.link.id,"}{}","~\\\\\\vspace{-0.5cm}\\includegraphics[width=0.4\\textwidth]{",file,"}~\\\\\n",sep="",collapse=""))
  }else{
	  ov.links.of.nonsig.results<-c(ov.links.of.nonsig.results,paste("\\hypertarget{ov",main.link.id,"}{}\n",sep="",collapse=""))
  }
}}


cat("\\end{document}",file=summary.file,append=TRUE)

cat(paste(ov.links.of.nonsig.results,sep="",collapse="\n"),file="ov.tex",append=FALSE);
# no significant results at all
if(length(p.vals)==0){cat("none",file="ov.tex",append=TRUE)
}else{
	ov.strs<-ov.strs[order(p.vals)]
	for(i in 1:length(ov.strs)){
		cat(ov.strs[i],"\n",file="ov.tex",append=TRUE)
		if(i==3){cat("\\newpage\n",file="ov.tex",append=TRUE)}
		if(i>3 && (i-3)%%4==0){cat("\\newpage\n",file="ov.tex",append=TRUE)}
	}
}

# write table with all results
write.table(d.out,file="000_feature_comparison_results.tab",row.names=FALSE,quote=FALSE,sep="\t")


# copy latex packages
cmd<-paste("cp ",dir.matt,"/external_progs/latex_packages/*.* .",sep="")
system(cmd)


ret<-system2("pdflatex",c("-interaction=nonstopmode","-halt-on-error","-file-line-error",summary.file))
ret<-system2("pdflatex",c("-interaction=nonstopmode","-halt-on-error","-file-line-error",summary.file))
ret<-system2("pdflatex",c("-interaction=nonstopmode","-halt-on-error","-file-line-error",summary.file))



#setwd(dir.tmp)


#quit(status=0)  # success
