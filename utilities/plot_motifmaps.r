#!/usr/bin/env Rscript
args <- commandArgs(trailingOnly = TRUE)
#args<-c("/home/crg/crg/projects/2016_matt/abilities/..","test","./npD3ufa_P8","50","15","region","right","100","pval=1","fdr=0.9","GAG,AAGAG","fg50,bg50","194,634")
#setwd("/home/crg/crg/projects/2018_bp5p_rnadoublestrand/yeast/motifmaps_kmers_from_u2snrna")
dir.matt<-args[1]
dir<-args[2]
fin<-args[3]
L<-as.numeric(args[4])
wl<-as.numeric(args[5])
m<-args[6]
orient<-args[7]
nperms<-as.numeric(args[8])
pval.thresh<-as.numeric(substr(args[9],6,nchar(args[9])))
fdr.thresh<-as.numeric(substr(args[10],5,nchar(args[10])))
d<-read.table(args[11],header=TRUE,sep="\t",as.is=TRUE)
mns<-read.table(args[12],as.is=TRUE)[,1]
motifs<-d[, colnames(d)==args[13] ]
gns<-unlist(strsplit(args[14],","))
ns<-unlist(strsplit(args[15],","))
ymax<-as.numeric(args[16])

fnsuf<-"_covered_reg"
ylab<-"     Region covered by motif [%]"
if(m=="hits"){ylab<-"Avg. hits per sequence";  fnsuf<-"_avghits_per_seq"}
if(substr(m,1,4)=="seqs"){n<-as.numeric(substr(m,6,nchar(m)-1));ylab<-paste("Fraction sequences with N>",n-1," hits",sep="",collapse=""); fnsuf<-paste("_seqs_with_",n,"hits",sep="",collapse="")}


##########
## Functions
##########
# returns a list with intervals (start_idx,end_idx) of intervals of pvals with p-values <= pval.t
# pvals can contain NAs  (intervals will be interrupted at NAs)
get_intervals<-function(pvals,pval.t){
  l<-list()   # for each interval (start,end)
  s<-1
  e<-NA
  while(s <= length(pvals)){
    if(is.na(pvals[s]) || pvals[s]>pval.t){s<-s+1;next;}
    e<-s
    if(s<length(pvals)){ for(i in (s+1):length(pvals)){ if(is.na(pvals[i]) || pvals[i]>pval.t){e<-i-1;break}; if(i==length(pvals)){e<-length(pvals)} } }
    l[[length(l)+1]]<-c(s,e)
    s<-e+1
  }
return(l)
}


#############
## Main
#############

# read one-dimensional data and put it into a matrix: each column is one curve
# columns are: data coverage, enrichment, difference enrichment to reference group, p val for motif 1
# then further on: data coverage, enrichment, difference enrichment to reference group, p val for motif 2
# and so on
d<-read.table(fin,sep="\t",header=FALSE)

pos.tmp<-seq(1,L,1)
pos.list<-list(pos.tmp)

cols<-c(rgb(0/255,149/255,255/255)
,rgb(153/255,48/255,41/255)
,rgb(86/255,205/255,153/255)
,rgb(168/255,174/255,62/255)
,rgb(104/255,111/255,213/255)
,rgb(105/255,160/255,80/255)
,rgb(196/255,108/255,189/255)
,rgb(188/255,126/255,54/255)
,rgb(223/255,107/255,84/255)
,rgb(185/255,73/255,115/255))

if(pval.thresh<1){ # set color of last group = reference group to grey
  cols[length(gns)]<-colors()[534]
}

# a vector with latex include statements for motif rna maps
include.strs<-c()
# a vector with difference scores for sorting the include.strs
include.scrs<-c()

for(i in 1:length(mns)){

  # dimension of these data matrices is:
  # first 4 columns belong to group 1 (region 1-4) (motif 1)
  # next 4 columns belong to next group  (region 1-4) (motif 1)
  # and so on...   for the current motif
  # data coverage 
  dc<-matrix(d[,1+4*(i-1)],ncol=length(gns)*1,nrow=L)
  # enrichment scores
  es<-matrix(d[,2+4*(i-1)],ncol=length(gns)*1,nrow=L)*100
  #differences
  ds<-matrix(d[,3+4*(i-1)],ncol=length(gns)*1,nrow=L)
  # pvals
  pv<-matrix(d[,4+4*(i-1)],ncol=length(gns)*1,nrow=L)
  col.ids<-seq(1:(length(gns)*1))

  for(a in 1:2){
  
  if(a==1){
    fn.tmp<-paste(dir,"/",gsub("\\.","dot",mns[i]),fnsuf,"_with_datacoverage.pdf",sep="",collapse="")
    pdf(file=fn.tmp,width=17,height=7)
    fn.tmp<-paste(gsub("\\.","dot",mns[i]),fnsuf,"_with_datacoverage.pdf",sep="",collapse="")
    include.strs[length(include.strs)+1]<-paste("\\includegraphics[width=\\textwidth]{",fn.tmp,"}\n",sep="",collapse="")
  }else{
    fn.tmp<-paste(dir,"/",gsub("\\.","dot",mns[i]),fnsuf,".pdf",sep="",collapse="")
    pdf(file=fn.tmp,width=17,height=7)
    fn.tmp<-paste(gsub("\\.","dot",mns[i]),fnsuf,".pdf",sep="",collapse="")
    include.strs[length(include.strs)]<-paste(include.strs[length(include.strs)],"\\vspace{1cm}\\\\\n\\includegraphics[width=\\textwidth]{",fn.tmp,"}\n",sep="",collapse="")
    names(include.strs)[length(include.strs)]<-mns[i]
  }


  # if some groups have been without any sequence, their values will be NA
  # if all values are NA, we need to set vmax and vmin hard to avoid errors, though the plot will not contain any curve
  if(length(which(!is.na(es[,col.ids])))==0){  vmax<-1;  vmin<-0  }else{  vmax<-max(es[,col.ids],na.rm=TRUE);  vmin<-min(es[,col.ids],na.rm=TRUE)  }
  if(vmax==0 && vmin==0){vmax<-1; vmin<-0}
  if(ymax!=-1){vmax<-ymax} #set by user  # -1 means user has not set ymax!

  vmin2<-vmin-(vmax-vmin)*0.1
  vmax2<-vmax

  if(a==1){
  par(mar=c(0.8,4.5,0.8,0.2),fig=c(0,1,0.04,0.19))
  plot(0,0,xlim=c(1,max(unlist(pos.list))),ylim=c(0,1),xlab="",ylab="",type="n",axes=FALSE,cex.lab=1.33,font.lab=2,las=1,main="",mgp = c(0, 1, 0))
  lines(x=c(0-max(unlist(pos.list))*0.0,max(unlist(pos.list))),y=c(1,1),col="grey")
  lines(x=c(0-max(unlist(pos.list))*0.0,max(unlist(pos.list))),y=c(0.75,0.75),col="grey")
  lines(x=c(0-max(unlist(pos.list))*0.0,max(unlist(pos.list))),y=c(0.5,0.5),col="grey")
  lines(x=c(0-max(unlist(pos.list))*0.0,max(unlist(pos.list))),y=c(0.25,0.25),col="grey")
  lines(x=c(0-max(unlist(pos.list))*0.0,max(unlist(pos.list))),y=c(0,0),col="grey")
  axis(2,at=c(1,0.75,0.5,0.25,0),labels=c("100","75","50","25","0"),lwd=2,cex.axis=1,las=1,line=-1)
  text(0+max(unlist(pos.list))*0.001,0.5,"Data [%]", srt=90,cex=1.15,font.lab=2)
  k<-1; l<-1
  for(j in 1:length(col.ids)){lines(x=pos.list[[l]],y=dc[,col.ids[j]],col=cols[k],lwd=2); if(l==1){l<-1; k<-k+1}else{l<-l+1};}
  
  par(mar=c(7,4.5,5,0.2),mgp=c(4,1,0),fig=c(0,1,0,1),xpd=TRUE,new=TRUE)
  }else{
  par(mar=c(3,4.5,5,0.2),mgp=c(4,1,0),xpd=TRUE)
  }
  
  plot(1,1,xlim=c(1,max(unlist(pos.list))),ylim=c(vmin2,vmax2),xlab="",ylab=ylab,type="n",axes=FALSE,cex.lab=1.91,font.lab=2,las=1,main="",mgp = c(3, 1, 0))
  title(main=gsub("_"," ",mns[i]),cex.main=2,line=3)
  k<-1; l<-1
  lwd.tmp<-3; if(pval.thresh<1){lwd.tmp<-2}
  diff.max<- NA
  for(j in 1:length(col.ids)){
    lines(x=pos.list[[l]],y=es[,col.ids[j]],col=cols[k],lwd=lwd.tmp)
    diffs.tmp<-sort(ds[,col.ids[j]])
    if(length(diffs.tmp>0)){  # length=0 if all values = NA, e.g., for the reference group
      if(is.na(diff.max) || abs(diffs.tmp[1])>abs(diff.max)){diff.max<-diffs.tmp[1]}
      if(abs(diffs.tmp[length(diffs.tmp)])>abs(diff.max)){diff.max<-diffs.tmp[length(diffs.tmp)]}
    }
    if(l==1){l<-1; k<-k+1}else{l<-l+1}
  }
  if(is.na(diff.max)){diff.max<-0}
  if(a==1){include.scrs[length(include.scrs)+1]<-diff.max; names(include.scrs)[length(include.scrs)]<-mns[i]}
  
  if(pval.thresh<1 || fdr.thresh<1){  # do high-lighting  // last group is always reference group!
    k<-1; l<-1;
    tmp.thresh<-pval.thresh
    if(fdr.thresh<1){  # determine thresh on p values for given FDR thresh
      sorted.p.vals<-sort(pv,na.last=NA)
      benjamini.hochberg<-sorted.p.vals <= fdr.thresh*((1:length(sorted.p.vals))/length(sorted.p.vals))
      if(length(which(benjamini.hochberg))==0){
	tmp.thresh<--1
      }else{
	tmp.thresh<-sorted.p.vals[ max(which(benjamini.hochberg)) ]
      }
    }
    for(j in 1:(length(col.ids))){
      if(j<=length(col.ids)-1){  # not yet at last group = refrence group
	tmp.highligth<-get_intervals(pv[,col.ids[j]],tmp.thresh)
      }else{  # last group = reference group without highligthing
	next
      }
      if(length(tmp.highligth)>0){
	for(r in 1:length(tmp.highligth)){
	  ids<-(tmp.highligth[[r]][1]):(tmp.highligth[[r]][2])
	  lines(x=pos.list[[l]][ids],y=es[,col.ids[j]][ids],col=cols[k],lwd=6);
	}
      }
      if(l==1){l<-1; k<-k+1}else{l<-l+1};
    }
  }

  if(a==1){
   y1<-vmin-(vmax-vmin)*0.45
   y2<-vmin-(vmax-vmin)*0.39
   y3<-mean(c(y1,y2))
  }else{
   y1<-vmin-(vmax-vmin)*0.20
   y2<-vmin-(vmax-vmin)*0.14
   y3<-mean(c(y1,y2))
  }
  
  ncol.legd<-length(gns); if(ncol.legd>5){ncol.legd<-5}
  ncol.ledg<-2
  legend(x=0-max(unlist(pos.list))*0.04,y=vmax+(vmax-vmin)*0.2,legend=paste(gsub("_"," ",gns)," (",ns,")",sep=""),col=cols[1:length(gns)],pch=rep(19,length(gns)),bg="transparent",bty="n",cex=1.5,ncol=5)

  if(a==1){
    text(x=max(unlist(pos.list))*0.95,y=vmax+(vmax-vmin)*0.25,paste("Sliding window L=",wl,sep="",collapse=""),cex=1.3)
    if(orient == "left"){
      text(x=max(unlist(pos.list))*0.95,y=vmax+(vmax-vmin)*0.19,"Sequences left-aligned",cex=1.3)
    }else{
      text(x=max(unlist(pos.list))*0.95,y=vmax+(vmax-vmin)*0.19,"Sequences right-aligned",cex=1.3)
    }
    text(x=max(unlist(pos.list))*0.95,y=vmax+(vmax-vmin)*0.13,paste("Max difference: ",round(100*include.scrs[length(include.scrs)],2),sep="",collapse=""),cex=1.3)
  }else{
    text(x=max(unlist(pos.list))*0.95,y=vmax+(vmax-vmin)*0.22,paste("Sliding window L=",wl,sep="",collapse=""),cex=1.3)
    if(orient == "left"){
      text(x=max(unlist(pos.list))*0.95,y=vmax+(vmax-vmin)*0.16,"Sequences left-aligned",cex=1.3)
    }else{
      text(x=max(unlist(pos.list))*0.95,y=vmax+(vmax-vmin)*0.16,"Sequences right-aligned",cex=1.3)
    }
    text(x=max(unlist(pos.list))*0.95,y=vmax+(vmax-vmin)*0.10,paste("Max difference: ",round(100*include.scrs[length(include.scrs)],2),sep="",collapse=""),cex=1.3)
  }
  if(pval.thresh<1 || fdr.thresh<1){
    if(pval.thresh<1){
      if(a==1){
	text(adj=c(0,0.5),x=max(unlist(pos.list))*1.0333,y=vmin+(vmax-vmin)*0.95,substitute(paste("p value", ""<="", m,"  | ",n," permutations"),list(m = tmp.thresh,n=nperms)),cex=1.3,srt=270)
	text(adj=c(0,0.5),x=max(unlist(pos.list))*1.02,y=vmin+(vmax-vmin)*0.95,"else",cex=1.3,srt=270)
	lines(x=c(max(unlist(pos.list))*1.02,max(unlist(pos.list))*1.02),y=c(vmin+(vmax-vmin)*1.025,vmin+(vmax-vmin)*0.975),col="black",lwd=2)
      }else{
        text(adj=c(0,0.5),x=max(unlist(pos.list))*1.0333,y=vmin+(vmax-vmin)*0.95,substitute(paste("p value", ""<="", m,"  | ",n," permutations"),list(m = tmp.thresh,n=nperms)),cex=1.5,srt=270)
	text(adj=c(0,0.5),x=max(unlist(pos.list))*1.0185,y=vmin+(vmax-vmin)*0.95,"else",cex=1.5,srt=270)
        lines(x=c(max(unlist(pos.list))*1.0185,max(unlist(pos.list))*1.0185),y=c(vmin+(vmax-vmin)*1.025,vmin+(vmax-vmin)*0.975),col="black",lwd=2)
      }
    }
    if(fdr.thresh<1){
      if(a==1){
	text(adj=c(0,0.5),x=max(unlist(pos.list))*1.0333,y=vmin+(vmax-vmin)*0.95,substitute(paste("FDR", ""<="", m,"  | ",n," permutations"),list(m = fdr.thresh,n=nperms)),cex=1.3,srt=270)
        text(adj=c(0,0.5),x=max(unlist(pos.list))*1.02,y=vmin+(vmax-vmin)*0.95,"else",cex=1.3,srt=270)
        lines(x=c(max(unlist(pos.list))*1.02,max(unlist(pos.list))*1.02),y=c(vmin+(vmax-vmin)*1.025,vmin+(vmax-vmin)*0.975),col="black",lwd=2)
      }else{
        text(adj=c(0,0.5),x=max(unlist(pos.list))*1.0333,y=vmin+(vmax-vmin)*0.95,substitute(paste("FDR", ""<="", m,"  | ",n," permutations"),list(m = fdr.thresh,n=nperms)),cex=1.5,srt=270)
	text(adj=c(0,0.5),x=max(unlist(pos.list))*1.0185,y=vmin+(vmax-vmin)*0.95,"else",cex=1.5,srt=270)
	lines(x=c(max(unlist(pos.list))*1.0185,max(unlist(pos.list))*1.0185),y=c(vmin+(vmax-vmin)*1.025,vmin+(vmax-vmin)*0.975),col="black",lwd=2)
      }
    }
    lines(x=c(max(unlist(pos.list))*1.0333,max(unlist(pos.list))*1.0333),y=c(vmin+(vmax-vmin)*1.025,vmin+(vmax-vmin)*0.975),col="black",lwd=6)
  }

  axis(2,lwd=2,cex.axis=1.51,las=1,line=-1)

  x.delta<-max(unlist(pos.list))*0.015
  at<-seq(1,L,length.out=7)
  step<-at[2]-at[2]%%10; if(step==0){step<-10}
  at<-c(1);
  for(q in 2:L){at[q]<-(q-1)*step; if((q-1)*step>=L){at[q]<-L;break}}
  lines(x=c(1,L),y=c(y1+(y2-y1)*0.3,y1+(y2-y1)*0.3),lwd=3)  
  if(orient == "left"){text(x=at,y=rep(y1-(y2-y1)*0.5,length(at)),labels=at,cex=1.33)
  }else{
    text(x=L-rev(at),y=rep(y1-(y2-y1)*0.5,length(at)),labels=rev(at)*-1,cex=1.33)
  }
  

  dev.off()
 }#for-k 
}


# PDF overview
include.strs<-include.strs[order(include.scrs,decreasing=TRUE)]
motifs<-motifs[order(include.scrs,decreasing=TRUE)]

summary.file<-paste(dir,"/0_all_motif_maps.tex",sep="",collapse="")
cat("\\documentclass{scrartcl}
\\usepackage[english]{babel}
\\usepackage{graphicx}
\\usepackage[nottoc]{tocbibind}
\\begin{document}
\\title{Summary of motif RNA-maps}
\\author{}
\\date{\\today}
\\maketitle
\\tableofcontents
\\newpage
\\section{Data sets}",file=summary.file,append=FALSE)
for(i in 1:(length(gns))){
    cat("\\verb+",gns[i],"+: ",ns[i],"elements\\\\\n",file=summary.file,append=TRUE)
}

cat("\\newpage\\section{Motif RNA-maps}\n
The motif maps are sorted wrt. to the difference of enrichment scores of 
all groups wrt. the last group, which acts as a reference group. Motif maps with
largest positive difference come first, motif maps with largest negative difference come last.",file=summary.file,append=TRUE)

for(i in 1:length(include.strs)){
  cat("\\newpage\\subsection{~~",gsub("_"," ",names(include.strs)[i]),"}\n",file=summary.file,append=TRUE)
  cat("Motif: {\\footnotesize\\begin{verbatim}",motifs[i],"\\end{verbatim}}~\\\\",file=summary.file,append=TRUE)
  cat(include.strs[i],file=summary.file,append=TRUE)
}
cat("\\end{document}",file=summary.file,append=TRUE)



cwd<-getwd()
setwd(dir)
# copy latex packages
cmd<-paste("cp ",dir.matt,"/external_progs/latex_packages/*.* .",sep="")
system(cmd)

ret<-system2("pdflatex",c("-interaction=nonstopmode","-halt-on-error","-file-line-error","0_all_motif_maps.tex"))
ret<-system2("pdflatex",c("-interaction=nonstopmode","-halt-on-error","-file-line-error","0_all_motif_maps.tex"))
ret<-system2("pdflatex",c("-interaction=nonstopmode","-halt-on-error","-file-line-error","0_all_motif_maps.tex"))

setwd(cwd)



quit(status=0)  # success
