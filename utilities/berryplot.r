#!/usr/bin/env Rscript
args <- commandArgs(trailingOnly = TRUE)

# read Utilities.r only if it not yet have been called
if(!exists("readPWM_matt")){

  getScriptPath <- function(){
    cmd.args <- commandArgs()
    m <- regexpr("(?<=^--file=).+", cmd.args, perl=TRUE)
    script.dir <- dirname(regmatches(cmd.args, m))
    if(length(script.dir) == 0) stop("can't determine script dir: please call the script with Rscript")
    if(length(script.dir) > 1) stop("can't determine script dir: more than one '--file' argument detected")
    return(script.dir)
  }
  my.path<-getScriptPath()
  source(paste(my.path,"/Utilities.r",sep="",collapse=""))
}


#rm(list=ls())

# pwm1: rows -> letters, columns -> positions
# ONLY for ACGT/U pwms
#
# pwm2: rows -> letters, columns -> positions
#
# pwm1 and pwm2 can be filenames or R matrices - need to have rownames A, C, G, T/U
#
# fn.out: output PDF file name
berryplot<-function(pwm1,name1,pwm2,name2,fn.out=NA){

    if(!is.matrix(pwm1)){   # pwm1 is assumed to be a file and we read in the pwm from it
      pwm.tmp<-readPWM_matt(pwm1)
      pwm1<-pwm.tmp
      rm(pwm.tmp)
    }
    if(nrow(pwm1)!=4){stop("Given PWM1 needs to have 4 rows (nts) and as many columns as positions")}
    rownames(pwm1)<-toupper( rownames(pwm1) )
    pwm1<-pwm1[order(rownames(pwm1)) ,]  # sort column according positons A, C, G, T/U
    
    if(!is.matrix(pwm2)){   # pwm2 is assumed to be a file and we read in the pwm from it
      pwm.tmp<-readPWM_matt(pwm2)
      pwm2<-pwm.tmp
      rm(pwm.tmp)
    }
    if(nrow(pwm2)!=4){stop("Given PWM needs to have 4 rows (nts) and as many columns as positions")}
    rownames(pwm2)<-toupper( rownames(pwm2) )
    pwm2<-pwm2[order(rownames(pwm2)) ,]  # sort column according positons A, C, G, T/U
    
    # check if alphabet is identical
    if(length(which(!is.element(rownames(pwm1),rownames(pwm2))))!=0 || length(which(!is.element(rownames(pwm2),rownames(pwm1))))!=0){
      stop("Alphabets of both PWMs are not the same: ",paste(rownames(pwm1),collapse=",")," <-> ",paste(rownames(pwm2),collapse=","),"\n")
    }

  pwm1[pwm1==0]<- 0.000001
  pwm2[pwm2==0]<- 0.000001
  pwm<-log(pwm1/pwm2)/log(2)
  
  xlim.diff<-0.6666
  ylim.diff<-0.5
  jitter.fac<-0.75
  col.bg<-colors()[245]
  col.axis<-colors()[185]
  cols.acgt<-colors()[c(496,435,148,505)]
  names(cols.acgt)<-rownames(pwm1)

  xs<-jitter( rep(1:ncol(pwm),4), factor=jitter.fac)
  ys<-as.vector(t(pwm))
  pchs<-rep(rownames(pwm),each=ncol(pwm))
  cols<-rep(cols.acgt,each=ncol(pwm))
  cex.let<-3
  ylim.max<-2#max(2.5,max(ys)+ylim.diff)
  ylim.min<--2#min(-2.5,min(ys)-ylim.diff)

  if(!is.na(fn.out)){pdf(file=fn.out,height=7,width=ncol(pwm)*1.2)}
    
  par(mar=c(4,5,1,0.5),cex.axis=1.5,cex.lab=2.2,xaxs="i")
  plot(0,0,type="n",xlim=c(1-xlim.diff,ncol(pwm)+xlim.diff),ylim=c(ylim.min,ylim.max),main="",ylab="Log2 ratio of nt probabilities",xlab="Position",axes=FALSE,col.lab=col.axis)
  polygon(x=c(0,0,ncol(pwm)+xlim.diff,ncol(pwm)+xlim.diff),y=c(ylim.min,ylim.max,ylim.max,ylim.min),col=col.bg,border=NA)
  axis(1,at=1:ncol(pwm),1:ncol(pwm),cex.axis=1.8,lwd=2,col=col.axis,col.ticks=col.axis,col.axis=col.axis)
  axis(2,lwd=2,cex.axis=1.8,col=col.axis,col.ticks=col.axis,col.axis=col.axis)
  grid(NULL, NULL, lwd = 3,lty="solid",col="white")
  abline(h=0,lwd=8,col=colors()[230])
  text(x=(ncol(pwm)+1)/2,y=ylim.max/2,labels=name1,cex=4,col=colors()[340],font=2)
  text(x=(ncol(pwm)+1)/2,y=ylim.min/2,labels=name2,cex=4,col=colors()[340],font=2)
  text(x=xs,y=ys,labels=pchs,col=cols,cex=cex.let,font=2)

  if(!is.na(fn.out)){trash<-dev.off()}
}


#pfm <- matrix(data=c(5, 3, 16, 1, 0, 17, 17, 0, 0, 16, 12, 8,
#6, 9, 1, 1, 18,  1, 0, 0, 18,  1, 0, 2,
#2,3, 1, 0, 0, 0, 0, 1, 0, 0, 1, 2,
#5, 3, 0, 16, 0, 0, 1, 17,  0, 1, 5, 6), 
#byrow=TRUE,nrow=4,dimnames=list(c('A','C','G','T')))

#pwm1 <- apply(pfm, 2, function(x) x/sum(x))
#pwm2 <- apply(pfm+10, 2, function(x) x/sum(x))

#berryplot(pwm2[],"PWM2",pwm1,"PWM1","~/test.pdf")
#berryplot(pwm2[,3:7],"PWM2",pwm1[,1:5],"PWM1","~/test2.pdf")
#berryplot(cbind(pwm2,pwm2,pwm2),"PWM2",cbind(pwm1,pwm1,pwm1),"PWM1","~/test3.pdf")


########
## main
########

berryplot(args[1],args[2],args[3],args[4],args[5])


quit(status=0)  # success
