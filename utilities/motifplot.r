#!/usr/bin/env Rscript
args <- commandArgs(trailingOnly = TRUE)

# read Utilities.r only if it not yet have been called
if(!exists("readPWM_matt")){

  getScriptPath <- function(){
    cmd.args <- commandArgs()
    m <- regexpr("(?<=^--file=).+", cmd.args, perl=TRUE)
    script.dir <- dirname(regmatches(cmd.args, m))
    if(length(script.dir) == 0) stop("can't determine script dir: please call the script with Rscript")
    if(length(script.dir) > 1) stop("can't determine script dir: more than one '--file' argument detected")
    return(script.dir)
  }
  my.path<-getScriptPath()
  source(paste(my.path,"/Utilities.r",sep="",collapse=""))
}




#######
## funs
#######
#rm(list=ls())

# pwm: rows -> letters, columns -> positions , need to have rownames (4 letters, any are possible, colors mapped will fit A, C, G, T/U
# ONLY for ACGT/U pwms
# pwm can be a R matrix or a file name; if it is a file name, it is supposed to be a matt pwm
#
# fn.out: output PDF file name
motifplot<-function(pwm,fn.out=NA){

  makeTransparent = function(..., alpha=0.5) {

    if(alpha<0 | alpha>1) stop("alpha must be between 0 and 1")
    alpha = floor(255*alpha)  
    newColor = col2rgb(col=unlist(list(...)), alpha=FALSE)
    .makeTransparent = function(col, alpha) {rgb(red=col[1], green=col[2], blue=col[3], alpha=alpha, maxColorValue=255)}
    newColor = apply(newColor, 2, .makeTransparent, alpha=alpha)
  return(newColor)
  }

  # partSums(1:4) = 1, 3, 6, 10
  partSums<-function(vs){
    ret<-vs
    if(length(ret)>1){for(i in 2:length(ret)){ret[i]<-ret[i]+ret[i-1]}}
  ret
  }

  if(!is.matrix(pwm)){   # pwm is assumed to be a file and we read in the pwm from it
    pwm.tmp<-readPWM_matt(pwm)
    pwm<-pwm.tmp
    rm(pwm.tmp)
  }

  if(nrow(pwm)!=4){stop("Given PWM needs to have 4 rows (nts) and as many columns as positions")}
  rownames(pwm)<-toupper( rownames(pwm) )
  pwm<-pwm[order(rownames(pwm)) ,]  # sort column according positons A, C, G, T/U
  
  xlim.diff<-0.6666
  jitter.fac<-0.75
  cex.min<-2.5
  col.bg<-colors()[245]
  col.axis<-colors()[185]
  cols.acgt<-colors()[c(496,435,148,505)]  # A, C, G, T/U
  names(cols.acgt)<-rownames(pwm)
  cols.acgt.tr<-makeTransparent(cols.acgt,alpha=0.70)

  # information content in bits per row
  ic <- apply(pwm, 2, function (x) { x[which(x==0)] = 0.000001; 2 + sum(x*log2(x))})

  pwm2<-pwm
  cols.mat<-pwm
  lets<-pwm
  for(c in 1:ncol(pwm2)){
    ids<-order(pwm2[,c])
    pwm2[,c]<-partSums(pwm2[ids,c])
    cols.mat[,c]<-cols.acgt.tr[ rownames(pwm2)[ids] ]
    lets[,c]<-rownames(pwm2)[ids]
  }
  pwm2<-rbind(0,pwm2)

  
  if(!is.na(fn.out)){pdf(file=fn.out,height=10,width=ncol(pwm)*1.5)}
  
  par(mar=c(0.05,5,0.8,0.5),cex.axis=1.5,cex.lab=2.2,mfrow=c(2,1),xaxs="i")
  
  # first plot: nt probabilities as stacked bars per positions
  plot(1,0.5,type="n",axes=FALSE,col.lab=col.axis,main="",ylab="Probability",ylim=c(0,1),xlim=c(1-xlim.diff,ncol(pwm2)+xlim.diff))
  bw<-0.7
  for(c in 1:ncol(pwm2)){
  for(r in 2:nrow(pwm2)){
    rect(xleft=c-bw/2, ybottom=pwm2[r-1,c], xright=c+bw/2, ytop=pwm2[r,c], col=cols.mat[r-1,c],border=FALSE)
    y.width<-pwm2[r,c]-pwm2[r-1,c]
    if(y.width>=0.1){
      if(r<nrow(pwm2)){
	text(x=c,y=pwm2[r-1,c]+(y.width)/2,lets[r-1,c],col="white",cex=3,font=2)
      }else{
	 text(x=c,y=0.9,lets[r-1,c],col="white",cex=5,font=2)
      }
    }
  }}
  axis(2,lwd=2,cex.axis=1.8,col=col.axis,col.ticks=col.axis,col.axis=col.axis)

  
  xs<-jitter( rep(1:ncol(pwm),4), factor=jitter.fac)
  ys<-as.vector(t(pwm)) * ic
  pchs<-rep(rownames(pwm),each=ncol(pwm))
  cols<-rep(cols.acgt,each=ncol(pwm))
  cexs<-ys * 5
  ids<-cexs<cex.min
  cexs[ids]<-cex.min
  fonts<-rep(2,length(cexs))
  fonts[ids]<-1

  par(mar=c(4,5,0.05,0.5),cex.axis=1.5,cex.lab=2.2,xaxs="i")
  plot(0,0,type="n",xlim=c(1-xlim.diff,ncol(pwm)+xlim.diff),ylim=c(-0.1,2.3),main="",ylab="Information content [bit]",xlab="Position",axes=FALSE,col.lab=col.axis)
  polygon(x=c(0,0,ncol(pwm)+xlim.diff,ncol(pwm)+xlim.diff),y=c(-0.2,2.4,2.4,-0.2),col=col.bg,border=NA)
  axis(1,at=1:ncol(pwm),1:ncol(pwm),cex.axis=1.8,lwd=2,col=col.axis,col.ticks=col.axis,col.axis=col.axis)
  axis(2,lwd=2,cex.axis=1.8,col=col.axis,col.ticks=col.axis,col.axis=col.axis)
  grid(NULL, NULL, lwd = 3,lty="solid",col="white")
  text(x=xs,y=ys,labels=pchs,col=cols,cex=cexs,font=fonts)

  if(!is.na(fn.out)){trash<-dev.off()}
}


#pfm <- matrix(data=c(5, 3, 16, 1, 0, 17, 17, 0, 0, 16, 12, 8,
#6, 9, 1, 1, 18,  1, 0, 0, 18,  1, 0, 2,
#2,3, 1, 0, 0, 0, 0, 1, 0, 0, 1, 2,
#5, 3, 0, 16, 0, 0, 1, 17,  0, 1, 5, 6), 
#byrow=TRUE,nrow=4,dimnames=list(c('A','C','G','T')))

#pwm <- apply(pfm, 2, function(x) x/sum(x))

#motifplot("/home/crg/crg/projects/2016_matt/data_external/pwm/sf1_human_l7.tab","~/test.pdf")
#motifplot(cbind(pwm,pwm,pwm),"~/test2.pdf")
#motifplot(pwm[,3:7],"~/test3.pdf")



########
## main
########

motifplot(args[1],args[2])


quit(status=0)  # success
